# TunelChat

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

## Instalación
 instalar las dependencias `npm install` .

 ### Problemas con NPM Install ###
En caso de tener algún problema con la instalación por estar bajo un proxy, se deben setear las variables de entorno:

```
npm config set proxy http://proxy80.tchile.local:80
npm config set https-proxy http://proxy80.tchile.local:80
```

## Development server

ejecutar `node server.js` para iniciar el servidor, es necesario esta forma para habilitar el servicio rest de encriptacion. de no iniciar ejecutar primero  `ng build --prod` y volver a ejecutar el comando desde la carpeta `dist/`

para probar bien se puede hacer lo siguiente. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

`ng serve --host 0.0.0.0 --port 4201` para especificar el puerto de la app. esto no ejecutara el servicio rest de encriptacion

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

# these are equivalent
ng build --prod --env=prod
ng build --prod
# and so are these
ng build --dev --e=dev
ng build --dev

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
