(function() {
	'use strict';

	const PORT = 8003;
	const FOLDER = __dirname;
	
	var Blowfish = require("./blowfish.js");
	const key ='Ypbc7rV62trCX8rn';
	var http = require('http');
	var https = require('https');	
	var express = require('express');
	var cors = require('cors')
	var app = express();
	app.use(cors());
 

	var fs = require('fs');
	var privateKey  = fs.readFileSync(FOLDER+'/assets/ssl/server.key', 'utf8');
	var certificate = fs.readFileSync(FOLDER+'/assets/ssl/server.crt', 'utf8');

	var credentials = {key: privateKey, cert: certificate};

	app.use(express.static(FOLDER));

	app.get('/chat', function(req, res) {
		res.sendFile(FOLDER + '/index.html'); // load the single view file (angular will handle the page changes on the front-end)
	});

	app.listen(PORT, function() {
		console.log("App listening http on port: " + PORT);
	});

	/*** REST */
	app.get('/encriptacion/', function(req, res) {
		var parametros={'id_servicio':req.query.idservicio,'nombre':req.query.nombre,'apellido_paterno':req.query.apellidopaterno,'apellido_materno':req.query.apellidomaterno,'mail':req.query.mail,'rut_titular':req.query.rut_titular};
		var sEncryptedParams = Blowfish.blowfish.encrypt(JSON.stringify(parametros), key, {
			outputType: 1,
			cipherMode: 0
		});
		res.send('{"encrypt":"'+sEncryptedParams+'"}');
	});

	 //var httpServer = http.createServer(app);
	 // httpServer.listen(8002);
	 var httpsServer = https.createServer(credentials, app);
	 httpsServer.listen(8004); 
	 console.log("App listening https on port: " + 8004);


})();
