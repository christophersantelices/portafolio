import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ActivatedRoute,RouterModule} from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ChatComponent } from './chat/chat.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UrlEncriptacionService } from './service/url-encriptacion.service';


@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path:'chat',
        component: ChatComponent
      },
      { path: '',
        redirectTo: '/chat',
        pathMatch: 'full'
      },
      { path: '**', 
        component: PageNotFoundComponent 
      } 
    ],{ enableTracing: false }) 
  ],
  providers: [UrlEncriptacionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
