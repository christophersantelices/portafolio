import { TestBed, inject } from '@angular/core/testing';

import { UrlEncriptacionService } from './url-encriptacion.service';

describe('UrlEncriptacionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrlEncriptacionService]
    });
  });

  it('should be created', inject([UrlEncriptacionService], (service: UrlEncriptacionService) => {
    expect(service).toBeTruthy();
  }));
});
