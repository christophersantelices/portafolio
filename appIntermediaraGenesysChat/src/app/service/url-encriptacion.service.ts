import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UrlEncriptacionService {

  private ipServidor:string="https://chatsmcss.movistar.cl";
  //private ipServidor:string="http://10.186.30.88:8003";

  private url:string;
  
  constructor(public http: Http) { 
  }

  public encriptarURL(data){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    let url = encodeURI(this.ipServidor+'/encriptacion/?idservicio='+data['idservicio']+'&nombre='+data['nombre']+'&apellidopaterno='+data['apellidopaterno']+'&apellidomaterno='+data['apellidomaterno']+'&mail='+data['mail']+'&rut_titular='+this.formatearRUT(data['rut_titular']));
    let result= this.http.get(url, {headers: headers}).map(res => res.json());
    return result;
  }

  public formatearRUT(rut){
    var f_rut=rut.toUpperCase().replace("_RUT", "");
    f_rut=f_rut.substring(0,f_rut.length-1)+"-"+f_rut.substring(f_rut.length-1);
    return f_rut;
  }
}
