import { Component, OnInit  } from '@angular/core';
import { AfterViewInit,ElementRef, ViewChild } from '@angular/core';

//para que la url carge y no de problemas de seguridad
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UrlEncriptacionService } from '../service/url-encriptacion.service';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  public carga: boolean=null;
  @ViewChild('iframe') iframe: ElementRef;
  public box:boolean;
  public data:any;
  private ipServidor:string="https://chats.movistar.cl/";
  //private ipServidor:string="http://10.186.30.88:8000/";
  private url:string;

  constructor(private sanitizer: DomSanitizer,private route: ActivatedRoute,private urlEncrip: UrlEncriptacionService) {
    console.log("chat.component succes");
  }

  ngOnInit() {
  /*this.route.params.subscribe((params: Params) => {
      let userId = params['userId'];
      console.log(userId);
    });
    let param1 = this.route.snapshot.queryParams["param1"];
    */
    this.route.queryParams.subscribe(
      data => {
        this.box=(data['box']=='true');
        this.data=data;
        console.log('box ', this.box); //este parametro indica si es un iframe dentro de una caja o no
        console.log('id servicio ', data['idservicio']);
        console.log('nombre ', data['nombre']);
        console.log('apellido paterno ', data['apellidopaterno']);
        console.log('apellido materno ', data['apellidomaterno']);
        console.log('mail ', data['mail']);
        console.log('rut titular ', data['rut_titular']);
      }
    );
    this.encriptarURL();
  }

  private cleanURL(){
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.ipServidor+this.url);
  }

  public encriptarURL(){
    this.urlEncrip.encriptarURL(this.data).subscribe(
      data => {
        console.log("encriptarURL succes");
        this.carga=true;
        this.url=data.encrypt;
      },
      err => {
        console.log("ERROR encriptarURL!: "+err.status, err);
      });
  }



}
