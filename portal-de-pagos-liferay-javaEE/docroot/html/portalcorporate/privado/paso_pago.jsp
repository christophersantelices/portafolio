<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>

<portlet:defineObjects />

<portlet:actionURL var="errorPagoPrivadoActionURL" windowState="normal"
	name="error_Pago_Privado">
</portlet:actionURL>

<portlet:actionURL var="cierrePagoPrivadoActionURL" windowState="normal"
	name="cierre_Pago_Privado">
</portlet:actionURL>

<%
	String medioDePagoPasoPago = (String) renderRequest.getAttribute("medioDePagoPasoPago");
	String urlMedioDePago = (String) renderRequest.getAttribute("urlMedioDePago");
	String idCanal = (String) renderRequest.getAttribute("idCanal");
	String ordenDeCompra = (String) renderRequest.getAttribute("ordenDeCompra");
	String campoEncriptado = (String) renderRequest.getAttribute("campoEncriptado");
	String solicitudCredito = campoEncriptado.replaceAll("\n", "");
%>

<div class="titulo-pagina normado titulo-post-selects">
    <h1><%=UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate")%></h1>
</div>

<main class="container medio-pago-orden width-100 padding-00-i text-center"  style="max-width:1350px;">
	<form class="clearfix inline-block width-100" novalidate="novalidate" style="max-width:none;">
		<div class="steps-new pasos5 col-md-12">
				<ul class="desktop">
					<li class="">
						<a href="" class="relativo">
							<i class="iconp-documentos"></i>
							<span class="margin-left-20">Resumen de cuentas</span>
						</a>
					</li>
					<li class="active">
						<a href="" class="relativo">
							<i class="iconp-tarjeta"></i>
							<span>Medios de pago</span>
						</a>
					</li>
					<li class=""><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
				</ul>
			</div>
			<div class="menu-opciones-mobile col-md-12">
				<ul>
					<li class=""><a href="" class="relativo"><i class="iconp-documentos"></i> <span>Resumen de cuentas</span></a></li>
					<li class="active"><a href="" class="relativo"><i class="iconp-documento"></i> <span>Medios de pago</span></a></li>
				</ul>
			</div>
		<div class="medio-pago bg-white inline-block width-100" style="padding: 6% 7% 50px 7%;">
			
			<!-- Aqui el contenido -->
			
            <div id="divPago" name="divPago" class="frame-holder"></div>
		</div>
		<div class="menu-opciones-mobile medio-de-pago col-md-12 margin-top-20">
			<ul>
				<li class=""><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
			</ul>
		</div>
	</form>
	
	<input type="hidden" id="numeroOrden" name="numeroOrden" value="<%=ordenDeCompra%>">
	<form id="formularioSolicitudCredito" name="formularioSolicitudCredito" style="display: none;" method="post" action="<%=urlMedioDePago%>" target="iframe-pago">
        <input type="hidden" name="SolicitudCredito" id="SolicitudCredito" value="<%=solicitudCredito%>"> 
        <input type="hidden" id="IdCanal" name="IdCanal" value="<%=idCanal%>">
    </form>
                        
    <form id="formError" style="display: none;" method="post" action="<%=errorPagoPrivadoActionURL%>"></form>

    <form id="formCierre" style="display: none;" method="post" action="<%=cierrePagoPrivadoActionURL%>"></form>

	<input type="button" value="enviar" onclick="consultar()" id="refreshButton" style="display: none"></input>
	<input type="button" value="error" id="errorButton" style="display: none"></input> 
	<input type="button" value="cierre" id="cierreButton" style="display: none"></input>
</main>

<script src="<%=request.getContextPath()%>/js/JSIniciaPago.js?<%=System.currentTimeMillis()%>"></script>

