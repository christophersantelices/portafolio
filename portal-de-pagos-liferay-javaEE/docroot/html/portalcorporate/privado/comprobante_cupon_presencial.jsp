<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>

<portlet:defineObjects />
<portlet:resourceURL var="enviaNotificacionPago"></portlet:resourceURL>

<%
	String identificador = (String) renderRequest.getAttribute("identificador");
	String monto = (String) renderRequest.getAttribute("monto");
	String fechaValidez = (String) renderRequest.getAttribute("fechaValidez");
%>


<div class="titulo-pagina normado titulo-post-selects">
    <h1><%= UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate") %></h1>
</div>

<main class="container comprobante-ticket width-100 padding-00-i text-center">
    <div class="contenido clearfix inline-block width-100">
        <div class="steps-new pasos5 col-md-12">
            <ul class="desktop">
                <li class="">
                    <a href="" class="relativo">
                        <i class="iconp-documentos"></i>
                        <span class="margin-left-20">Resumen de cuenta</span>
                    </a>
                </li>
                <li class="">
                    <a href="" class="relativo">
                        <i class="iconp-tarjeta"></i>
                        <span>Medios de pago</span>
                    </a>
                </li>
                <li class="active"><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
            </ul>
        </div>

        <div class="menu-opciones-mobile col-md-12">
            <ul>
                <li class=""><a href="" class="relativo"><i class="iconp-documentos"></i> <span>Resumen de cuenta</span></a></li>
                <li class="">
                    <a href="" class="relativo">
                        <i class="iconp-tarjeta"></i>
                        <span>Medios de pago</span>
                    </a>
                </li>
                <li class="active"><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
            </ul>
        </div>
        <!--/ .steps-new -->

        <div class="cont-comprobante-ticket inline-block text-center">
            <div class="no-direccion inline-block width-100 bg-white">
                <div class="cont-icon-advertencia inline-block">
                    <img class="icon-advertencia" src="/mo-theme/images/img-ticket-pago-presencial2.png">
                </div>
                <div class="texto block">
                    <h4>Ticket de Pago Presencial</h4>
                </div>
                
                <div class="codigo inline-block">
                    <span>N�</span>
                    <span><%=identificador%></span>
                </div>

                <div class="enviaComprobante inline-block width-100" id="cuerpoMensaje" name="cuerpoMensaje">
                    <p>Envia el ticket a tu email</p>
                    <div id="divCajaCorreo" >
	                    <form action="" id="formEnvio" name="formEnvio" class="form-enviaComprobante inline-block width-100">
	                        <input type="text" id="txtCorreo" name="txtCorreo" placeholder="correo@email.com" class="inline-block">
	                        <button type="button" id="btnEnviar" name="btnEnviar" onClick="enviaNotificacionCorreo()" class="inline-block btn-verde">Enviar</button>
	                    </form>
                    </div>
                    <div id="email-invalido" class="envioexitoso inline-block" style="display:none;"> <i class="MCSS-error_v2"> </i><span>Debes ingresar correctamente el correo</span></div>
                </div>

                <span class="obien inline-block">
                    <h5><span>O bien</span></h5>
                </span>

                <div class="cont-boton inline-block width-100">
                    <div class="cont-btn pull-left">
                        <a href="javascript:void(0);" onClick="descargaPDF()" class="btn2 bg-clear vertical-middle ">
                            <i class="iconp-descargar"></i> Descargar ticket de pago</a>
                    </div>
                    <div class="cont-btn pull-right width-1">
                        <a href="javascript:void(0);" onClick="imprimeComprobante()" class="btn2 bg-clear vertical-middle ">
                            <i class="iconp-imprimir"></i> Imprimir ticket de pago</a>
                    </div>
                </div>
                
                <form action="<%=enviaNotificacionPago%>" target="_blank" method="POST">
                
                </form>
                
                <div class="cont-totalapagar">
                    <p class="block total-a-pagar">Total a pagar</p>
                    <span class="block precio-a-pagar"><strong>$</strong><%=monto%></span><br>
                    <p class="inline-block margin-top-15">V�lido hasta: <strong><%=fechaValidez%></strong></p>
                </div>
                
                <div class="cont-conesteticket">
                    <p>Con este <strong>Ticket de Pago Presencial</strong> puedes pagar personalmente en una sucursal, los saldos que t� elijas. Env�alo por email, impr�melo y ll�valo a la sucursal de tu preferencia.</p>
                </div>
                <h5 class="width-100 inline-block">Es v�lido s�lo por dos d�as</h5>
            </div>
        </div>
    </div>
    <!--/ .white-wrap -->
</main>

<script>
	
	function enviaNotificacionCorreo(){
		
		console.log("Llamada ajax a notificar pago por correo");
		var correo = $("#txtCorreo").val();
		$("#email-invalido").hide();
		if (validateEmail(correo)) {
			
			jQuery.ajax({
	 	     	type: "POST",
	 	     	url: "<%=renderResponse.encodeURL(enviaNotificacionPago.toString())%>",
	 	     	cache: false,
	 	     	dataType: "html",
	 	     	data: { 
	 	     		 '<portlet:namespace/>operacion': 'notifica',
	 	     		 '<portlet:namespace/>tipoComprobante': 'presencial',
	 	             '<portlet:namespace/>correo': correo
	 	       	},
	 	     	success: function(resp){

	 	     		console.log("La respuesta de enviar correo es : " + resp);
	 	     		
	 	     		if (resp == 'OK'){
	 	     			document.getElementById('divCajaCorreo').innerHTML="";
	 	 	     		$("#divCajaCorreo").append('<div class="envioexitoso inline-block"><i class="MCSS-exito_v2"></i><span>Recibir�s el comprobante en tu correo en los pr�ximos minutos.</span></div>');
	 	     		}
	 	     		
	 	     		//$("#divCajaCorreo").remove();
	 	     		
	 	     		//Mostrar mensaje correo enviado
	 	     	}
	 	 	});
		}
		else {
			$("#email-invalido").show();
		}
		return false;
	}
	
	function descargaPDF(){
		console.log("Llamada ajax a descarga PDF");
		window.open('<portlet:resourceURL id="enviaNotificacionPago"></portlet:resourceURL>&<portlet:namespace/>operacion=download_pdf&<portlet:namespace/>tipo_comprobante=ticket'); 
		//jQuery.ajax({
 	    // 	type: "POST",
 	    // 	url: "<%=renderResponse.encodeURL(enviaNotificacionPago.toString())%>",
 	    // 	cache: false,
 	    // 	dataType: "html",
 	    // 	data: { 
 	    // 		 '<portlet:namespace/>operacion': 'download_pdf'
 	    //   	},
 	    // 	success: function(resp){
 	    // 		//$("#contenedorModalDetalleServicios").html(resp);
 	    // 		
 	    //		console.log("La respuesta de descarga PDF es : " + resp);

 	    // 	}
 	 	//});
		
	}
	
	function imprimeComprobante(){
		console.log("Llamada ajax a imprimir comprobante");
		
		jQuery.ajax({
 	     	type: "POST",
 	     	url: "<%=renderResponse.encodeURL(enviaNotificacionPago.toString())%>",
 	     	cache: false,
 	     	async: false,
 	     	dataType: "html",
 	     	data: { 
 	     		 '<portlet:namespace/>operacion': 'imprime',
 	     		 '<portlet:namespace/>tipo_comprobante': 'ticket'
 	       	},
 	     	success: function(resp){
 	     		//$("#contenedorModalDetalleServicios").html(resp);
 	     		//console.log("La respuesta de imprimir es : " + resp);
 	     	   	var mywindow = window.open('', 'my div', 'height=600,width=789');
 	          	mywindow.document.write('<html><head><title>Comprobante mo</title>');
 	            /*optional stylesheet*/
 	            //mywindow.document.write('<link rel="stylesheet" href="/mo-theme/css/main.css" type="text/css" />');
 	           	mywindow.document.write('</head><body class="e3-03" style="background: #fff;">');
 	           	mywindow.document.write(resp);
 	           	mywindow.document.write('</body></html>');
 	           	mywindow.document.close(); // necessary for IE >= 10
 	           	
 	           	
 	           myDelay = setInterval(checkReadyState, 10);
 	           function checkReadyState() {
 	              if (mywindow.document.readyState == "complete") {
 	                  clearInterval(myDelay);
 	                  mywindow.focus(); // necessary for IE >= 10
 	                  mywindow.print();
 	                  mywindow.close();
 	              }
 	           }	           	
 	           	//mywindow.focus(); // necessary for IE >= 10
 	           	//mywindow.print();
 	           //setTimeout(function(){
 	        	  //mywindow.close();
 	           //}, 500);
 	           	return true;
 	     	}
 	 	});
	}
	
	function validateEmail(email) {
		  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		  return re.test(email);
	}
</script>