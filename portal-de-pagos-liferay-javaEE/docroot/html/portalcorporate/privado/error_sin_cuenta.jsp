<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>

<portlet:defineObjects />



<div class="titulo-pagina normado titulo-post-selects">
    <h1><%= UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate") %></h1>
</div>

<main class="container e3-indisponibilidad width-100 padding-00-i text-center">
    <section class="sub-container clearfix inline-block width-100">
       
        <div class="disculpa-por-inconveniente">
            <div class="imagen">
                <i class="MCSS-alerta_v2"></i>
            </div>
            <div class="texto">
                <h5>Disculpa por el inconveniente</h5>
             	 <p>Error en la cuenta</p>
            </div>
        </div>
        
    </section>
</main>
