<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>

<portlet:defineObjects />

<portlet:actionURL var="volverAPaso1PrivadoActionURL" windowState="normal"
	name="volver_Paso1_Privado">
</portlet:actionURL>

<%
	String ordenDeCompra = (String) renderRequest.getAttribute("ordenDeCompra");
%>

<div class="titulo-pagina normado titulo-post-selects">
    <h1><%= UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet") %></h1>
</div>

<main class="container comprobante-fallido width-100 padding-00-i text-center">

    <div class="contenido clearfix inline-block width-100">

        <div class="steps-new pasos5 col-md-12">
            <ul class="desktop">
                <li class="">
                    <a href="" class="relativo">
                        <i class="iconp-documentos"></i> 
                        <span class="margin-left-20">Resumen de cuenta</span>
                    </a>
                </li>
                <li class="">
                    <a href="javascript:void(0)" class="relativo">
                        <i class="iconp-tarjeta"></i>
                        <span>Medios de pago</span>
                    </a>
                </li>
                <li class="active"><a href="javascript:void(0)" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
            </ul>
        </div>

        <div class="menu-opciones-mobile col-md-12">
            <ul>
                <li class=""><a href="" class="relativo"><i class="iconp-documentos"></i> <span>Resumen de cuenta</span></a></li>
                <li class="">
                    <a href="" class="relativo">
                        <i class="iconp-tarjeta"></i>
                        <span>Medios de pago</span>
                    </a>
                </li>
                <li class="active"><a href="javascript:void(0)" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
            </ul>
        </div>
        <!--/ .steps-new -->

        <div class="cont-comprobante-ticket inline-block text-center">
            
               <div class="no-direccion inline-block width-100 bg-white">
                <div class="cont-icon-advertencia inline-block">
                    <img class="icon-advertencia" src="/mo-theme/images/img-alerta.png">
                </div>
                <div class="texto block">
                    <h4>No fue posible realizar el pago</h4>
                    <h5>Existe un problema en la conexi�n con el servicio de pago</h5>
                </div>
                   <div class="cont-boton inline-block width-100">
                       <div class="cont-btn width">
                           <a href="<%=volverAPaso1PrivadoActionURL%>" class="btn2 bg-clear vertical-middle ">
                               <i class="iconp-reenviar"></i>
                               Reintentar pago</a>
                       </div>
                   </div>
                   <div class="no-fue-posible">
                       <p>De todas maneras, te recomendamos:</p>
                       <ul>
                           <li>Revisar tus datos ingresados.</li>
                           <li>Que tienes el cupo suficiente.</li>
                           <li>Intentar con otro medio de pago.</li>
                       </ul>
                       <p>Si el problema continua o tuviste alg�n inconveniente mayor, cont�ctanos y ten a mano este n�mero de transacci�n:</p>
                   </div>
                   <div class="numero-transaccion"><%=ordenDeCompra%></div>
            </div>
        </div>
    </div>
</main>
