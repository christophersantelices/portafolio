<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>

<portlet:defineObjects />

<portlet:actionURL var="eligeMedioDePagoPrivadoActionURL" windowState="normal" name="eligeMedioDePago_privado" >
</portlet:actionURL>

<portlet:resourceURL var="muestraDetalleCuentasServicios" ></portlet:resourceURL>
<portlet:resourceURL var="verificaProrroga"></portlet:resourceURL>

<%
	String faPorDefecto = (String) renderRequest.getAttribute("faPorDefecto");
	String cuerpoDeuda = (String) renderRequest.getAttribute("cuerpoDeuda");
	String tienePat = (String) renderRequest.getAttribute("tienePat");
	String montoTotal = (String) renderRequest.getAttribute("montoTotal");
	String fechaActual = (String) renderRequest.getAttribute("fechaActual");
	String tieneMontoEnDisputa = (String) renderRequest.getAttribute("tieneMontoEnDisputa");
	String otrasDeudas = (String) renderRequest.getAttribute("otrasDeudas");
	String botonLineas = (String) renderRequest.getAttribute("botonLineas");
	String puedeAbonar = (String) renderRequest.getAttribute("puedeAbonar");
	String tieneProrroga = (String) renderRequest.getAttribute("tieneProrroga");
	String montoProrroga = (String) renderRequest.getAttribute("montoProrroga");
	String fechaProrroga = (String) renderRequest.getAttribute("fechaProrroga");
	String linkOtrasDeudas = (String) renderRequest.getAttribute("linkOtrasDeudas");
	String abono = (String) renderRequest.getAttribute("abono");
	String textoMontoEnDisputa = "";
	
 	if(tieneMontoEnDisputa.equalsIgnoreCase("true")){
		textoMontoEnDisputa = "<span class='tooltips' title='Existe un monto en disputa pendiente. Si pagas y se prueba como cobro err�neo, se te descontar� de tu siguiente boleta'>Monto en disputa</span>";
	}
	String clasePat = "";
 	if(tienePat.equalsIgnoreCase("true")){
		clasePat = "<div class=\"tiene-activo\"><p><span>Tiene activo un </span><strong>Pago Automatico PAC o PAT</strong></p></div>";
	}
 	String prorroga = "";
 	if (tieneProrroga.equalsIgnoreCase("true")) {
 		prorroga = "<button class='btn btn2 bg-dark text-center' onclick='solicitarProrroga()' data-toggle='modal' data-target='#modal_solicitar_prorroga'>Solicitar pr�rroga</button>";
 	}
%>

<div class="titulo-pagina normado titulo-post-selects">
    <h1><%= UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate") %></h1>
</div>

<main class="container comprobante-fallido width-100 padding-00-i text-center">
     <div class="contenido clearfix inline-block width-100">
         <div class="steps-new pasos5 col-md-12">
             <ul class="desktop">
                 <li class="active">
                     <a href="" class="relativo">
                         <i class="iconp-documentos"></i>
                         <span class="margin-left-20">Resumen de cuentas</span>
                     </a>
                 </li>
                 <li class="">
                     <a href="" class="relativo">
                         <i class="iconp-tarjeta"></i>
                         <span>Medios de pago</span>
                     </a>
                 </li>
                 <li class=""><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
             </ul>
         </div>
         <div class="menu-opciones-mobile col-md-12">
             <ul>
                 <li class="active"><a href="" class="relativo"><i class="iconp-documentos"></i> <span>Resumen de cuentas</span></a></li>
             </ul>
         </div>
         <div class="detalle-cuentas inline-block text-center bg-white">
             <form>
                 <div class="cont-detalle-cuentas">
                    <%=cuerpoDeuda%>
                     <div class="cont-link pull-right ">
                         <a class="link-simple bt-slide-panel" >
                             Ver detalle de mis boletas y pago por servicio
                         </a>
                     </div>
                     <div class="cont-total-a-pagar">
                         <div class="text-left text-numero-venta-2">
                             <p class="inline-block">
                                 <strong class="display-block">Total a pagar</strong>
                                 <span class="fecha display-block"><%=fechaActual%></span>
                             </p>
                             <span class="precio inline-block">$<strong id="total-a-pagar"><%=montoTotal%></strong></span>
                         </div>
                         <div class="cont-btn pull-right">
	                         <button id="medio-pago" type="button" class="btn-verde" data-toggle="modal" data-target="#modal_pago_automatico">
	                             Seleccionar medio de pago
	                         </button>
                     	</div>
                     </div>
                 </div>
             </form>
         </div>
         <div class="menu-opciones-mobile col-md-12">
             <ul>
                 <li class="">
                     <a href="" class="relativo">
                         <i class="iconp-tarjeta"></i>
                         <span>Medios de pago</span>
                     </a>
                 </li>
                 <li class=""><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
             </ul>
         </div>
     </div>
 </main>


<div id="slide-page" class="slide-page slide-panel">

        <div class="volver-al-resumen width-100 inline-block bt-close-panel">
            <img src="/mo-theme/images/img-volver-resumen.png" alt="">
            <h4 id="tituloVolver">Volver al resumen de cuenta</h4>
        </div>
        <div class="detalle-cuenta-interaccion">
            <div class="cont-detalle-cuenta-interaccion">
                <div class="detalle-de-tus-cuentas width-100">
                    <span>Estas viendo</span>
                    <h5>Detalle de tus documentos y servicios</h5>
                </div>

                <div class="cuenta-numero-2 fixed">
                    <div class="cuenta-n">
                        <p>
                        <span>Cuenta N� </span>
                        <strong> <%=faPorDefecto%> </strong>
                        </p>
                        <%=botonLineas%>
                    </div>
                    <%=clasePat%>
                    
                    <div class="ver-lineas-asociandas">
        				<%=prorroga %>
    				</div>
                </div>
                <%-- <%=puedeAbonar%> --%>
                
                <div class="boletas detalle" id="contenedorDetalle">
                
                </div>
                <div id="contenedorServiciosCuentaPrincipal"></div>
                
                <%=linkOtrasDeudas%>
                <div id="cont-ver-todas-las-cuentas">
                    <h2 class="estas-son">Estas son otras cuentas asociadas a tu RUT que puedas pagar</h2>
                    <!-- Aqui agregar el detalle de las cuentas -->
                    <%=otrasDeudas%>
                </div>
                <div id="contenedorServiciosOtrasCuentas"></div>
            </div>
        </div>
        
        <div id="pie-pagar-boletas" class="pie-pagar-boletas">
            <div class="cont-pie-pagar-boletas">

                <div id="no-estas-pagando-total-deuda-principal" class="pie-no-estas-pagando" style="display:none;">
                    <div class="pie-seleccion">
                        <i class="MCSS-alerta_v2"></i>
                        <div>
                            <p><strong>No estas pagando todas tus cuentas.</strong></p>
                            <p>El monto original es $ <span><%=montoTotal%></span></p>
                        </div>
                    </div>
                </div>
                <div id="pagas-otros-montos" class="pie-seleccionaste" style="display: inline-block ; visibility:hidden" >
                    <div class="pie-seleccion">
                        <p><strong>Seleccionaste pagar boletas adicionales</strong></p>
                        <p>que se encuentran asociadas a su RUT</p>
                    </div>
                </div>

                <div class="pie-total-pagar hidden-xs">
                    <p><strong>Total a pagar</strong></p>
                    <p><%=fechaActual%></p>
                </div>
                <div class="pie-total-pagar visible-xs">
                    <p><strong>Total a pagar</strong></p>
                    <em><i>$</i><span id="total-a-pagar2"><%=montoTotal%></span></em>
                </div>
                <div class="pie-precio"><em><i>$</i><span id="total-a-pagar3"><%=montoTotal%></span></em></div>
                <div class="pie-boton">
                    <button class="btn-verde" id="medio-pago-detalle" type="button" data-toggle="modal" data-target="#modal_pago_automatico">Pagar</button>
                </div>
            </div>
        </div>
    </div>
	<div id="contenedorModalDetalleServicios"></div>
	
	<div id="contenedorModalLineas"></div>
	
	<!-- Mensaje modal NO PUEDES dejar de pagar la vencida -->
	<div id="modal_no_dejar_de_pagar_vencida" style="display:none" class="modal-mo" role="dialog" data-keyboard="false">
        <div class="modal-dialog no_dejar_de_pagar_vencida">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="caja-dialogo inline-block width-100 bg-white">
                        <div class="cont-icon-advertencia inline-block">
                            <img class="icon-advertencia" src="/mo-theme/images/img-alerta.png">
                        </div>
                        <div class="cont-modal-no_dejar_de_pagar_vencida">
                            <h4>No puedes dejar de pagar tu cuenta vencida.</h4>

                            <div class="cont-border-bottom">
                                <p>Pagar en l�nea. Es r�pido y seguro</p>
                                <a href="javascript:void(0)" onclick="cierraModal()" class="btn-verde bt-close-modal-mo">Entendido</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<!-- Modal ABONO -->    
 <div id="modal_abonar" style="display:none" class="modal-mo" role="dialog" data-keyboard="false">
        <div class="modal-dialog detalle_de_servicios">
    		<div class="modal-content">
                <div class="modal-body">
                    <div class="caja-dialogo inline-block width-100 bg-white">
                        <div class="cabecera">
                            <span class="no-spacing">
                                Abonar a una cuenta
                            </span>
                            <i class="MCSS-cancel close" data-dismiss="modal" onclick="cerrarAbonar()"></i>
                        </div>
                        <div class="cont-modal">
                            <p>Abona a tu cuenta y evita la suspenci�n de tus servicios.</p>
                            <div class="importante">
                                <div class="cont-alerta">
                                    <i class="MCSS-alerta_v2" onclick="cerrarAbonar()"></i>
                                </div>
                                <div class="cont-importante">
                                    <h5>Importante</h5>
                                    <p>Podr�s realizar solo un abono por periodo y a una cuenta. Revisa en detalle <a href="#0" class="c-white bt-slide-panel" data-dismiss="modal"><strong>tus boletas y servicios.</strong></a></p>
                                </div>
                            </div>
                            <form action="" novalidate="novalidate">
                                <div class="insertar-abono">
                                    <div class="input-abono">
                                        <label for="abonar">Abonar a cuenta N�:</label>
                                        <%=abono%>
                                        <input type="text" id="txtMontoAbono" name="txtMontoAbono" placeholder="Ingresa monto">
                                    </div>
                                    <div class="botones">
                                        <a class="link-simple-arrow-left" href="#0" data-dismiss="modal" onclick="cerrarAbonar()">Volver</a>
                                        <!-- <button class="btn-verde" onclick="realizarAbono()">Abonar</button> -->
                                        <a href="javascript:void(0)" class="btn-verde bt-close-modal-mo" onclick="realizarAbono()" >Abonar </a> 
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
         </div>
       </div>
    
	<div id="modal_solicitar_prorroga" class="modal-mo" role="dialog" data-keyboard="false" style="display: none;">
        <div class="modal-dialog desmarcar_servicios">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <form method="post" action="#">
                        <div class="caja-dialogo inline-block width-100 bg-white">
                            <div class="cont-icon-advertencia inline-block">
                                <img class="icon-advertencia" src="/mo-theme/images/img-alerta.png">
                            </div>
                            <div class="cont-modal-desmarcar_servicios">
                                <h4>Solicitud de pr�rroga</h4>

                                <div class="cont-border-bottom fixed">
                                    <p class="txt-1">Estas solicitando una pr�rroga para la <strong><span id="ncuenta">cuenta <%=faPorDefecto%></span></strong>, la cual tiene un costo de <strong>$<%=montoProrroga %></strong> y ser� facturado en tu pr�xima boleta.</p>

                                    <p class="txt-2">Solo puedes solicitar una pr�rroga por periodo, en caso de que necesites solicitar otra, esta podr� ser a partir del <strong><%=fechaProrroga %></strong></p>
                                    <div class="terminos-condiciones">
                                        <input type="checkbox" value="None" id="acetpo-terminos-p" name="check">
                                        <label for="acetpo-terminos-p">Acepto los <a href="javascript:void(0)" class="link-simple">t�rminos y condiciones</a></label>
                                    </div>

                                    <div class="cont-botones" >
                                        <a href="javascript:cancelarProrroga()" class="link-simple-arrow-left" data-dismiss="modal">Cancelar pr�rroga</a>
                                        <button type="submit" id="enviar-solicitud-prorroga" class="btn-azul close" data-dismiss="modal" disabled>Entendido</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="<%=faPorDefecto%>" name="<portlet:namespace/>fa">
                    </form>
                </div>
            </div>
        </div>
    </div>
	<!-- MODAL PAGO DIFERENCIADO -->
    <div id="modal_pago_diferenciado" style="display:none" class="modal-mo">
        <div class="modal-dialog pago_diferenciado">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="caja-dialogo inline-block width-100 bg-white">
                        <div class="cont-icon-advertencia inline-block">
                            <img class="icon-advertencia hidden-desktop" src="/mo-theme/images/img-alerta.png">
                        </div>
                        <div class="cont-modal-pago_diferenciado">
                            <h4>Ya realizaste el pago diferenciado de servicios m�s de dos veces durante este a�o.</h4>

                            <div class="cont-border-bottom">
                                <p>Ahora debes pagar todas las boletas de esta cuenta.
                                </p>
                                <a href="javascript:void(0)" class="link-simple-arrow-right bt-close-modal-mo">Conocer m�s del pago diferenciado</a>
                                <a href="javascript:void(0)" onclick="cierraModal()"  class="btn-azul bt-close-modal-mo">Entendido</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<!-- MODAL DESMARCAR SERVICIO -->
    <div id="modal_desmarcar_servicio" style="display:none" class="modal-mo">
        <div class="modal-dialog desmarcar_servicios">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <form action="">
                        <div class="caja-dialogo inline-block width-100 bg-white">
                            <div class="cont-icon-advertencia inline-block">
                                <img class="icon-advertencia hidden-desktop" src="/mo-theme/images/img-alerta.png">
                            </div>
                            <div class="cont-modal-desmarcar_servicios">
                                <h4>Los servicios que has desmarcado quedar�n deshabilitados hasta 
                                        que realices el pago del servicio correspondiente.</h4>
                                <div class="cont-border-bottom">
                                    <p class="txt-1">Esta opci�n de pago selectivo de servicios puede efectuarse m�ximo <strong>2 veces por a�o.</strong> Si tu boleta est� vencida, no se descartan acciones de cobranza.</p>

                                    <p class="txt-2"><strong>Esta modificaci�n afecta solo al pago de esta boleta.</strong> Si quieres modificar los servicios de todas las boletas, debes ingresar en el detalle de cada boleta.</p>
                                    <div class="terminos-condiciones">
                                        <input type="checkbox" value="None" id="acetpo-terminos" name="check" />
                                        <label for="acetpo-terminos"></label>
                                        <p>Acepto los <a href="http://www.mo.cl/web/mo/aviso-legal" target="_blank" class="link-simple">terminos y condiciones</a> del pago diferenciado de servicios.</p>
                                    </div>
                                    <div class="cont-botones">
                                        <a href="javascript:void(0)" onClick="cierraModalConfirmacionPPS()" class="link-simple-arrow-right bt-close-modal-mo">Cancelar modificaci�n</a>
                                        <button id="confirma-servicios-a-quitar" type="button" onClick="modificaServicios()" class="btn-azul bt-close-modal-mo close" disabled >Entendido</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
        <!-- MODAL ABONAR CUENTA -->
    <div id="modal_abonar_cuenta" style="display:none" class="modal-mo">
        <div class="modal-dialog detalle_de_servicios">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">

                    <div class="caja-dialogo inline-block width-100 bg-white">
                        <div class="cabecera">
                            <span class="no-spacing">
                                Abonar a una cuenta
                            </span>
                            <i class="MCSS-cancel close" data-dismiss="modal"></i>
                        </div>

                        <div class="cont-modal">

                            <p>Abona a tu cuenta y evita la suspenci�n de tus servicios.</p>

                            <div class="importante">

                                <div class="cont-alerta">
                                    <i class="MCSS-alerta_v2"></i>
                                </div>
                                <div class="cont-importante">
                                    <h5>Importante</h5>
                                    <p>Podr�s realizar solo un abono por periodo y a una cuenta. Revisa en detalle tus cuentas <a href="#0" class="c-white"><strong>ac�.</strong></a></p>
                                </div>
                            </div>
                            <form action="">
                                <div class="insertar-abono">
                                    <div class="input-abono">
                                        <label for="abonar">Abonar a cuenta N�:</label>
                                        <select name="abonar" id="abonar">
                                            <option value="0">8345928344 - Total: $39.990</option>
                                            <option value="1">333362231931 - Total: $10.800</option>
                                            <option value="2">333362231931 - Total: $19.990</option>
                                        </select>
                                        <input type="text" name="monto-abonar" placeholder="Ingresa monto">
                                    </div>
                                    <div class="botones">
                                        <a class="link-simple-arrow-left" href="#0" data-dismiss="modal">Volver</a>
                                        <button class="btn-verde">Abonar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<form id="formDeudaPrivado" action="<%=eligeMedioDePagoPrivadoActionURL%>" style="display:none" method="POST">
	
		<input type="hidden" id="carroPagoPrivado" value=""/>
		<input type="hidden" id="temporalServiciosAQuitar" value=""/>
		<input type="hidden" id="<portlet:namespace/>carroPagoPrivadoFinal" name="<portlet:namespace/>carroPagoPrivadoFinal" value=""/>
	
	</form>
	
	<form id="formAbono" name="formAbono" action="#" method="POST" style="display:none;">
		<input type="hidden" id="<portlet:namespace/>montoAbono" name="<portlet:namespace/>montoAbono" value=""/> 
		<input type="hidden" id="<portlet:namespace/>cuentaAbono" name="<portlet:namespace/>cuentaAbono" value=""/> 
	</form>
	
 <script>
 

 $(document).ready(function(){
	 $("input:checkbox").prop('checked', true);
	 $("#acetpo-terminos").prop('checked', false);
	 asignaEstadoInicial();
 });


 function callObtenerServicios(padre){
 	
 	var hijos = $(padre).find(':input');
 	var operacion = 'getServicios';
    var faSeleccionada = "";
    var docSeleccionado = "";
 	
 	$(hijos).each(
 	    function(index){  
 	        var input = $(this);
 	        if(input.attr('id') == "cuenta_padre"){
 	        	faSeleccionada = input.val();
 	        }
 	        else if(input.attr('id') == "documento_marcado"){
 	        	docSeleccionado = input.val();
 	        }
 	    }
 	);
 	console.log("Llamada ajax a obtener servicios >> Cuenta : " + faSeleccionada + " Documento : " + docSeleccionado);

 	if(document.getElementById("contServiciosD"+docSeleccionado+"C"+faSeleccionada).innerHTML.length < 10){
 		jQuery.ajax({
 	     	type: "POST",
 	     	url: "<%=renderResponse.encodeURL(muestraDetalleCuentasServicios.toString())%>",
 	     	cache: false,
 	     	dataType: "html",
 	     	data: { 
 	             '<portlet:namespace/>operacion': operacion, 
 	             '<portlet:namespace/>faSeleccionada': faSeleccionada,
 	             '<portlet:namespace/>docSeleccionado': docSeleccionado
 	       	},
 	     	success: function(resp){
 	     		//$("#contenedorModalDetalleServicios").html(resp);
 	     		
 	     		$("#contServiciosD"+docSeleccionado+"C"+faSeleccionada).html(resp);
 	     		
 	     		$("#modal_detalle_servicios_D"+docSeleccionado+"C"+faSeleccionada).show();
 	     		console.log("height 1"+document.body.offsetHeight);
 	     		setTimeout(sendMessage, 8000);
 	     	}
 	 	});
 	}
 	else{
 		// Si ya fue llamado anteriormente, no lo vuelvo  llamar
 		$("#modal_detalle_servicios_D"+docSeleccionado+"C"+faSeleccionada).show();
 	}
 }
 
	function obtenerLineas(cuentaFinanciera){
		
		console.log("Llamada ajax a obtener lineas de la cuenta " + cuentaFinanciera);
		jQuery.ajax({
 	     	type: "POST",
 	     	url: "<%=renderResponse.encodeURL(muestraDetalleCuentasServicios.toString())%>",
 	     	cache: false,
 	     	dataType: "html",
 	     	data: { 
 	     		 '<portlet:namespace/>operacion': 'getLineasPorFA',
 	             '<portlet:namespace/>faSeleccionada': cuentaFinanciera,
 	             '<portlet:namespace/>docSeleccionado': ''
 	       	},
 	     	success: function(resp){
 	     		//$("#contenedorModalDetalleServicios").html(resp);
 	     		
 	     		console.log("La respuesta de obtener lineas es : " + resp);
 	     		
 	     		$("#contenedorModalLineas").html(resp);
 	     		$("#modal_lineas_asociadas").show();
 	     	}
 	 	});
		
	}
 
	function cierraModalLineas(){
		 $("#modal_lineas_asociadas").hide();		 
	 }
 
 
 function cierraModalServicios(idModal){
	 $("#modal_detalle_servicios_"+idModal).hide();
	 $("#temporalServiciosAQuitar").val ("");
	 
 }

 function callObtenerDocumentosOtrasCuentas(idCuenta, location){

 	 var operacion = 'getDocumentosOtrasCuentas';
     var faSeleccionada = idCuenta;
     var docSeleccionado = "";
     
     
     if (document.getElementById("otraCuenta_"+location).innerHTML.length < 2){
    	 	jQuery.ajax({
    	     	type: "POST",
    	     	url: "<%=renderResponse.encodeURL(muestraDetalleCuentasServicios.toString())%>",
    	     	cache: false,
    	     	dataType: "html",
    	     	data: { 
    	             '<portlet:namespace/>operacion': operacion,
    	             '<portlet:namespace/>faSeleccionada': faSeleccionada,
    	             '<portlet:namespace/>docSeleccionado': docSeleccionado
    	       	},
    	     	success: function(resp){
    	     		document.getElementById("otraCuenta_"+location).innerHTML = resp;
    	     		$("#otraCuenta_"+location).find('div').each(function(){
 	 	    			if ( $(this).hasClass("marcadorContServ")){
 	 	    				$("#contenedorServiciosOtrasCuentas").append($(this).clone());
 	 	    				$(this).remove();
 	 	    			}
 	    			});
    	     	}
   	 		});	
     }
 }

	 function muestraNoTienePPS(){	
	 	$("#modal_pago_diferenciado").show();
	 }
		function noDejarDePagarSinObj(){
			console.log("Mostrando no dejar de pagar");
			$("#modal_no_dejar_de_pagar_vencida").show();
		}
 
	 function recibeCuentaPrincipal(cuentaObj){
	 		
	 		console.log("Recibo cuenta principal " + $(cuentaObj).val());
	 		
	 		var carro = $("#carroPagoPrivado").val();
	 		var idCheck = $(cuentaObj).attr('id');
	 		var cuentaPrincipal = $("#cuentaPrincipal").val();
	 		var esVencida  = (idCheck.indexOf('V') > -1 ) ? true : false;
	 		var estaSeleccionada = $(cuentaObj).is(':checked');
	 		console.log("Cuenta objeto"+cuentaObj);
	 		var monto = $(cuentaObj).val();
	 		var pagaTotal = (  $("#V"+cuentaPrincipal).is(':checked') && $("#T"+cuentaPrincipal).is(':checked')  ) ? true :false;
	 		
	 		// Condicion de paga total para cuando solo tiene deuda por vencer
	 		if(   $("#V"+cuentaPrincipal).length <= 0  &&  $("#T"+cuentaPrincipal).is(':checked')  ){
	 			pagaTotal = true;
	 		}
	 		//Agregar misma condicion para cuando tiene solo vencida ???
	 		console.log("es vencida "+esVencida)
	 		console.log("estea seleccionad "+estaSeleccionada);
	 		console.log ("pagatotal" +pagaTotal);

	 	 		
	 		$(cuentaObj).removeClass('neutro');
	 		
	 		if(esVencida){
	 			if(estaSeleccionada) {
	 				console.log("Es la cuenta vencida y esta seleccionada");
	 				//Eliminar todos servicios y documentos  vencidos del carro 
	 				//Marcar todos los documentos vencidos y todos los servicios 
	 				//Quitar clase amarilla 
	 				//Poner clase roja
	 				//Ocultar no estas pagando de los documentos
	 				$("#V"+cuentaPrincipal+"no-estas-pagando").hide();
	 				//Quitar clase neutro
	 				$("#V"+cuentaPrincipal).removeClass('neutro');
	 				if(pagaTotal){
	 					console.log("Paga el total de la deuda principal");
	 					//Eliminar todos los documentos y servicios de la cuenta, vencidos y por vencer
	 					$ ("#contenedorDetalle").find(':checkbox').each(function(){
	 						$("#contServicios"+$(this).attr('id')).find(':checkbox').each(function(){
	 							if(  $(this).hasClass('regulado')){
	 								carro = carro.replace ( $(this).attr('id')+";" , ""  );
	 								$(this).prop('checked' , true);
	 							}
	 						});
	 						carro = carro.replace ($(this).attr('id') + ";" , "") ;
	 						$(this).removeClass('neutro')
	 						$(this).prop('checked' , true);
	 						$(this).parent().parent().parent().removeClass('amarilla')
	 						$(this).parent().parent().next().hide();
	 						if(  $(this).parent().parent().parent().hasClass('boletaVencida')   ){
	 							$(this).parent().parent().parent().addClass('roja') 
	 						}
	 	 				});
	 					carro = carro.replace (  "V"+cuentaPrincipal+";" ,  ""   ) ;
	 					carro = "T"+cuentaPrincipal+";"+carro;
	 				}
	 				else{
	 					console.log("Paga solo la vencida");
	 					var pagaParcial = ( $("#T"+cuentaPrincipal).hasClass('neutro')) ? true :false;
	 					
	 					if (pagaParcial){
	 						console.log ("Hay elementos de la deuda por vencer seleccionados");
	 						//Eliminar todos servicios de deuda por vencida
	 						//Agregar todos los documentos vencidos
	 						// No tocar los por vencer
	 						$ ("#contenedorDetalle").find(':checkbox').each(function(){
	 							if( $(this).parent().parent().parent().hasClass('boletaVencida')){
	 								$("#contServicios"+$(this).attr('id')).find(':checkbox').each(function(){
	 									carro = carro.replace ( $(this).attr('id')+";" , ""  );
	 									$(this).prop('checked', true );
	 	 							});
	 								carro = carro + $(this).attr('id')+";";
	 								$(this).prop('checked', true);
	 								$(this).removeClass('neutro');
	 								$(this).parent().parent().parent().removeClass('amarilla');
	 								$(this).parent().parent().parent().addClass('roja');
	 								$(this).parent().parent().next().hide();
	 							}
	 						});
	 					}
	 					else{
	 						console.log ("No hay elementos de la deuda por vencer seleccionados");
	 						//No paga ningun documento o servicio de la deuda por vencer
	 						// Poner carro en V
	 						// Eliminar todos los servicios vencidos 
	 						$ ("#contenedorDetalle").find(':checkbox').each(function(){
	 							if(   $(this).parent().parent().parent().hasClass('boletaVencida')    ){
	 								$("#contServicios"+$(this).attr('id')).find(':checkbox').each(function(){
	 	 	 							if(  $(this).hasClass('regulado')){
	 	 	 								carro = carro.replace ( $(this).attr('id')+";" , ""  );
	 	 	 								$(this).prop('checked' , true);
	 	 	 							}
	 	 	 						});
	 	 	 						carro = carro.replace ($(this).attr('id') + ";" , "") ;
	 	 	 						$(this).prop('checked' , true);
	 	 	 						$(this).parent().parent().parent().removeClass('amarilla');
	 	 	 						$(this).parent().parent().parent().addClass('roja');
	 	 	 						$(this).parent().parent().next().hide();
	 							}
	 	 	 				});
	 	 					carro = carro.replace (  "T"+cuentaPrincipal+";" ,  ""   ) ;
	 	 					carro = "V"+cuentaPrincipal+";"+carro;
	 					}
	 				}
	 	 		}

	 		}
	 		else{
	 			//Es la por vencer
	 			if(estaSeleccionada){
	 				console.log("Marcando cuenta por vencer")
	 				if(pagaTotal){
	 					console.log("Paga el total de la deuda principal");
	 					carro = carro.replace("V"+cuentaPrincipal+";" , "");
	 	 				carro = carro.replace("T"+cuentaPrincipal+";" , "");
	 	 				carro = "T"+cuentaPrincipal+";" + carro;
	 	 				//Marcar todos los documentos por vencer y todos los servicios 
	 	 				$ ("#contenedorDetalle").find(':checkbox').each(function(){
	 						//Eliminar todos servicios y documentos  de la cuenta principal del carro 
	 	 	 				$("#contServicios"+ $(this).attr('id')).find(':checkbox').each(function (){
	 	 						if(  $(this).hasClass('regulado') ){
	 	 							$(this).prop('checked' , true);
	 	 						}
	 	 						carro = carro.replace ( $(this).attr('id') + ";" , "");
	 	 					});
	 	 					// Validar que no elimina referencias a otros documentos
	 	 					// En teoria no, porque elimino primero los servicios del mismo documento
	 	 					carro = carro.replace ( $(this).attr('id') + ";" , "");
	 	 					$(this).prop('checked' , true);
	 	 					//Quitar clase amarilla 
	 	 					$(this).parent().parent().parent().removeClass('amarilla');
	 	 					//Agregar clase roja si es vencido
	 	 					if(  $(this).parent().parent().parent().hasClass('boletaVencida')   ){
	 	 						$(this).parent().parent().parent().addClass('roja');
	 	 					}
	 	 					//Quitar clase neutro
	 	 					$(this).removeClass('neutro');
	 	 					//Ocultar no estas pagando de los documentos
	 	 					$(this).parent().parent().next().hide();
	 	 				});
	 				}
	 				else{
	 					//Comprobar que no este pagando documentos (cuanto son mas de 1 ) o servicios vencidos
	 					console.log("Paga documentos y/o servicios de documentos vencidos")
	 					//Eliminar V o T
	 					//Eliminar todos los servicios de los documentos por vencer 
	 					// Agregar todos los documentos por vencer que no esten agregados
	 					carro = carro.replace("T"+cuentaPrincipal+";" , "");
		 	 			carro = carro.replace("V"+cuentaPrincipal+";" , "");
		 	 			$ ("#contenedorDetalle").find(':checkbox').each(function(){
		 	 				if( ! $(this).parent().parent().parent().hasClass('boletaVencida')   ){
		 	 					$("#contServicios"+ $(this).attr('id')).find(':checkbox').each(function (){
			 	 					carro = carro.replace( $(this).attr('id')+";" , "");
			 	 					$(this).prop('checked', true);
			 	 				});
		 	 					if(    carro.indexOf( $(this).attr('id')+";"  ) == -1    ){
		 	 						carro = carro + $(this).attr('id')+";";
		 	 					}
		 	 					$(this).prop('checked' , true);
		 	 					$(this).parent().parent().parent().removeClass('amarilla');
		 	 					$(this).parent().parent().next().hide();
		 	 				}
		 	 			});
	 				}
	 				$("#no-estas-pagando-total-deuda-principal").css({ 'display': "none" });
	 				$("pagas-otros-montos").show();
					$("#"+"T"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
					$("#T"+cuentaPrincipal).removeClass('neutro');
	 	 		}
	 	 		else{
	 	 			console.log("Desmarcando cuenta por vencer")
	 	 			var pagaParcial = ( $("#V"+cuentaPrincipal).hasClass('neutro')) ? true :false;
	 	 			if(pagaParcial){
	 	 				//Desmarcar solo los por vencer 
	 	 				console.log("Esta pagando algunos documentos o servicios de la cuenta vencida")
	 	 				//Eliminar todos los servicios de los documentos por vencer
	 	 				//Eliminar todos los documentos por vencer 
	 	 				$ ("#contenedorDetalle").find(':checkbox').each(function(){
	 	 					if( !  $(this).parent().parent().parent().hasClass('boletaVencida')   ){
	 	 						$("#contServicios"+ $(this).attr('id')).find(':checkbox').each(function (){
	 	 							carro = carro.replace( $(this).attr('id')+";" , "");
			 	 					$(this).prop('checked', false);
	 	 	 					});
	 	 						carro = carro.replace( $(this).attr('id')+";" , "");
	 	 						$(this).prop('checked' , false);
		 	 					$(this).parent().parent().parent().removeClass('amarilla');
		 	 					$(this).parent().parent().next().hide();
	 	 	 				}
	 	 				});
	 	 			}
	 	 			else{
	 	 				//Eliminar todos servicios y documentos por vencer del carro
	 	 				console.log("No paga documentos y/o servicios de la cuenta vencida");
	 	 	 			carro = carro.replace("T"+cuentaPrincipal+";" , "");
		 	 			carro = carro.replace("V"+cuentaPrincipal+";" , "");
		 	 			
		 	 			//Agrego la vencida solo si tiene vencida por vencer
		 	 			if( $("#V"+cuentaPrincipal).length > 0  ){
		 	 				carro = "V"+cuentaPrincipal+";" + carro;
		 	 			}
		 	 			
	 	 	 			$("#no-estas-pagando-total-deuda-principal").css({ 'display': "none" });
	 	 	 			$("#pagas-otros-montos").show();
	 					$("#"+"T"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
	 					$("#T"+cuentaPrincipal).removeClass('neutro');
	 	 	 			$ ("#contenedorDetalle").find(':checkbox').each(function(){
	 	 	 				if( !  $(this).parent().parent().parent().hasClass('boletaVencida')   ){
	 	 	 					//Desmarcar todos los documentos y servicios asociados a la deuda por vencer de la cuenta principal
	 	 	 					//Desmarcando servicios del los documentos por vencer, eliminando del carro
	 	 	 					$("#contServicios"+ $(this).attr('id')).find(':checkbox').each(function (){
	 	 	 						carro = carro.replace( $(this).attr('id')+";" , "");
	 	 	 						if( $(this).hasClass('regulado') ){
	 	 	 							$(this).prop('checked' , false);
	 	 	 						}
	 	 	 					});
	 	 	 					carro = carro.replace( $(this).attr('id')+";" , "");
	 	 	 					$(this).prop('checked' , false);
	 	 	 					//Quitar clase amarilla 
	 	 	 					$(this).parent().parent().parent().removeClass('amarilla');
	 	 	 					//Quitar clase neutro
	 	 	 					$(this).removeClass('neutro');
	 	 	 					//Ocultar no estas pagando de los documentos
	 	 	 					$(this).parent().parent().next().hide();
	 	 	 				}
	 	 				});
	 	 			}
	 	 		}
	 		}
	 		$("#carroPagoPrivado").val(carro);
			console.log("Carro antes de salir : " + $("#carroPagoPrivado").val());
			calculaTotales();
	 	}
 
 	function recibeDocumentoCuentaPrincipal(documentoObj){
 		
 		console.log("Recibo Documento Cuenta Principal, valor : " + $(documentoObj).val());
 		
 		// Si paga parcial el check va falso
 		var carro = $("#carroPagoPrivado").val();
 		var cuentaPrincipal = $("#cuentaPrincipal").val();
 		var idCheck = $ (documentoObj).attr('id');
 		var seleccionado = $ (documentoObj).is(':checked');
 		var pagaTodos = true;
 		
 		var cantDocVencidos = 0;
 		var cantDocPorVencer = 0;
		var cantDocVencidosSeleccionados = 0;
		var cantDocPorVencerSeleccionados = 0;
 		
 		$ ("#contenedorDetalle").find(':checkbox').each(function(){
 			if(  $(this).parent().parent().parent().hasClass('boletaVencida')   ){
 				cantDocVencidos ++;
 				if (  $(this).is(':checked')  ){
 					cantDocVencidosSeleccionados ++;
 				}
 			}
 			else{
 				cantDocPorVencer ++;
 				if (  $(this).is(':checked')  ){
 					cantDocPorVencerSeleccionados++;
 				}
 			}
 			if( ! $(this).is(':checked')){
 				pagaTodos = false;
 			}
 		});
 		
 		// Eliminar T o V
 		// Eliminar clase amarilla y neutro del documento, si es documento vencido, poner clase roja
 		$ (documentoObj).parent().parent().parent().removeClass('amarilla');
 		$ (documentoObj).removeClass('neutro');
 		$ (documentoObj).parent().parent().next().hide();
 		if($ (documentoObj).parent().parent().parent().hasClass('boletaVencida')){
 			$ (documentoObj).parent().parent().parent().addClass('roja');
 		}
 		if (seleccionado){
 			// Eliminar todos los servicios asociados 
 			// Marcar todos los servicios
 			$("#contServicios" + idCheck).find(':checkbox').each(function (){
				carro = carro.replace( $(this).attr('id')+";" , "");
				if( $(this).hasClass('regulado') ){
					$(this).prop('checked' , true);
				}
			});
 		}
 		else{
 			// Eliminar todos los servicios asociados 
 			// Desmarcar todos los servicios 
 			$("#contServicios" + idCheck).find(':checkbox').each(function (){
				carro = carro.replace( $(this).attr('id')+";" , "");
				if( $(this).hasClass('regulado') ){
					$(this).prop('checked' , false);
				}
			});
 		}
 		// Si paga todos los documentos de la cuenta, poner T y eliminar todos los documentos y servicios que estaban agregados
 		// Agregar al carro todos los documetos que est�n seleccionados 
		if (pagaTodos){
			console.log("Paga todos los documentos de la cuenta principal"); 
			//Eliminar todos los documentos, agregar T
			$ ("#contenedorDetalle").find(':checkbox').each(function(){
				console.log("id "+$(this).attr('id'));
				carro = carro.replace (  $(this).attr('id')+";" , ""  );
			});
			
			//Elimino la vencida en el caso que haya 1 doc vencido y 1 doc por vencer
			console.log("elementos del carro "+carro);
			carro = carro.replace("V"+cuentaPrincipal+";" ,  "");
			
			if ($(".cont-montos.por-pagar").length) {
				console.log("tiene por vencer");
				carro = "T"+cuentaPrincipal + ";" + carro;
			}
			else {
				console.log("tiene por vencer");
				carro = "T"+cuentaPrincipal + ";" + carro;
				//console.log("tiene vencido");
				//carro = "V"+cuentaPrincipal + ";" + carro;
			}
			
			console.log("elementos del carro2 "+carro);
			// Eliminar, no estas pagando 
			$("#no-estas-pagando-total-deuda-principal").css({ 'display': "none"});
			$("#pagas-otros-montos").show();
			$("#"+"T"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
			$("#T"+cuentaPrincipal).removeClass('neutro');
			$("#"+"V"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
			$("#V"+cuentaPrincipal).removeClass('neutro');
		}
		else{
			console.log("No paga todos los documentos de la cuenta principal");
			//Eliminar T
			carro = carro.replace (  "T"+cuentaPrincipal+";" , "" );
			carro = carro.replace (  "V"+cuentaPrincipal+";" , "" );
			if(seleccionado){
				//Agregar el documento
				if (cantDocVencidos ==  cantDocVencidosSeleccionados &&  cantDocPorVencerSeleccionados == 0){
					console.log("Paga solo los documentos vencidos de la cuenta ");
					carro = "V"+cuentaPrincipal + ";" + carro;
					// Eliminar neutro y no estas pagando de la cuenta vencida
					// Desmarcar la cuenta por vencer y eliminar no estas pagando y neutro
					$("#"+"V"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
					$("#V"+cuentaPrincipal).removeClass('neutro');
					$("#V"+cuentaPrincipal).prop('checked' , true);
					
					$("#"+"T"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
					$("#T"+cuentaPrincipal).removeClass('neutro');
					$("#T"+cuentaPrincipal).prop('checked' , false);
				}
				else{
					console.log("Agregando los documentos seleccionados");
					$ ("#contenedorDetalle").find(':checkbox').each(function(){
						if ( $(this).is(':checked')  ){
							if(  carro.indexOf( $(this).attr('id') + ";" ) == -1  ){
								carro = carro + $(this).attr('id') + ";";
							}
						}
					});
				}
			}
			else{
				//Eliminar el documento
				carro = carro.replace( idCheck + ";" , "");
				// Si paga todos los vencidos, y ninguno por vencer poner V, si no ; 
				// agregar todos los documentos seleccionados y eliminar V
				if( cantDocVencidos ==  cantDocVencidosSeleccionados &&  cantDocPorVencerSeleccionados == 0){
					console.log("Paga solo los documentos vencidos de la cuenta , o no tiene documentos vencidos");
					//Solo si tiene doc vencidos pongo V
					if(cantDocVencidos > 0){
						carro = "V"+cuentaPrincipal + ";" + carro;
					}
					//Eliminar todos los documentos vencidos del carro
					$ ("#contenedorDetalle").find(':checkbox').each(function(){
						if(  $(this).parent().parent().parent().hasClass('boletaVencida')  ){
							carro = carro.replace (  $(this).attr('id')+";" , "" );
						}
					});
					$("#"+"V"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
					$("#V"+cuentaPrincipal).removeClass('neutro');
					
					$("#"+"T"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
					$("#T"+cuentaPrincipal).removeClass('neutro');
					$("#T"+cuentaPrincipal).prop('checked' , false);
				}
				else{
					console.log("Paga alguno/ninguno o todos los vencidos y uno o mas por vencer");
					carro = carro.replace (  "V"+cuentaPrincipal+";" , "" );
					$ ("#contenedorDetalle").find(':checkbox').each(function(){
						if ( $(this).is(':checked')){
							if(  carro.indexOf( $(this).attr('id') + ";" ) == -1  ){
								carro = carro + $(this).attr('id') + ";";
							}
						}
					});
				}
			}
		}
 		if (cantDocPorVencer != cantDocPorVencerSeleccionados){
 			$("#"+"T"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "inline-block"});
			$("#T"+cuentaPrincipal).addClass('neutro');
 		}
 		if(cantDocVencidos != cantDocVencidosSeleccionados){
 			$("#"+"V"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "inline-block"});
			$("#V"+cuentaPrincipal).addClass('neutro');
 		}
 		if(cantDocPorVencerSeleccionados < cantDocPorVencer){
			$("#T"+cuentaPrincipal+"no-estas-pagando").css({ 'display': "inline-block"});
			$("#T"+cuentaPrincipal).addClass('neutro');
			$("#T"+cuentaPrincipal).prop('checked' , false);
			$("#no-estas-pagando-total-deuda-principal").css({ 'display': "inline-block"});
			$("#no-estas-pagando-total-deuda-principal").css({ 'visibility': "visible"});
		}
		else if(cantDocPorVencerSeleccionados == cantDocPorVencer){
			console.log("Paga todos los por vencer");
			$("#"+"T"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
			$("#T"+cuentaPrincipal).removeClass('neutro');
			$("#T"+cuentaPrincipal).prop('checked' , true);
		}
		if(cantDocPorVencerSeleccionados == 0){ //Aqui le saqu� el else
			console.log("No paga ninguno por vencer");
			$("#"+"T"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
			$("#T"+cuentaPrincipal).removeClass('neutro');
			$("#T"+cuentaPrincipal).prop('checked' , false);
		}
		if(cantDocVencidosSeleccionados == cantDocVencidos){
			console.log("Paga todos los vencidos");
			$("#"+"V"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "none"});
			$("#V"+cuentaPrincipal).removeClass('neutro');
			$("#V"+cuentaPrincipal).prop('checked', true);
		}
		else{
			console.log("No paga todos los vencidos");
			if (cantDocVencidosSeleccionados == 0){
				console.log("No paga ningun vencido");
				$("#V"+cuentaPrincipal+"no-estas-pagando").css({ 'display': "none"});
				$("#V"+cuentaPrincipal).removeClass('neutro');
				$("#V"+cuentaPrincipal).prop('checked', false);
			}
			else{
				console.log("Paga algunos vencidos");
				$("#V"+cuentaPrincipal+"no-estas-pagando").css({ 'display': "inline-block"});
				$("#V"+cuentaPrincipal).addClass('neutro');
				$("#V"+cuentaPrincipal).prop('checked', false);
			}
		}
 		if(! pagaTodos){
 			// Mostrar no estas pagando
 	 		$("#no-estas-pagando-total-deuda-principal").css({ 'display': "inline-block" });
 	 		$("#no-estas-pagando-total-deuda-principal").css({ 'visibility': "visible"});
 	 		$("#pagas-otros-montos").hide();
 		}
 		else{
 			$("#no-estas-pagando-total-deuda-principal").css({ 'display': "none" });
 			$("#pagas-otros-montos").show();
 		}
		$("#carroPagoPrivado").val(carro);
		console.log("El carro antes de salir : " + $("#carroPagoPrivado").val());
		calculaTotales()
 	}

 	function recibeOtraCuenta (otraCuentaObj){
 		
 		console.log("Recibo Otra Cuenta " + $(otraCuentaObj).val());
 		var carro = $("#carroPagoPrivado").val();
 		var cuentaPrincipal = $("#cuentaPrincipal").val();
 		var idCheck = $ (otraCuentaObj).attr('id');
 		var seleccionado = $ (otraCuentaObj).is(':checked');
 		if (seleccionado){
 			console.log("Agrego cuenta");
 			// Eliminar documentos y servicios del carro 
 			$("#otras-cuentas-"+idCheck).next().find(':checkbox').each(function(){
 				$("#contServicios"+ $(this).attr('id')).find(':checkbox').each(function(){		
 					if ($(this).hasClass('regulado')){
 						$(this).prop('checked', true);
 						carro = carro.replace (  $(this).attr('id') +";"   , "" );
 					}
 				});
 				// Marcar documentos y servicios
 				$(this).prop('checked', true);
 				//Eliminar no estas pagando 
 				$(this).parent().parent().next().hide();
 				//Eliminar neutro
 				$(this).removeClass('neutro');
 				//Eliminar clase amarilla
 				$(this).parent().parent().parent().removeClass('amarilla');
				carro = carro.replace (  $(this).attr('id') +";"   , "" );
 			});
 			// Agregar cuenta al carro
 			carro = carro + idCheck + ";";
 		}
 		else{
 			console.log("Elimino cuenta");
 			// Eliminar documentos y servicios del carro
 			// Desmarcar documentos y servicios
 			// Eliminar cuenta del carro
 			$("#otras-cuentas-"+idCheck).next().find(':checkbox').each(function(){
 				$("#contServicios"+ $(this).attr('id')).find(':checkbox').each(function(){		
 					if ($(this).hasClass('regulado')){
 						$(this).prop('checked', false);
 						carro = carro.replace (  $(this).attr('id') +";"   , "" );
 					}
 				});
 				// Marcar documentos y servicios
 				$(this).prop('checked', false);
 				//Eliminar no estas pagando 
 				$(this).parent().parent().next().hide();
 				//Eliminar neutro
 				$(this).removeClass('neutro');
 				//Eliminar clase amarilla
 				$(this).parent().parent().parent().removeClass('amarilla');
				carro = carro.replace (  $(this).attr('id') +";"   , "" );
 			});
 			carro = carro.replace (  idCheck +";"   , "" );
 		}
 		$("#carroPagoPrivado").val(carro);
		console.log("El carro antes de salir : " + $("#carroPagoPrivado").val());
		calculaTotales()
 	}
 
 
 	function recibeDocumentoOtraCuenta(otroDocumentoObj){
 		
 		console.log("recibe Documento Otra Cuenta " + $(otroDocumentoObj).val());
 		var carro = $("#carroPagoPrivado").val();
 		var idCheck = $ (otroDocumentoObj).attr('id');
 		var seleccionado = $ (otroDocumentoObj).is(':checked');
 		var cuentaPadre = idCheck.substring(idCheck.indexOf('C') , idCheck.length);
 		var pagaTodos = true;
 		var cantDocMarcados = 0;
 		console.log("La cuenta padre es : " + cuentaPadre);
 		$("#otras-cuentas-"+cuentaPadre).next().find(':checkbox').each(function(){
 			if (! $(this).is(':checked')){
 				pagaTodos = false;
 			}
 			else{
 				cantDocMarcados ++;
 			}
 		});
 		// Eliminar Servicios del carro
 		$("#contServicios"+idCheck).find(':checkbox').each(function(){
 			if(  $(this).hasClass('regulado')  ){
 				carro = carro.replace (  $(this).attr('id') +";"   , "" );
 				if(seleccionado){
 					$(this).prop('checked', true);
 				}
 				else{
 					$(this).prop('checked', false);
 				}
 			}
 		});
 		// Quitar neutro y amarilla
 		$("#"+idCheck).removeClass('neutro');
 		$("#"+idCheck).parent().parent().parent().removeClass('amarilla');
 		// Eliminar no estas pagando
 		$("#"+idCheck).parent().parent().next().hide();
 		
 		
 		if(seleccionado){
 			console.log("agregando documento");
 			if(pagaTodos){
 				console.log("Paga todos los documentos de la cuenta");
 				$("#"+cuentaPadre).removeClass('neutro');
 				$("#"+cuentaPadre).prop('checked', true);
 				//Eliminar todos los documentos del carro
 				$("#otras-cuentas-"+cuentaPadre).next().find(':checkbox').each(function(){
 					carro = carro.replace (  $(this).attr('id') +";"   , "" );
 				});
 				//Agregar cuenta al carro
 				carro = carro + cuentaPadre + ";";
 			}
 			else{
 				console.log("Paga algunos documentos de la cuenta");
 				$("#"+cuentaPadre).addClass('neutro');
 				$("#"+cuentaPadre).prop('checked', false);
 				// Eliminar todos los documentos del carro
 				$("#otras-cuentas-"+cuentaPadre).next().find(':checkbox').each(function(){
 					carro = carro.replace (  $(this).attr('id') +";"   , "" );
 				});
 				// Eliminar cuenta del carro
 				carro = carro.replace(cuentaPadre + ";" , "");
 				// Agregar todos los documentos seleccionados 
 				$("#otras-cuentas-"+cuentaPadre).next().find(':checkbox').each(function(){
 					if(  $(this).is(':checked') ){
 						carro = carro + $(this).attr('id') + ";" ;
 					}
 				});
 			}
 		}
 		else{
 			console.log("Eliminando documento");
 			if(cantDocMarcados == 0){
 				console.log("No paga ningun documentos de la cuenta")
 				$("#"+cuentaPadre).removeClass('neutro');
 				$("#"+cuentaPadre).prop('checked', false);
 				//Eliminar todos los documentos de la cuenta
 				$("#otras-cuentas-"+cuentaPadre).next().find(':checkbox').each(function(){
 					carro = carro.replace (  $(this).attr('id') +";"   , "" );
 				});
 				//Eliminar cuenta
 				carro = carro.replace(cuentaPadre + ";" , "");
 			}
 			else{
 				console.log("Paga algunos documentos de la cuenta");
 				$("#"+cuentaPadre).addClass('neutro');
 				$("#"+cuentaPadre).prop('checked', false);
 				// Eliminar todos los documentos del carro
 				$("#otras-cuentas-"+cuentaPadre).next().find(':checkbox').each(function(){
 					carro = carro.replace (  $(this).attr('id') +";"   , "" );
 				});
 				// Eliminar cuenta del carro
 				carro = carro.replace(cuentaPadre + ";" , "");
 				// Agregar todos los documentos seleccionados
 				$("#otras-cuentas-"+cuentaPadre).next().find(':checkbox').each(function(){
 					if(  $(this).is(':checked') ){
 						carro = carro + $(this).attr('id') + ";" ;
 					}
 				});
 			}
 		}
 		$("#carroPagoPrivado").val(carro);
		console.log("El carro antes de salir : " + $("#carroPagoPrivado").val());
		calculaTotales();
 	}

 	
 	function recibeServicio(checkServicio) {
 		
 		console.log("Recibo servicio " + $(checkServicio).val());
 		var cantidadSeleccionada = 0;
		var cantidadDesmarcada = 0;
		var cantidadTotal = 0;
		var documentoPadre = '';
		var cuentaPadre = '';
		var idServicioElegido = $(checkServicio).attr('id');
		$(checkServicio).parent().parent().parent().parent().parent()
						.parent().parent().parent().parent().parent()
						.find('input[type=hidden]').each(function(){
			
			if($(this).attr('id') == 'documentoPadre'){
				documentoPadre = $(this).val();
			}
			else if( $(this).attr('id') == 'cuentaPadre' ){
				cuentaPadre = $(this).val();
			}
		});
		console.log("La cuenta padre es : " + cuentaPadre + " - El documento padre es : " + documentoPadre)
		var servTemporalQuitar  = "";
		var sumaServicios = 0;
		$(checkServicio).parent().parent().find('input[type=checkbox]').each(function () {
			cantidadTotal ++ ;
	        if ($(this).is(':checked')) {
        		cantidadSeleccionada ++;
        		console.log($(this).val());
        		sumaServicios = sumaServicios + parseInt ($(this).val());
        		if($(this).hasClass('regulado')){
        			servTemporalQuitar = servTemporalQuitar +  $(this).attr('id')+ ";";
        		}
	        }
	        else{
	        	cantidadDesmarcada ++;
	        }
     	});
		
		sumaServicios = sumaServicios.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
		sumaServicios = sumaServicios.split('').reverse().join('').replace(/^[\.]/,'');
		
		$(checkServicio).parent().parent().find(".txt-pagar").html("$"+sumaServicios);
		//$(this).parent().parent().parent().find(".txt-pagar").css( "background-color", "red" );
		console.log($(checkServicio).parent().parent().find(".txt-pagar") );
		//$("#txtpagar").html("$"+sumaServicios);
		
		if(cantidadSeleccionada == 0){
			console.log("Todos los servicios desmarcados");
			$(checkServicio).parent().parent().find('button').prop('disabled', true);
		}
		else if(cantidadTotal > cantidadSeleccionada){
			console.log("Existen servicios desmarcados");
			$(checkServicio).parent().parent().find('button').removeAttr("disabled");
		}
		else{
			console.log("Todos los servicios est�n marcados");
			servTemporalQuitar = "TD"+documentoPadre+"C"+cuentaPadre;
			//$(checkServicio).parent().parent().find('button').prop('disabled', true);
		}
		$("#temporalServiciosAQuitar").val (servTemporalQuitar);
		console.log ("Servicios a eliminar : " + $("#temporalServiciosAQuitar").val ());
		
 		console.log("cantidadSeleccionada "+cantidadSeleccionada);
 		console.log("cantidadDesmarcada "+cantidadDesmarcada);
 		console.log("cantidadTotal "+cantidadTotal);
 		console.log("documentoPadre "+documentoPadre);
 		console.log("cuentaPadre "+cuentaPadre);
 	}
 	
 	function modificaServicios() {
 		
 		console.log("Agregando servicios : " + $("#temporalServiciosAQuitar").val());
 		$("#modal_desmarcar_servicio").hide();
 		//Ocultar todos los modales de servicio, 
 		$("[id*=modal_detalle_servicios_D]").hide();
 		$("#modal_desmarcar_servicio").find('input[type=checkbox]').each(function () { 
 			$(this).prop('checked', false);
 			// lineas no sirven...
 			//$(this).parent().parent().parent().find('p').hide();
 			//console.log($( this ).parent().get( 0 ).tagName);
     	});
 		
 		$("#confirma-servicios-a-quitar").prop('disabled', true);
 		// Eliminar del carro los servicios 
 		
 		var sonServiciosDeCuentaPrincipal = false;
 		var documentoPadre = "";
 		var cuentaPadre = "";
 	
 		var documentos = $("#temporalServiciosAQuitar").val().split(";");
 		var serviciosFinales = [];
 		var pagaTodo = ($("#temporalServiciosAQuitar").val().indexOf("TD") > -1  )? true :false;
 		
 		if(pagaTodo){
 			
 			//Eliminar todos los servicios del carro
 			var temp = $("#temporalServiciosAQuitar").val();
 			var idDocumento = temp.substring(  2 , temp.indexOf("C"));
 			var idCuenta =  temp.substring(  temp.indexOf("C")+1 , temp.length)  ;
 			console.log ("Paga todos los servicios del documento " + idDocumento + " : "+ idCuenta);
 			var checkPadre = "D"+idDocumento + "C"+idCuenta;
 			
 			//Marcar el documento completo (click? )
 			
 			//$("#"+ checkPadre).prop("checked", false);
 			//$("#"+ checkPadre).click();
 			//Quitar clase amarilla al documento
 			$("#"+ checkPadre).parent().parent() .parent().removeClass('amarilla');
 			$("#"+ checkPadre).removeClass('neutro');

 			//Si es vencido, poner clase roja
 			
 			if($("#"+ checkPadre).parent().parent() .parent().hasClass('boletaVencida')){
 				$("#"+ checkPadre).parent().parent() .parent().addClass('roja');
 			}
 			// Quitar mensaje no estas pagando todo
 			
 			$("#"+ checkPadre).parent().parent() .next().hide();
 			$("#"+ checkPadre).prop ('checked', true);

 			//reiniciar el input "temporalServiciosAQuitar" a vacio
 	 		$("#temporalServiciosAQuitar").val ("");
 		}
 		else{
 			console.log("paso");
 			var montoServicios = 0;
 	 		for (var i = 0 ; i < documentos.length -1 ; i++){
 	 			
 	 			var servicioCompleto = documentos[i];
 	 			console.log("El servicio completo es : " + servicioCompleto);
 	 			var idServ = servicioCompleto.substring(1, servicioCompleto.indexOf("D")   );;
 	 			var idDocumento = servicioCompleto.substring(  servicioCompleto.indexOf("D")+1 , servicioCompleto.indexOf("C")  )    ;
 	 			var idCuenta = servicioCompleto.substring (   servicioCompleto.indexOf("C") +1,  servicioCompleto.length )   ;
 	 			
 	 			console.log("El servicio " + idServ + " pertenece al documento "+ idDocumento + " que pertenece a la cuenta " + idCuenta);
 	 			
 	 			documentoPadre = idDocumento;
 	 			cuentaPadre = idCuenta
 	 			serviciosFinales.push(idServ);
 	 			//Suma de servicios
 	 			montoServicios += parseInt($('#'+servicioCompleto).val());
 	 			
 	 			console.log("montoservicios dentro for"+montoServicios);
 	 		}
 	 		console.log("montoservicios"+montoServicios);
 	 		
 	 		$("#D28637517C500006846").parent().parent().parent().find('em').html('<i>$</i>'+formatoMiles.format(montoServicios));
 	 		
 	 		//agregar div dentro del contenedor del documento
 	 		// agregar clase amarilla a la boleta
 	 		$("#D"+documentoPadre+"C"+cuentaPadre).addClass('neutro');
 	 		$("#D"+documentoPadre+"C"+cuentaPadre).parent().parent().parent().removeClass('roja');
 	 		$("#D"+documentoPadre+"C"+cuentaPadre).parent().parent().parent().addClass('amarilla'); 
 	 		$("#D"+documentoPadre+"C"+cuentaPadre).parent().parent().next().show();
 	 		$("#D"+documentoPadre+"C"+cuentaPadre).prop("checked" , false);
 	 		
 	 		$("#no-estas-pagando-total-deuda-principal").css({ 'display': "inline-block" });
 	 		$("#no-estas-pagando-total-deuda-principal").css({ 'visibility': "visible"});
 	 		$("#pagas-otros-montos").hide();
 	 		var esDeDocumentoVencido = ($("#D"+documentoPadre+"C"+cuentaPadre).parent().parent().parent().hasClass('boletaVencida')) ? true :false;
 	 		
 	 		console.log("Es servicios de documento vencido de la cuenta principal ? " + esDeDocumentoVencido);
 	 		
 	 		if(esDeDocumentoVencido){
 				$("#"+"V"+$("#cuentaPrincipal").val()).prop('checked' , false);
 				$("#"+"V"+$("#cuentaPrincipal").val()).addClass('neutro');
 				$("#"+"V"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "inline-block"});
 	 		}
 	 		else{
 				$("#"+"T"+$("#cuentaPrincipal").val()).prop('checked' , false);
 				$("#"+"T"+$("#cuentaPrincipal").val()).addClass('neutro');
 				$("#"+"T"+$("#cuentaPrincipal").val()+"no-estas-pagando").css({ 'display': "inline-block"});
 	 		}
 	 		
 	 		// Eliminar del carro el documento
 	 		
 	 		var esServicioDeCuentaPrincipal = (cuentaPadre == $("#cuentaPrincipal").val())? true : false;
 	 		
 	 		console.log("Carro al iniciar : " + $("#carroPagoPrivado").val());
 	 		
 	 		if (esServicioDeCuentaPrincipal){
 	 			
 	 			console.log("Agregando servicios de la cuenta principal");
 	 			
 	 			// Comprobar si paga la cuenta completa ( T )
 	 			// Si si : agregar todos los documentos de la cuenta principal que no pertenecen al servicio
 	 			
 	 			var carroAux = $("#carroPagoPrivado").val();
 	 			
 	 			if(carroAux.indexOf("T") > -1){
 	 				console.log ("Pagaba el total de la deuda principal");
 	 				var cantidadElementosArr = carroAux.split(";");
 	 				//Eliminar T 
 	 				carroAux = carroAux.replace ("T"+cuentaPadre+";" , "");
 	 				// agregar documentos que no son del servicio
 	 				$("#contenedorDetalle").find('input[type=checkbox]').each(function () {
 	 					console.log ( "Comparando " + "D"+documentoPadre+"C"+cuentaPadre + " con " +  $(this).prop('id'));
 	 					if($(this).prop('id')  != "D"+documentoPadre+"C"+cuentaPadre){
 	 						console.log ("Agregando documentos que no son del servicio");
 	 						carroAux = carroAux + $(this).prop('id')+";";
 	 					}
 	 		     	});
 	 				// Agregar servicios
 	 				console.log ("El largo de los servicios finales es : " + serviciosFinales.length);
 	 				for ( var i = 0 ; i < serviciosFinales.length ; i++){
 	 					console.log ("Agregando los servicios seleccionados al carro ");
 	 					var serv = serviciosFinales[i];
 	 					carroAux = carroAux + "S"+serv+"D"+documentoPadre+"C"+cuentaPadre+";";
 	 				}
 	 				$("#carroPagoPrivado").val(carroAux);
 	 			}
 	 			else if ( carroAux.indexOf("V") > -1 ){
 	 				console.log ("Pagaba la deuda vencida de la cuenta principal");
 	 				//Eliminar V
 	 				carroAux = carroAux.replace ("V"+cuentaPadre+";" , "");
 	 				// Agregar los documentos que no son del servicio
 	 				$("#contenedorDetalle").find('input[type=checkbox]').each(function () {
 	 					console.log ( "Comparando " + "D"+documentoPadre+"C"+cuentaPadre + " con " +  $(this).prop('id'));
 	 					// No agregar el documento de los servicios
 	 					if($(this).prop('id')  != "D"+documentoPadre+"C"+cuentaPadre){
 	 						console.log ("Agregando documentos que no son del servicio y que estan seleccionados");
 	 						if (  $(this).prop('checked')   ){
 	 							carroAux = carroAux + $(this).prop('id')+";";
 	 						}
 	 					}
 	 		     	});
 	 				// Agregar servicios
 	 				for ( var i = 0 ; i < serviciosFinales.length ; i++){
 	 					var serv = serviciosFinales[i];
 	 					carroAux = carroAux + "S"+serv+"D"+documentoPadre+"C"+cuentaPadre+";";
 	 				}
 	 				$("#carroPagoPrivado").val(carroAux);
 	 			}
 	 			else{
 	 				console.log ("Pagaba otros documentos de la cuenta principal");
 	 				// Eliminar documentos del servicio
 	 				carroAux = carroAux.replace ("D"+documentoPadre+"C"+cuentaPadre+";", "");
 	 				// agregar servicios
 	 				for ( var i = 0 ; i < serviciosFinales.length ; i++){
 	 					console.log ("Agregando los servicios seleccionados al carro ");
 	 					var serv = serviciosFinales[i];
 	 					carroAux = carroAux + "S"+serv+"D"+documentoPadre+"C"+cuentaPadre+";";
 	 				}
 	 				$("#carroPagoPrivado").val(carroAux);
 	 			}		
 	 		}
 	 		else{
 	 			console.log("Agregando servicios de otra cuenta");
 	 			var carroAux = $("#carroPagoPrivado").val();
 	 			console.log("El documento padre es : " + documentoPadre + " que pertenece a la cuenta : " + cuentaPadre);
 	 			
 	 			//Eliminar la cuenta, eliminar el documento
 	 			var indiceCuenta = carroAux.indexOf("C"+cuentaPadre+";");
 	 			console.log("Indice cuenta " + indiceCuenta + " : " + carroAux.substring(indiceCuenta -1 , indiceCuenta));
 	 			if(  carroAux.substring(indiceCuenta -1, indiceCuenta) == ";" || indiceCuenta == 0 ){
 	 				carroAux = carroAux.replace("C"+cuentaPadre+";" ,  ""  );
 	 			}
 	 			console.log("Carro temp : " + carroAux);
 	 			var indiceDocumento = carroAux.indexOf("D"+documentoPadre+"C"+cuentaPadre+";");
 	 			console.log("Indice documento " + indiceDocumento + " : " + carroAux.substring(indiceDocumento -1, indiceDocumento));
 	 			if(  carroAux.substring(indiceDocumento -1, indiceDocumento) == ";" || indiceDocumento == 0){
 	 				carroAux = carroAux.replace("D"+documentoPadre+"C"+cuentaPadre+";" ,  ""  );
 	 			}
 	 			console.log("Carro temp : " + carroAux);
 	 			//Agregar todos los documentos de la cuenta que est�n seleccionados
 	 			$("#otras-cuentas-C"+cuentaPadre).next().find(':checkbox').each(function(){
 	 				if(  $(this).is(':checked')){
 	 					if( ! carroAux.indexOf(  $(this).attr('id')+";" ) > -1  ){
 	 						carroAux = carroAux + $(this).attr('id')+";" ;
 	 					}
 	 				}
 	 				console.log("Carro temp : " + carroAux);
 	 			});
 	 			for ( var i = 0 ; i < serviciosFinales.length ; i++){
 					var serv = serviciosFinales[i];
 					carroAux = carroAux + "S"+serv+"D"+documentoPadre+"C"+cuentaPadre+";";
 				}
 	 			$("#carroPagoPrivado").val(carroAux);
 	 		}
 		}
 		//reiniciar el input "temporalServiciosAQuitar" a vacio
 		$("#temporalServiciosAQuitar").val ("");
 		
 		console.log ( "El carro al terminar de agregar servicios : " + $("#carroPagoPrivado").val());
 		
 		calculaTotales();
 	}
 	 	
 	function calculaTotales(){
 		
 		var carro = $("#carroPagoPrivado").val();
 		var cuentaPrincipal = $("#cuentaPrincipal").val();
 		console.log(cuentaPrincipal);
 		
 		var pagaOtrasCuentas = true;
 		
 		var montoTotalCuentaPrincipal = 0;
 		var montoTotalOtrasCuentas = 0;
 		var montoTotal = 0;
 		
 		var elementos = carro.split(';');
 		
 		for (  var i  = 0 ; i < elementos.length -1 ; i++  ){
 			
 			var vaina = elementos [i];
 			var monto = $("#"+vaina).val();
 			if (!monto) {
 				monto = 0;
 			}
 			
 			if (  vaina.indexOf(cuentaPrincipal) >-1   ){
 				console.log ("El elemento " + vaina + " pertenece a la cuenta principal y tiene valor de " + monto );
 				
 				if(vaina.indexOf('T') > -1){
 					
 					if( $("#V"+cuentaPrincipal).length > 0  ){
 						montoTotalCuentaPrincipal += ( (parseInt(monto)) +  (  parseInt(  $("#V"+cuentaPrincipal).val()))) ;
 					}
 					else{
 						montoTotalCuentaPrincipal += (parseInt(monto));
 					}
 				}
 				else{
 					montoTotalCuentaPrincipal += (parseInt(monto));
 				}
 			}
 			else{
 				console.log ("El elemento " + vaina + " pertenece a otra cuenta y tiene valor de " + monto );
 				montoTotalOtrasCuentas += (parseInt(monto));
 			}
 		}
 		
 		console.log("El monto total a pagar de la cuenta principal es : " + montoTotalCuentaPrincipal);
 		console.log("El monto total a pagar de otras cuentas es : " + montoTotalOtrasCuentas);
 		
 		montoTotal = montoTotalCuentaPrincipal + montoTotalOtrasCuentas;
 		
 		if( montoTotalOtrasCuentas > 0  ){
 			$("#montosOtrasCuentas").show();
 			$("#pagas-otros-montos").show();
 	 		$("#montoTotalOtrasCuentas").html(montoTotalOtrasCuentas);
 		}
 		else{
 			$("#montosOtrasCuentas").hide();
 			$("#pagas-otros-montos").css({ 'visibility': "hidden"});
 			
 	 		$("#montoTotalOtrasCuentas").html(montoTotalOtrasCuentas);
 		}
 		
 		$("#total-a-pagar") .html(formatoMiles.format (montoTotal));
 		$("#total-a-pagar2").html(formatoMiles.format (montoTotal));
 		$("#total-a-pagar3").html(formatoMiles.format (montoTotal));
 	
		verificarNoPagar(montoTotal);
 	}
 	
 	function verificarNoPagar(montoTotal){
 		if(montoTotal==0){
 	 		console.log("El monto total es 0" );
 			$("#medio-pago-detalle").prop('disabled', true);
 			$("#medio-pago").prop('disabled', true);
 			
 		}else{
 			$("#medio-pago-detalle").prop('disabled', false);
 			$("#medio-pago").prop('disabled', false);
 		}
 	}

	function cierraModal(){
		
		$("#modal_detalle_servicios").hide();
		$("#modal_no_dejar_de_pagar_vencida").hide()
		$("#modal_pago_diferenciado").hide();
		$("#modal_desmarcar_servicio").hide();
	}
 
 	function muestraModalDesmarcarServicio(){

 		console.log("Entrando a mostrar segundo modal");
 		$("#modal_detalle_servicios").hide();
 		$("#modal_desmarcar_servicio").show();
 	}
 
 	function cierraModalConfirmacionPPS(){
 		
 		$("#modal_desmarcar_servicio").hide();
 		$("#modal_desmarcar_servicio").find("input:checkbox").attr('checked' , false);
 		$("#confirma-servicios-a-quitar").prop('disabled', true);
 	}
 	
 	//totar el evento de cancelar en el modal de pps y poner check de confirmacion en checked false 
 	
 	function abonar(idCuenta){
 		console.log("Abonando a la cuenta : " + idCuenta);
 		$("#<portlet:namespace/>cuentaAbono").val(idCuenta);
 		$("#modal_abonar").show();
 	}
 	function cerrarAbonar() {
 		$("#modal_abonar").hide();
 	}
 	function solicitarProrroga(idCuenta) {
 		
 		$("#<portlet:namespace/>cuentaProrroga").val(idCuenta)
 		$("#modal_solicitar_prorroga").show();
 	}
 	function cancelarProrroga() {
 		$("#modal_solicitar_prorroga").hide();
 	}
 	
 	function realizarAbono(){
 		
		var monto = $("#txtMontoAbono").val();
		$("#<portlet:namespace/>montoAbono").val( monto );
		var cuenta = $("#abonar").val();
		$("#<portlet:namespace/>cuentaAbono").val( cuenta );
 		console.log("El monto a abonar es : " + monto + "a la cuenta"+ cuenta );
 		$("#formAbono").submit();
 	}
 	
	$("input[name=check-boleta-vencida]").change(function (){
		
	 });
	
	function noDejarDePagar(cuentaObj){
		console.log("Mostrando no dejar de pagar");
		$("#modal_no_dejar_de_pagar_vencida").show();
		$(cuentaObj).prop('checked',true);
	}
 	
 	function asignaEstadoInicial(){
 		
 	$('.bt-slide-panel').click(function() {
 	    $('html,body').animate({'scrollTop' : 0},1);
 	    $("html").addClass("opened-slide-panel");
 	    $("footer").hide();
 	    $(".caja-blanca").hide();
 	    var operacion = 'getDocumentos';
 	    var faSeleccionada = '<%=faPorDefecto%>';
 	    var docSeleccionado = "";
 	    var cuerpo = document.getElementById("contenedorDetalle").innerHTML;
 	    if(cuerpo.length < 2){
 	    	jQuery.ajax({
 	 	    	type: "POST",
 	 	    	url: "<%= renderResponse.encodeURL(muestraDetalleCuentasServicios.toString())%>",
 	 	    	cache: false,
 	 	    	dataType: "html",
 	 	    	data: {
 	 	            '<portlet:namespace/>operacion': operacion, 
 	 	            '<portlet:namespace/>faSeleccionada': faSeleccionada,
 	 	            '<portlet:namespace/>docSeleccionado': docSeleccionado
 	           	},
 	 	    	success: function(resp){
 	 	    		document.getElementById("contenedorDetalle").innerHTML = resp;
 	 	    		if (  ! $("#T"+  $("#cuentaPrincipal").val()  ) .prop('checked')   ){
 	 	    			$("#contenedorDetalle").find (':checkbox').each(function(){
 	 	    				if ($(this).parent().parent().parent().hasClass('boletaVencida')){
 	 	    					//
 	 						}
 	 	    				else{
 	 	    					$(this).attr('checked' , false); 
 	 	    				}
 	 	    			});
 	 	    		}
 	 	    		
 	 	    		$("#contenedorDetalle").find('div').each(function(){
 	 	    			if ( $(this).hasClass("marcadorContServ")){
 	 	    				$("#contenedorServiciosCuentaPrincipal").append($(this).clone());
 	 	    				$(this).remove();
 	 	    			}
 	    			});
 	 	    	}
 	     	});
 	    }
 	    $("#pie-pagar-boletas").hide();
 	    $("#pie-pagar-boletas").show();
 	    console.log("height 2"+document.body.offsetHeight);
 	    setTimeout(sendMessage, 8000);
 	    
 	});
 	  
 	$('.bt-close-panel').click(function() {
 	    $("footer").show();
 	    $(".caja-blanca").show();
 	    $("html").removeClass("opened-slide-panel");
 	    $("#pie-pagar-boletas").show();
 	    $("#pie-pagar-boletas").hide();
 	});
 	
 	$("#ver-todas-las-cuentas").click(function() {
 		var datos = "";
 		$(".faseleccionada").each(function( index ) {
 			  console.log( index + ": " + $( this ).val() );
 			  datos += $( this ).val()+";"
 		});
	 	jQuery.ajax({
	     	type: "POST",
	     	url: "<%=verificaProrroga%>",
	     	cache: false,
	     	dataType: "html",
	     	data: {
	             '<portlet:namespace/>datos': datos,
	             '<portlet:namespace/>operacion': "verificaProrroga"
	       	},
	     	success: function(resp){
	     		var rpta = JSON.parse(resp);
	     		for (var i=0;i<rpta.length; i++) {
	     			if (rpta[i].prorroga==="true" ) {
	     				$("#prorroga-"+rpta[i].cuenta).show();
	     			}
	     		}
	     		$(".ver-todas-las-cuentas").hide();
	     		$("#cont-ver-todas-las-cuentas").show();
	     		console.log("height 3"+document.body.offsetHeight);
	     		setTimeout(sendMessage, 8000);
	     	}
	 		});
 	});
 	
 	$(".bt-close-modal-mo").click(function(){
 		$("#modal_detalle_servicios").hide();
 	});
 	
 	$("#modal_desmarcar_servicio").find('input:checkbox').click( function(){
 		if ( $(this).prop('checked') ){
 			$("#modal_desmarcar_servicio").find('button').removeAttr("disabled");
 		}
 		else{
 			$("#modal_desmarcar_servicio").find('button').prop('disabled', true);
 		}
 	});
 	
 	$(".otras-cuentas .flecha").click(function () {
 		var idCuenta = $(this).parent().find('.faseleccionada').val();
 		var destino = $(this).parent().find('.destinoresultado').val();
         $(this).parents('.otras-cuentas').next().slideToggle("slow", function () {
 			if($(this).is(':visible')) {
 				$(this).parents('.otras-cuentas').find('.flecha img').attr('src','/mo-theme/images/img-flecha-arriba-otras-cuentas.png');
 				callObtenerDocumentosOtrasCuentas(idCuenta, destino);
 			}
 			else {
 				$(this).parents('.otras-cuentas').find('.flecha img').attr('src','/mo-theme/images/img-flecha-abajo-otras-cuentas.png');
 			}
 		});
     });
 	
 	$("#carroPagoPrivado").val( $("#carroPorDefectoPrivado").val());
 	$("#medio-pago , #medio-pago-detalle").click(function (){
 		$("#<portlet:namespace/>carroPagoPrivadoFinal").val($("#carroPagoPrivado").val());
 		$("#formDeudaPrivado").submit();
 	});
 	
 	
 	$("#acetpo-terminos").change(function(){
 		if($(this).is(":checked")) {
	      $("#confirma-servicios-a-quitar").prop('disabled', false);
	    }
 		else{
 			$("#confirma-servicios-a-quitar").prop('disabled', true);
 		}
 	}); 	
 	

 	
 }
 	$("#acetpo-terminos-p").change(function(){
 		if($(this).is(":checked")) {
 		      $("#enviar-solicitud-prorroga").prop('disabled', false);
 		    }
 	 		else{
 	 			$("#enviar-solicitud-prorroga").prop('disabled', true);
 	 		}
 	 	});
 
 </script>

