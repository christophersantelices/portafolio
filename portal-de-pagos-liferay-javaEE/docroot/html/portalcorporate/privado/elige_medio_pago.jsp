<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>


<portlet:defineObjects />

<portlet:actionURL var="iniciaPagoPrivadoActionURL" windowState="normal" name="iniciaPago_privado">
</portlet:actionURL> 

<portlet:actionURL var="volverAPaso1PrivadoActionURL" windowState="normal" name="volver_Paso1_Privado"> 
</portlet:actionURL> 

<%
	String montoTotal = (String) renderRequest.getAttribute("montoTotal");
	String fechaActual = (String) renderRequest.getAttribute("fechaActual");
%>

<div class="titulo-pagina normado titulo-post-selects">
    <h1><%=UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate")%></h1>
</div>

<main class="container medio-pago-orden width-100 padding-00-i text-center">

    <form class="clearfix inline-block width-100" id="formEligeMedio" name="formEligeMedio" action="<%=iniciaPagoPrivadoActionURL%>" method="POST">


		<input type="hidden" id="<portlet:namespace/>pay_method" name="<portlet:namespace/>pay_method" value=""/>
        <div class="steps-new pasos5 col-md-12">
                <ul class="desktop">
                    <li class="">
                        <a href="" class="relativo">
                            <i class="iconp-documentos"></i>
                            <span class="margin-left-20">Resumen de cuentas</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="" class="relativo">
                            <i class="iconp-tarjeta"></i>
                            <span>Medios de pago</span>
                        </a>
                    </li>
                    <li class=""><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
                </ul>
            </div>

            <div class="menu-opciones-mobile col-md-12">
                <ul>
                    <li class=""><a href="" class="relativo"><i class="iconp-documentos"></i> <span>Resumen de cuentas</span></a></li>
		<li class="active"><a href="" class="relativo"><i class="iconp-documento"></i> <span>Medios de pago</span></a></li>
		
                </ul>
            </div>

        <!--/ .steps-new -->

        <div class="medio-pago bg-white inline-block width-100">
            <h5>Presiona sobre un medio de pago</h5>

			<%=UtilesContenidos.obtenerContenidoTexto(request, "mp_pago_privado_corporate")%>

            <div class="cont-link-boton">
                <div class="volver hidden-xs">
                    <a href="javascript:history.back()" class="link-simple-arrow-left">
                        Volver al resumen de cuenta
                    </a>
                </div>
                <div class="pagar bg-white inline-block pull-right">
                    <button id="medio-pago" class="btn-verde">Pagar $<%=montoTotal%><span></span>
                        <i class="arrow-left-white"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="menu-opciones-mobile medio-de-pago col-md-12 margin-top-20">
            <ul>
                <li class=""><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
            </ul>
        </div>
    </form>
    <!--/ .white-wrap -->
</main>


<script>

$(document).ready(function(){ 
	
	
	$("#s-webpayws,#s-webpay,#s-servipag,#s-bci,#s-bancoestado, #ticket-pago-presencial").click(function () {
        $(this).addClass('active').find('input').prop('checked', true);
        $("#s-webpay,#s-servipag,#s-bci,#s-bancoestado, #ticket-pago-presencial").not(this).removeClass("active");

        if ($(this).attr("id") == "s-webpay") {
            $("#medio-pago span").text(" con Webpay");
            $("#<portlet:namespace/>pay_method").val("Webpay");
            
        } 
        else if ($(this).attr("id") == "s-servipag") {
            $("#medio-pago span").text(" con Servipag");
            $("#<portlet:namespace/>pay_method").val("Servipag");
        } 
        else if ($(this).attr("id") == "s-bci") {
            $("#medio-pago span").text(" con BCI");
            $("#<portlet:namespace/>pay_method").val("BCI");
        } 
        else if ($(this).attr("id") == "s-bancoestado") {
            $("#medio-pago span").text(" con Banco Estado");
            $("#<portlet:namespace/>pay_method").val("Banco estado");
        }
        else if ($(this).attr("id") == "s-webpayws") {
            $("#medio-pago span").text(" con WebPay");
            $("#<portlet:namespace/>pay_method").val("WebpayWS");
        }
        else {
            $("#medio-pago span").text(" con Ticket");
            $("#<portlet:namespace/>pay_method").val("coupon_presential");
        }

    });
	
	$("#s-webpay").click();
	
	
	$(".checked").parent().parent().addClass( "active" );
    $( "#pay_box label" ).click(function() {
        $("#pay_box label").parent().removeClass( "active" );
        $(this).parent().addClass( "active" );
        actualizaBotonPago();
    });
	
	$("#medio-pago").click(function(){
		
		$("#formEligeMedio").submit();
		return false;
		
	});
	
});

function actualizaBotonPago(){
	var nombreElegido = "";
	$('#pay_box').find('input').each(function(){
		var medio =  $(this).attr("id"); 
		if ($('#'+medio).is(':checked')){
			nombreElegido = $('#'+medio).attr("data-pago");
		}		
	});
	document.getElementById("spanMedio").innerHTML = nombreElegido;
}

</script>