<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>

<portlet:defineObjects />

<portlet:resourceURL var="enviaNotificacionPago"></portlet:resourceURL> 

<%
	String monto = (String) renderRequest.getAttribute("monto");
	String medioPago = (String) renderRequest.getAttribute("medioPago");
	String fechaPago = (String) renderRequest.getAttribute("fechaPago");
	String idPago = (String) renderRequest.getAttribute("idPago");
	String identificador = (String) renderRequest.getAttribute("identificador");
	String codTransaccion = (String) renderRequest.getAttribute("codTransaccion");
	String finTarjeta = (String) renderRequest.getAttribute("finTarjeta");
%>
<%
	String numCuotas = (String) renderRequest.getAttribute("numCuotas");
	String codAutorizacion = (String) renderRequest.getAttribute("codAutorizacion");
	String tipoTransaccion = (String) renderRequest.getAttribute("tipoTransaccion");
	String tipoTarjeta = (String) renderRequest.getAttribute("tipoTarjeta");
	String tipoCuota = (String) renderRequest.getAttribute("tipoCuota");
%>

<div class="titulo-pagina normado titulo-post-selects">
    <h1><%= UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate") %></h1>
</div>

<main class="container comprobante-exito2-enviado width-100 padding-00-i text-center">

    <div class="contenido clearfix inline-block width-100">
        <div class="steps-new pasos5 col-md-12">
            <ul class="desktop">
                <li class="">
                <a href="" class="relativo">
                    <i class="iconp-documentos"></i> 
                    <span class="margin-left-20">Resumen de cuenta</span>
                </a>
                </li>
                <li class="">
                    <a href="" class="relativo">
                        <i class="iconp-tarjeta"></i>
                        <span>Medios de pago</span>
                    </a>
                </li>
                <li class="active"><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
            </ul>
        </div>

        <div class="menu-opciones-mobile col-md-12">
            <ul>
                <li class=""><a href="" class="relativo"><i class="iconp-documentos"></i> <span>Resumen de cuenta</span></a></li>
                <li class="">
                    <a href="" class="relativo">
                        <i class="iconp-tarjeta"></i>
                        <span>Medios de pago</span>
                    </a>
                </li>
                <li class="active"><a href="" class="relativo"><i class="iconp-documento"></i> <span>Comprobante</span></a></li>
            </ul>
        </div>

        <div class="cont-comprobante-ticket inline-block text-center">

            <div class="no-direccion inline-block width-100 bg-white">
                <div class="cont-icon-advertencia inline-block">
                    <img class="icon-advertencia" src="/mo-theme/images/img-cuenta-pagada-exitosamente.png">
                </div>
                <div class="texto block">
                    <h4>Su cuenta mo ha sido pagada con �xito</h4>
                </div>

                <div class="enviaComprobante inline-block width-100">
                    <p>Envia el comprobante a tu email</p>
                    <div id="divCajaCorreo" >
                    	<form action="" class="form-enviaComprobante inline-block width-100">
	                        <input id="txtCorreo" type="text" placeholder="correo@email.com" class="inline-block">
	                        <button type="button" class="inline-block btn-verde" onClick="enviaNotificacionCorreo()">Enviar</button>
                    	</form>
                    </div>
                    <div id="email-invalido" class="envioexitoso inline-block" style="display:none;"> <i class="MCSS-error_v2"> </i><span>Debes ingresar correctamente el correo</span></div>
                </div>
                <span class="obien inline-block">
                    <h5><span>O bien</span></h5>
                </span>
                <div class="cont-boton inline-block width-100">
                    <div class="cont-btn pull-left">
                        <a href="javascript:void(0)" onClick="descargaPDF()" class="btn2 bg-clear vertical-middle responsive-btn responsive-btn2">
                            <i class="iconp-descargar"></i> Descargar el comprobante</a>
                    </div>
                    <div class="cont-btn pull-right width-1">
                        <a href="javascript:void(0)" onClick="imprimeComprobante()" class="btn2 bg-clear vertical-middle responsive-btn responsive-btn2">
                            <i class="iconp-imprimir"></i> Imprimir el comprobante</a>
                    </div>
                </div>
                <p class="detalle-transaccion">Detalles de la transacci�n:</p>
                <table class="detalle-transaccion comprobante table-ff">
                    <tbody>
                        <tr>
							<td>Monto pagado</td>
							<td>$ <%=monto%></td>
						</tr>
						<tr>
							<td>Fecha de pago</td>
							<td><%=fechaPago%></td>
						</tr>
						<tr>
							<td>ID de pago</td>
							<td><%=idPago%></td>
						</tr>
						<tr>
							<td>ID de ticket</td>
							<td><%=identificador%></td>
						</tr>
						<tr>
							<td>Medio de pago</td>
							<td><%=medioPago%></td>
						</tr>
						<tr>
							<td>C�digo de transacci�n</td>
							<td><%=codTransaccion%></td>
						</tr>
						<tr>
							<td>C�digo autorizaci�n</td>
							<td><%=codAutorizacion%></td>
						</tr>
						<tr>
							<td>Tipo de tarjeta</td>
							<td><%=tipoTarjeta%></td>
						</tr>
						<tr>
							<td>�ltimos 4 d�gitos tarjeta</td>
							<td><%=finTarjeta%></td>
						</tr>
						<tr>
							<td>Tipo de cuotas</td>
							<td><%=tipoCuota%></td>
						</tr>
						<tr>
							<td>N�mero de cuotas</td>
							<td><%=numCuotas%></td>
						</tr>
                    </tbody>
                </table>
                <div class="cont-recuerda">
                    <p>En caso de consultas, guarda este comprobante de pago. Si tu servicio se encontraba suspendido, sera restablecido a la brevedad.</p>
                </div>
                <h5 class="width-100 inline-block">Vis�tanos en <a href="http://www.mo.cl/" target="_blank">www.mo.cl</a></h5>
            </div>
        </div>
    </div>
</main>

<script>

$(document).ready(function(){ });
	
	function enviaNotificacionCorreo(){
		console.log("Llamada ajax a notificar pago por correo");
		var correo = $("#txtCorreo").val();
		$("#email-invalido").hide();
		
		if (validateEmail(correo)) {
			
			jQuery.ajax({
	 	     	type: "POST",
	 	     	url: "<%=renderResponse.encodeURL(enviaNotificacionPago.toString())%>",
	 	     	cache: false,
	 	     	dataType: "html",
	 	     	data: { 
	 	     		 '<portlet:namespace/>operacion': 'notifica',
	 	             '<portlet:namespace/>correo': correo
	 	       	},
	 	     	success: function(resp){
	 	     		//$("#contenedorModalDetalleServicios").html(resp);
	 	     		console.log("La respuesta de enviar correo es : " + resp);
	 	     		if (resp == 'OK'){
	 	     			document.getElementById('divCajaCorreo').innerHTML="";
	 	 	     		$("#divCajaCorreo").append('<div class="envioexitoso inline-block"><i class="MCSS-exito_v2"></i><span>Recibir�s el comprobante en tu correo en los pr�ximos minutos.</span></div>');
	 	     		}
	 	     	}
	 	 	});
			
		}
		else {
			$("#email-invalido").show();
		}
		return false;
	}
	function descargaPDF(){
		console.log("Llamada ajax a descarga PDF");
		window.open('<portlet:resourceURL id="enviaNotificacionPago"></portlet:resourceURL>&<portlet:namespace/>operacion=download_pdf&<portlet:namespace/>tipo_comprobante=pago');
	}
	function imprimeComprobante(){
		console.log("Llamada ajax a imprimir comprobante");
		jQuery.ajax({
 	     	type: "POST",
 	     	url: "<%=renderResponse.encodeURL(enviaNotificacionPago.toString())%>",
 	     	cache: false,
 	     	async: false,
 	     	dataType: "html",
 	     	data: { 
 	     		 '<portlet:namespace/>operacion': 'imprime',
 	     		 '<portlet:namespace/>tipo_comprobante': 'pago'
 	       	},
 	     	success: function(resp){
 	     	   	var mywindow = window.open('', 'my div', 'height=600,width=789');
 	          	mywindow.document.write('<html><head><title>Comprobante mo</title>');
 	           	mywindow.document.write('</head><body class="e3-03" style="background: #fff;">');
 	           	mywindow.document.write(resp);
 	           	mywindow.document.write('</body></html>');
 	           	mywindow.document.close(); // necessary for IE >= 10
 	            myDelay = setInterval(checkReadyState, 10);
 	            function checkReadyState() {
 	               if (mywindow.document.readyState == "complete") {
 	                   clearInterval(myDelay);
 	                   mywindow.focus(); // necessary for IE >= 10
 	                   mywindow.print();
 	                   mywindow.close();
 	               }
 	            }
 	           	return true;
 	     	}
 	 	});
	}
	function validateEmail(email) {
		  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		  return re.test(email);
	}
</script>