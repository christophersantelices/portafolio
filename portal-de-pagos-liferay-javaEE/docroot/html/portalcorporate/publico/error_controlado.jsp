<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>

<portlet:defineObjects />
<portlet:actionURL var="volverAHome" windowState="normal" name="volver_paso1_publico"></portlet:actionURL>
<portlet:actionURL var="consultaFAURL" windowState="normal" name="consultaFa_publico"></portlet:actionURL>

<%
	String tipo_error = (String) renderRequest.getAttribute("tipo_error");
	String fa_cliente = (String) renderRequest.getAttribute("fa_cliente");
%>


<div id="wrapper" class="login container">
	<div id="heading">
				<h1><%= UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate") %></h1>
				<p><%= UtilesContenidos.obtenerContenidoTexto(request, "sub_titulo_portlet_corporate") %></p>
	</div>
	<div id="inner" class="row">
		<div class="left col-xs-12 col-sm-7 col-md-8 col-lg-8">
			<div class="alert_holder alert_rut" style="display:block;">
				<div class="alert grey_box">
					<a class="close"></a> <img class="smile"
						src="/mo-theme/images/icon_success.png" width="66"
						height="55" alt="smile"> 
						
						<!--JSP IF implementation.-->
						<%if(tipo_error.equals("fa_no_existe")){%>
							<span><strong>Error cuenta </strong></span>
							<p>La cuenta financiera <%=fa_cliente%> no es valida<p>
						<%}else if(tipo_error.equals("fa_sin_deuda")){%>
							<span><strong>Cliente sin deuda </strong></span>
							<p>La cuenta financiera <%=fa_cliente%> no presenta pagos pendientes<p>
						<%}%>

					<img class="triangle_bottom_small"
						src="/mo-theme/images/triangle_bottom_small.png" width="64"
						height="24" alt="">
				</div>
			</div>
			        <div id="box_login" class="trans_box">
                        <div id="box_login_2" data-easytabs="true">
                            <form id="tab-numero" name="tab-numero" action="<%=consultaFAURL%>" method="POST" style="display: block;">
								<fieldset>
									<label class="fromLabel">Ingresa n�mero de cuenta financiera:</label>
									<input maxlength="15" id="<portlet:namespace/>fa" name="<portlet:namespace/>fa" type="text" oninvalid="this.setCustomValidity('Debe ingresar una FA')" oninput="setCustomValidity('')"  required>
									<label class="fromLabel">Confirmar n�mero de cuenta financiera:</label>
									<input maxlength="15" id="<portlet:namespace/>fa2" name="<portlet:namespace/>fa" type="text"  required>
								   
								    <button id="btnPagarFA" >Continuar</button>	
								</fieldset>								
							</form>	
                        </div>
                    </div>
		</div>
         <div class="right col-xs-12 col-sm-12 col-md-12 col-lg-4">
             <div class="row">
                   	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_azul")%>
                 	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_blanca")%>
             </div>
         </div>
    </div>
         <hr class="desktop">
		<div class="bottom row left col-xs-12 col-sm-7 col-md-8 col-lg-8">
                <div class="left col-xs-12 col-sm-7 col-md-8 col-lg-9" id="medios_pago">
                    <div class="trans_box" >
                        	<%=UtilesContenidos.obtenerContenidoTexto(request, "lista_medios_pagos")%>
                    </div>
                </div>
                <div class="right col-xs-12 col-sm-5 col-md-4 col-lg-3" id="caja_norton">
                    <div class="trans_box">
                    	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_norton")%>     
                    </div>
                </div>
        </div>
</div>

<script>

$( document ).ready(function() {
	$("body").css("background", "white");
	$("html").css("background", "white");
	
    $( ".alert .close" ).click(function() {
        $(".alert_holder").slideUp( "slow", function() {});
    });
    
});
</script>