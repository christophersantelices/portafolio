<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>

<portlet:defineObjects />

<portlet:actionURL var="volverAHome" windowState="normal" name="volver_home_publico">
</portlet:actionURL> 

<portlet:actionURL var="iniciaPagoActionURL" windowState="normal" name="iniciaPago_publico">
</portlet:actionURL>

<%

		String cuerpoDeuda = (String) renderRequest.getAttribute("cuerpoDeuda");
		String montoTotal = (String) renderRequest.getAttribute("montoTotal");
		String fechaActual = (String) renderRequest.getAttribute("fechaActual");
		String fa = (String) renderRequest.getAttribute("fa_a_Pagar");
		String tieneMontoEnDisputa = (String) renderRequest.getAttribute("tieneMontoEnDisputa");
		String nombreCliente = (String) renderRequest.getAttribute("nombreCliente");
		String textoMontoEnDisputa = "";
		if(tieneMontoEnDisputa.equals("true")){
			textoMontoEnDisputa = "<span class='tooltips' title='Existe un monto en disputa pendiente. Si pagas y se prueba como cobro err�neo, se te descontar� de tu siguiente boleta'>Monto en disputa</span>";
		}
%>

  <div id="wrapper" class="container detail interior">
            <div id="heading">
				<h1><%= UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate") %></h1>
				<p><%= UtilesContenidos.obtenerContenidoTexto(request, "sub_titulo_portlet_corporate") %></p>
            </div>
            <div id="inner" class="row">
                <div class="left col-xs-12 col-sm-12 col-md-12 col-lg-8">
                    <div class="trans_box" id="account_details">
					<div class="edit">
						<span>Esta Cuenta financiera esta asociada a </span> 
						
						<a href="<%=volverAHome%>" class="bt_white bt_small"><%=fa%>  &nbsp;  | &nbsp; <img src="/mo-theme/images/icon_pen.png" width="16" height="16" alt="editar"></a>
							
						<br><strong> <%=nombreCliente %></strong>
						<img src="/mo-theme/images/triangle_small_white_bottom.png" width="44" height="13" alt="">
					</div>
						<%=cuerpoDeuda%>
                            
                        <hr class="desktop">
						<form id="formPasoDeuda" name="formPasoDeuda" action="<%=iniciaPagoActionURL%>" method="POST" >
                            <div id="pay_box" class="trans_box">
                                <div class="top">
                                    <div class="left">
									<strong>Total a pagar:</strong> <span><%=fechaActual%></span>

                                    </div>
									<div id="contenerMontoTotal" class="right">
										<strong>$ <em><%=montoTotal%></em></strong> 
										<%=textoMontoEnDisputa%>
									</div>

                                </div>
								<img src="/mo-theme/images/triangle_bottom.png" width="722" height="24" alt="triangle">
								<%=UtilesContenidos.obtenerContenidoTexto(request, "mp_pago_publico_corporate")%>
                            </div>
							<button id="btnIniciaPago">Pagar $<%=montoTotal%> con WebpayWS</button>
                        </form>
                        <div style="clear: both"></div>
                    </div>

                </div>
                <div class="right col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="row">
                          	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_azul")%>
                        	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_blanca")%>
                    </div>
                </div>
            </div>
            <hr class="desktop">
		<div class="bottom row left col-xs-12 col-sm-7 col-md-8 col-lg-8">
                <div class="left col-xs-12 col-sm-7 col-md-8 col-lg-9" id="medios_pago">
                    <div class="trans_box" >
                        	<%=UtilesContenidos.obtenerContenidoTexto(request, "lista_medios_pagos")%>
                    </div>
                </div>
                <div class="right col-xs-12 col-sm-5 col-md-4 col-lg-3" id="caja_norton">
                    <div class="trans_box">
                    	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_norton")%>     
                    </div>
                </div>
        </div>
 </div>
<script>
$(document).ready(function() {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
	$("body").css("background", "white");
	$("html").css("background", "white");
    asignaEstadoInicial();
});

function asignaEstadoInicial(){	
	
	$("#webpay").attr('checked', true);
	
	$('#btnIniciaPago').click(function() {
		validaPaso2Publico();
	});	
	$( ".active .ticket" ).click(function() {
        $( this ).toggleClass( "selected" );
        actualizaInformacionPago(this);
    });
    $( "#accounts .bt_grey" ).click(function() {
        $("#accounts").toggleClass( "expanded" );
    });
    $('input').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red',
        labelHover: false
    });
    //$('input').on('ifChecked', function(event){
    //	  actualizaInformacionPago(event);
	//});
    
    $('div.iradio_minimal-red').find('input').each(function(){
    	$(this).parent().parent().click(function(){
    		actualizaInformacionPago(null);
    	});
    });
    calculaTotalInicial();
}


function actualizaInformacionPago ( elemento ){
	
	if (elemento == null){
		var nombreMedioSeleccionado = "";
		var mediosPago =  $('div.iradio_minimal-red').find('input');
		for (var i = 0 ; i < mediosPago.length ; i++){
			var medio = mediosPago[i].id;
			if($('#'+medio).is(':checked')){
				nombreMedioSeleccionado = $('#'+medio).val();
				break;
			}
		}
		var textoBotonPago = document.getElementById('btnIniciaPago').innerHTML;
		var ind = textoBotonPago.indexOf('con');
		textoBotonPago = textoBotonPago.substring(0 , ind);
		document.getElementById('btnIniciaPago').innerHTML = textoBotonPago + " con " + nombreMedioSeleccionado;
	}
	else{
		var idEle = $(elemento).attr('id');
		var indCuenta = idEle.indexOf('T');
		var idCuenta = idEle.substring(0, indCuenta);
		//console.log("ID cuenta seleccionada : " + idCuenta);
		var tieneVencida = "false";
		var tienePorVencer = "false";
		var fullVencida = "";
		var fullPorVencer = "";
		
		$("#accounts").find("ul").find("li").find("span").each(function(){
			 var elspan = $(this); 
			 if(elspan.hasClass('ticket')){
				 //console.log("Span recorrido : " + $(this).attr('id'))
				if ( $(this).attr('id').indexOf(idCuenta) > -1){
					//console.log("Entra , " + "indice cuenta : " + $(this).attr('id').indexOf(idCuenta) + ", indice T : " + $(this).attr('id').indexOf('T') + ", indice V : " + $(this).attr('id').indexOf('V'));
			    	if ( $(this).attr('id').indexOf(idCuenta) > -1 && $(this).attr('id').indexOf('T') > -1 ){
				    	tienePorVencer = "true";
				    	fullPorVencer = $(this).attr('id');
					}
					if ( $(this).attr('id').indexOf(idCuenta) > -1 && $(this).attr('id').indexOf('V') > -1 ){
						tieneVencida = "true";
						fullVencida = $(this).attr('id');
					}
				}
			 }	 
		});
		//console.log("Tiene vencida : " + tieneVencida + " - es : " + fullVencida);
		//console.log("Tiene por vencer : " + tienePorVencer + " - es : " + fullPorVencer);
		var carro = $("#carro_deuda").val();
		if($(elemento).hasClass('selected')){
			console.log("Agregando " + idEle);
			if(tieneVencida == "true" && tienePorVencer == "true"){
				carro = carro.replace(fullVencida , fullPorVencer);
			}
			if(tieneVencida == "false" && tienePorVencer == "true"){
				carro = carro + fullPorVencer + ";";
			}
		}
		else{
			console.log("Quitando " + idEle);
			if(tieneVencida == "true" && tienePorVencer == "true"){
				carro = carro.replace(fullPorVencer , fullVencida  );
			}
			if(tieneVencida == "false" && tienePorVencer == "true"){
				carro = carro.replace(fullPorVencer + ";" , "");
			}
		}
		$("#carro_deuda").val(carro);
		console.log("El carro es : " + $("#carro_deuda").val());
		calculaTotal(carro);
	}
}

function calculaTotal(carro){
	var total = 0;
	var arrMontos = carro.split(';');
	for (var i = 0 ; i < arrMontos.length -1 ; i++){
		var ind = "";
		if (arrMontos[i].indexOf('T') > -1){
			ind = arrMontos[i].indexOf('T');
		}
		else{
			ind = arrMontos[i].indexOf('V');
		}
		var monto = arrMontos[i].substring(ind + 1 , arrMontos[i].length );
		total = total + ( parseInt(monto));
		
	}
	total =  formatoMiles.format(total);
	console.log("Monto total : " + total);
	
	var textoBotonPago = document.getElementById('btnIniciaPago').innerHTML;
	var ind = textoBotonPago.indexOf('con');
	textoBotonPago = textoBotonPago.substring( ind , textoBotonPago.length );
	document.getElementById('btnIniciaPago').innerHTML = "Pagar $" + total + " "+ textoBotonPago ;
	var contTotal = document.getElementById('contenerMontoTotal');
	var hijos = $('#contenerMontoTotal').children('span');
	if(hijos.length == 0){
		contTotal.innerHTML = "<strong>$ <em>" + total +"</em></strong>";
	}
	else{
		contTotal.innerHTML = "<strong>$ <em>" + total +"</em></strong>" + "";
	}
}

function calculaTotalInicial(){
	
	var carroDefecto = $("#carro_por_defecto").val();
	$("#carro_deuda").val(carroDefecto);
	console.log("Carro inicial : " + carroDefecto);
	var montoTotal = 0;
	var arrElementos = $("#carro_por_defecto").val().split(';');
	for (var i = 0 ; i < arrElementos.length - 1 ; i++){
		console.log("iterando");
		var aux = arrElementos[i];
		console.log("Elemento obtenido : " + aux);
		var indice = aux.indexOf('T');
		if (indice == -1 ){
			indice = aux.indexOf('V');
		}
		console.log("El indice es : " + indice);
		var monto = aux.substring(indice +1 , aux.length);
		console.log("El monto es : " + monto);
		montoTotal = montoTotal + ( parseInt(monto));
	}
	
	montoTotal = formatoMiles.format(montoTotal);
	console.log("Monto total : " + montoTotal);
	var contTotal = document.getElementById('contenerMontoTotal');
	var hijos = $('#contenerMontoTotal').children('span');
	if(hijos.length == 0){
		contTotal.innerHTML = "<strong>$ <em>" + montoTotal +"</em></strong>";
	}
	else{
		contTotal.innerHTML = "<strong>$ <em>" + montoTotal +"</em></strong>" + "<span class='tooltips' title='Existe un monto en disputa pendiente. Si pagas y se prueba como cobro err�neo, se te descontar� de tu siguiente boleta'>Monto en disputa</span>";
	}
	var nombreMedioSeleccionado = "";
	var mediosPago =  $('div.iradio_minimal-red').find('input');
	for (var i = 0 ; i < mediosPago.length ; i++){
		var medio = mediosPago[i].id;
		if($('#'+medio).is(':checked')){
			nombreMedioSeleccionado = $('#'+medio).val();
			break;
		}
	}
	document.getElementById('btnIniciaPago').innerHTML = "Pagar $" + montoTotal + " con " + nombreMedioSeleccionado;
	//$("#carro_deuda").val(deuda);
	
}

function aaacalculaTotalInicial(){
	
	var montoTotal = 0;
	var deuda = "";
	$("#accounts").find("ul").find("li").find("span").each(function(){
		 var elspan = $(this); 
		 if(elspan.hasClass('ticket')){
			 console.log("Span recorrido : " + $(this).attr('id'))
			 if(elspan.hasClass('selected') || elspan.hasClass('tooltips')){
				 
				 console.log("Agrego la por vencer");
				 var idspan = elspan.attr("id");
				 deuda += idspan+";";
				 var indice = 0 ;
				 for(var i = 0 ; i < idspan.length ; i++){
					 var caracter = idspan.substring(i,i+1);
					 if(caracter == "T" || caracter == "V"){
						 indice = i+1;
						 break;
					 }
				 }
				 var montoAux = idspan.substring (indice, idspan.length);
				 montoTotal += (parseInt(montoAux)) / 100;
			 }
		 }	 
	});
	montoTotal = formatoMiles.format(montoTotal);
	var contTotal = document.getElementById('contenerMontoTotal');
	var hijos = $('#contenerMontoTotal').children('span');
	if(hijos.length == 0){
		contTotal.innerHTML = "<strong>$ <em>" + montoTotal +"</em></strong>";
	}
	else{
		contTotal.innerHTML = "<strong>$ <em>" + montoTotal +"</em></strong>" + "<span class='tooltips' title='Existe un monto en disputa pendiente. Si pagas y se prueba como cobro err�neo, se te descontar� de tu siguiente boleta'>Monto en disputa</span>";
	}
	var nombreMedioSeleccionado = "";
	var mediosPago =  $('div.iradio_minimal-red').find('input');
	for (var i = 0 ; i < mediosPago.length ; i++){
		var medio = mediosPago[i].id;
		if($('#'+medio).is(':checked')){
			nombreMedioSeleccionado = $('#'+medio).val();
			break;
		}
	}
	document.getElementById('btnIniciaPago').innerHTML = "Pagar $" + montoTotal + " con " + nombreMedioSeleccionado;
	$("#carro_deuda").val(deuda);
}

function validaPaso2Publico(){
	$("#formPasoDeuda").submit();
	return false;
}

</script>

<!--  <script src="<%=request.getContextPath()%>/js/validadorPaso2Publico.js?<%=System.currentTimeMillis()%>"></script>-->