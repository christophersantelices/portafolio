<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>


<portlet:defineObjects />

<portlet:actionURL var="errorPagoActionURL" windowState="normal" name="errorPago_publico">
</portlet:actionURL>

<portlet:actionURL var="cierrePagoActionURL" windowState="normal" name="cierrePago_publico">
</portlet:actionURL>

<%
	String encabezadoPasoPago = (String) renderRequest.getAttribute("encabezadoPasoPago");
	String medioDePagoPasoPago = (String) renderRequest.getAttribute("medioDePagoPasoPago");
	String urlMedioDePago = (String) renderRequest.getAttribute("urlMedioDePago");
	String idCanal = (String) renderRequest.getAttribute("idCanal");
	String ordenDeCompra = (String) renderRequest.getAttribute("ordenDeCompra");
	String campoEncriptado = (String) renderRequest.getAttribute("campoEncriptado");
	String solicitudCredito = campoEncriptado.replaceAll("\n", "");
%>


<div id="wrapper" class="container interior">
	<div id="heading">
		<h1><%=UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet")%></h1>
		<p><%=UtilesContenidos.obtenerContenidoTexto(request, "sub_titulo_portlet")%></p>
	</div>
	<div id="inner" class="row">
		<div class="left col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="trans_box">
				<div class="indicador-pago">
					<p><%=encabezadoPasoPago%></p>
				</div>
				<div class="medio-pago">
					<h5><%=medioDePagoPasoPago%></h5>
					<a href="#">Seleccionar otro medio de pago</a>
				</div>
				<input type="hidden" id="numeroOrden" name="numeroOrden" value="<%=ordenDeCompra%>">
				<div id="divPago" name="divPago" class="frame-holder"></div>

				<form id="formularioSolicitudCredito"
					name="formularioSolicitudCredito" style="display: none;"
					method="post" action="<%=urlMedioDePago%>" target="iframe-pago">
					<input type="hidden" name="<portlet:namespace />SolicitudCredito" id="SolicitudCredito"
						value="<%=solicitudCredito%>"> <input type="hidden"
						id="IdCanal" name="IdCanal" value="<%=idCanal%>">
				</form>

				<form id="formError" style="display: none;" method="post"
					action="<%=errorPagoActionURL%>"></form>

				<form id="formCierre" style="display: none;" method="post"
					action="<%=cierrePagoActionURL%>"></form>

				<input type="button" value="enviar" onclick="consultar()"
					id="refreshButton" style="display: none"></input> <input
					type="button" value="error" id="errorButton" style="display: none"></input>
				<input type="button" value="cierre" id="cierreButton"
					style="display: none"></input>
			</div>
		</div>
	</div>
</div>

<script src="<%=request.getContextPath()%>/js/JSIniciaPago.js?<%=System.currentTimeMillis()%>"></script> 


