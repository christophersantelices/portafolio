<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>


<portlet:defineObjects />
<portlet:actionURL var="consultaFAURL" windowState="normal" name="consultaFa_publico"></portlet:actionURL>


<!-- <link rel="stylesheet" href="/portal-de-pagos-corporate-portlet/css/main.min.css">  -->

        <div id="wrapper" class="login container homepublico">
            <div id="heading">
                <h1>Paga tu cuenta m�vil empresa</h1>
                <p>Ingresa tu n�mero de cuenta y revisa tu saldo a pagar</p>
            </div>
            <div id="inner" class="row">
                <div class="left col-xs-12 col-sm-7 col-md-8 col-lg-8">
                    <div id="box_login" class="trans_box">
                        <div id="box_login_2" data-easytabs="true">
                            <form id="tab-numero" name="tab-numero" action="<%=consultaFAURL%>" method="POST" style="display: block;">
								<fieldset>
									<label class="fromLabel">Ingresa n�mero de cuenta financiera:</label>
									<input maxlength="15" id="<portlet:namespace/>fa" name="<portlet:namespace/>fa" type="text" oninvalid="this.setCustomValidity('Debe ingresar una FA')" oninput="setCustomValidity('')"  required>
									<label class="fromLabel">Confirmar n�mero de cuenta financiera:</label>
									<input maxlength="15" id="<portlet:namespace/>fa2" name="<portlet:namespace/>fa" type="text"  required>
								   
								    <button id="btnPagarFA" >Continuar</button>	
								</fieldset>								
							</form>	
                        </div>
                    </div>
                </div>
                <div class="right col-xs-12 col-sm-5 col-md-4 col-lg-4">
                    <div class="row">
                        	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_azul")%>
                        	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_blanca")%>
                    </div>
                </div>
            </div>
            <div class="bottom row caja left col-xs-12 col-sm-7 col-md-8 col-lg-8">
                <div class="left col-xs-12 col-sm-7 col-md-8 col-lg-9" id="medios_pago">
                    <div class="trans_box" >
                        	<%=UtilesContenidos.obtenerContenidoTexto(request, "lista_medios_pagos")%>
                    </div>
                </div>
                <div class="right col-xs-12 col-sm-5 col-md-4 col-lg-3" id="caja_norton">
                    <div class="trans_box">
                    	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_norton")%>     
                    </div>
                </div>
            </div>
        </div>
<script>
$( document ).ready(function() {
	//$('link[rel=stylesheet][href*="mo-theme-pagos-theme/css/main.css"]').remove();
	$("body").css("background", "white");
	$("html").css("background", "white");

	$("#<portlet:namespace/>fa").keydown(function (e) {    	
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) { 
            e.preventDefault();
        }
    });
    $("#<portlet:namespace/>fa2").keydown(function (e) {    	
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) { 
            e.preventDefault();
        }
    });
    
	   var fa = document.getElementById("<portlet:namespace/>fa")
	    , fa2 = document.getElementById("<portlet:namespace/>fa2");

		  function validatePassword(){
		    if(fa.value != fa2.value) {
		      fa2.setCustomValidity("Las FA no coinciden");
		    } else {
		      fa2.setCustomValidity('');
		    }
		  }
	  fa.onchange = validatePassword;
	  fa2.onchange = validatePassword;
	  fa2.onkeyup = validatePassword;
})

</script>