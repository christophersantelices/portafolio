
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>
<portlet:defineObjects />

<portlet:actionURL var="volverAHome" windowState="normal" name="volver_home_publico">
</portlet:actionURL>

<%
	String ordenDeCompra = (String) renderRequest.getAttribute("ordenDeCompra");
%>


<div id="wrapper" class="container interior">
	<div id="heading">
		<h1><%= UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate") %></h1>
		<p><%= UtilesContenidos.obtenerContenidoTexto(request, "sub_titulo_portlet_corporate") %></p>
	</div>
	<div id="inner" class="row">
	<div class="left col-xs-12 col-sm-12 col-md-12 col-lg-8">
		<div id="box_login" class="grey_box comp-holder">
			<div class="comprobante error">
				<span class="semi_circle">
					<svg class="svg_error top_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="841.89px" height="841.89px" viewBox="0 0 841.89 841.89" enable-background="new 0 0 841.89 841.89" xml:space="preserve"> <path d="M423.556,574.109c-21.021,0-38.088-17.106-38.088-38.127c0-20.966,17.066-38.02,38.088-38.02 c6.01,0,11.992,1.454,17.335,4.19l-13.516,26.378c-5.021-2.592-12.26,1.467-12.26,7.451c0,2.377,0.934,4.607,2.563,6.115 l6.091-6.556l-4.087,7.986c5.29,2.792,12.259-1.511,12.259-7.546c0-2.471-1.068-4.701-2.805-6.238l20.166-21.725 c7.826,7.235,12.286,17.438,12.286,27.963C461.589,557.003,444.522,574.109,423.556,574.109z"/> <g> <path d="M423.503,457.872c-12.848,0-23.237-10.419-23.237-23.249V219.159c0-12.834,10.39-23.25,23.237-23.25 c12.847,0,23.236,10.415,23.236,23.25v215.464C446.739,447.453,436.35,457.872,423.503,457.872z"/> </g> <g> <path d="M423.076,730.118c-81.064,0-158.2-29.555-218.91-84.228c-65.037-58.587-103.364-138.994-107.932-226.4 c-4.594-87.435,25.134-171.407,83.733-236.471c58.574-65.064,138.996-103.433,226.439-108.011 c180.235-9.51,335.015,129.74,344.416,310.188c3.98,75.652-18.589,150.826-63.542,211.683 c-7.612,10.309-22.168,12.554-32.504,4.887c-10.337-7.625-12.526-22.182-4.889-32.505c38.569-52.217,57.905-116.72,54.514-181.636 c-8.094-154.847-140.329-273.902-295.562-266.198c-75.027,3.94-144.043,36.86-194.337,92.696 c-50.267,55.836-75.773,127.911-71.848,202.935c3.926,75.014,36.806,144.004,92.655,194.297 c55.822,50.293,128.444,75.667,202.882,71.874c55.369-2.897,108.013-21.74,152.243-54.527c10.362-7.665,24.893-5.474,32.505,4.849 c7.666,10.31,5.476,24.866-4.834,32.52c-51.576,38.233-112.953,60.216-177.481,63.579 C434.774,729.958,428.896,730.118,423.076,730.118z M423.556,574.083c-21.021,0-38.088-17.095-38.088-38.101 c0-20.981,17.066-38.048,38.088-38.048c6.117,0,12.179,1.511,17.575,4.341c1.415,0.748,2.75,1.63,3.926,2.618 c1.442,0.828,2.805,1.816,4.086,2.978c7.906,7.21,12.446,17.455,12.446,28.111C461.589,556.988,444.522,574.083,423.556,574.083z"/> </g> </svg>
				</span>
				<div class="box-comprobate">
					<h2>No fue posible realizar el pago</h2>
					<h3>Existe un problema en la conexi�n con el servicio de pago</h3>
					<p>De todas maneras, te recomendamos:</p>
					<ul class="data-list">
						<li>Revisar tus datos ingresados.</li>
						<li>Que tienes el cupo suficiente.</li>
						<li>Intentar con otro medio de pago.</li>
					</ul>
					<p>Si el problema continua o tuviste alg�n inconveniente mayor, cont�ctanos y ten a mano en este n�mero de transacci�n:</p>
					<br />
					<h3><%=ordenDeCompra%></h3>
				</div>
				<span class="clearfix"></span>
			</div>
		</div>
		<div class="control-nav text-center">
			<a href="<%=volverAHome%>" class="btn">Reintentar pago</a>
		</div>
	</div>
                <div class="right col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="row">
                          	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_azul")%>
                        	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_blanca")%>
                    </div>
                </div>
    </div>
            <hr class="desktop">
		<div class="bottom row left col-xs-12 col-sm-7 col-md-8 col-lg-8">
                <div class="left col-xs-12 col-sm-7 col-md-8 col-lg-9" id="medios_pago">
                    <div class="trans_box" >
                        	<%=UtilesContenidos.obtenerContenidoTexto(request, "lista_medios_pagos")%>
                    </div>
                </div>
                <div class="right col-xs-12 col-sm-5 col-md-4 col-lg-3" id="caja_norton">
                    <div class="trans_box">
                    	<%=UtilesContenidos.obtenerContenidoTexto(request, "caja_norton")%>     
                    </div>
                </div>
        </div>
 </div>
 
 <script>

$( document ).ready(function() {
	$("body").css("background", "white");
	$("html").css("background", "white");
});
</script>