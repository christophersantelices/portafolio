<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ page import="cl.ev.mo.portalcorporate.utiles.UtilesContenidos" %>


<portlet:defineObjects />

<portlet:actionURL var="volverAPaso1ActionURL" windowState="normal" name="volver_home_publico">
</portlet:actionURL> 
<%
	//Obtencion de variables
%>

 
 <div id="wrapper" class="container interior">				
      <div id="heading">
      		<h1><%= UtilesContenidos.obtenerContenidoTexto(request, "titulo_portlet_corporate") %></h1>
			<p><%= UtilesContenidos.obtenerContenidoTexto(request, "sub_titulo_portlet_corporate") %></p>
      </div>
      <div id="inner" class="row">
          <div class="left col-xs-12 col-sm-12 col-md-12 col-lg-8">
              <div class="trans_box">
                  <div class="">
                      <main class="container e3-indisponibilidad width-100 padding-00-i text-center">
						    <section class="sub-container clearfix inline-block width-100">
						       
						        <div class="disculpa-por-inconveniente">
						            <div class="imagen">
						                <i class="MCSS-alerta_v2"></i>
						            </div>
						            <div class="texto">
						                <h5>Disculpa por el inconveniente</h5>
						                <p>En estos momentos no podemos atender tu pago.</p>
						                <p><strong>Por favor int�ntalo en unos minutos.</strong></p>
						            </div>
						        </div>
						        
						    </section>
						</main>
                  </div>
                  <div class="medio-pago">
                      <h5></h5>
                      <a href="<%=volverAPaso1ActionURL%>">Volver a iniciar el pago</a>
                  </div>
              </div>
          </div>
      </div>
</div>