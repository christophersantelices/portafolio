<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%> 

<portlet:defineObjects />
<portlet:actionURL var="validaEntradaActionURL" windowState="normal" name="validaEntrada">
</portlet:actionURL> 
 
<script type="text/javascript" src="<%=request.getContextPath()%>/js/funciones.js"> </script>

<form id="form-inicio" name="tab-rut" style="display:none" action="<%=validaEntradaActionURL%>" method="POST">
	<input type="hidden" id="<portlet:namespace/>token" name="<portlet:namespace/>token">
</form> 
<script>
	$(document).ready(function() {	
	 	var params = getUrlParams();
	    var id = params['token'];  
	    document.getElementById("<portlet:namespace/>token").value = id;
	    console.log(document.getElementById("<portlet:namespace/>token").value);
		document.getElementById("form-inicio").submit();
	});	  	
</script>