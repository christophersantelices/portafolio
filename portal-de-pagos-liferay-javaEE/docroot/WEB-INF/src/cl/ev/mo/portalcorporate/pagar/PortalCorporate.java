package cl.ev.mo.portalcorporate.pagar;

import cl.ev.mo.portalcorporate.beans.CarroDePago;
import cl.ev.mo.portalcorporate.beans.CierreTransaccion;
import cl.ev.mo.portalcorporate.beans.IntegracionPDP;
import cl.ev.mo.portalcorporate.beans.ObjetoDePago;
import cl.ev.mo.portalcorporate.beans.RespuestaFindAccount;
import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.utiles.PathPaginas;
import cl.ev.mo.portalcorporate.clientesWS.ClienteCreateTicket;
import cl.ev.mo.portalcorporate.devetelWS.ValidaAccesoRespuesta;
import cl.ev.mo.portalcorporate.serviciosHelper.WSHelper;
import cl.ev.mo.portalcorporate.utiles.DBSupport;
import cl.ev.mo.portalcorporate.utiles.SessionUtil;
import cl.ev.mo.portalcorporate.utiles.UtilesPago;
import cl.ev.mo.portalcorporate.utiles.UtilesContenidos;
import cl.ev.mo.portalcorporate.pagar.PagaTuCuentaCorporate;
import cl.ev.mo.portalcorporate.beans.BillingCustomerFA;
import cl.ev.mo.portalcorporate.beans.CuentaFinancieraFA;
import cl.ev.mo.portalcorporate.beans.DocumentoFA;
import cl.ev.mo.portalcorporate.clientesWS.ClienteFindAccount;
import cl.ev.mo.portalcorporate.clientesWS.ClienteSendLegacyEvent;

import com.google.gson.Gson;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortalContext;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.ws.WebServiceException;

/**
 * 
 * @author ev
 *
 */
public class PortalCorporate extends MVCPortlet {
 
	static Log log = LogFactoryUtil.getLog(PortalCorporate.class);



	public void validaEntrada(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {

		 String parametro = "";
		 String paramAux = URLDecoder.decode(ParamUtil.getString(actionRequest, "token"), "UTF-8");
		 
			try{
				SessionUtil.setVariable(actionRequest, Constantes.IDENTIFICADOR,UtilesPago.generaIdentificadorUnicoSinDB(actionResponse));
			}catch(Exception e){
				log.error("Se produjo un error al generar el identificador de session");
			}
			
		log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" -- Valida entrada --");

		if (paramAux.equals("undefined")||paramAux==null||paramAux.equals("")) {
			log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+"  Voy a paso 1 publico");
			actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_home);
		}else{
			try {
				log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Token antes : "	 + paramAux);
				for (int i = 0; i < paramAux.length(); i++) {
					char cha = paramAux.charAt(i);
					if (cha == ' ') {
						char segundo = paramAux.charAt(i + 1);
						if (segundo == ' ') {
							i++;
							continue;
						} else {
							parametro += '+';
						}
					} else {
						parametro += cha;
					}
				} 
				log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Parametro : " + parametro);
				
				IntegracionPDP inte = UtilesPago.getDatosIntegracion(parametro);

				if (inte.getProcedencia().equals(Constantes.PAGO_DE_DEUDAS_PUBLICO)) {
					log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Voy a home publico");
					SessionUtil.setVariable(actionRequest, Constantes.PROCEDENCIA,Constantes.PROCEDENCIA_PUBLICA);
					actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_home);
				} else {
					log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Voy a home privado");
					SessionUtil.setVariable(actionRequest, Constantes.PROCEDENCIA,Constantes.PROCEDENCIA_PRIVADA);
					log.debug("FA obtenida desde AMDOCS : "+ inte.getJsonIntegracion().getPayment().getSearchParameters().getFinancialAccount());
					generaPaso1_privado(actionRequest, actionResponse,inte.getFa(),inte.getDoc());
				}
			} catch (WebServiceException e) {
				log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Error al inicializar el cliente del servicio : " + e);
				e.printStackTrace();
			} catch (Exception e) {
				log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Error al inicio de la aplicación : " + e);
				e.printStackTrace();
				actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_error_generico);
			}
		}
	}
	
	/*flujo publico*/
	public void iniciaPago_publico(ActionRequest actionRequest,ActionResponse actionResponse) throws IOException, PortletException {

		log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Inicio de pago...");
		
		String medioPago = ParamUtil.getString(actionRequest, "pay_method");
		log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" El medio de pago elegido es : " + medioPago);
		
		String carroDeuda = ParamUtil.getString(actionRequest, "carro_deuda");
		log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" El carrito es: " + carroDeuda);

		SessionUtil.setVariable(actionRequest, Constantes.CARRO_COMPRAS,carroDeuda);
		CarroDePago carrito = UtilesPago.validaCarroDeuda(actionRequest,carroDeuda);
		if (!carrito.isValido()) {
			log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Intento de fraude...");
			actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_error_generico);
			return;
		} else {
			// Obtener los folios y montos asociados
			log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Carro de deuda validado correctamente...");

			Iterator<ObjetoDePago> it = carrito.getObjetosDePago().iterator();
			while (it.hasNext()) {
				ObjetoDePago obj = it.next();
				log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Token de pago : " + obj.getTokenDePago());
				log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" ID FA : " + obj.getIdCuentaFinanciera());
				log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" System ID : " + obj.getSystemID());
				log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Monto a pagar : " + obj.getMontoAPagar());
				log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Nobre del pagador : " + obj.getNombreCliente());
			}
		}
		
		String fa_cliente=SessionUtil.getVariable(actionRequest, Constantes.FA_CLIENTE);

		String encabezadoPasoPago = "Cuenta financiera <strong>"+fa_cliente+"</strong>";
	
		/*
		String medioDePagoPasoPago = "";
		
		if (medioPago.equals(Constantes.servipag)) {
			medioDePagoPasoPago = "Servipag";
		} else if (medioPago.equals(Constantes.webPay)) {
			medioDePagoPasoPago = "WebPay";
		} else if (medioPago.equals(Constantes.bci)) {
			medioDePagoPasoPago = "Bci";
		} else if (medioPago.equals(Constantes.bancoEstado)) {
			medioDePagoPasoPago = "Banco Estado";
		} else {
			medioDePagoPasoPago = medioPago;
		}*/
		
		
		log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Nombre medio de pago elegido en session : "+ medioPago);
		SessionUtil.setVariable(actionRequest, Constantes.MEDIO_PAGO,medioPago);
		SessionUtil.setVariable(actionRequest,Constantes.MONTO_TOTAL_A_PAGAR,carrito.getMontoTotalPago());
		
		//SessionUtil.setVariable(actionRequest,Constantes.FECHA_VENCIMIENTO_DEUDA, "20150923");
		
		// Desde aquí Kit Mula :D 
		boolean kitSimulado = (UtilesContenidos.obtenerContenidoTexto(actionRequest, Constantes._kit_simulado).equals("true")) ? true: false;
		
		if (kitSimulado) {
			log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Va por kit simulado");
			try {
				WSHelper helper = new WSHelper(actionRequest);
				
				String tipoTicket = ClienteCreateTicket.TICKET_INMEDIATO_SIN_RUT;
				log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" tipo ticket:"+tipoTicket);
				String idTicketRDP = helper.callCreateTicket(actionRequest,carrito,tipoTicket);
				SessionUtil.setVariable(actionRequest,Constantes.ID_TICKET_RDP, idTicketRDP);
				SessionUtil.setVariable(actionRequest,Constantes.ID_TRX_DEVETEL,"TRX" + System.currentTimeMillis());
				SessionUtil.setVariable(actionRequest,Constantes.ID_CANAL_DE_PAGO, "0");

				String canalDePago = SessionUtil.getVariable(actionRequest,Constantes.ID_CANAL_DE_PAGO);
				String urlMedioDePago = "";
				String idTRX = SessionUtil.getVariable(actionRequest,Constantes.ID_TRX_DEVETEL);
				String idMedio = "";
				String campoEncriptado = "";
				
				boolean insercionCorrecta = true;				
				
				if (!insercionCorrecta) {
					log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Error al insertar el inicio de la transaccion :(");
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_generico);
				}
				actionRequest.setAttribute("encabezadoPasoPago",encabezadoPasoPago);
				actionRequest.setAttribute("medioDePagoPasoPago",medioPago);
				actionRequest.setAttribute("idCanal", canalDePago);
				actionRequest.setAttribute("urlMedioDePago", urlMedioDePago);
				actionRequest.setAttribute("campoEncriptado", campoEncriptado);
				actionRequest.setAttribute("ordenDeCompra", SessionUtil.getVariable(actionRequest,Constantes.ID_TICKET_RDP));
				actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_paso_pago_mula);

			} catch (Exception e) {
				log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Error al levantar el kit de pago simulado "+ e);
				e.printStackTrace();
				actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_generico);
			}
		} else {
			log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Va por Devetel");
			try {
				WSHelper helper = new WSHelper(actionRequest);
				/*String tipoTicket = (esPagoPorRut(actionRequest) ? ClienteCreateTicket.TICKET_INMEDIATO_CON_RUT
						: ClienteCreateTicket.TICKET_INMEDIATO_SIN_RUT);*/
				String tipoTicket= ClienteCreateTicket.TICKET_INMEDIATO_SIN_RUT;
				String idTicketRDP = helper.callCreateTicket(actionRequest,carrito,tipoTicket);
				SessionUtil.setVariable(actionRequest,Constantes.ID_TICKET_RDP, idTicketRDP);

				ValidaAccesoRespuesta respuestaValida = helper.callValidaAcceso(Constantes.PROCEDENCIA_PUBLICA);
				SessionUtil.setVariable(actionRequest,Constantes.ID_TRX_DEVETEL, respuestaValida.getAUTENTIFICACION().getIDTRX());
				SessionUtil.setVariable(actionRequest,Constantes.ID_CANAL_DE_PAGO, respuestaValida.getAUTENTIFICACION().getIDCANAL());

				String canalDePago = SessionUtil.getVariable(actionRequest,Constantes.ID_CANAL_DE_PAGO);
				String urlMedioDePago = PagaTuCuentaCorporate.getURLMedioDePagoElegido(medioPago,respuestaValida);
				String idTRX = SessionUtil.getVariable(actionRequest,Constantes.ID_TRX_DEVETEL);

				String idMedio = PagaTuCuentaCorporate.getIdMedioDePagoElegido(medioPago, respuestaValida);
				String campoEncriptado = UtilesPago.encriptaSolicitudDeCredito(canalDePago, idMedio, idTRX);

				DBSupport dbs = new DBSupport();
			
				boolean insercionCorrecta = dbs.iniciaRegistroTransaccion(
						SessionUtil.getVariable(actionRequest,Constantes.PAYMENT_SYSTEM_ID),
						idTicketRDP, 
						SessionUtil.getVariable(actionRequest,Constantes.TOKEN_DE_PAGO),
						Constantes.PROCEDENCIA_PUBLICA, 
						SessionUtil.getVariable(actionRequest,Constantes.MONTO_TOTAL_A_PAGAR),
						Constantes.CUENTA_FINANCIERA, 
						fa_cliente, 
						SessionUtil.getVariable(actionRequest,Constantes.CARRO_COMPRAS),
						SessionUtil.getVariable(actionRequest,Constantes.MEDIO_PAGO));

				dbs.insertaDatosSession(idTicketRDP,
						SessionUtil.getVariable(actionRequest,Constantes.PAYMENT_SYSTEM_ID),
						SessionUtil.getVariable(actionRequest,Constantes.TOKEN_DE_PAGO),
						SessionUtil.getVariable(actionRequest,Constantes.MONTO_TOTAL_A_PAGAR),
						SessionUtil.getVariable(actionRequest,Constantes.MEDIO_PAGO),
						SessionUtil.getVariable(actionRequest, Constantes.CUENTA_FINANCIERA));
				
				if (!insercionCorrecta) {
					log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Error al insertar el inicio de la transaccion :(");
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_generico);
				}
				actionRequest.setAttribute("encabezadoPasoPago",encabezadoPasoPago);
				actionRequest.setAttribute("medioDePagoPasoPago",medioPago);
				actionRequest.setAttribute("idCanal", canalDePago);
				actionRequest.setAttribute("urlMedioDePago", urlMedioDePago);
				actionRequest.setAttribute("campoEncriptado", campoEncriptado);
				actionRequest.setAttribute("ordenDeCompra", SessionUtil.getVariable(actionRequest,Constantes.ID_TICKET_RDP));
				actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_paso2_pago);
			} catch (Exception e) {
				log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Error desconocido al intentar levantar el pago por Devetel... "+ e);
				actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_generico);
			}
		}
	}
	
	public void consultaFa_publico(ActionRequest actionRequest,	ActionResponse actionResponse) throws IOException, PortletException {

		String fa_cliente = ParamUtil.getString(actionRequest, "fa");
		log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Entro a publico_consultaFA"+" FA : " + fa_cliente);	
		
		SessionUtil.setVariable(actionRequest, Constantes.FA_CLIENTE,fa_cliente);

		log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Session fa_cliente : " + fa_cliente);
		WSHelper helper = new WSHelper(actionRequest);
		try {
			RespuestaFindAccount respFind = helper.callFindAccount_publico(fa_cliente);
			if (!respFind.isEjecutadoCorrectamente()) {
				log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Codigo de respuesta de findAccount es "+respFind.getCodigoError());
				//si el fa no existe, misma pantalla que sin deuda distinto mensaje. 
				if(respFind.getCodigoError().equals(Constantes.NO_EXISTE)){
					actionRequest.setAttribute(Constantes.FA_CLIENTE, fa_cliente);
					actionRequest.setAttribute(Constantes.TIPO_ERROR, Constantes.FA_NO_EXISTE);
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_controlado);
				}else{
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_generico);
				}
			}else{
				if (respFind.isTieneDeuda()) {
					String cuerpoDeuda = PagaTuCuentaCorporate.generaCuerpoDeudaPublico(respFind); 
					Gson gson = new Gson();
					String json = gson.toJson(respFind);
					log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" El objeto en JSON es : " + json); 
					
					SessionUtil.setVariable(actionRequest, Constantes.detalle_deuda,json);
					SessionUtil.setVariable(actionRequest, Constantes.nombre_cliente,respFind.getNombreCliente());		
										
					actionRequest.setAttribute("cuerpoDeuda", cuerpoDeuda);
					actionRequest.setAttribute("montoTotal", UtilesPago.montoEnFormatoFront(respFind.getTotalDeuda()));
					actionRequest.setAttribute("fechaActual",UtilesPago.getFechaActualEnFormatoFront());
					/** No se sabe si es 1 o mas lineas */
					actionRequest.setAttribute("fa_a_Pagar",fa_cliente);
					actionRequest.setAttribute("tieneMontoEnDisputa",String.valueOf(respFind.tieneMontoEnDisputa()));
					actionRequest.setAttribute("nombreCliente",respFind.getNombreCliente());
					log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Al paso 1 publico"); 
					actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_paso1);
				} else {		
					log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" No tiene deuda");
					actionRequest.setAttribute(Constantes.FA_CLIENTE, fa_cliente);
					actionRequest.setAttribute(Constantes.TIPO_ERROR, Constantes.FA_SIN_DEUDA);
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_controlado);
				}
			}
			
		} catch (Exception e) {
			log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Error al intentar renderizar los datos de la deuda "+ e);
			e.printStackTrace();
			/* Redirijo a la pagina de error */
			actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_error_generico);
		}
		
	}
	
	public void cierrePago_publico(ActionRequest actionRequest,ActionResponse actionResponse) throws IOException, PortletException {
		DBSupport db = new DBSupport();

		String estadoOc;
		try {
			log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Validando estado en base de datos para el ticket " + Constantes.ID_TICKET_RDP);
			estadoOc = db.consultaEstadoOC(SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP));
			log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" El estado del ticket es  " + estadoOc);
		} catch (Exception e1) {
			log.error("Fallo al obtener orden de compra de base de datos" + e1);
			estadoOc = "99";
		}
		if (estadoOc.equalsIgnoreCase("00")) {

			log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Cierre de pago correcto...");
			WSHelper helper = new WSHelper(actionRequest);
			boolean cierreCorrecto = helper.callCreateDebtPayment(true,actionRequest);

			if (!cierreCorrecto) {
				cierreCorrecto = helper.callCreateDebtPayment(true,
						actionRequest);
			}
			if (cierreCorrecto) {
				CierreTransaccion cierre = null;
				try {
					cierre = db.getDatosCierre(SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP));
					actionRequest.setAttribute("monto", cierre.getMonto());
					actionRequest.setAttribute("fechaPago",cierre.getFechaPago());
					actionRequest.setAttribute("idPago",cierre.getIdentificadorDePago());
					actionRequest.setAttribute("identificador",cierre.getNumeroDeOrden());
					actionRequest.setAttribute("medioPago",cierre.getMedioDePago());
					actionRequest.setAttribute("codTransaccion",cierre.getCodigoTransaccion());
					actionRequest.setAttribute("finTarjeta",cierre.getUltimosDigitosTarjeta());
					actionRequest.setAttribute("numCuotas",cierre.getNumeroCuotas());
					actionRequest.setAttribute("codAutorizacion",cierre.getCodigoAutorizacion());
					actionRequest.setAttribute("tipoTransaccion",cierre.getTipoTransaccion());
					actionRequest.setAttribute("tipoTarjeta",cierre.getTipoTarjeta());
					actionRequest.setAttribute("tipoCuota",cierre.getTipoCuotas());
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_comprobante_exito);
				} catch (Exception e) {
					log.error("Error al obtener los datos del cierre...");
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_generico);
				}
			} else {
				actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_generico);
			}
		} else {
			actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_error_generico);
		}
	}

	public void errorPago_publico(ActionRequest actionRequest,ActionResponse actionResponse) throws IOException, PortletException {
		log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Error en el pago : Se invoca desde el front... timeout de pago?");
		actionRequest.setAttribute("ordenDeCompra", SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP));
		actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_comprobante_fracaso);
	}
	
	public void volver_home_publico(ActionRequest actionRequest,ActionResponse actionResponse) throws IOException, PortletException {
		log.debug(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Volviendo a home publico...");
		actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.publico_home);
	}
	
	public void volver_paso1_publico(ActionRequest actionRequest,ActionResponse actionResponse) throws IOException, PortletException {
		log.debug("Volviendo a paso 1 privado...");
		actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_paso1);
	}
	/*fin flujo publico*/
	
	
	/*flujo privado*/
	public void generaPaso1_privado(ActionRequest actionRequest,ActionResponse actionResponse, String fa_cliente, String documento) {

		try {
			SessionUtil.setVariable(actionRequest, Constantes.FA_CLIENTE,fa_cliente);
			SessionUtil.setVariable(actionRequest, Constantes.DOCUMENTO,documento);
		}catch(Exception e){}
				
		if(documento.equals("")){
			log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+"  fa sin documento");
			
			WSHelper helper = new WSHelper(actionRequest);
			try {
				RespuestaFindAccount respFind = helper.callFindAccount_privado(fa_cliente, Constantes.CONSULTA_MOVIL_CUENTA);
				// Valido que se ejecutó correctamente
				if (!respFind.isEjecutadoCorrectamente()) {
					log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Codigo de respuesta de findAccount es "+respFind.getCodigoError());
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);

				}else{
					log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+"  callFindAccount_privado exitoso");
					// Para clientes con deuda
					if (respFind.isTieneDeuda()) {									
						boolean tieneProrroga = false;
						
						String cuerpoDeuda = PagaTuCuentaCorporate.generaCuerpoDeudaPrivado(respFind, fa_cliente,tieneProrroga);
						String tienePat = PagaTuCuentaCorporate.cuentaConPat(respFind,fa_cliente);
						
						SessionUtil.guardaDetalleDeuda(actionRequest, respFind);
						SessionUtil.setVariable(actionRequest,Constantes.NOMBRE_CLIENTE,respFind.getNombreCliente());
						
						actionRequest.setAttribute("tieneProrroga", "false");
						actionRequest.setAttribute("faPorDefecto", fa_cliente);
						actionRequest.setAttribute("cuerpoDeuda", cuerpoDeuda);
						actionRequest.setAttribute("linkOtrasDeudas","");
						actionRequest.setAttribute("tienePat", tienePat);
						actionRequest.setAttribute("puedeAbonar", "");
						actionRequest.setAttribute("otrasDeudas", "");
						actionRequest.setAttribute("montoTotal", UtilesPago
								.montoEnFormatoFront(respFind.getTotalDeuda(fa_cliente)));
						actionRequest.setAttribute("fechaActual",
								UtilesPago.getFechaActualEnFormatoFront());
						//cambiar
						actionRequest.setAttribute("tieneMontoEnDisputa",
								String.valueOf(respFind.tieneMontoEnDisputa()));
						actionRequest.setAttribute("botonLineas", "");
						actionRequest.setAttribute("abono", "");

						if (cuerpoDeuda.equalsIgnoreCase("error")) {
							actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_sin_cuenta);
						} else {
							actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_paso1_detalle);
						}
					}else {
						// Para clientes sin deuda
						log.debug("Redirijo a página sin deuda privada...");
						//cambiar
						actionRequest.setAttribute("rutAPagar",SessionUtil.getVariable(actionRequest, fa_cliente));
						actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_sin_deuda);
					}
				}
			}catch (Exception e) {
				log.error("Error al intentar renderizar los datos de la deuda..." +e);
				e.printStackTrace();
				actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_error_generico);
			} 
		}else{
			log.info(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+"  fa con documento");
			WSHelper helper = new WSHelper(actionRequest);
			try {
				RespuestaFindAccount respFind = helper.callFindAccount_privado(fa_cliente,documento, Constantes.CONSULTA_MOVIL_DOCUMENTO);
				//guardo fecha de vencimiento
				SessionUtil.setVariable(actionRequest,Constantes.FECHA_VENCIMIENTO_DEUDA,respFind.getBillingCustomer().get(0).getCuentasFinancieras().get(0).getDocumentos().get(0).getFecha());
				log.debug("1");
				if (!respFind.isEjecutadoCorrectamente()) {
					log.error(SessionUtil.getVariable(actionRequest, Constantes.IDENTIFICADOR)+" Codigo de respuesta de findAccount es "+respFind.getCodigoError());
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
				}else{
					log.debug("2");
					// Para clientes con deuda
					if (respFind.isTieneDeuda()) {
						log.debug("3");
						String cuerpoDeuda = PagaTuCuentaCorporate.generaCuerpoDeudaDocumentoPrivado(respFind,fa_cliente);

						SessionUtil.guardaDetalleDeuda(actionRequest, respFind);
						actionRequest.setAttribute("fa_cliente", fa_cliente);
						actionRequest.setAttribute("documento", documento);
						actionRequest.setAttribute("cuerpoDeuda", cuerpoDeuda);
						actionRequest.setAttribute("montoTotal", UtilesPago.montoEnFormatoFront(respFind.getBillingCustomer().get(0).getCuentasFinancieras().get(0).getDocumentos().get(0).getMonto()));
						actionRequest.setAttribute("fechaActual",UtilesPago.getFechaActualEnFormatoFront());

						if (cuerpoDeuda.equalsIgnoreCase("error")) {
							actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
						} else {
							actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_paso1_detalle_documento);
						}
					}else {
						log.debug("Redirijo a página sin deuda privada...");
						actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_sin_deuda);
					}
				}
			}catch (Exception e) {
				log.error("Error al intentar renderizar los datos de la deuda... "+ e);
				e.printStackTrace();
				actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_error_generico);
			} 
		}
	
	}
	
	public void eligeMedioDePago_privado(ActionRequest actionRequest,ActionResponse actionResponse) throws IOException, PortletException {

		try {
			log.debug("****Eligiendo medio de pago para privado****");
			String carroNuevo = ParamUtil.getString(actionRequest,"carroPagoPrivadoFinal");
			log.debug("El carro nuevo es : " + carroNuevo);
			// TODO: revisar esta funcion. mucha cosa 
			CarroDePago carrito = UtilesPago.validaCarroDeudaPrivado(actionRequest, carroNuevo);
			SessionUtil.setVariable(actionRequest,Constantes.CARRO_COMPRAS, carroNuevo);

			if (!carrito.isValido()) {
				log.error("Intento de fraude...");
				actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
			} else {
				log.debug("Carro de deuda validado correctamente...");
				Iterator<ObjetoDePago> it = carrito.getObjetosDePago().iterator();
				while (it.hasNext()) {
					ObjetoDePago obj = it.next();
					log.debug("Token de pago : " + obj.getTokenDePago());
					log.debug("ID FA : " + obj.getIdCuentaFinanciera());
					log.debug("System ID : " + obj.getSystemID());
					log.debug("Monto a pagar : " + obj.getMontoAPagar());
					log.debug("Nombre del pagador : " + obj.getNombreCliente());
				}
				
				SessionUtil.setVariable(actionRequest,Constantes.MONTO_TOTAL_A_PAGAR,carrito.getMontoTotalPago());
				SessionUtil.setVariable(actionRequest, Constantes.TIPO_PAGO_ABONO, "pagoprivado");
				
				actionRequest.setAttribute("montoTotal", UtilesPago.montoEnFormatoFront(carrito.getMontoTotalPago()));
				actionRequest.setAttribute("fechaActual",UtilesPago.getFechaActualEnFormatoFront());
				actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_elige_medio_pago);
			}
		} catch (Exception e) {
			log.error("Error al procesar el pago : " + e);
			actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_error_generico);
		}
	}

	public void iniciaPago_privado(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		String carroPago = SessionUtil.getVariable(actionRequest, Constantes.CARRO_COMPRAS);
		log.debug("carro pago  "+carroPago);
		CarroDePago carrito = new CarroDePago();
		
		//TODO: validar si se puede abonar
		if ("abono".equalsIgnoreCase(SessionUtil.getVariable(actionRequest, Constantes.TIPO_PAGO_ABONO )) ) {
			carrito = UtilesPago.validaCarroAbono(actionRequest,carroPago, SessionUtil.getVariable(actionRequest, Constantes.MONTO_ABONO));
			log.debug("largo del carro "+carrito.getObjetosDePago().size());
		}
		else {
			carrito = UtilesPago.validaCarroDeudaPrivado(actionRequest, carroPago);
		}
		
		String medioPago = ParamUtil.getString(actionRequest, "pay_method");
	
		log.debug("Nombre medio de pago elegido en session : "+ medioPago);
		SessionUtil.setVariable(actionRequest, Constantes.MEDIO_PAGO,medioPago);

		try {
			WSHelper helper = new WSHelper(actionRequest);
			String idTicketRDP = "";
			
			if (medioPago.equals(Constantes.CUPON_PRESENCIAL)) {

				//TODO: que mierda con esta funcion de createTicket, funciona??? mil weas en duro
				idTicketRDP = helper.callCreateTicket(actionRequest,carrito,ClienteCreateTicket.TICKET_DIFERIDO_SIN_RUT);
				
				log.debug("El ticket obtenido para cupon es : " + idTicketRDP);
				if (idTicketRDP.equalsIgnoreCase("")|| (idTicketRDP.equalsIgnoreCase("0"))) {
					log.error("Error en la llamada de createTicket para cupon presencial");
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
				}else{
					SessionUtil.setVariable(actionRequest, Constantes.ID_TICKET_RDP,idTicketRDP);
					actionRequest.setAttribute("monto", UtilesPago.montoEnFormatoFront(SessionUtil.getVariable(
									actionRequest,Constantes.MONTO_TOTAL_A_PAGAR)));
					actionRequest.setAttribute("identificador", SessionUtil.getVariable(actionRequest,Constantes.ID_TICKET_RDP));
					//TODO: obtener fecha vencimiento ticket
					actionRequest.setAttribute("fechaValidez",
									UtilesPago.formateaFechaValizTicketFormatoFront(SessionUtil.getVariable(actionRequest,Constantes.FECHA_VENCIMIENTO_DEUDA)));
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_cupon_presencial);
				}
			}else{

				idTicketRDP = helper.callCreateTicket(actionRequest,carrito,ClienteCreateTicket.TICKET_INMEDIATO_SIN_RUT);
				log.debug("El ticket obtenido para medio de pago es : "+ idTicketRDP);
				if ("" == idTicketRDP || (idTicketRDP.equalsIgnoreCase("0"))) {
					log.error("Error en la llamada de createTicket para medio de pago");
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
				}else{
					SessionUtil.setVariable(actionRequest,Constantes.ID_TICKET_RDP, idTicketRDP);
					
					boolean kitSimulado = (UtilesContenidos.obtenerContenidoTexto(actionRequest, Constantes._kit_simulado).equals("true")) ? true: false;

					if (kitSimulado) {
						log.debug("Va por kit simulado");

						SessionUtil.setVariable(actionRequest,Constantes.ID_TRX_DEVETEL,"TRX" + System.currentTimeMillis());
						SessionUtil.setVariable(actionRequest,Constantes.ID_CANAL_DE_PAGO, "0");

						String canalDePago = SessionUtil.getVariable(actionRequest,Constantes.ID_CANAL_DE_PAGO);
						String urlMedioDePago = "";
						String idTRX = SessionUtil.getVariable(actionRequest,Constantes.ID_TRX_DEVETEL);
						String idMedio = "";
						String campoEncriptado = "";

						boolean insercionCorrecta = true;					
						
						if (!insercionCorrecta) {
							log.error("Error al insertar el inicio de la transaccion :(");
							actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
						}else{
							actionRequest.setAttribute("medioDePagoPasoPago",medioPago);
							actionRequest.setAttribute("idCanal", canalDePago);
							actionRequest.setAttribute("urlMedioDePago", urlMedioDePago);
							actionRequest.setAttribute("campoEncriptado", campoEncriptado);
							actionRequest.setAttribute("ordenDeCompra", SessionUtil.getVariable(actionRequest,Constantes.ID_TICKET_RDP));
							actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_paso_pago_mula);
						}
					}else {
						log.debug("Va por Devetel");
						
						// TODO: Cambiar a procedencia privada y llenar con datos del ->>>> porq tenia la procedencia en publico???
						ValidaAccesoRespuesta respuestaValida = helper.callValidaAcceso(Constantes.PROCEDENCIA_PRIVADA);
						SessionUtil.setVariable(actionRequest,Constantes.ID_TRX_DEVETEL, respuestaValida
										.getAUTENTIFICACION().getIDTRX());
						SessionUtil.setVariable(actionRequest,Constantes.ID_CANAL_DE_PAGO, respuestaValida
										.getAUTENTIFICACION().getIDCANAL());

						String canalDePago = SessionUtil.getVariable(actionRequest,Constantes.ID_CANAL_DE_PAGO);
						String urlMedioDePago = PagaTuCuentaCorporate.getURLMedioDePagoElegido(medioPago,respuestaValida);
						String idTRX = SessionUtil.getVariable(actionRequest,Constantes.ID_TRX_DEVETEL);

						String idMedio = PagaTuCuentaCorporate.getIdMedioDePagoElegido(medioPago, respuestaValida);
						String campoEncriptado = UtilesPago.encriptaSolicitudDeCredito(canalDePago, idMedio, idTRX);

						DBSupport dbs = new DBSupport();

						boolean insercionCorrecta = dbs.iniciaRegistroTransaccion(
								SessionUtil.getVariable(actionRequest,Constantes.PAYMENT_SYSTEM_ID),
								idTicketRDP, 
								SessionUtil.getVariable(actionRequest,Constantes.TOKEN_DE_PAGO),
								Constantes.PROCEDENCIA_PUBLICA, 
								SessionUtil.getVariable(actionRequest,Constantes.MONTO_TOTAL_A_PAGAR),
								Constantes.CUENTA_FINANCIERA, 
								SessionUtil.getVariable(actionRequest, Constantes.FA_CLIENTE),
								SessionUtil.getVariable(actionRequest,Constantes.CARRO_COMPRAS),
								SessionUtil.getVariable(actionRequest,Constantes.MEDIO_PAGO));
						
						dbs.insertaDatosSession(idTicketRDP,
								SessionUtil.getVariable(actionRequest,Constantes.PAYMENT_SYSTEM_ID),
								SessionUtil.getVariable(actionRequest,Constantes.TOKEN_DE_PAGO),
								SessionUtil.getVariable(actionRequest,Constantes.MONTO_TOTAL_A_PAGAR),
								SessionUtil.getVariable(actionRequest,Constantes.MEDIO_PAGO),
								SessionUtil.getVariable(actionRequest, Constantes.CUENTA_FINANCIERA));
						
						if (!insercionCorrecta) {
							log.error("Error al insertar el inicio de la transaccion");
							actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
						}else{
							actionRequest.setAttribute("medioDePagoPasoPago",medioPago);
							actionRequest.setAttribute("idCanal", canalDePago);
							actionRequest.setAttribute("urlMedioDePago", urlMedioDePago);
							actionRequest.setAttribute("campoEncriptado", campoEncriptado);
							actionRequest.setAttribute("ordenDeCompra", SessionUtil.getVariable(actionRequest,Constantes.ID_TICKET_RDP));
							actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_paso_pago);
						}
					}
				}
			}			
		} catch (NullPointerException e) {
			log.error("Error al intentar obtener la respuesta de valida acceso...");
			log.error("Error : " + e);
			// Redirijo a la pagina de error 
			actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_error_generico);
		} catch (Exception e) {
			log.error("Error inesperado al intentar levantar el kit de pago  :( ");
			log.error("Error : " + e);
			// Redirijo a la pagina de error 
			actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_error_generico);
		}
	}

	public void volver_Paso1_Privado(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		log.debug("Volviendo a paso 1 privado...");
		//actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_paso1_detalle);
		generaPaso1_privado(actionRequest,actionResponse,SessionUtil.getVariable(actionRequest, Constantes.FA_CLIENTE),SessionUtil.getVariable(actionRequest, Constantes.DOCUMENTO));
	}
	
	public void error_Pago_Privado(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		actionRequest.setAttribute("ordenDeCompra", SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP));
		actionResponse.setRenderParameter(PathPaginas.mvc_path, PathPaginas.privado_comprobante_fracaso);
	}
	
	public void cierre_Pago_Privado(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		String estadoOc;
		DBSupport db = new DBSupport();
		
		try {
			estadoOc = db.consultaEstadoOC(SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP));
			log.debug("El estado del ticket es  " + estadoOc);
		} catch (Exception e1) {
			log.error("Fallo al obtener orden de compra de base de datos " + e1);
			estadoOc = "99";
		}

		if (estadoOc.equalsIgnoreCase("00")) {
			log.info("Cierre de pago correcto...");
			
			WSHelper helper = new WSHelper(actionRequest);
			boolean cierreCorrecto = helper.callCreateDebtPayment(false,actionRequest);
			if (!cierreCorrecto) {
				log.debug("Segundo llamado a Create Debt Payment");
				cierreCorrecto = helper.callCreateDebtPayment(true,actionRequest);
			}
			if (cierreCorrecto) {
				CierreTransaccion cierre = null;
				try {
					cierre = db.getDatosCierre(SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP));

					actionRequest.setAttribute("monto", cierre.getMonto());
					actionRequest.setAttribute("fechaPago",cierre.getFechaPago());
					actionRequest.setAttribute("idPago",cierre.getIdentificadorDePago());
					actionRequest.setAttribute("identificador",cierre.getNumeroDeOrden()); // ID ticket
					actionRequest.setAttribute("medioPago",cierre.getMedioDePago());
					actionRequest.setAttribute("orden",cierre.getNumeroDeOrden());
					actionRequest.setAttribute("codTransaccion",cierre.getCodigoTransaccion());
					actionRequest.setAttribute("finTarjeta",cierre.getUltimosDigitosTarjeta());
					actionRequest.setAttribute("numCuotas",cierre.getNumeroCuotas());
					actionRequest.setAttribute("codAutorizacion",cierre.getCodigoAutorizacion());
					actionRequest.setAttribute("tipoTransaccion",cierre.getTipoTransaccion());
					actionRequest.setAttribute("tipoTarjeta",cierre.getTipoTarjeta());
					actionRequest.setAttribute("tipoCuota",cierre.getTipoCuotas());
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_comprobante_exito);
				} catch (Exception e) {
					log.error("Error al obtener los datos del cierre...");
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
				}
			} else {
				log.error("Error en el llamado forzado al create debt payment");
				actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
			}
		}
	}

	/*fin flujo privado*/
	


	/*kit mula */
	public void finalizarPagoSimulado_publico(ActionRequest actionRequest,ActionResponse actionResponse) throws IOException, PortletException {

		log.debug("Entrando a finalizar el pago por el kit simulado");

		String operacion = ParamUtil.getString(actionRequest, "operacionMula");
		log.debug("La operacion es : " + operacion);

		if (operacion.equals("Pagar")) {

			log.error("Inicio cierre simulado...");
			WSHelper helper = new WSHelper(actionRequest);
			//boolean cierreCorrecto = helper.callCreateDebtPaymentSimulado(false,actionRequest);
			boolean cierreCorrecto=true;
			
			if (!cierreCorrecto) {
				cierreCorrecto = helper.callCreateDebtPaymentSimulado(true,actionRequest);
			}
			if (cierreCorrecto) {

				DBSupport db = new DBSupport();
				CierreTransaccion cierre = null;
				try {
					cierre = db.getDatosCierreSimulado(actionRequest);

					actionRequest.setAttribute("monto", cierre.getMonto());
					actionRequest.setAttribute("fechaPago",cierre.getFechaPago());
					actionRequest.setAttribute("idPago",cierre.getIdentificadorDePago());
					actionRequest.setAttribute("identificador",cierre.getNumeroDeOrden()); // ID ticket
					actionRequest.setAttribute("medioPago",cierre.getMedioDePago());
					actionRequest.setAttribute("orden",cierre.getNumeroDeOrden());
					actionRequest.setAttribute("codTransaccion",cierre.getCodigoTransaccion());
					actionRequest.setAttribute("finTarjeta",cierre.getUltimosDigitosTarjeta());
					actionRequest.setAttribute("numCuotas",cierre.getNumeroCuotas());
					actionRequest.setAttribute("codAutorizacion",cierre.getCodigoAutorizacion());
					actionRequest.setAttribute("tipoTransaccion",cierre.getTipoTransaccion());
					actionRequest.setAttribute("tipoTarjeta",cierre.getTipoTarjeta());
					actionRequest.setAttribute("tipoCuota",cierre.getTipoCuotas());

					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_comprobante_exito);
				} catch (Exception e) {
					log.error("Error al obtener los datos del cierre...");
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_generico);
				}
			} else {
				actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_error_generico);
			}
		} else {
			log.error("Pago anulado...");
			actionRequest.setAttribute("ordenDeCompra", SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP));
			actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.publico_comprobante_fracaso);
		}
	}

	public void finalizarPagoSimulado_privado(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {

		log.debug("Entrando a finalizar el pago por el kit simulado");
		String operacion = ParamUtil.getString(actionRequest, "operacionMula");
		log.debug("La operacion es : " + operacion);
		if (operacion.equals("Pagar")) {
			log.error("Inicio cierre simulado...");
			WSHelper helper = new WSHelper(actionRequest);
			
			boolean cierreCorrecto = helper.callCreateDebtPaymentSimulado(false,actionRequest);
			//boolean cierreCorrecto =true;
			if (!cierreCorrecto) {
				cierreCorrecto = helper.callCreateDebtPaymentSimulado(true,
						actionRequest);
			}
			if (cierreCorrecto) {
				DBSupport db = new DBSupport();
				CierreTransaccion cierre = null;
				try {
					cierre = db.getDatosCierreSimulado(actionRequest);
					actionRequest.setAttribute("monto", cierre.getMonto());
					actionRequest.setAttribute("fechaPago",cierre.getFechaPago());
					actionRequest.setAttribute("idPago",cierre.getIdentificadorDePago());
					actionRequest.setAttribute("identificador",cierre.getNumeroDeOrden()); // ID ticket
					actionRequest.setAttribute("medioPago",cierre.getMedioDePago());
					actionRequest.setAttribute("orden",cierre.getNumeroDeOrden());
					actionRequest.setAttribute("codTransaccion",cierre.getCodigoTransaccion());
					actionRequest.setAttribute("finTarjeta",cierre.getUltimosDigitosTarjeta());
					actionRequest.setAttribute("numCuotas",cierre.getNumeroCuotas());
					actionRequest.setAttribute("codAutorizacion",cierre.getCodigoAutorizacion());
					actionRequest.setAttribute("tipoTransaccion",cierre.getTipoTransaccion());
					actionRequest.setAttribute("tipoTarjeta",cierre.getTipoTarjeta());
					actionRequest.setAttribute("tipoCuota",cierre.getTipoCuotas());
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_comprobante_exito);
				} catch (Exception e) {
					log.error("Error al obtener los datos del cierre...");
					actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
				}
			} else {
				actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
			}
		} else if (operacion.equals("Anular"))  {
			log.error("Pago anulado...");
			actionRequest.setAttribute("ordenDeCompra", SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP));
			actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_comprobante_fracaso);
		} else {
			log.error("Error al en cierre de pago...");
			actionResponse.setRenderParameter(PathPaginas.mvc_path,PathPaginas.privado_error_generico);
		}
	}

	/*fin kit mula */
	
	/* Para llamadas ajax */
	public void serveResource(ResourceRequest request, ResourceResponse response)throws PortletException, IOException {
		
		String operacion = request.getParameter("operacion");
		log.debug("serveResource "+operacion);
		

		switch (operacion) {

		case Constantes.SEND_NOTIFICATION: {

			log.debug("Enviando notificacion de pago...");
			String destCorreo = request.getParameter("correo");
			log.debug("El correo del destinatario es : " + destCorreo);

			String tipoNotificacion = "";
			try {
				tipoNotificacion = request.getParameter("tipoComprobante");
				log.debug("Tipo notificacion : " + tipoNotificacion);
				if (!tipoNotificacion.equals("presencial")) {
					tipoNotificacion = "Normal";
				}
			} catch (Exception e) {
				tipoNotificacion = "Normal";
			}
			log.debug("El tipo de comprobante de correo es : "
					+ tipoNotificacion);
			PrintWriter writer = response.getWriter();
			String endpointSendLegacy = UtilesContenidos.obtenerContenidoTexto(
					request, Constantes._endpoint_send_legacy_event);
			WSHelper helper = new WSHelper(endpointSendLegacy, request);
			try {
				boolean simulado = (UtilesContenidos.obtenerContenidoTexto(request,
						Constantes._kit_simulado).equals("true") ? true : false);
				if (tipoNotificacion.equals("Normal")) {
					boolean notificado = helper.callSendLegacyEvent(destCorreo,
							simulado, Constantes.NOTIFICACION_PAGO);
					writer.write("OK");
				} else if (tipoNotificacion.equals("presencial")) {
					boolean notificado = helper
							.callSendLegacyEvent(
									destCorreo,
									simulado,
									Constantes.NOTIFICACION_TICKET_PRESENCIAL);
				}
			} catch (Exception e) {
				log.error("Error al generar el comprobante PDF : " + e);
		}
			break;
		}
		case Constantes.IMPRIMIR: {
			log.debug("Generando comprobante imprimir");

			String tipoComprobante = (null != request
					.getParameter("tipo_comprobante")) ? request
					.getParameter("tipo_comprobante") : "";

				if (tipoComprobante.equals("pago")) {
					log.debug("Generando comprobante pago");
					PrintWriter writer = response.getWriter();
					writer.write(PagaTuCuentaCorporate
							.generaComprobanteImprimir(request));
					
				}
			break;
		}
		case Constantes.DOWNLOAD_PDF: {
			log.debug("Llamada ajax a descarga pdf...");
			try {
				StringBuffer buf = new StringBuffer();
				String tipoComprobante = request
						.getParameter("tipo_comprobante");
				log.error("Tipo comprobante : " + tipoComprobante);
				if (tipoComprobante.equals("pago")) {
					log.error("PDF Comprobante pago");
					buf.append(PagaTuCuentaCorporate
							.generaComprobantePDF(request));
				} 
				response.setContentType("application/pdf");
				response.setProperty(HttpHeaders.CONTENT_DISPOSITION,
						"attachement;filename=comprobantePago.pdf");
				response.addProperty(HttpHeaders.CACHE_CONTROL,
						"max-age=3600, must-revalidate");
				Document document = new Document();
				// step 2
				PdfWriter.getInstance(document,
						response.getPortletOutputStream());
				// step 3
				document.open();
				// step 4
				PdfPTable table = new PdfPTable(1);
				PdfPCell cell = new PdfPCell();
				ElementList list = XMLWorkerHelper.parseToElementList(
						buf.toString(), null);
				for (Element element : list) {
					cell.addElement(element);
				}
				table.addCell(cell);
				document.add(table);
				// step 5
				document.close();
			} catch (Exception ex) {
				log.error("Error al generar el comprobante PDF : " + ex);
			}
			break;
		}
		case Constantes.GET_DOCUMENTOS:{
			log.debug("detalle_privado");
			String faSeleccionada = request.getParameter("faSeleccionada");
			String docSeleccionado = request.getParameter("docSeleccionado");
			log.debug("Llamada ajax a obtener documentos");
			String endpointFind = UtilesContenidos.obtenerContenidoTexto(request,
					Constantes._endpoint_find_account);
			String biller = SessionUtil.getVariable(request, Constantes.PROCEDENCIA).equals( Constantes.PROCEDENCIA_PUBLICA) ? 
					UtilesContenidos.obtenerContenidoTexto(request,Constantes._biller_find_account_corporate_publico):UtilesContenidos.obtenerContenidoTexto(request,Constantes._biller_find_account_corporate_privado);
			log.debug("Endpoint obtenido para findAccount : " + endpointFind);
			WSHelper helper = new WSHelper(endpointFind, biller);
			PrintWriter writer = response.getWriter();
			PagaTuCuentaCorporate corporate = new PagaTuCuentaCorporate();
			RespuestaFindAccount respFind = null;
			try {
				respFind = helper.callFindAccount_privado(faSeleccionada, Constantes.CONSULTA_MOVIL_DOCUMENTO); 				
				RespuestaFindAccount deudaPrincipal = SessionUtil.getDetalleDeuda(request);
				Iterator<BillingCustomerFA> it = deudaPrincipal
						.getBillingCustomer().iterator();
				while (it.hasNext()) {
					BillingCustomerFA bill = it.next();
					Iterator<CuentaFinancieraFA> it2 = bill
							.getCuentasFinancieras().iterator();
					while (it2.hasNext()) {
						CuentaFinancieraFA cuenta = it2.next();
						if (cuenta.getIdCuentaFinanciera().equals(
								faSeleccionada)) {
							log.debug("Encontré la cuenta principal");
							cuenta.setDocumentos(respFind.getBillingCustomer()
									.get(0).getCuentasFinancieras().get(0)
									.getDocumentos());
						}
					}
				}
				SessionUtil.guardaDetalleDeuda(request, deudaPrincipal);
				/* Fin guardar documentos en session */
				writer.write(corporate.generaDetalleBoletasServicios(respFind,
						faSeleccionada, deudaPrincipal));
			} catch (Exception e) {
				log.error("Error de servicio en la llamada ajax : " + e);
			}
			break;
		}
		case Constantes.GET_SERVICIOS: {
			log.debug("Llamada ajax a obtener servicios");
			
			String faSeleccionada = request.getParameter("faSeleccionada");
			String docSeleccionado = request.getParameter("docSeleccionado");
			String endpointFind = UtilesContenidos.obtenerContenidoTexto(request,Constantes._endpoint_find_account);
			log.debug("Endpoint obtenido para findAccount : " + endpointFind);
			String biller = SessionUtil.getVariable(request, Constantes.PROCEDENCIA).equals( Constantes.PROCEDENCIA_PUBLICA) ? 
					UtilesContenidos.obtenerContenidoTexto(request,Constantes._biller_find_account_corporate_publico):UtilesContenidos.obtenerContenidoTexto(request,Constantes._biller_find_account_corporate_privado);
			WSHelper helper = new WSHelper(endpointFind, biller);
			PrintWriter writer = response.getWriter();
			PagaTuCuentaCorporate corporate = new PagaTuCuentaCorporate();

			RespuestaFindAccount respFind = null;
			RespuestaFindAccount deudaPrincipal = SessionUtil.getDetalleDeuda(request);
			try {
				respFind = helper.callFindAccountDetalleServicio_privado(faSeleccionada , docSeleccionado);
				Iterator<BillingCustomerFA> it = deudaPrincipal.getBillingCustomer().iterator();
				
				while (it.hasNext()) {
					BillingCustomerFA bill = it.next();
					Iterator<CuentaFinancieraFA> it2 = bill.getCuentasFinancieras().iterator();
					while (it2.hasNext()) {
						CuentaFinancieraFA cuenta = it2.next();
						if (cuenta.getIdCuentaFinanciera().equals(faSeleccionada)) {
							log.debug("Encontré la cuenta seleccionada");

							Iterator<DocumentoFA> it3 = cuenta.getDocumentos().iterator();
							while (it3.hasNext()) {

								DocumentoFA doc = it3.next();
								if (doc.getIdDocumento().equals(docSeleccionado)) {
									log.debug("Encontre el documento");
									doc.setServicios(respFind
											.getBillingCustomer().get(0)
											.getCuentasFinancieras().get(0)
											.getDocumentos().get(0)
											.getServicios());
								}
							}
						}
					}
				}
				SessionUtil.guardaDetalleDeuda(request, deudaPrincipal);
				/* Fin guardar documentos en session */
				writer.write(corporate.generaDetalleServicios(respFind,docSeleccionado, faSeleccionada));
			} catch (Exception e) {
				log.error("Error de servicio en la llamada ajax : " + e);
			}
			break;
		}
		default:
			log.error("serveResource default");
			break;
		}
	}
	/* fin llamadas ajax */
	

}
