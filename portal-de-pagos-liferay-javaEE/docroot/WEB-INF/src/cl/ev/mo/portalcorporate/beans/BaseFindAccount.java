package cl.ev.mo.portalcorporate.beans;

public class BaseFindAccount {

	protected String montoTotal ;
	protected String tokenDePago;
	protected String systemID;
	
	public BaseFindAccount() {
		super();
	}

	public String getTokenDePago() {
		return tokenDePago;
	}
	public void setTokenDePago(String tokenDePago) {
		this.tokenDePago = tokenDePago;
	}
	public String getSystemID() {
		return systemID;
	}
	public void setSystemID(String systemID) {
		this.systemID = systemID;
	}
}
