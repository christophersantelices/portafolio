package cl.ev.mo.portalcorporate.utiles;

public class PathPaginas {
	public static final String mvc_path = "mvcPath";

	public static final String pag_home = "/html/portalcorporate/home.jsp";
	public static final String publico_paso_pago_mula ="/html/portalcorporate/kitmula/kitMulaPublico.jsp";
	public static final String privado_paso_pago_mula ="/html/portalcorporate/kitmula/kitMulaPrivado.jsp";

	
	/** Paginas publicas */
	public static final String publico_home = "/html/portalcorporate/publico/home.jsp";
	public static final String publico_paso1 = "/html/portalcorporate/publico/paso1.jsp";
	public static final String publico_paso2_pago="/html/portalcorporate/publico/paso2_pago.jsp";
	public static final String publico_comprobante_exito="/html/portalcorporate/publico/comprobante_exito.jsp";
	public static final String publico_comprobante_fracaso="/html/portalcorporate/publico/comprobante_fracaso.jsp";
	public static final String publico_error_generico = "/html/portalcorporate/publico/error_generico.jsp";
	public static final String publico_error_controlado = "/html/portalcorporate/publico/error_controlado.jsp";


	
	/** Paginas privadas */
	public static final String privado_error_generico="/html/portalcorporate/privado/error_generico.jsp";
	public static final String privado_paso1_detalle="/html/portalcorporate/privado/paso1_detalle.jsp";
	public static final String privado_paso1_detalle_documento="/html/portalcorporate/privado/paso1_detalle_documento.jsp";
	public static final String privado_error_sin_cuenta="/html/portalcorporate/privado/error_sin_cuenta.jsp";
	public static final String privado_sin_deuda = "/html/portalcorporate/privado/sin_deuda.jsp";
	public static final String privado_elige_medio_pago = "/html/portalcorporate/privado/elige_medio_pago.jsp";
	public static final String privado_comprobante_exito = "/html/portalcorporate/privado/comprobante_exito.jsp";
	public static final String privado_comprobante_fracaso = "/html/portalcorporate/privado/comprobante_fracaso.jsp";
	public static final String privado_cupon_presencial ="/html/portalcorporate/privado/comprobante_cupon_presencial.jsp";
	public static final String privado_paso_pago ="/html/portalcorporate/privado/paso_pago.jsp";

}
