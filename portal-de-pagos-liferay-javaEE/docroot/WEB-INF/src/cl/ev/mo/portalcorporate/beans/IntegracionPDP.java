package cl.ev.mo.portalcorporate.beans;
 
public class IntegracionPDP {
		
	private IntegrateJSONObject jsonIntegracion;
	private String procedencia;
	private String fa; 
	private String doc;
	
	/**
	 * Datos que serán recibidos por el JSON encriptado
	 */
	public IntegracionPDP(){
		setProcedencia("");
		setFa("");
		setDoc("");
	}
	
	public String getProcedencia() {
		return procedencia;
	}
	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia;
	} 

	public String getFa() {
		return fa;
	}

	public void setFa(String fa) {
		this.fa = fa;
	}

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}
	public IntegrateJSONObject getJsonIntegracion() {
		return jsonIntegracion;
	}

	public void setJsonIntegracion(IntegrateJSONObject jsonIntegracion) {
		this.jsonIntegracion = jsonIntegracion;
	}
}
