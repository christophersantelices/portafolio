package cl.ev.mo.portalcorporate.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class RespuestaFindAccount implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<BillingCustomerFA> billingCustomer;
	private String fa;
	private String nombreCliente;
	private boolean tieneDeuda ;
	private boolean ejecutadoCorrectamente;
	private boolean pagoPorRut;
	private boolean pagoDeOrden;
	//Monto proveniente de Amdocs
	private String amount;
	private String codigoError;
	

	public RespuestaFindAccount(){
		billingCustomer = new ArrayList<BillingCustomerFA>();
		tieneDeuda = false;
		ejecutadoCorrectamente = true;
		pagoPorRut = false;
		pagoDeOrden = false;
	}
	public void setTieneDeuda(boolean tieneDeuda) {
		this.tieneDeuda = tieneDeuda;
	}
	public boolean isTieneDeuda(){
		return tieneDeuda;
	}
	public boolean isEjecutadoCorrectamente() {
		return ejecutadoCorrectamente;
	}
	public void setEjecutadoCorrectamente(boolean ejecutadoCorrectamente) {
		this.ejecutadoCorrectamente = ejecutadoCorrectamente;
	}
	public String getFA() {
		return fa;
	}
	public void setFA(String fa) {
		this.fa = fa;
	}
	public ArrayList<BillingCustomerFA> getBillingCustomer() {
		return billingCustomer;
	}
	public void setBillingCustomer(ArrayList<BillingCustomerFA> billingCustomer) {
		this.billingCustomer = billingCustomer;
	}
	public String getTotalDeuda(){
		String totalDeuda = "";
		Iterator<BillingCustomerFA> it1 = this.getBillingCustomer().iterator();
		int montoTotal = 0;
		while (it1.hasNext()){
			BillingCustomerFA bc = it1.next();
			Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
			while(it2.hasNext()){
				CuentaFinancieraFA fa = it2.next();	
				int monto;
				try{
					monto = Integer.parseInt(fa.getMontoTotal());
				}catch(Exception e){
					monto = 0;
				}
				montoTotal += monto;
			}
		}
		totalDeuda += montoTotal;
		return totalDeuda;
	}
	
	public String getTotalDeuda(String faSeleccionada){
		String totalDeuda = "";
		Iterator<BillingCustomerFA> it1 = this.getBillingCustomer().iterator();
		long montoTotal = 0;
		while (it1.hasNext()){
			BillingCustomerFA bc = it1.next();
			Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
			while(it2.hasNext()){
				CuentaFinancieraFA fa = it2.next();			
				if(fa.getIdCuentaFinanciera().equals(faSeleccionada)){
					long monto;
					try{
						monto = Long.parseLong(fa.getMontoTotal());
					}catch(Exception e){
						monto = 0;
					}
					montoTotal += monto;
				}
			}
		}
		totalDeuda += montoTotal;
		return totalDeuda;
	}
	
	public boolean isPagoPorRut() {
		return pagoPorRut;
	}
	public void setPagoPorRut(boolean pagoPorRut) {
		this.pagoPorRut = pagoPorRut;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public boolean tieneMontoEnDisputa() {
		boolean tieneMontoEnDisputa = false;
		Iterator<BillingCustomerFA> it1 = this.getBillingCustomer().iterator();
		while (it1.hasNext()){
			BillingCustomerFA bc = it1.next();
			Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
			while(it2.hasNext()){
				CuentaFinancieraFA fa = it2.next();	
				int monto;
				try{
					monto = Integer.parseInt(fa.getMontoEnDisputa());
					
				}catch(Exception e){
					monto = 0;
				}
				if(monto > 0){
					tieneMontoEnDisputa = true;
					break;
				}
			}
			if(tieneMontoEnDisputa){
				break;
			}
		}
		return tieneMontoEnDisputa;
	}
	public boolean isPagoDeOrden() {
		return pagoDeOrden;
	}
	public void setPagoDeOrden(boolean pagoDeOrden) {
		this.pagoDeOrden = pagoDeOrden;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

}
