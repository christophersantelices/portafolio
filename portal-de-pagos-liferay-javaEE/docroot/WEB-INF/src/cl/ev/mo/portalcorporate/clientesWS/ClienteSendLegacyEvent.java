package cl.ev.mo.portalcorporate.clientesWS;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;

import javax.portlet.ResourceRequest;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cl.ev.mo.portalcorporate.beans.CierreTransaccion;
import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.utiles.DBSupport;
import cl.ev.mo.portalcorporate.utiles.SessionUtil;
import cl.ev.mo.portalcorporate.serviciosHelper.UtilesClientesWS;
import cl.ev.mo.portalcorporate.utiles.UtilesPago;

import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.SendLegacyEvent;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.SendLegacyEventFault;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.SendLegacyEventPortType;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.ActivityAbstract;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.ContactMedium;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.Customer;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.CustomerAccount;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.CustomerCreditProfile;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.CustomerOrder;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.CustomerOrderItem;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.DataReq;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.DocumentAbstract;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.EmailContact;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.GeographicAddress;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.IndividualName;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.LoyaltyBalance;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.Money;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.ObjectFactory;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.PaymentMethod;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.Product;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.ProductOffering;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.Resource;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.SalesChannel;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.SendLegacyEventRequest;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.SendLegacyEventResponse;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.Service;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.ServiceCharacteristicValue;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.TelephoneNumber;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.TemplateParameters;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.TimePeriod;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.UserAbstract;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderRequest;

public class ClienteSendLegacyEvent extends UtilesClientesWS {

	Log log = LogFactoryUtil.getLog(ClienteSendLegacyEvent.class);
	
	private String endpoint = "http://pca2635.ctc.cl:9183/EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1";
	private static final QName SERVICE_NAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1", "SendLegacyEvent");
	
	URL wsdlURL;
	SendLegacyEvent ss;
	SendLegacyEventPortType port;
	ObjectFactory factory;
	
	
	public ClienteSendLegacyEvent(String endpoint){
		this.endpoint = endpoint;
		wsdlURL = this.getClass().getResource("/wsdl/SendLegacyEvent.wsdl");
		if(wsdlURL==null){
			System.out.println("URL null");
		}
		ss = new SendLegacyEvent(wsdlURL , SERVICE_NAME);
		port = ss.getSendLegacyEventPort();
		BindingProvider provider = (BindingProvider) port;
		provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY , endpoint);
		//Armar el ObjectFactory?
		factory = new ObjectFactory();
	}
	
	public SendLegacyEventResponse enviaNotificacionPago(String correo, String ordenDeCompra, boolean simulado, String tipoNotificacion, ResourceRequest request) throws Exception{
		
		log.debug("Metodo enviaNotificacionPago : " + correo + " , " + ordenDeCompra + " , " + simulado);
		log.debug("Tipo de notificacion : " + tipoNotificacion);
		
		try{
			if (tipoNotificacion.equals(Constantes.NOTIFICACION_VENTA)){
				return this.enviaComprobanteDeVenta(correo, ordenDeCompra, simulado, tipoNotificacion, request);
			}
			else if (tipoNotificacion.equals(Constantes.NOTIFICACION_TICKET_PRESENCIAL)){
				return this.enviaComprobanteTicketPresencial(correo, ordenDeCompra, simulado, tipoNotificacion, request);
			}
			else if (tipoNotificacion.equals(Constantes.NOTIFICACION_PAGO)){
				return this.enviaComprobantePagoCuenta(correo, ordenDeCompra, simulado, tipoNotificacion, request);
			}
			else {
				return null;
			}
		}
		catch(Exception e){
			throw e;
		}
	}
	
	private SendLegacyEventResponse enviaComprobanteTicketPresencial(String correo, String ordenDeCompra, boolean simulado, String tipoNotificacion, ResourceRequest request) throws Exception{
		SendLegacyEventResponse response = null;
		
		try{
			DBSupport dbSupport = new DBSupport();
			CierreTransaccion cierre = null;
			if(simulado){
				cierre = dbSupport.getDatosCierreSimulado(request);
			}
			else{
				cierre = dbSupport.getDatosCierre(ordenDeCompra);
			}
			log.debug("Llenando datos notificacion desde cierre");
			int monto = 0;
			String aux = cierre.getMonto();
			try{
				log.debug("Monto obtenido desde cierre : " + cierre.getMonto());
				aux = aux.replace(".", "");
				monto = Integer.parseInt(aux);
			}
			catch(Exception e){
				log.error("Error al parsear el monto para comprobante mail : " + cierre.getMonto() + "/" + aux);
				monto = 0;
			}			
			
			String triggeredSystem = "PORTAL DE PAGOS";
			String notificationType = tipoNotificacion;
			String identificationType = "RUT" ;
			log.debug("El numero de pago es : " + cierre.getIdentificadorDePago());
			String identificationCode = "5";
			String documentID = cierre.getNumeroDeOrden();
			
			String preferredContactMedia = "EMAIL";
			
			String email = correo;
			String identificationNumber = "11111111-1";
			String numeroTelefono = "56999999999";

			String fechaVigenciaTicket = UtilesPago.formateaFechaValizTicket(SessionUtil.getVariable(request, Constantes.FECHA_VENCIMIENTO_DEUDA));
			
			
			String orderID = SessionUtil.getVariable(request, Constantes.VENTAS_NUMERO_DE_ORDEN);
			
			String paymentMethodID = ordenDeCompra;
			String paymentMethodType = cierre.getMedioDePago();
			String transactionType = cierre.getIdentificadorDePago();
			String transactionCode = cierre.getCodigoTransaccion();
			
			
			String fechaPago = cierre.getFechaPago();
			
			String quotaType = cierre.getTipoCuotas();
			
			Long numCuotasParse =  0L;
			try{
				numCuotasParse = Long.parseLong(cierre.getNumeroCuotas());
			}
			catch(Exception e){
				log.error("Error al parsear el numero de cuotas : " + cierre.getNumeroCuotas());
			}
			
			BigInteger numberOfQuotas = BigInteger.valueOf(numCuotasParse);
			BigInteger quotaNumber = BigInteger.valueOf(7L);
			String authorizationCode = cierre.getCodigoAutorizacion();
			String creditCardType = cierre.getTipoTarjeta();
			String securityCode = cierre.getUltimosDigitosTarjeta();
			
			
			log.debug("Invocando sendLegacyEvent...");
	        SendLegacyEventRequest _sendLegacyEvent_sendLegacyEventRequest = new SendLegacyEventRequest();
	        TEFHeaderRequest _sendLegacyEvent_sendLegacyEventRequestHeader = new TEFHeaderRequest();
	        //Llenando cabecera
	        fillSendLegacyEventHeader(_sendLegacyEvent_sendLegacyEventRequestHeader);
	        //Seteando cabecera
	        _sendLegacyEvent_sendLegacyEventRequest.setHeader(_sendLegacyEvent_sendLegacyEventRequestHeader);
	        
	        DataReq _sendLegacyEvent_sendLegacyEventRequestData = new DataReq();
	        javax.xml.bind.JAXBElement<java.lang.Long> _sendLegacyEvent_sendLegacyEventRequestDataNotificationId = null;
	        _sendLegacyEvent_sendLegacyEventRequestData.setNotificationId(_sendLegacyEvent_sendLegacyEventRequestDataNotificationId);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataExternalID = null;
	        _sendLegacyEvent_sendLegacyEventRequestData.setExternalID(_sendLegacyEvent_sendLegacyEventRequestDataExternalID);
	        _sendLegacyEvent_sendLegacyEventRequestData.setTriggeringSystem(triggeredSystem);
	        _sendLegacyEvent_sendLegacyEventRequestData.setNotificationType(notificationType);
	        cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber _sendLegacyEvent_sendLegacyEventRequestDataIdentification = new cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber();
	        _sendLegacyEvent_sendLegacyEventRequestDataIdentification.setIdentificationType(identificationType);
	        _sendLegacyEvent_sendLegacyEventRequestDataIdentification.setIdentificationNumber(identificationNumber);

	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataIdentificationIdentificationCode = new cl.telefonica.enterpriseapplicationintegration.tefheader.v1.ObjectFactory().createTEFChileanIdentificationNumberIdentificationCode(identificationCode);
	        _sendLegacyEvent_sendLegacyEventRequestDataIdentification.setIdentificationCode(_sendLegacyEvent_sendLegacyEventRequestDataIdentificationIdentificationCode);
	        _sendLegacyEvent_sendLegacyEventRequestData.setIdentification(_sendLegacyEvent_sendLegacyEventRequestDataIdentification);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataPreferredContactMedia = factory.createDataReqPreferredContactMedia(preferredContactMedia);
	        _sendLegacyEvent_sendLegacyEventRequestData.setPreferredContactMedia(_sendLegacyEvent_sendLegacyEventRequestDataPreferredContactMedia);
	        
	        
	        
	        EmailContact emailContactValue = new EmailContact();
	        emailContactValue.setEMailAddress(factory.createEmailContactEMailAddress(email));
	        javax.xml.bind.JAXBElement<EmailContact> _sendLegacyEvent_sendLegacyEventRequestDataEmail = factory.createDataReqEmail(emailContactValue);
	        _sendLegacyEvent_sendLegacyEventRequestData.setEmail(_sendLegacyEvent_sendLegacyEventRequestDataEmail);
	        
	        
	        TelephoneNumber telefonoNumberValue = new TelephoneNumber();
	        telefonoNumberValue.setNumber(factory.createTelephoneNumberNumber(numeroTelefono));
	        javax.xml.bind.JAXBElement<TelephoneNumber> _sendLegacyEvent_sendLegacyEventRequestDataTelephoneNumber = factory.createDataReqTelephoneNumber(telefonoNumberValue);
	        _sendLegacyEvent_sendLegacyEventRequestData.setTelephoneNumber(_sendLegacyEvent_sendLegacyEventRequestDataTelephoneNumber);
	        
	        
	        // Parametros de la plantilla
	        TemplateParameters _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters = new TemplateParameters();
	        
	        // No se usan....
	        javax.xml.bind.JAXBElement<ProductOffering> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOffering = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setProductOffering(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOffering);
	        javax.xml.bind.JAXBElement<Product> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProduct = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setProduct(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProduct);
	        javax.xml.bind.JAXBElement<Product> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOld = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setProductOld(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOld);
	        javax.xml.bind.JAXBElement<LoyaltyBalance> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersLoyaltyBalance = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setLoyaltyBalance(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersLoyaltyBalance);
	        javax.xml.bind.JAXBElement<Service> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersService = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setService(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersService);
	        javax.xml.bind.JAXBElement<ServiceCharacteristicValue> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersServiceCharacteristicValue = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setServiceCharacteristicValue(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersServiceCharacteristicValue);
	        javax.xml.bind.JAXBElement<Resource> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersResource = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setResource(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersResource);
	        javax.xml.bind.JAXBElement<GeographicAddress> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersGeographicAddress = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setGeographicAddress(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersGeographicAddress);
	        javax.xml.bind.JAXBElement<UserAbstract> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUserAbstract = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setUserAbstract(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUserAbstract);
	        
	        
	        
	        DocumentAbstract docAbs = factory.createDocumentAbstract();
	        //
	        
	        javax.xml.bind.JAXBElement<String> value = factory.createDocumentAbstractDocumentID(documentID);
	        docAbs.setDocumentID(value);
	        
	        
	        //
	        javax.xml.bind.JAXBElement<DocumentAbstract> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDocumentAbstract = factory.createTemplateParametersDocumentAbstract(docAbs);
	        
	        
	        
	        
	        
	        
	        
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setDocumentAbstract(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDocumentAbstract);
	        javax.xml.bind.JAXBElement<ActivityAbstract> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersActivityAbstract = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setActivityAbstract(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersActivityAbstract);
	        javax.xml.bind.JAXBElement<TEFChileanIdentificationNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerIdentification = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerIdentification(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerIdentification);
	        javax.xml.bind.JAXBElement<TEFChileanIdentificationNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersReceiverIdentification = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setReceiverIdentification(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersReceiverIdentification);
	        javax.xml.bind.JAXBElement<TEFChileanIdentificationNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactIdentification = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setContactIdentification(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactIdentification);
	        javax.xml.bind.JAXBElement<IndividualName> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersIndividualName = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setIndividualName(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersIndividualName);
	        javax.xml.bind.JAXBElement<ContactMedium> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactMedium = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setContactMedium(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactMedium);
	        javax.xml.bind.JAXBElement<Customer> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomer = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomer(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomer);
	        javax.xml.bind.JAXBElement<CustomerAccount> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerAccount = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerAccount(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerAccount);
	        javax.xml.bind.JAXBElement<CustomerCreditProfile> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerCreditProfile = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerCreditProfile(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerCreditProfile);
	        
	        
	        //Seteo customer order
	        CustomerOrder customerOrder = new CustomerOrder();
	        customerOrder.setOrderID(factory.createCustomerOrderOrderID(orderID));
	        javax.xml.bind.JAXBElement<CustomerOrder> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrder = factory.createTemplateParametersCustomerOrder(customerOrder);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerOrder(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrder);
	        
	        
	        javax.xml.bind.JAXBElement<CustomerOrderItem> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrderItem = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerOrderItem(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrderItem);
	        
	        // Seteo fecha
	        //_sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setMessage(value);
	        
	        
	        PaymentMethod payment = new PaymentMethod();
	        // Setiar valores al payment
	        
	        
	        payment.setID(factory.createPaymentMethodID(paymentMethodID)); 
	        payment.setPaymentMethodType(factory.createPaymentMethodPaymentMethodType(paymentMethodType));
	        payment.setTransactionType(factory.createPaymentMethodTransactionType(transactionType));
	        payment.setTransactionCode(factory.createPaymentMethodTransactionCode(transactionCode));
	        payment.setQuotaType(factory.createPaymentMethodQuotaType(quotaType));
	        payment.setNumberOfQuotas(factory.createPaymentMethodNumberOfQuotas(numberOfQuotas));
	        payment.setQuotaNumber(factory.createPaymentMethodQuotaNumber(quotaNumber));
	        payment.setAuthorizationCode(factory.createPaymentMethodAuthorizationCode(authorizationCode));
	        payment.setCreditCardType(factory.createPaymentMethodCreditCardType(creditCardType));
	        payment.setSecurityCode(factory.createPaymentMethodSecurityCode(securityCode));
	        
	        
	        Money montoTicketPresencial = factory.createMoney();
	        log.debug("Monto INT al WS : " + monto);
	        Long montoLong1 = Long.valueOf(monto);
	        BigDecimal bigDecimal1 =  BigDecimal.valueOf(montoLong1);
	        log.debug("Monto BIG decimal al WS : " + bigDecimal1); 
	        log.debug("Monto BIG decimal sin escala al WS : " + bigDecimal1.setScale(2));
	        montoTicketPresencial.setAmount(bigDecimal1);
	        payment.setAmount(factory.createPaymentMethodAmount(montoTicketPresencial));
	        
	        
	        
	        javax.xml.bind.JAXBElement<PaymentMethod> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersPaymentMethod = factory.createTemplateParametersPaymentMethod(payment);;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setPaymentMethod(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersPaymentMethod);
	        
	        //
	        javax.xml.bind.JAXBElement<SalesChannel> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersSalesChannel = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setSalesChannel(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersSalesChannel);
	        javax.xml.bind.JAXBElement<TelephoneNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactNumber = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setContactNumber(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactNumber);
	        
	        // Seteo email
	        EmailContact emailContact = new EmailContact();
	        emailContact.setEMailAddress(factory.createEmailContactEMailAddress(email));
	        javax.xml.bind.JAXBElement<EmailContact> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersEmailContact = factory.createTemplateParametersEmailContact(emailContact);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setEmailContact(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersEmailContact);
	        
	        
	        // Para vigencia del ticket
	        TimePeriod timePeriod = new TimePeriod();
	        XMLGregorianCalendar calendarEndDataTime = javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(fechaVigenciaTicket);
	        timePeriod.setEndDateTime(factory.createTimePeriodEndDateTime(calendarEndDataTime));
	        javax.xml.bind.JAXBElement<TimePeriod> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersTimePeriod = factory.createTemplateParametersTimePeriod(timePeriod);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setTimePeriod(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersTimePeriod);
	        
	        
	        // Seteo amount
	        log.debug("Monto INT al WS : " + monto);
	        Money money = new Money();
	        Long montoLong = Long.valueOf(monto);
	        BigDecimal bigDecimal =  BigDecimal.valueOf(montoLong);
	        log.debug("Monto BIG decimal al WS : " + bigDecimal); 
	        log.debug("Monto BIG decimal sin escala al WS : " + bigDecimal.setScale(2));
	        bigDecimal.setScale(2);
	        money.setAmount(bigDecimal);
	        javax.xml.bind.JAXBElement<Money> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersAmount = factory.createTemplateParametersAmount(money);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setAmount(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersAmount);
	        
	        
	        
	        
	        javax.xml.bind.JAXBElement<XMLGregorianCalendar> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDate = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setDate(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDate);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersMessage = factory.createTemplateParametersMessage(fechaPago);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setMessage(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersMessage);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUrl = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setUrl(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUrl);
	        _sendLegacyEvent_sendLegacyEventRequestData.setTemplateParameters(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters);
	        _sendLegacyEvent_sendLegacyEventRequest.setData(_sendLegacyEvent_sendLegacyEventRequestData);
	        this.imprimir(_sendLegacyEvent_sendLegacyEventRequestData);
	        response = port.sendLegacyEvent(_sendLegacyEvent_sendLegacyEventRequest);
            System.out.println("sendLegacyEvent result = " + response);
		}
		
		catch(SendLegacyEventFault e){
			
			log.error("Ha ocurrido un error en el llamado al WS SendLegacyEvent : " + e);
			log.error("Causa : " + e.getFaultInfo().getDetails().getCause());
		}
		
		catch(Exception e){
			log.debug("Ha ocurrido un error en el llamado al WS SendLegacyEvent : " + e);
		}
		return response;
	}
	
	
	private SendLegacyEventResponse enviaComprobanteDeVenta (String correo, String ordenDeCompra, boolean simulado, String tipoNotificacion, ResourceRequest request) throws Exception{
		SendLegacyEventResponse response = null;
		
		try{
			DBSupport dbSupport = new DBSupport();
			CierreTransaccion cierre = null;
			if(simulado){
				cierre = dbSupport.getDatosCierreSimulado(request);
			}
			else{
				cierre = dbSupport.getDatosCierre(ordenDeCompra);
			}
			log.debug("Llenando datos notificacion desde cierre");
			int monto = 0;
			String aux = cierre.getMonto();
			try{
				log.debug("Monto obtenido desde cierre : " + cierre.getMonto());
				aux = aux.replace(".", "");
				monto = Integer.parseInt(aux);
			}
			catch(Exception e){
				log.error("Error al parsear el monto para comprobante mail : " + cierre.getMonto() + "/" + aux);
				monto = 0;
			}			
			
			String triggeredSystem = "PORTAL DE PAGOS";
			String notificationType = tipoNotificacion;
			String identificationType = "RUT" ;
			log.debug("El numero de pago es : " + cierre.getIdentificadorDePago());
			String identificationCode = "5";
			String documentID = cierre.getNumeroDeOrden();
			String preferredContactMedia = "EMAIL";
			String email = correo;
			String identificationNumber = "11111111-1";
			String numeroTelefono = "56999999999";
			String orderID = SessionUtil.getVariable(request, Constantes.VENTAS_NUMERO_DE_ORDEN);
			
			String paymentMethodID = "123456789";
			String paymentMethodType = cierre.getMedioDePago();
			String transactionType = cierre.getIdentificadorDePago() ;
			String transactionCode = cierre.getCodigoTransaccion();
			
			
			String fechaPago = cierre.getFechaPago();
			
			String quotaType = cierre.getTipoCuotas();
			
			Long numCuotasParse =  0L;
			try{
				numCuotasParse = Long.parseLong(cierre.getNumeroCuotas());
			}
			catch(Exception e){
				log.error("Error al parsear el numero de cuotas : " + cierre.getNumeroCuotas());
			}
			
			BigInteger numberOfQuotas = BigInteger.valueOf(numCuotasParse);
			BigInteger quotaNumber = BigInteger.valueOf(7L);
			String authorizationCode = cierre.getCodigoAutorizacion();
			String creditCardType = cierre.getTipoTarjeta();
			String securityCode = cierre.getUltimosDigitosTarjeta();
			
			
			log.debug("Invocando sendLegacyEvent...");
	        SendLegacyEventRequest _sendLegacyEvent_sendLegacyEventRequest = new SendLegacyEventRequest();
	        TEFHeaderRequest _sendLegacyEvent_sendLegacyEventRequestHeader = new TEFHeaderRequest();
	        //Llenando cabecera
	        fillSendLegacyEventHeader(_sendLegacyEvent_sendLegacyEventRequestHeader);
	        //Seteando cabecera
	        _sendLegacyEvent_sendLegacyEventRequest.setHeader(_sendLegacyEvent_sendLegacyEventRequestHeader);
	        
	        DataReq _sendLegacyEvent_sendLegacyEventRequestData = new DataReq();
	        javax.xml.bind.JAXBElement<java.lang.Long> _sendLegacyEvent_sendLegacyEventRequestDataNotificationId = null;
	        _sendLegacyEvent_sendLegacyEventRequestData.setNotificationId(_sendLegacyEvent_sendLegacyEventRequestDataNotificationId);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataExternalID = null;
	        _sendLegacyEvent_sendLegacyEventRequestData.setExternalID(_sendLegacyEvent_sendLegacyEventRequestDataExternalID);
	        _sendLegacyEvent_sendLegacyEventRequestData.setTriggeringSystem(triggeredSystem);
	        _sendLegacyEvent_sendLegacyEventRequestData.setNotificationType(notificationType);
	        cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber _sendLegacyEvent_sendLegacyEventRequestDataIdentification = new cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber();
	        _sendLegacyEvent_sendLegacyEventRequestDataIdentification.setIdentificationType(identificationType);
	        _sendLegacyEvent_sendLegacyEventRequestDataIdentification.setIdentificationNumber(identificationNumber);

	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataIdentificationIdentificationCode = new cl.telefonica.enterpriseapplicationintegration.tefheader.v1.ObjectFactory().createTEFChileanIdentificationNumberIdentificationCode(identificationCode);
	        _sendLegacyEvent_sendLegacyEventRequestDataIdentification.setIdentificationCode(_sendLegacyEvent_sendLegacyEventRequestDataIdentificationIdentificationCode);
	        _sendLegacyEvent_sendLegacyEventRequestData.setIdentification(_sendLegacyEvent_sendLegacyEventRequestDataIdentification);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataPreferredContactMedia = factory.createDataReqPreferredContactMedia(preferredContactMedia);
	        _sendLegacyEvent_sendLegacyEventRequestData.setPreferredContactMedia(_sendLegacyEvent_sendLegacyEventRequestDataPreferredContactMedia);
	        
	        
	        
	        EmailContact emailContactValue = new EmailContact();
	        emailContactValue.setEMailAddress(factory.createEmailContactEMailAddress(email));
	        javax.xml.bind.JAXBElement<EmailContact> _sendLegacyEvent_sendLegacyEventRequestDataEmail = factory.createDataReqEmail(emailContactValue);
	        _sendLegacyEvent_sendLegacyEventRequestData.setEmail(_sendLegacyEvent_sendLegacyEventRequestDataEmail);
	        
	        
	        TelephoneNumber telefonoNumberValue = new TelephoneNumber();
	        telefonoNumberValue.setNumber(factory.createTelephoneNumberNumber(numeroTelefono));
	        javax.xml.bind.JAXBElement<TelephoneNumber> _sendLegacyEvent_sendLegacyEventRequestDataTelephoneNumber = factory.createDataReqTelephoneNumber(telefonoNumberValue);
	        _sendLegacyEvent_sendLegacyEventRequestData.setTelephoneNumber(_sendLegacyEvent_sendLegacyEventRequestDataTelephoneNumber);
	        
	        
	        // Parametros de la plantilla
	        TemplateParameters _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters = new TemplateParameters();
	        
	        // No se usan....
	        javax.xml.bind.JAXBElement<ProductOffering> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOffering = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setProductOffering(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOffering);
	        javax.xml.bind.JAXBElement<Product> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProduct = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setProduct(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProduct);
	        javax.xml.bind.JAXBElement<Product> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOld = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setProductOld(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOld);
	        javax.xml.bind.JAXBElement<LoyaltyBalance> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersLoyaltyBalance = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setLoyaltyBalance(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersLoyaltyBalance);
	        javax.xml.bind.JAXBElement<Service> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersService = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setService(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersService);
	        javax.xml.bind.JAXBElement<ServiceCharacteristicValue> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersServiceCharacteristicValue = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setServiceCharacteristicValue(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersServiceCharacteristicValue);
	        javax.xml.bind.JAXBElement<Resource> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersResource = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setResource(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersResource);
	        javax.xml.bind.JAXBElement<GeographicAddress> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersGeographicAddress = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setGeographicAddress(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersGeographicAddress);
	        javax.xml.bind.JAXBElement<UserAbstract> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUserAbstract = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setUserAbstract(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUserAbstract);
	        
	        
	        
	        DocumentAbstract docAbs = factory.createDocumentAbstract();
	        //
	        
	        javax.xml.bind.JAXBElement<String> value = factory.createDocumentAbstractDocumentID(documentID);
	        docAbs.setDocumentID(value);
	        
	        
	        //
	        javax.xml.bind.JAXBElement<DocumentAbstract> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDocumentAbstract = factory.createTemplateParametersDocumentAbstract(docAbs);
	        
	        
	        
	        
	        
	        
	        
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setDocumentAbstract(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDocumentAbstract);
	        javax.xml.bind.JAXBElement<ActivityAbstract> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersActivityAbstract = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setActivityAbstract(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersActivityAbstract);
	        javax.xml.bind.JAXBElement<TEFChileanIdentificationNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerIdentification = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerIdentification(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerIdentification);
	        javax.xml.bind.JAXBElement<TEFChileanIdentificationNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersReceiverIdentification = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setReceiverIdentification(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersReceiverIdentification);
	        javax.xml.bind.JAXBElement<TEFChileanIdentificationNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactIdentification = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setContactIdentification(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactIdentification);
	        javax.xml.bind.JAXBElement<IndividualName> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersIndividualName = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setIndividualName(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersIndividualName);
	        javax.xml.bind.JAXBElement<ContactMedium> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactMedium = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setContactMedium(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactMedium);
	        javax.xml.bind.JAXBElement<Customer> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomer = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomer(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomer);
	        javax.xml.bind.JAXBElement<CustomerAccount> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerAccount = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerAccount(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerAccount);
	        javax.xml.bind.JAXBElement<CustomerCreditProfile> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerCreditProfile = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerCreditProfile(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerCreditProfile);
	        
	        
	        //Seteo customer order
	        CustomerOrder customerOrder = new CustomerOrder();
	        customerOrder.setOrderID(factory.createCustomerOrderOrderID(orderID));
	        javax.xml.bind.JAXBElement<CustomerOrder> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrder = factory.createTemplateParametersCustomerOrder(customerOrder);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerOrder(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrder);
	        
	        
	        javax.xml.bind.JAXBElement<CustomerOrderItem> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrderItem = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerOrderItem(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrderItem);
	        
	        // Seteo fecha
	        //_sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setMessage(value);
	        
	        
	        PaymentMethod payment = new PaymentMethod();
	        // Setiar valores al payment
	        
	        
	        payment.setID(factory.createPaymentMethodID(paymentMethodID));
	        payment.setPaymentMethodType(factory.createPaymentMethodPaymentMethodType(paymentMethodType));
	        payment.setTransactionType(factory.createPaymentMethodTransactionType(transactionType));
	        payment.setTransactionCode(factory.createPaymentMethodTransactionCode(transactionCode));
	        payment.setQuotaType(factory.createPaymentMethodQuotaType(quotaType));
	        payment.setNumberOfQuotas(factory.createPaymentMethodNumberOfQuotas(numberOfQuotas));
	        payment.setQuotaNumber(factory.createPaymentMethodQuotaNumber(quotaNumber));
	        payment.setAuthorizationCode(factory.createPaymentMethodAuthorizationCode(authorizationCode));
	        payment.setCreditCardType(factory.createPaymentMethodCreditCardType(creditCardType));
	        payment.setSecurityCode(factory.createPaymentMethodSecurityCode(securityCode));
	        
	        
	        javax.xml.bind.JAXBElement<PaymentMethod> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersPaymentMethod = factory.createTemplateParametersPaymentMethod(payment);;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setPaymentMethod(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersPaymentMethod);
	        
	        //
	        javax.xml.bind.JAXBElement<SalesChannel> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersSalesChannel = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setSalesChannel(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersSalesChannel);
	        javax.xml.bind.JAXBElement<TelephoneNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactNumber = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setContactNumber(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactNumber);
	        
	        // Seteo email
	        EmailContact emailContact = new EmailContact();
	        emailContact.setEMailAddress(factory.createEmailContactEMailAddress(email));
	        javax.xml.bind.JAXBElement<EmailContact> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersEmailContact = factory.createTemplateParametersEmailContact(emailContact);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setEmailContact(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersEmailContact);
	        
	        
	        // No se usa
	        javax.xml.bind.JAXBElement<TimePeriod> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersTimePeriod = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setTimePeriod(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersTimePeriod);
	        
	        
	        // Seteo amount
	        log.debug("Monto INT al WS : " + monto);
	        Money money = new Money();
	        Long montoLong = Long.valueOf(monto);
	        BigDecimal bigDecimal =  BigDecimal.valueOf(montoLong);
	        log.debug("Monto BIG decimal al WS : " + bigDecimal); 
	        log.debug("Monto BIG decimal sin escala al WS : " + bigDecimal.setScale(2));
	        bigDecimal.setScale(2);
	        money.setAmount(bigDecimal);
	        javax.xml.bind.JAXBElement<Money> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersAmount = factory.createTemplateParametersAmount(money);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setAmount(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersAmount);
	        
	        
	        
	        
	        javax.xml.bind.JAXBElement<XMLGregorianCalendar> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDate = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setDate(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDate);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersMessage = factory.createTemplateParametersMessage(fechaPago);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setMessage(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersMessage);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUrl = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setUrl(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUrl);
	        _sendLegacyEvent_sendLegacyEventRequestData.setTemplateParameters(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters);
	        _sendLegacyEvent_sendLegacyEventRequest.setData(_sendLegacyEvent_sendLegacyEventRequestData);
	        this.imprimir(_sendLegacyEvent_sendLegacyEventRequestData);
	        response = port.sendLegacyEvent(_sendLegacyEvent_sendLegacyEventRequest);
            System.out.println("sendLegacyEvent result = " + response);
		}
		
		catch(SendLegacyEventFault e){
			
			log.error("Ha ocurrido un error en el llamado al WS SendLegacyEvent : " + e);
			log.error("Causa : " + e.getFaultInfo().getDetails().getCause());
		}
		
		catch(Exception e){
			log.debug("Ha ocurrido un error en el llamado al WS SendLegacyEvent : " + e);
		}
		return response;
	}
	
	private SendLegacyEventResponse enviaComprobantePagoCuenta (String correo, String ordenDeCompra, boolean simulado, String tipoNotificacion, ResourceRequest request) throws Exception{
		SendLegacyEventResponse response = null;
		
		try{
			DBSupport dbSupport = new DBSupport();
			CierreTransaccion cierre = null;
			if(simulado){
				cierre = dbSupport.getDatosCierreSimulado(request);
			}
			else{
				cierre = dbSupport.getDatosCierre(ordenDeCompra);
			}
			log.debug("Llenando datos notificacion desde cierre");
			int monto = 0;
			String aux = cierre.getMonto();
			try{
				log.debug("Monto obtenido desde cierre : " + cierre.getMonto());
				aux = aux.replace(".", "");
				monto = Integer.parseInt(aux);
			}
			catch(Exception e){
				log.error("Error al parsear el monto para comprobante mail : " + cierre.getMonto() + "/" + aux);
				monto = 0;
			}			
			
			String triggeredSystem = "PORTAL DE PAGOS";
			String notificationType = tipoNotificacion;
			String identificationType = "RUT" ;

			log.debug("El numero de pago es : " + cierre.getIdentificadorDePago());
			String identificationCode = "5";
			String documentID = cierre.getNumeroDeOrden();
			String preferredContactMedia = "EMAIL";
			String email = correo;
			String identificationNumber = "11111111-1";
			String numeroTelefono = "56999999999";
			String orderID = SessionUtil.getVariable(request, Constantes.VENTAS_NUMERO_DE_ORDEN);
			
			String paymentMethodID = "123456789";
			String paymentMethodType = cierre.getMedioDePago();
			String transactionType = cierre.getIdentificadorDePago() ;
			String transactionCode = cierre.getCodigoTransaccion();
			
			
			String fechaPago = cierre.getFechaPago();
			
			String quotaType = cierre.getTipoCuotas();
			
			Long numCuotasParse =  0L;
			try{
				numCuotasParse = Long.parseLong(cierre.getNumeroCuotas());
			}
			catch(Exception e){
				log.error("Error al parsear el numero de cuotas : " + cierre.getNumeroCuotas());
			}
			
			BigInteger numberOfQuotas = BigInteger.valueOf(numCuotasParse);
			BigInteger quotaNumber = BigInteger.valueOf(7L);
			String authorizationCode = cierre.getCodigoAutorizacion();
			String creditCardType = cierre.getTipoTarjeta();
			String securityCode = cierre.getUltimosDigitosTarjeta();
			
			
			log.debug("Invocando sendLegacyEvent...");
	        SendLegacyEventRequest _sendLegacyEvent_sendLegacyEventRequest = new SendLegacyEventRequest();
	        TEFHeaderRequest _sendLegacyEvent_sendLegacyEventRequestHeader = new TEFHeaderRequest();
	        //Llenando cabecera
	        fillSendLegacyEventHeader(_sendLegacyEvent_sendLegacyEventRequestHeader);
	        //Seteando cabecera
	        _sendLegacyEvent_sendLegacyEventRequest.setHeader(_sendLegacyEvent_sendLegacyEventRequestHeader);
	        
	        DataReq _sendLegacyEvent_sendLegacyEventRequestData = new DataReq();
	        javax.xml.bind.JAXBElement<java.lang.Long> _sendLegacyEvent_sendLegacyEventRequestDataNotificationId = null;
	        _sendLegacyEvent_sendLegacyEventRequestData.setNotificationId(_sendLegacyEvent_sendLegacyEventRequestDataNotificationId);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataExternalID = null;
	        _sendLegacyEvent_sendLegacyEventRequestData.setExternalID(_sendLegacyEvent_sendLegacyEventRequestDataExternalID);
	        _sendLegacyEvent_sendLegacyEventRequestData.setTriggeringSystem(triggeredSystem);
	        _sendLegacyEvent_sendLegacyEventRequestData.setNotificationType(notificationType);
	        cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber _sendLegacyEvent_sendLegacyEventRequestDataIdentification = new cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber();
	        _sendLegacyEvent_sendLegacyEventRequestDataIdentification.setIdentificationType(identificationType);
	        _sendLegacyEvent_sendLegacyEventRequestDataIdentification.setIdentificationNumber(identificationNumber);

	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataIdentificationIdentificationCode = new cl.telefonica.enterpriseapplicationintegration.tefheader.v1.ObjectFactory().createTEFChileanIdentificationNumberIdentificationCode(identificationCode);
	        _sendLegacyEvent_sendLegacyEventRequestDataIdentification.setIdentificationCode(_sendLegacyEvent_sendLegacyEventRequestDataIdentificationIdentificationCode);
	        _sendLegacyEvent_sendLegacyEventRequestData.setIdentification(_sendLegacyEvent_sendLegacyEventRequestDataIdentification);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataPreferredContactMedia = factory.createDataReqPreferredContactMedia(preferredContactMedia);
	        _sendLegacyEvent_sendLegacyEventRequestData.setPreferredContactMedia(_sendLegacyEvent_sendLegacyEventRequestDataPreferredContactMedia);
	        
	        
	        
	        EmailContact emailContactValue = new EmailContact();
	        emailContactValue.setEMailAddress(factory.createEmailContactEMailAddress(email));
	        javax.xml.bind.JAXBElement<EmailContact> _sendLegacyEvent_sendLegacyEventRequestDataEmail = factory.createDataReqEmail(emailContactValue);
	        _sendLegacyEvent_sendLegacyEventRequestData.setEmail(_sendLegacyEvent_sendLegacyEventRequestDataEmail);
	        
	        
	        TelephoneNumber telefonoNumberValue = new TelephoneNumber();
	        telefonoNumberValue.setNumber(factory.createTelephoneNumberNumber(numeroTelefono));
	        javax.xml.bind.JAXBElement<TelephoneNumber> _sendLegacyEvent_sendLegacyEventRequestDataTelephoneNumber = factory.createDataReqTelephoneNumber(telefonoNumberValue);
	        _sendLegacyEvent_sendLegacyEventRequestData.setTelephoneNumber(_sendLegacyEvent_sendLegacyEventRequestDataTelephoneNumber);
	        
	        
	        // Parametros de la plantilla
	        TemplateParameters _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters = new TemplateParameters();
	        
	        // No se usan....
	        javax.xml.bind.JAXBElement<ProductOffering> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOffering = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setProductOffering(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOffering);
	        javax.xml.bind.JAXBElement<Product> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProduct = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setProduct(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProduct);
	        javax.xml.bind.JAXBElement<Product> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOld = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setProductOld(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersProductOld);
	        javax.xml.bind.JAXBElement<LoyaltyBalance> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersLoyaltyBalance = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setLoyaltyBalance(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersLoyaltyBalance);
	        javax.xml.bind.JAXBElement<Service> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersService = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setService(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersService);
	        javax.xml.bind.JAXBElement<ServiceCharacteristicValue> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersServiceCharacteristicValue = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setServiceCharacteristicValue(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersServiceCharacteristicValue);
	        javax.xml.bind.JAXBElement<Resource> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersResource = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setResource(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersResource);
	        javax.xml.bind.JAXBElement<GeographicAddress> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersGeographicAddress = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setGeographicAddress(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersGeographicAddress);
	        javax.xml.bind.JAXBElement<UserAbstract> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUserAbstract = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setUserAbstract(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUserAbstract);
	        
	        
	        
	        DocumentAbstract docAbs = factory.createDocumentAbstract();
	        //
	        
	        javax.xml.bind.JAXBElement<String> value = factory.createDocumentAbstractDocumentID(documentID);
	        docAbs.setDocumentID(value);
	        
	        
	        //
	        javax.xml.bind.JAXBElement<DocumentAbstract> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDocumentAbstract = factory.createTemplateParametersDocumentAbstract(docAbs);
	        
	        
	        
	        
	        
	        
	        
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setDocumentAbstract(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDocumentAbstract);
	        javax.xml.bind.JAXBElement<ActivityAbstract> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersActivityAbstract = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setActivityAbstract(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersActivityAbstract);
	        javax.xml.bind.JAXBElement<TEFChileanIdentificationNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerIdentification = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerIdentification(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerIdentification);
	        javax.xml.bind.JAXBElement<TEFChileanIdentificationNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersReceiverIdentification = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setReceiverIdentification(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersReceiverIdentification);
	        javax.xml.bind.JAXBElement<TEFChileanIdentificationNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactIdentification = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setContactIdentification(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactIdentification);
	        javax.xml.bind.JAXBElement<IndividualName> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersIndividualName = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setIndividualName(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersIndividualName);
	        javax.xml.bind.JAXBElement<ContactMedium> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactMedium = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setContactMedium(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactMedium);
	        javax.xml.bind.JAXBElement<Customer> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomer = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomer(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomer);
	        javax.xml.bind.JAXBElement<CustomerAccount> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerAccount = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerAccount(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerAccount);
	        javax.xml.bind.JAXBElement<CustomerCreditProfile> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerCreditProfile = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerCreditProfile(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerCreditProfile);
	        
	        
	        //Seteo customer order
	        CustomerOrder customerOrder = new CustomerOrder();
	        customerOrder.setOrderID(factory.createCustomerOrderOrderID(orderID));
	        javax.xml.bind.JAXBElement<CustomerOrder> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrder = factory.createTemplateParametersCustomerOrder(customerOrder);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerOrder(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrder);
	        
	        
	        javax.xml.bind.JAXBElement<CustomerOrderItem> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrderItem = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setCustomerOrderItem(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersCustomerOrderItem);
	        
	        // Seteo fecha
	        //_sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setMessage(value);
	        
	        
	        PaymentMethod payment = new PaymentMethod();
	        // Setiar valores al payment
	        
	        
	        payment.setID(factory.createPaymentMethodID(paymentMethodID));
	        payment.setPaymentMethodType(factory.createPaymentMethodPaymentMethodType(paymentMethodType));
	        payment.setTransactionType(factory.createPaymentMethodTransactionType(transactionType));
	        payment.setTransactionCode(factory.createPaymentMethodTransactionCode(transactionCode));
	        payment.setQuotaType(factory.createPaymentMethodQuotaType(quotaType));
	        payment.setNumberOfQuotas(factory.createPaymentMethodNumberOfQuotas(numberOfQuotas));
	        payment.setQuotaNumber(factory.createPaymentMethodQuotaNumber(quotaNumber));
	        payment.setAuthorizationCode(factory.createPaymentMethodAuthorizationCode(authorizationCode));
	        payment.setCreditCardType(factory.createPaymentMethodCreditCardType(creditCardType));
	        payment.setSecurityCode(factory.createPaymentMethodSecurityCode(securityCode));
	        
	        
	        javax.xml.bind.JAXBElement<PaymentMethod> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersPaymentMethod = factory.createTemplateParametersPaymentMethod(payment);;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setPaymentMethod(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersPaymentMethod);
	        
	        //
	        javax.xml.bind.JAXBElement<SalesChannel> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersSalesChannel = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setSalesChannel(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersSalesChannel);
	        javax.xml.bind.JAXBElement<TelephoneNumber> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactNumber = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setContactNumber(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersContactNumber);
	        
	        // Seteo email
	        EmailContact emailContact = new EmailContact();
	        emailContact.setEMailAddress(factory.createEmailContactEMailAddress(email));
	        javax.xml.bind.JAXBElement<EmailContact> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersEmailContact = factory.createTemplateParametersEmailContact(emailContact);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setEmailContact(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersEmailContact);
	        
	        
	        // No se usa
	        javax.xml.bind.JAXBElement<TimePeriod> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersTimePeriod = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setTimePeriod(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersTimePeriod);
	        
	        
	        // Seteo amount
	        log.debug("Monto INT al WS : " + monto);
	        Money money = new Money();
	        Long montoLong = Long.valueOf(monto);
	        BigDecimal bigDecimal =  BigDecimal.valueOf(montoLong);
	        log.debug("Monto BIG decimal al WS : " + bigDecimal); 
	        log.debug("Monto BIG decimal sin escala al WS : " + bigDecimal.setScale(2));
	        bigDecimal.setScale(2);
	        money.setAmount(bigDecimal);
	        javax.xml.bind.JAXBElement<Money> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersAmount = factory.createTemplateParametersAmount(money);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setAmount(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersAmount);
	        
	        javax.xml.bind.JAXBElement<XMLGregorianCalendar> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDate = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setDate(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersDate);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersMessage = factory.createTemplateParametersMessage(fechaPago);
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setMessage(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersMessage);
	        javax.xml.bind.JAXBElement<String> _sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUrl = null;
	        _sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters.setUrl(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParametersUrl);
	        _sendLegacyEvent_sendLegacyEventRequestData.setTemplateParameters(_sendLegacyEvent_sendLegacyEventRequestDataTemplateParameters);
	        _sendLegacyEvent_sendLegacyEventRequest.setData(_sendLegacyEvent_sendLegacyEventRequestData);
	        this.imprimir(_sendLegacyEvent_sendLegacyEventRequestData);
	        response = port.sendLegacyEvent(_sendLegacyEvent_sendLegacyEventRequest);
		}
		
		catch(SendLegacyEventFault e){
			
			log.error("Ha ocurrido un error en el llamado al WS SendLegacyEvent : " + e);
			log.error("Causa : " + e.getFaultInfo().getDetails().getCause());
		}
		
		catch(Exception e){
			log.debug("Ha ocurrido un error en el llamado al WS SendLegacyEvent : " + e);
		}
		return response;
	}
	
	
	/**
	 * Metodo para imprimir la data dentro del request
	 * @param data Request de llamada al servicio SendLegacy
	 */
	private void imprimir(DataReq data){
		try{
			log.debug("*********************Data dentro del Request************************");
			log.debug("PreferredContactMedia: " + data.getPreferredContactMedia().getValue());
			log.debug("Email: " + data.getEmail().getValue().getEMailAddress().getValue());				
			log.debug("NotificationType: " + data.getNotificationType());
			log.debug("Amount: "+ data.getTemplateParameters().getAmount().getValue().getAmount().toString());
			log.debug("*********************************************************************");
			
		}catch(NullPointerException e){
			log.error(e);
		}

	}
	
	
	
}
