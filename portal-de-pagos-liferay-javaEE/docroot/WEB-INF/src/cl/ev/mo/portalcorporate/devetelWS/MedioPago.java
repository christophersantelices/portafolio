
package cl.ev.mo.portalcorporate.devetelWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para medioPago complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="medioPago"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID_MEDIO_PAGO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IMG_MEDIO_PAGO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NOMBRE_MEDIO_PAGO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="URL_MEDIO_PAGO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "medioPago", propOrder = {
    "idmediopago",
    "imgmediopago",
    "nombremediopago",
    "urlmediopago"
})
public class MedioPago {

    @XmlElement(name = "ID_MEDIO_PAGO")
    protected String idmediopago;
    @XmlElement(name = "IMG_MEDIO_PAGO")
    protected String imgmediopago;
    @XmlElement(name = "NOMBRE_MEDIO_PAGO")
    protected String nombremediopago;
    @XmlElement(name = "URL_MEDIO_PAGO")
    protected String urlmediopago;

    /**
     * Obtiene el valor de la propiedad idmediopago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDMEDIOPAGO() {
        return idmediopago;
    }

    /**
     * Define el valor de la propiedad idmediopago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDMEDIOPAGO(String value) {
        this.idmediopago = value;
    }

    /**
     * Obtiene el valor de la propiedad imgmediopago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMGMEDIOPAGO() {
        return imgmediopago;
    }

    /**
     * Define el valor de la propiedad imgmediopago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMGMEDIOPAGO(String value) {
        this.imgmediopago = value;
    }

    /**
     * Obtiene el valor de la propiedad nombremediopago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOMBREMEDIOPAGO() {
        return nombremediopago;
    }

    /**
     * Define el valor de la propiedad nombremediopago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOMBREMEDIOPAGO(String value) {
        this.nombremediopago = value;
    }

    /**
     * Obtiene el valor de la propiedad urlmediopago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getURLMEDIOPAGO() {
        return urlmediopago;
    }

    /**
     * Define el valor de la propiedad urlmediopago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setURLMEDIOPAGO(String value) {
        this.urlmediopago = value;
    }

}
