
package cl.ev.mo.portalcorporate.devetelWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para detalleCompra complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="detalleCompra"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CANT_PRODUCTOS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FECHA_VENC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MONTO_TOTAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NUM_ORDEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PARAM_RESP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productos" type="{http://ws/}producto" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TIPO_DOC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "detalleCompra", propOrder = {
    "cantproductos",
    "fechavenc",
    "montototal",
    "numorden",
    "paramresp",
    "productos",
    "tipodoc"
})
public class DetalleCompra {

    @XmlElement(name = "CANT_PRODUCTOS")
    protected String cantproductos;
    @XmlElement(name = "FECHA_VENC")
    protected String fechavenc;
    @XmlElement(name = "MONTO_TOTAL")
    protected String montototal;
    @XmlElement(name = "NUM_ORDEN")
    protected String numorden;
    @XmlElement(name = "PARAM_RESP")
    protected String paramresp;
    @XmlElement(nillable = true)
    protected List<Producto> productos;
    @XmlElement(name = "TIPO_DOC")
    protected String tipodoc;

    /**
     * Obtiene el valor de la propiedad cantproductos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCANTPRODUCTOS() {
        return cantproductos;
    }

    /**
     * Define el valor de la propiedad cantproductos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCANTPRODUCTOS(String value) {
        this.cantproductos = value;
    }

    /**
     * Obtiene el valor de la propiedad fechavenc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFECHAVENC() {
        return fechavenc;
    }

    /**
     * Define el valor de la propiedad fechavenc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFECHAVENC(String value) {
        this.fechavenc = value;
    }

    /**
     * Obtiene el valor de la propiedad montototal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMONTOTOTAL() {
        return montototal;
    }

    /**
     * Define el valor de la propiedad montototal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMONTOTOTAL(String value) {
        this.montototal = value;
    }

    /**
     * Obtiene el valor de la propiedad numorden.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMORDEN() {
        return numorden;
    }

    /**
     * Define el valor de la propiedad numorden.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMORDEN(String value) {
        this.numorden = value;
    }

    /**
     * Obtiene el valor de la propiedad paramresp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARAMRESP() {
        return paramresp;
    }

    /**
     * Define el valor de la propiedad paramresp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARAMRESP(String value) {
        this.paramresp = value;
    }

    /**
     * Gets the value of the productos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Producto }
     * 
     * 
     */
    public List<Producto> getProductos() {
        if (productos == null) {
            productos = new ArrayList<Producto>();
        }
        return this.productos;
    }

    /**
     * Obtiene el valor de la propiedad tipodoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPODOC() {
        return tipodoc;
    }

    /**
     * Define el valor de la propiedad tipodoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPODOC(String value) {
        this.tipodoc = value;
    }

}
