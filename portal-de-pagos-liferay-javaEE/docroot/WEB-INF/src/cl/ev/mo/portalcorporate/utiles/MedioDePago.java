package cl.ev.mo.portalcorporate.utiles;


/**
 * Clase enumeracion de medios de pago
 * @author Marcela
 *
 */
public enum MedioDePago {
	BANCO_ESTADO("24", "Banco Estado"),
	SANTANDER("25", "Santander"),
	ONE_CLICK("28", "One Click"),
	SERVIPAG("22", "Servipag"),
	WEBPAY("21", "WebPay"),
	BCI("23", "BCI"),
	WEBPAY_WS("27", "Webpay");
	
	private String id;
	private String name;
	
	
	private MedioDePago(String id, String name){
		this.id = id;
		this.name= name;
	}
	
	public String getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public static MedioDePago getById(String id) {
	    for(MedioDePago e : values()) {
	        if(e.getId().equalsIgnoreCase(id)) 
	        	return e;
	    }
	    return null;
	 }

}
