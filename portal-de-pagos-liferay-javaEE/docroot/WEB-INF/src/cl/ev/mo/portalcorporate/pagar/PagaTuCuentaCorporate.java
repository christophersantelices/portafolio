package cl.ev.mo.portalcorporate.pagar;

import java.util.Iterator;

import javax.portlet.ResourceRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil; 

import cl.ev.mo.portalcorporate.beans.BillingCustomerFA;
import cl.ev.mo.portalcorporate.beans.CuentaFinancieraFA;
import cl.ev.mo.portalcorporate.beans.RespuestaFindAccount;
import cl.ev.mo.portalcorporate.utiles.UtilesPago; 
import cl.ev.mo.portalcorporate.devetelWS.MedioPago;
import cl.ev.mo.portalcorporate.devetelWS.ValidaAccesoRespuesta;
import cl.ev.mo.portalcorporate.beans.CierreTransaccion;
import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.utiles.DBSupport;
import cl.ev.mo.portalcorporate.utiles.SessionUtil;
import cl.ev.mo.portalcorporate.utiles.UtilesContenidos;
import cl.ev.mo.portalcorporate.beans.DocumentoFA;
import cl.ev.mo.portalcorporate.beans.ServicioFA;

public class PagaTuCuentaCorporate {

	static Log log =  LogFactoryUtil.getLog(PagaTuCuentaCorporate.class);

	public static String generaCuerpoDeudaPublico(RespuestaFindAccount resp){
		try{
			log.debug("Entrada a generar html deuda publico.");
			StringBuilder html = new StringBuilder();
			Iterator<BillingCustomerFA> it1 = resp.getBillingCustomer().iterator();
			html.append("<ul id=\"accounts\" class=\"collapsed\">");
			String carroPorDefecto = "";
			while(it1.hasNext()){
				BillingCustomerFA bc = it1.next();
				Iterator<CuentaFinancieraFA> it = bc.getCuentasFinancieras().iterator();
				while(it.hasNext()){		
					CuentaFinancieraFA cuenta = it.next();	
					if (cuenta.getMontoTotal().equals("0")){
						continue;
					}
					log.debug("####################################");
					log.debug("Monto total : " + cuenta.getMontoTotal());
					log.debug("Monto por vencer : " + cuenta.getMontoPorVencer());
					log.debug("Monto vencido : " + cuenta.getMontoVencido());
					log.debug("Monto en disputa : " + cuenta.getMontoEnDisputa());
					log.debug("Tiene pat : " + cuenta.isTienePat());
					log.debug("Tiene pago por servicio : " + cuenta.isTienePagoPorServicio());
					html.append("<li>");
					html.append("<div class=\"left\">");
					html.append("<img class=\"mobile rounded_sheet\"\r\n" + 
							"src=\"/mo-theme/images/retina/icon_sheet_grey.png\" width=\"76\" height=\"58\"\r\n" + 
							"alt=\"\"> <span> <img class=\"desktop\"\r\n" + 
							"src=\"/mo-theme/images/icon_sheet.png\" width=\"20\" height=\"24\"\r\n" + 
							"alt=\"numero cuenta\"> Cuenta financiera N°: <strong>" +  cuenta.getIdCuentaFinanciera() + "\r\n" + 
							"</strong> <img data-toggle=\"tooltip\" class=\"tooltips\"\r\n" + 
							"title=\"Cuenta financiera es tu antiguo c&oacute;digo de cliente\"\r\n" + 
							"src=\"/mo-theme/images/icon_question.png\" width=\"23\" height=\"23\" alt=\"ayuda\">\r\n" + 
							"</span> ");
					if(cuenta.isTienePat()){
						html.append("<a class=\"bt_grey\">\r\n" + 
								"            <img src=\"/mo-theme/images/icon_time.png\" width=\"18\"\r\n" + 
								"               height=\"16\" alt=\"\"> <strong>PAC/PAT activado</strong> <span\r\n" + 
								"               class=\"expander\"></span>\r\n" + 
								"            <p>\r\n" + 
								"               ¡Atención, esta cuenta <strong>tiene Pago Autom&aacute;tico\r\n" + 
								"               activado.</strong>Si pagas, puede que se te cobre 2 veces.\r\n" + 
								"            </p>\r\n" +
								"         </a>");
					}
					html.append("</div>");
					html.append(" <div class=\"right\">\r\n" + 
							"         <div class=\"triangle_right desktop\"></div>");
					html.append("<ul>");
					
					//TODO: Validar condicion para que muestra la cuenta vencida
					if(cuenta.getMontoVencido().equals("0") || cuenta.getMontoVencido().equals("")){
						log.debug("No tiene deuda vencida");
					}
					else{
						html.append("<li class=\"inactive\">\r\n" + 
								"    	<img src=\"/mo-theme/images/label_expired.png\"\r\n" + 
								"       width=\"67\" height=\"73\" alt=\"vencida\"> "+
								
								/*"		<span id=\""+ cuenta.getIdCuentaFinanciera()+"V"+  cuenta.getMontoVencido() + "\" \r\n" + 
								"       class=\"ticket ticketPublico \"\r\n" + 
								"       title=\"No puedes dejar de pagar tu cuenta vencida.\"></span>\r\n" + */
								
								"       <div>\r\n" + 
								"       <span>Vencida</span> <strong>$ <em>" +  UtilesPago.montoEnFormatoFront(cuenta.getMontoVencido()) + "</em></strong>\r\n" + 
								"       </div>\r\n" + 
								"   </li>");
					}
					
					//String montoPorVencer = (resp.isPagoPorRut() ? cuenta.getMontoPorVencer() : cuenta.getMontoTotal());
					String montoPorVencer =  cuenta.getMontoPorVencer() ;
					if(montoPorVencer.equals("") || montoPorVencer.equals("0")){
						log.debug("No tiene deuda por vencer");
						carroPorDefecto = carroPorDefecto + cuenta.getIdCuentaFinanciera()+"V"+cuenta.getMontoVencido()+";";
					}
					else{
						carroPorDefecto = carroPorDefecto + cuenta.getIdCuentaFinanciera()+"T"+cuenta.getMontoTotal()+";";
						boolean seleccion=true;
						if(seleccion){
							if(cuenta.getMontoVencido().equals("0") || cuenta.getMontoVencido().equals("")){
								log.debug("No tiene deuda vencida");
								html.append("<li class=\"active\">\r\n" + 
										"      <div>\r\n" + 
										"      <span>Por vencer</span> <strong>$ <em>" + UtilesPago.montoEnFormatoFront(montoPorVencer) +"</em></strong>\r\n" + 
										"      </div>\r\n" + 
										"   </li>");
							}else{
								html.append("<li class=\"active\">\r\n" + 
										"      <span class=\"ticket ticketPublico selected\"  id=\""+ cuenta.getIdCuentaFinanciera()+"T"+ cuenta.getMontoTotal() +"\"></span>\r\n" + 
										"      <div>\r\n" + 
										"      <span>Por vencer</span> <strong>$ <em>" + UtilesPago.montoEnFormatoFront(montoPorVencer) +"</em></strong>\r\n" + 
										"      </div>\r\n" + 
										"   </li>");
							}
						}else{
							html.append("<li class=\"active\">\r\n" + 
									"      <div>\r\n" + 
									"      <span>Por vencer</span> <strong>$ <em>" + UtilesPago.montoEnFormatoFront(montoPorVencer) +"</em></strong>\r\n" + 
									"      </div>\r\n" + 
									"   </li>");
						}
					}
					html.append("</ul>");
					html.append("</div>");
					html.append("</li>");
				}
			}
			html.append("</ul>");
			html.append("<input type=\"hidden\" id=\"carro_por_defecto\" name=\"carro_por_defecto\" value=\""+carroPorDefecto +"\"></input>");
			return html.toString();
		}
		catch(Exception e){
			//TODO: Retornando HTML en duro por problemas de findAccount
			return "error";
		}
	}
	
	public static String getIdMedioDePagoElegido(String nombreMedioElegido , ValidaAccesoRespuesta respuestaValida){
		log.debug("Entro a obtener el ID del medio de pago");
		log.debug("El medio de pago elegido es : " + nombreMedioElegido);
		String id = "21";
		Iterator<MedioPago> it = respuestaValida.getMEDIOSPAGOLIST().getMEDIOPAGO().iterator();
		while(it.hasNext()){
			MedioPago mp = it.next();
			if(mp.getNOMBREMEDIOPAGO().trim().equalsIgnoreCase(nombreMedioElegido)){
				log.debug("Los nombres coinciden, el ID del medio de pago elegido es : " + mp.getIDMEDIOPAGO());
				id = mp.getIDMEDIOPAGO();
				break;
			}
		}
		log.debug("El ID obtenido es : " + id);
		return id;
	}
	
	/**
	 * @param nombreMedioElegido
	 * @param respuestaValida
	 * @return retorna la URL del medio de pago elegido, de no encontrar coincidencias retorna la URL de Webpay por defecto
	 */
	public static String getURLMedioDePagoElegido(String nombreMedioElegido , ValidaAccesoRespuesta respuestaValida){
		log.debug("Entro a obtener la URL del medio de pago");
		log.debug("El medio de pago elegido es : " + nombreMedioElegido);
		//TODO: En duro URL por defecto
		String url = "https://gmppre.dvtel.cl/ws_cre_gmp_tbk/jsp/receiver_webpay_normal.jsp";
		Iterator<MedioPago> it = respuestaValida.getMEDIOSPAGOLIST().getMEDIOPAGO().iterator();
		while(it.hasNext()){
			MedioPago mp = it.next();
			log.debug("El nombre del medio de pago de Devetel es : " + mp.getNOMBREMEDIOPAGO() + " medio de pago elegido : " + nombreMedioElegido);
			if(mp.getNOMBREMEDIOPAGO().trim().equalsIgnoreCase(nombreMedioElegido)){
				log.debug("Los nombres coinciden, la URL del medio de pago elegido es : " + mp.getURLMEDIOPAGO());
				url = mp.getURLMEDIOPAGO();
				break;
			}
		}
		log.debug("La URL obtenida es : " + url);
		return url;
	}
	
	public static String generaComprobanteImprimir(ResourceRequest request){
		StringBuilder sb = new StringBuilder();
		
		boolean simulado = (UtilesContenidos.obtenerContenidoTexto(request, Constantes._kit_simulado).equals("true") ? true : false);
		DBSupport db = new DBSupport();
		CierreTransaccion datosCierre = new CierreTransaccion();
		try{
			if(simulado){
				log.debug("Obteniendo datos cierre simulado...");
				datosCierre = db.getDatosCierreSimulado(request);
			}
			else{
				String oc = SessionUtil.getVariable(request, Constantes.ID_TICKET_RDP);
				log.debug("Obteniendo datos cierre desde DB...");
				datosCierre = db.getDatosCierre(oc);
			}
		}
		catch(Exception e){
			log.error("Error al obtener los datos del cierre de ventas : " + e);
			e.printStackTrace();
		}
		
		sb.append("\r\n" + 
				"<main class=\"container comprobante-ticket width-100 padding-00-i text-center\" style=\"max-width: 789px;\">\r\n" + 
				"	<div class=\"contenido clearfix inline-block width-100\">\r\n" + 
				"		<div class=\"cont-comprobante-ticket inline-block text-center\" style=\"border: 1px solid #ECF0F6;background-color: #F6F8FA;margin-bottom:50px\">\r\n" + 
				"			<div class=\"no-direccion inline-block width-100 bg-white\" style=\"max-width: 700px;\">\r\n" + 
				"				<div class=\"cont-icon-advertencia inline-block\">\r\n" + 
				"					<img class=\"icon-advertencia hidden-desktop\" src=\"/mo-theme/images/img-cuenta-pagada-exitosamente.png\">\r\n" + 
				"				</div>\r\n" + 
				"				<div class=\"texto block\">\r\n" + 
				"					<h4>La cuenta ha sido pagada exitosamente</h4>\r\n" + 
				"				</div>\r\n" + 
				"				<div style=\"display:none;\" class=\"cobro-a-tu-cuenta\">\r\n" + 
				"					<i class=\"MCSS-alerta_v2 inline-block\"></i>\r\n" + 
				"					<p>\r\n" + 
//				"						Recuerde presentar este comprobante de pago, al momento de recibir su producto.\r\n" + 
				"					</p>\r\n" + 
				"				</div>\r\n" + 
				"				<p class=\"detalle-transaccion\">Detalles de la transacción:</p>\r\n" + 
				"				<table class=\"detalle-transaccion comprobante\">\r\n" + 
				"					<tbody>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>Monto pagado</td>\r\n" + 
				"							<td>$ "+ datosCierre.getMonto() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>Fecha de pago</td>\r\n" + 
				"							<td>"+ datosCierre.getFechaPago()+"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>ID de pago</td>\r\n" + 
				"							<td>"+ datosCierre.getIdentificadorDePago() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>ID de Ticket</td>\r\n" + 
				"							<td>"+ datosCierre.getNumeroDeOrden() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>Medio de pago</td>\r\n" + 
				"							<td>"+ datosCierre.getMedioDePago() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>Código de transacción</td>\r\n" + 
				"							<td>"+ datosCierre.getCodigoTransaccion() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>Código autorización</td>\r\n" + 
				"							<td>"+ datosCierre.getCodigoAutorizacion() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>Tipo de tarjeta</td>\r\n" + 
				"							<td>"+ datosCierre.getTipoTarjeta() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>Últimos 4 dígitos tarjeta</td>\r\n" + 
				"							<td>"+ datosCierre.getUltimosDigitosTarjeta() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>Tipo de cuotas</td>\r\n" + 
				"							<td>"+ datosCierre.getTipoCuotas() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr>\r\n" + 
				"							<td>Números de cuotas</td>\r\n" + 
				"							<td>"+ datosCierre.getNumeroCuotas() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"					</tbody>\r\n" + 
				"				</table>\r\n" + 
				"				<div class=\"cont-recuerda\">\r\n" + 
				"					<p style=\"display:none;\" >Recuerda que si el servicio se encontraba suspendido, lo reestableceremos en un máximo de 24 horas. Este pago puede reflejarse el día hábil siguiente.</p>\r\n" + 
				"				</div>\r\n" + 
				"				<h5 class=\"width-100 inline-block\">¡Visítanos en <a href=\"www.mo.cl\" target=\"_blank\">www.mo.cl</a>!</h5>\r\n" + 
				"			</div>\r\n" + 
				"		</div>\r\n" + 
				"	</div>\r\n" + 
				"</main>");
		
		return sb.toString();
	}
		
	public static String generaComprobantePDF(ResourceRequest request){
		
		StringBuilder sb = new StringBuilder();
		boolean simulado = (UtilesContenidos.obtenerContenidoTexto(request, Constantes._kit_simulado).equals("true") ? true : false);
		DBSupport db = new DBSupport();
		CierreTransaccion datosCierre = new CierreTransaccion();
		try{
			if(simulado){
				log.debug("Obteniendo datos cierre simulado...");
				datosCierre = db.getDatosCierreSimulado(request);
			}
			else{
				String oc = SessionUtil.getVariable(request, Constantes.ID_TICKET_RDP);
				log.debug("Obteniendo datos cierre desde DB...");
				datosCierre = db.getDatosCierre(oc);
			}
		}
		catch(Exception e){
			log.error("Error al obtener los datos del cierre de ventas : " + e);
			e.printStackTrace();
		}
		
		//String numeroOrden = SessionUtil.getVariable(request, VariablesSession.VENTAS_NUMERO_DE_ORDEN);
		
		sb.append("<html><body style=\"color: #353b47;font-family: 'Roboto';font-size: 14px;font-weight: normal;line-height: 120%;\">");
		sb.append("<header style=\"width: 100%;max-width: 791px;margin: 0 auto;float: none;background-repeat: no-repeat;-webkit-background-size: cover;-ms-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover;height: 175px;margin-top: 55px;padding: 2.2% 7.2%; \"><h2>Comprobante de pago</h2>" + 
				"        <h4>"+ UtilesPago.getFechaActualComprobantePDF() +"</h4>" + 
				"        </header>" + 
				"		<main class=\"container comprobante-ticket width-100 \" style=\"padding: 00px !important; text-align: center; max-width: 789px;\">\r\n" + 
				"			<div class=\"contenido clearfix inline-block width-100\"  style=\" max-width: 1000px; text-align: center; display: inline-block; \" >\r\n" + 
				"                <div class=\"cont-comprobante-ticket \" style=\" border: 1px solid #ECF0F6;background-color: #F6F8FA;margin-bottom: 50px;margin: 0 auto;float: none;text-align: center;width: 100%;\">\r\n" + 
				"                <div class=\"width-100 bg-white\" style=\"max-width: 700px; margin: 0 auto;border: 1px solid #A8CF37;margin-bottom: 70px;box-sizing: border-box;margin-top: 72px;min-height: inherit;height: auto;padding: 0px 10% 33px 10%;text-align: center; display: inline-block; background-color: #fff; \">" + 
				"                    <div class=\"cont-icon-advertencia inline-block\">" + 
				"					</div>" +
				"                    <div class=\"texto block\">" +
				"                        <h4>La cuenta ha sido pagada exitosamente</h4>" +
				"                    </div>" + 
				"                    <div style=\"display:none;\"class=\"cobro-a-tu-cuenta\" style=\"background-color: #59A6C1;width: 100%;min-height: 47px;margin: 0 auto;text-align: left;padding: 12px 3%;display: inline-block;\">" + 
				"                        <i class=\"MCSS-alerta_v2 inline-block\"></i>" + 
				"                        <p>" + 
//				"                            Recuerda presentar este comprobante de pago, al momento de recibir su producto." + 
				"                        </p>" + 
				"                    </div>" + 
				"                    <p class=\"detalle-transaccion\">Detalles de la transacci&oacute;n:</p>" + 
				"                    <table class=\"detalle-transaccion comprobante\" style=\" width: 100%;font-family: RobotoLight;color: #656D77;font-size: 13px;line-height: 120%;font-weight: 300;text-align: left;border-collapse: collapse;position: relative;\">" + 
				"					<tbody style=\"text-align: left;\">" + 
				"						<tr style=\"height: 47px; background-color: #ecf0f6\">" + 
				"							<td>Monto pagado</td>" + 
				"							<td>$ "+ datosCierre.getMonto() +"</td>" + 
				"						</tr>" + 
				"						<tr style=\"height: 47px;\">\r\n" + 
				"							<td >Fecha de pago</td>\r\n" + 
				"							<td> " + datosCierre.getFechaPago()+ "</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr style=\"height: 47px; background-color: #ecf0f6\">" + 
				"							<td>ID de pago</td>" + 
				"							<td>"+ datosCierre.getIdentificadorDePago() +"</td>" + 
				"						</tr>\r\n" + 
				
				"						<tr style=\"height: 47px;\">\r\n" + 
				"							<td>ID de ticket</td>\r\n" + 
				"							<td>"+ datosCierre.getNumeroDeOrden() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr style=\"height: 47px;background-color: #ecf0f6\">\r\n" + 
				"							<td>Medio de pago</td>\r\n" + 
				"							<td> "+ datosCierre.getMedioDePago() +" </td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr style=\"height: 47px;\">\r\n" + 
				"							<td>C&oacute;digo transacci&oacute;n</td>\r\n" + 
				"							<td> "+ datosCierre.getCodigoTransaccion() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr style=\"height: 47px;background-color: #ecf0f6\">\r\n" + 
				"							<td>C&oacute;digo de autorizaci&oacute;n</td>\r\n" + 
				"							<td> "+ datosCierre.getCodigoAutorizacion() +" </td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr style=\"height: 47px;\">\r\n" + 
				"							<td>Tipo de tarjeta</td>\r\n" + 
				"							<td>"+ datosCierre.getTipoTarjeta() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr style=\"height: 47px;background-color: #ecf0f6\">\r\n" + 
				"							<td>Ultimos 4 digitos de la tarjeta</td>\r\n" + 
				"							<td>"+ datosCierre.getUltimosDigitosTarjeta() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr style=\"height: 47px;\">\r\n" + 
				"							<td>Tipo de cuotas</td>\r\n" + 
				"							<td>"+ datosCierre.getTipoCuotas() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"						<tr style=\"height: 47px;background-color: #ecf0f6\">\r\n" + 
				"							<td>Numero de cuotas</td>\r\n" + 
				"							<td>"+ datosCierre.getNumeroCuotas() +"</td>\r\n" + 
				"						</tr>\r\n" + 
				"					</tbody>\r\n" + 
				"					</table>\r\n" + 
				"                    <h5 class=\"width-100 inline-block\">&iexcl;Visítanos en www.mo.cl!\r\n" + 
				"					</h5>\r\n" + 
				"                </div>\r\n" + 
				"				</div>\r\n" + 
				"			</div>   \r\n" + 
				"        </main>");
		sb.append("</body></html>");
		return sb.toString();
	}
	
	public static String generaCuerpoDeudaPrivado(RespuestaFindAccount resp , String faPorDefecto, boolean tieneProrroga) {
		
		StringBuilder retorno  = new StringBuilder();
		try{
			int contadorcuentas = 0;
			Iterator<BillingCustomerFA> it1 = resp.getBillingCustomer().iterator();
			while(it1.hasNext()){	
				BillingCustomerFA bc = it1.next();
				Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
				while(it2.hasNext()){
					CuentaFinancieraFA fa = it2.next();
							
					log.debug("Comparando FA : " + fa.getIdCuentaFinanciera() + " con : " + faPorDefecto);
					
					if(fa.getIdCuentaFinanciera().equals(faPorDefecto)){
						contadorcuentas++;
						long vencida = Long.parseLong(fa.getMontoVencido());
						long porVencer = Long.parseLong(fa.getMontoPorVencer());
						if(vencida > 0){
							// Para los que tienen vencida y por vencer
							if(porVencer > 0){
								log.debug("Cliente tiene deuda vencida y por vencer !");
								// Si tiene PAT
								retorno.append("<div class=\"cuenta-numero\">\r\n" + 
										"                            <div class=\"cuenta-n\">\r\n" + 
										"                                <p><span>CUENTA </span><strong>N°" + fa.getIdCuentaFinanciera() + "</strong></p>\r\n" + 
										"                            </div>\r\n");
								if (fa.isTienePat()){
									retorno.append("<div class=\"tiene-activo\">\r\n" + 
											" <p><span>Tiene activo un </span><strong>Pago Automatico PAC o PAT</strong></p>\r\n");
								}

								/*TODO:validar
								retorno.append("<div class='nuevas-funciones'>"+
									      "<a class='link-icon abonar' href='#0' onclick='abonar("+faPorDefecto+")' data-toggle='modal' data-target='#modal_abonar_cuenta'>Abonar</a>");
								if (tieneProrroga) {
									retorno.append("<a class='link-icon prorroga' href='#0' onclick='solicitarProrroga("+faPorDefecto+")' data-toggle='modal' data-target='#modal_solicitar_prorroga'>Solicitar prórroga</a>");
								}
								retorno.append( "</div>"); */
								retorno.append("</div>");
								log.debug("entra a la parte donde se setea el T+coso ");
								retorno.append("<div class=\"montos\">\r\n" + 
										"                            <ul class=\"cont-montos\">\r\n" + 
										"                                <li class=\"vencido\">\r\n" + 
										"                                    <div class=\"cont-valores\" onclick=\"noDejarDePagarSinObj()\">\r\n" + 
										"                                        <input type=\"checkbox\"  value=\" "  + fa.getMontoVencido() +"  \" id=\"V"+ faPorDefecto +"\" name=\"check\" checked=\"\" disabled >\r\n" + 
										"                                        <label for=\"V"+ faPorDefecto +"\">\r\n" + 
										"                                            <p>\r\n" + 
										"                                                <span class=\"txt-1 hidden-xs\">Montos vencidos</span>\r\n" + 
										"                                                <span class=\"txt-1 visible-xs\">Montos vencidos</span>\r\n" + 
										"                                                <span class=\"txt-2\">\r\n" + 
										"                                                        <em><i>$</i>" +  UtilesPago.montoEnFormatoFront(fa.getMontoVencido()) + "</em>\r\n" + 
										"                                                    </span>\r\n" + 
										"                                            </p>\r\n" + 
										"                                        </label>\r\n" + 
										"                                    </div>\r\n" + 
										
										"<div id=\"V"+ faPorDefecto +"no-estas-pagando\" class=\"no-estas-pagando\" style=\"display: none;\">\r\n" + 
										"        <div class=\"no-estas-pagando-s1\">\r\n" + 
										"        		<i class=\"MCSS-alerta\"></i>\r\n" + 
										"        </div>\r\n" + 
										"        <div class=\"no-estas-pagando-s2\">\r\n" + 
										"        		No estas pagando todas tus boletas. El monto original es <span>$ "+ UtilesPago.montoEnFormatoFront(fa.getMontoVencido()) +"</span>.\r\n" + 
										"        </div>\r\n" + 
										"</div>"+
										
										"                                </li>\r\n" + 
										"                                <li class=\"por-pagar\">\r\n" + 
										"                                    <div class=\"cont-valores\">\r\n" + 
										"                                        <input type=\"checkbox\" value=\" " + fa.getMontoPorVencer() + "\" id=\"T"+faPorDefecto+"\" name=\"check\" checked=\"true\" onClick=\"recibeCuentaPrincipal(this)\">\r\n" + 
										"                                        <label for=\"T"+faPorDefecto+"\">\r\n" + 
										"                                            <p>\r\n" + 
										"                                                <span class=\"txt-1 hidden-xs\">Montos por pagar</span>\r\n" + 
										"                                                <span class=\"txt-1 visible-xs\">Montos por pagar</span>\r\n" + 
										"                                                <span class=\"txt-2\">\r\n" + 
										"                                                        <em><i>$</i>" + UtilesPago.montoEnFormatoFront(fa.getMontoPorVencer()) + "</em>\r\n" + 
										"                                                    </span>\r\n" + 
										"                                            </p>\r\n" + 
										"                                        </label>\r\n" + 
										"                                    </div>\r\n" + 
										
										"<div id=\"T"+ faPorDefecto +"no-estas-pagando\" class=\"no-estas-pagando\" style=\"display: none;\">\r\n" + 
										"        <div class=\"no-estas-pagando-s1\">\r\n" + 
										"        		<i class=\"MCSS-alerta\"></i>\r\n" + 
										"        </div>\r\n" + 
										"        <div class=\"no-estas-pagando-s2\">\r\n" + 
										"        		No estas pagando todas tus boletas. El monto original es <span>$ "+ UtilesPago.montoEnFormatoFront(fa.getMontoPorVencer()) +"</span>.\r\n" + 
										"        </div>\r\n" + 
										"</div>"+
										
										"</li>\r\n" + 
										"<li id=\"montosOtrasCuentas\" class=\"seleccionado\">\r\n" + 
										"    <div class=\"cont-valores\">\r\n" + 
										"    <input type=\"checkbox\" value=\"3\" id=\"montos-check-3\" name=\"check\" checked=\"\">\r\n" + 
										"    <label for=\"montos-check-3\">\r\n" + 
										"    	 <p>\r\n" + 
										"        <span class=\"txt-1\">Montos seleccionados de otras cuentas</span>\r\n" + 
										"        <span class=\"txt-2\">\r\n" + 
										"        	<em><i>$</i><strong id=\"montoTotalOtrasCuentas\">0</strong></em>\r\n" + 
										"        </span>\r\n" + 
										"        </p>\r\n" + 
										"    </label>\r\n" + 
										"    </div>\r\n" + 
										"</li>\r\n" + 
										"</ul>\r\n" + 
										"</div>");
							}
							// Para los que tienen solo vencida
							else{
								log.debug("Cliente tiene solo deuda vencida !");
								// Si tiene PAT
								
								retorno.append("<div class=\"cuenta-numero\">\r\n" + 
										"                            <div class=\"cuenta-n\">\r\n" + 
										"                                <p><span>CUENTA </span><strong>N°" + fa.getIdCuentaFinanciera() + "</strong></p>\r\n" + 
										"                            </div>\r\n");
								if (fa.isTienePat()){
									log.debug("Cliente con PAT");
									retorno.append("<div class=\"tiene-activo\">\r\n" + 
											"       	<p><span>Tiene activo un </span><strong>Pago Automatico PAC o PAT</strong></p>\r\n" + 
											"       </div>\r\n");
								}

								//TODO:validar esto
								/*
								retorno.append("<div class='nuevas-funciones'>"+
									      "<a class='link-icon abonar' href='#0' onclick='abonar("+faPorDefecto+")' data-toggle='modal' data-target='#modal_abonar_cuenta' >Abonar</a>");
								if (tieneProrroga) {
									retorno.append("<a class='link-icon prorroga' href='#0' onclick='solicitarProrroga("+faPorDefecto+")' data-toggle='modal' data-target='#modal_solicitar_prorroga'>Solicitar prórroga</a>");
								}
								retorno.append("</div>");
								*/
								retorno.append("</div>");

								retorno.append("<div class=\"montos\">\r\n" + 
										"                            <ul class=\"cont-montos\">\r\n" + 
										"                                <li class=\"vencido\">\r\n" + 
										"                                    <div class=\"cont-valores\" onclick=\"noDejarDePagarSinObj()\">\r\n" + 
										"                                        <input type=\"checkbox\" value=\" "  + fa.getMontoVencido() +"  \" id=\"V"+ faPorDefecto +"\" name=\"check\" checked=\"\" disabled>\r\n" +
										//"										 <input type='hidden' id='T"+faPorDefecto+"' value='"+fa.getMontoTotal()+"'> "+
										"                                        <label for=\"V"+ faPorDefecto +"\">\r\n" + 
										"                                            <p>\r\n" + 
										"                                                <span class=\"txt-1 hidden-xs\">Montos vencidos</span>\r\n" + 
										"                                                <span class=\"txt-1 visible-xs\">Montos vencidos</span>\r\n" + 
										"                                                <span class=\"txt-2\">\r\n" + 
										"                                                        <em><i>$</i>" +  UtilesPago.montoEnFormatoFront(fa.getMontoVencido()) + "</em>\r\n" + 
										"                                                    </span>\r\n" + 
										"                                            </p>\r\n" + 
										"                                        </label>\r\n" + 
										"                                    </div>\r\n" + 
										
										"<div id=\"V"+ faPorDefecto +"no-estas-pagando\" class=\"no-estas-pagando\" style=\"display: none;\">\r\n" + 
										"        <div class=\"no-estas-pagando-s1\">\r\n" + 
										"        		<i class=\"MCSS-alerta\"></i>\r\n" + 
										"        </div>\r\n" + 
										"        <div class=\"no-estas-pagando-s2\">\r\n" + 
										"        		No estas pagando todas tus boletas. El monto original es <span>$ "+ UtilesPago.montoEnFormatoFront(fa.getMontoVencido()) +"</span>.\r\n" + 
										"        </div>\r\n" + 
										"</div>"+
										"</li>\r\n" + 
										"<li id=\"montosOtrasCuentas\" class=\"seleccionado\">\r\n" + 
										"    <div class=\"cont-valores\">\r\n" + 
										"    <input type=\"checkbox\" value=\"3\" id=\"montos-check-3\" name=\"check\" checked=\"\">\r\n" + 
										"    <label for=\"montos-check-3\">\r\n" + 
										"    	 <p>\r\n" + 
										"        <span class=\"txt-1\">Montos seleccionados de otras cuentas</span>\r\n" + 
										"        <span class=\"txt-2\">\r\n" + 
										"        	<em><i>$</i><strong id=\"montoTotalOtrasCuentas\">0</strong></em>\r\n" + 
										"        </span>\r\n" + 
										"        </p>\r\n" + 
										"    </label>\r\n" + 
										"    </div>\r\n" + 
										"</li>\r\n" + 
										"</ul>\r\n" + 
										"</div>");
							}
						}
						else{
							//Para los que tienen solo por vencer
							log.debug("Cliente tiene solo deuda por vencer !");
							if (fa.isTienePat()){
								log.debug("Cliente con PAT");
								retorno.append("<div class=\"cuenta-numero\">\r\n" + 
										"                            <div class=\"cuenta-n\">\r\n" + 
										"                                <p><span>CUENTA </span><strong>N°" + fa.getIdCuentaFinanciera() + "</strong></p>\r\n" + 
										"                            </div>\r\n" + 
										"                            <div class=\"tiene-activo\">\r\n" + 
										"                                <p><span>Tiene activo un </span><strong>Pago Automatico PAC o PAT</strong></p>\r\n" + 
										"                            </div>\r\n" + 
										"                        </div>");
							}
							else{
								retorno.append("<div class=\"cuenta-numero\">\r\n" + 
												"   <div class=\"cuenta-n\">\r\n" + 
												"   <p><span>CUENTA </span><strong>N°" + fa.getIdCuentaFinanciera() + " </strong></p>\r\n" + 
												"   </div>\r\n" + 
												"</div>");
							}
							
							/* TODO:validar
							retorno.append("<div class='nuevas-funciones'>"+
								      "<a class='link-icon abonar' href='#0' onclick='abonar("+faPorDefecto+")' data-toggle='modal' data-target='#modal_abonar_cuenta'>Abonar</a>");
							if (tieneProrroga) {
								retorno.append("<a class='link-icon prorroga' href='#0' onclick='solicitarProrroga("+faPorDefecto+")' data-toggle='modal' data-target='#modal_solicitar_prorroga'>Solicitar prórroga</a>"+
									  "</div>");
							}
							*/
							
							retorno.append("<div class=\"montos\">\r\n" + 
									"	<ul class=\"cont-montos\">\r\n" + 
									"		<li class=\"por-pagar\">\r\n" + 
									"			<div class=\"cont-valores\">\r\n" + 
									"				<input type=\"checkbox\" value=\"" + fa.getMontoPorVencer()+ "\" id=\"" + "T"+ faPorDefecto+ "\" name=\"check\" onClick=\"recibeCuentaPrincipal(this)\" checked=\"true\">\r\n" + 
									"				<label for=\""+"T"+faPorDefecto+"\">\r\n" + 
									"					<p>\r\n" + 
									"						<span class=\"txt-1 hidden-xs\">Monto por pagar</span>\r\n" + 
									"						<span class=\"txt-1 visible-xs\">Cuentas por pagar</span>\r\n" + 
									"						<span class=\"txt-2\">\r\n" + 
									"							<em><i>$</i> "+ UtilesPago.montoEnFormatoFront(fa.getMontoPorVencer()) +" </em>\r\n" + 
									"						</span>\r\n" +
									"					</p>\r\n" + 
									"				</label>\r\n" + 
									"			</div>\r\n" + 
									"			<div id=\"T"+faPorDefecto+"no-estas-pagando\" class=\"no-estas-pagando\">\r\n" + 
									"				<div class=\"no-estas-pagando-s1\">\r\n" + 
									"					<i class=\"MCSS-alerta\"></i>\r\n" + 
									"				</div>\r\n" + 
									"				<div class=\"no-estas-pagando-s2\">\r\n" + 
									"					No estas pagando todas tus boletas El monto original es <span>$"+ UtilesPago.montoEnFormatoFront(fa.getMontoPorVencer())+"</span>\r\n" + 
									"				</div>\r\n" + 
									"			</div>\r\n" + 
									"		</li>\r\n" + 
									"		<li id=\"montosOtrasCuentas\" class=\"seleccionado\">\r\n" + 
									"   		<div class=\"cont-valores\">\r\n" + 
									"    		<input type=\"checkbox\" value=\"3\" id=\"montos-check-3\" name=\"check\" checked=\"\">\r\n" + 
									"    		<label for=\"montos-check-3\">\r\n" + 
									"    	 		<p>\r\n" + 
									"        		<span class=\"txt-1\">Montos seleccionados de otras cuentas</span>\r\n" + 
									"        		<span class=\"txt-2\">\r\n" + 
									"        			<em><i>$</i><strong id=\"montoTotalOtrasCuentas\">0</strong></em>\r\n" + 
									"        		</span>\r\n" + 
									"        		</p>\r\n" + 
									"    		</label>\r\n" + 
									"    		</div>\r\n" + 
									"		</li>\r\n" + 
									"	</ul>\r\n" + 
									"</div>");
						}
						retorno.append("<input type=\"hidden\" id=\"carroPorDefectoPrivado\" value=\"T"+faPorDefecto+";\"/>");
						retorno.append("<input type=\"hidden\" id=\"cuentaPrincipal\" value=\""+faPorDefecto+"\"/>");
						break;
					}
					// No se ha encontrado la cuenta financiera pasada por parametro en la respuesta de findAccount
					else{
						
						log.debug("No se ha encontrado la cuenta financiera pasada por parametro en la respuesta de findAccount");
						continue;
					}
					
					
				}
			}
			if (contadorcuentas==0) {
				retorno.setLength(0);
				retorno.append("error");
			}
		}
		catch(Exception e){
			log.error("Error al generar HTML para paso 1 privado");
			e.printStackTrace();
		}
		return retorno.toString();
	}
	
	public static String generaCuerpoDeudaDocumentoPrivado(RespuestaFindAccount resp,String fa_cliente) {
		
		StringBuilder retorno  = new StringBuilder();
		try{
			DocumentoFA docFa=resp.getBillingCustomer().get(0).getCuentasFinancieras().get(0).getDocumentos().get(0);

			boolean vencida=UtilesPago.validaPagoVencido(docFa.getFecha());

			if(vencida){
				log.debug("Cliente tiene solo deuda vencida !");
				resp.getBillingCustomer().get(0).getCuentasFinancieras().get(0).setMontoVencido(docFa.getMonto());
				resp.getBillingCustomer().get(0).getCuentasFinancieras().get(0).setMontoTotal(docFa.getMonto());

				
				retorno.append("<div class=\"cuenta-numero\">\r\n" + 
						"    <div class=\"cuenta-n\">\r\n" + 
						"         <p><span>NÚMERO DE DOCUMENTO </span> &nbsp;<strong>N°" + docFa.getNumeroBoleta() + "</strong></p>\r\n" + 
						"    </div>\r\n"
						+ "</div>");
				
				retorno.append("<div class=\"montos\">\r\n" + 
						"                            <ul class=\"cont-montos\">\r\n" + 
						"                                <li class=\"vencido\">\r\n" + 
						"                                    <div class=\"cont-valores\">\r\n" + 
						"                                        <input type=\"hidden\" value=\" "  +docFa.getMonto() +"  \" id=\"V"+ fa_cliente +"\" name=\"check\" checked=\"\" disabled>\r\n" +
						"                                        <label for=\"V"+ fa_cliente +"\">\r\n" + 
						"                                            <p>\r\n" + 
						"                                                <span class=\"txt-1 hidden-xs\">Montos vencidos</span>\r\n" + 
						"                                                <span class=\"txt-1 visible-xs\">Montos vencidos</span>\r\n" + 
						"                                                <span class=\"txt-2\">\r\n" + 
						"                                                        <em><i>$</i>" +  UtilesPago.montoEnFormatoFront(docFa.getMonto()) + "</em>\r\n" + 
						"                                                    </span>\r\n" + 
						"                                            </p>\r\n" + 
						"                                        </label>\r\n" + 
						"                                    </div>\r\n" + 
						"			<div class=\"slink verDetalle\">\r\n" +
						"				<a href=\"javascript:void(0);\" onClick=\"callObtenerServicios2()\"  class=\"link-simple\" data-toggle=\"modal\" data-target=\"#modal_detalle_servicios\">\r\n" + 
						"					<input type=\"hidden\" id=\"documento_marcado\" value=\""+ docFa.getIdDocumento() +"\"/>\r\n"+
						"					<input type=\"hidden\" id=\"cuenta_padre\" value=\""+ fa_cliente +"\"/>"+
						"					<img src=\"/mo-theme/images/icon-detalle-red.png\">\r\n" + 
						"					<span>Detalle de servicios</span>\r\n" +
						"				</a>\r\n" + 
						"			</div>\r\n" + 
						"</li>\r\n" + 
						"</ul>\r\n" + 
						"</div>");
			}else{
				log.debug("Cliente tiene solo deuda por vencer !");
				resp.getBillingCustomer().get(0).getCuentasFinancieras().get(0).setMontoPorVencer(docFa.getMonto());
				resp.getBillingCustomer().get(0).getCuentasFinancieras().get(0).setMontoTotal(docFa.getMonto());
				
				retorno.append("<div class=\"cuenta-numero\">\r\n" + 
									"   <div class=\"cuenta-n\">\r\n" + 
									"   <p><span>NÚMERO DE DOCUMENTO </span> &nbsp;<strong>N°" + docFa.getNumeroBoleta() + " </strong></p>\r\n" + 
									"   </div>\r\n" + 
								"</div>");
				
				retorno.append("<div class=\"montos\">\r\n" + 
						"	<ul class=\"cont-montos\">\r\n" + 
						"		<li class=\"por-pagar\">\r\n" + 
						"			<div class=\"cont-valores\">\r\n" + 
						"				<input type=\"hidden\" value=\"" + docFa.getMonto()+ "\" id=\"" + "T"+ fa_cliente+ "\" name=\"check\" onClick=\"recibeCuentaPrincipal(this)\" checked=\"true\">\r\n" + 
						"				<label for=\""+"T"+fa_cliente+"\">\r\n" + 
						"					<p>\r\n" + 
						"						<span class=\"txt-1 hidden-xs\">Monto por pagar</span>\r\n" + 
						"						<span class=\"txt-1 visible-xs\">Cuentas por pagar</span>\r\n" + 
						"						<span class=\"txt-2\">\r\n" + 
						"							<em><i>$</i> "+ UtilesPago.montoEnFormatoFront(docFa.getMonto()) +" </em>\r\n" + 
						"						</span>\r\n" +
						"					</p>\r\n" + 
						"				</label>\r\n" + 
						"			</div>\r\n" + 
						"			<div class=\"slink verDetalle\">\r\n" +
						"				<a href=\"javascript:void(0);\" onClick=\"callObtenerServicios2()\"  class=\"link-simple\" data-toggle=\"modal\" data-target=\"#modal_detalle_servicios\">\r\n" + 
						"					<input type=\"hidden\" id=\"documento_marcado\" value=\""+ docFa.getIdDocumento() +"\"/>\r\n"+
						"					<input type=\"hidden\" id=\"cuenta_padre\" value=\""+ fa_cliente +"\"/>"+
						"					<img src=\"/mo-theme/images/icon-detalle-red.png\">\r\n" + 
						"					<span>Detalle de servicios</span>\r\n" +
						"				</a>\r\n" + 
						"			</div>\r\n" + 
						"		</li>\r\n" + 
						"	</ul>\r\n" + 
						"</div>");
			}
			retorno.append("<input type=\"hidden\" id=\"carroPorDefectoPrivado\" value=\"T"+fa_cliente+";\"/>");
			retorno.append("<input type=\"hidden\" id=\"cuentaPrincipal\" value=\""+fa_cliente+"\"/>");
	
		}
		catch(Exception e){
			log.error("Error al generar HTML para paso 1 privado documento" +e);
			retorno.append("error");
		}
		return retorno.toString();
	}
	


	public static String generaOtrasDeudas(RespuestaFindAccount resp , String faPorDefecto){
		
		log.debug("Generando otras deudas...");
		StringBuilder sb =  new StringBuilder();
		try{
			int contadorDeCuentas = 1;
			Iterator<BillingCustomerFA> it1 = resp.getBillingCustomer().iterator();
			while(it1.hasNext()){
				BillingCustomerFA bc = it1.next();
				Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
				while(it2.hasNext()){
					CuentaFinancieraFA fa = it2.next();
					log.debug("Comparando FA : " + fa.getIdCuentaFinanciera() + " con : " + faPorDefecto);
					if(fa.getIdCuentaFinanciera().equals(faPorDefecto)){
						log.debug("Encuentro la por defecto... la ignoro");
						continue;
					}
					else{
						String tienePat = (fa.isTienePat())? "" : "hidden";
						sb.append("<div id=\"otras-cuentas-C"+fa.getIdCuentaFinanciera()+"\" class=\"otras-cuentas\">\r\n" + 
								"	<div class=\"checkbox\">\r\n" + 
								"		<input type=\"checkbox\" value=\" "+ fa.getMontoTotal() +" \" onClick=\"recibeOtraCuenta(this)\" id=\"C"+ fa.getIdCuentaFinanciera() +"\" name=\"check\">\r\n" + 
								"		<label for=\"C"+ fa.getIdCuentaFinanciera() +"\"></label>\r\n" + 
								"	</div>\r\n" + 
								"	<div class=\"cuenta-n\">\r\n" +
								"		<p><span>Cuenta N° </span><strong> "+fa.getIdCuentaFinanciera()+"</strong>" + 
								"<div class='link-n hidden-xs'>"+
		                        "<a href='#0' class='link-simple-arrow-forward c-white'>Ver líneas asociadas</a>"+
		                        "</div>"+
								"	</div>\r\n" +
								"	<div class=\"monto-a-pagar\">\r\n" + 
								"		<p>\r\n" + 
								"			<span>Monto a pagar</span>\r\n" + 
								"			<em><i>$</i>"+ UtilesPago.montoEnFormatoFront(fa.getMontoTotal()) +"</em>\r\n" + 
								"		</p>\r\n" + 
								"	</div>\r\n" + 
								"	<div class=\"tiene-activo "+tienePat+"\">\r\n" + 
								"		<p><span>Tiene activo un </span><strong>Pago Automatico PAC o PAT</strong></p>\r\n" + 
								"	</div>\r\n" +
								"	<div class=\"flecha\">\r\n" +
								"		<img src=\"/mo-theme/images/img-flecha-abajo-otras-cuentas.png\" alt=\"\">\r\n" +
								"		<input type=\"hidden\" class=\"faseleccionada\" value=\"" + fa.getIdCuentaFinanciera() +"\" />"+
								"		<input type=\"hidden\" class=\"destinoresultado\" value=\"" + contadorDeCuentas +"\" />"+
								"	</div>\r\n" + 
								"	<div class=\"ver-lineas-asociandas\">\r\n" + 
								//"		<button  onClick=\"obtenerLineas(" +fa.getIdCuentaFinanciera() +")\"  class=\"btn hidden-mobile fuenteTam12 btn2 bg-dark btn-ver-lineas-desktop\" >Ver lineas asociadas</button>\r\n" + 
								//"		<button  onClick=\"obtenerLineas(" +fa.getIdCuentaFinanciera() +")\"   class=\"btn hidden-desktop btn2 bg-dark btn-ver-lineas-mobile\" >Ver lineas</button>\r\n" + 
								"	<button id='prorroga-"+fa.getIdCuentaFinanciera()+"' class='btn btn2 bg-dark text-center' onclick='solicitarProrroga()' data-toggle='modal' data-target='#modal_solicitar_prorroga' style='display:none !important'>Solicitar prórroga</button>"+
								"	</div>\r\n" +
								"</div>");
						sb.append("<div id=\"otras-cuentas\" class=\"boletas\">\r\n" + 
								"	<ul id=\"otraCuenta_"+contadorDeCuentas+"\" class=\"cont-boletas\">");			
						
						sb.append("</ul>\r\n" + 
								"</div>");
					}
					contadorDeCuentas++;
				}
			}
		}
		catch(Exception e){
			log.error("Error al generar las otras deudas : " + e);
		}
		return sb.toString();
	}

	public static String tieneVariasCuentas(RespuestaFindAccount resp){
		log.debug("Contando cantidad de cuentas...");
		String tiene = "";
		int cantCuentas = 0;
		//Aqui while
		Iterator<BillingCustomerFA> itBilling = resp.getBillingCustomer().iterator();
		while (itBilling.hasNext()){
			
			BillingCustomerFA bill = itBilling.next();
			Iterator<CuentaFinancieraFA> itCuenta = bill.getCuentasFinancieras().iterator();
			while(itCuenta.hasNext()){
				itCuenta.next();
				cantCuentas ++;
				log.debug("Iterando : " + cantCuentas);
			}
		}
		log.debug("El men tiene " + cantCuentas + " cuentas asociadas");
		
		if(cantCuentas > 1){
			tiene = "<div class=\"ver-todas-las-cuentas\">\r\n" + 
					"     <span class=\"obien inline-block\">\r\n" + 
					"         <h5>\r\n" + 
					"             <a id=\"ver-todas-las-cuentas\" href=\"javascript:void(0)\" class=\"boton-masinfo\">\r\n" + 
					"                 Ver todas las cuentas asociadas a mi RUT\r\n" + 
					"             </a>\r\n" + 
					"         </h5>\r\n" + 
					"     </span>\r\n" + 
					"</div>";
		}
		return tiene;
	}
	
	public static String cuentaConPat(RespuestaFindAccount respFind , String fa){
		
		String tiene = "false";
		
		try{
			Iterator<BillingCustomerFA> it = respFind.getBillingCustomer().iterator();
			while(it.hasNext()){
				
				BillingCustomerFA bill = it.next();
				Iterator<CuentaFinancieraFA> itCuentas = bill.getCuentasFinancieras().iterator();
				while(itCuentas.hasNext()){
					
					CuentaFinancieraFA fas = itCuentas.next();
					
					if(fas.getIdCuentaFinanciera().equals(fa)){
						log.debug("Encuentro la cuenta para validar PAT");
						if(fas.isTienePat()){
							tiene="true";
						}
					}
				}	
			}
			
		}
		catch(Exception e){
			log.error("Error al obtener el PAT de la cuenta " + fa);
		}
		
		return tiene;
	}
	
public String generaDetalleBoletasServicios(RespuestaFindAccount respFind, String cuentaSeleccionada , RespuestaFindAccount deudaPrincipal){
		
		log.debug("Entro a obtener el cuerpo del detalle");		
		StringBuilder cuerpo = new StringBuilder();
		
		//DocumentoFA doc1 = respFind.getBillingCustomer().get(0).getCuentasFinancieras().get(0).getDocumentos().get(0);	
		//log.debug("Documento : " + doc1.getIdDocumento() + " token : " + doc1.getTokenDePago());		
		//DocumentoFA doc2 = respFind.getBillingCustomer().get(0).getCuentasFinancieras().get(0).getDocumentos().get(1);
		//log.debug("Documento : " + doc2.getIdDocumento() + " token : " + doc2.getTokenDePago());
		
		log.debug("La cuenta seleccionada en el detalle documento es : " + cuentaSeleccionada);
		Iterator<DocumentoFA> it = respFind.getBillingCustomer().get(0).getCuentasFinancieras().get(0).getDocumentos().iterator();
		cuerpo.append("<ul class=\"cont-boletas\">");
		
		boolean tienePPS = false;
		
		Iterator<BillingCustomerFA> itDeudaPrincipal = deudaPrincipal.getBillingCustomer().iterator();
		while(itDeudaPrincipal.hasNext()){
			
			BillingCustomerFA bill = itDeudaPrincipal.next();
			Iterator<CuentaFinancieraFA> itBill = bill.getCuentasFinancieras().iterator();
			while(itBill.hasNext()){
				CuentaFinancieraFA cuenta = itBill.next();
				log.debug("Comparando : " + cuenta.getIdCuentaFinanciera() + " con " + cuentaSeleccionada);
				if(cuenta.getIdCuentaFinanciera().equals(cuentaSeleccionada)){
					tienePPS = cuenta.isTienePagoPorServicio();
					break;
				}
				else{
					continue;
				}
			}
		}
		String metodoDetalleServicio = (tienePPS)? "callObtenerServicios(this)" : "muestraNoTienePPS()" ;
		
		log.debug("La cuenta financiera " + cuentaSeleccionada + " tiene pago por servicio ? " + tienePPS);
		
		String divDetalleServicios = "";
		
		while (it.hasNext()){
			DocumentoFA doc = it.next();

			if(doc.isVencido()){
				cuerpo.append("<li class=\"boleta roja boletaVencida\">\r\n" + 
						"		<div class=\"cont-boleta\">\r\n" +
						"			<div class=\"slabel\">\r\n" + 
						"				<input type=\"checkbox\" value=\" " + doc.getMonto() +  "\" onClick=\"recibeDocumentoCuentaPrincipal(this)\" id=\"D"+ doc.getIdDocumento()+"C"+ cuentaSeleccionada+"\" name=\"check\" checked=\"\" >\r\n" + 
						"				<label for=\"D"+doc.getIdDocumento()+"C"+cuentaSeleccionada+"\"></label>\r\n" +
						"			</div>\r\n" + 
						"			<div class=\"stext\">\r\n" + 
						"				<div class=\"s1\">\r\n" + 
						"					<p>\r\n" + 
						"						<span class=\"hidden-xs\">Documento <strong>vencido</strong></span>\r\n" + 
						"						<span class=\"visible-xs\">Documento </span>\r\n" + 
						"						<strong class=\"txt-1 hidden-xs\">N° "+ doc.getNumeroBoleta() +"</strong>\r\n" + 
						"						<strong class=\"txt-1 visible-xs\"> "+ doc.getNumeroBoleta() +"</strong>\r\n" + 
						"						<strong class=\"txt-2\"></strong>\r\n" + 
						"					</p>\r\n" + 
						"				</div>\r\n" + 
						"				<div class=\"s2\">\r\n" + 
						"					<p>\r\n" + 
						"						<span class=\"hidden-xs\">Fecha de vencimiento</span>\r\n" + 
						"						<span class=\"visible-xs\">Vencimiento</span>\r\n" + 
						"						<strong>" + doc.getFecha() + "</strong>\r\n" + 
						"					</p>\r\n" + 
						"				</div>\r\n" + 
						"				<div class=\"s3\">\r\n" + 
						"					<p>\r\n" + 
						"						<span class=\"hidden-xs\">Subtotal</span>\r\n" + 
						"						<span class=\"visible-xs\">Monto</span>\r\n" + 
						"						<em><i>$</i>"+ UtilesPago.montoEnFormatoFront(doc.getMonto()) +"</em>\r\n" + 
						"					</p>\r\n" + 
						"				</div>\r\n" + 
						"			</div>\r\n" + 
						"		</div>\r\n" + 
						"<div class=\"mens1 mensaje\" style=\"display: none;\">\r\n" + 
						"      <i class=\"MCSS-alerta\"></i>\r\n" + 
						"      <span class=\"txt1\">Has modificado los servicios a pagar de esta boleta</span>\r\n" + 
						"      <span class=\"txt2\">El monto total de esta boleta fue \r\n" + 
						"      		cambiado porque has modificado los \r\n" + 
						"           servicios a pagar.</span>\r\n" + 
						"</div>"+
						
						"	</li>");
			}
			else{
				cuerpo.append("<li class=\"boleta\">\r\n" + 
						"		<div class=\"cont-boleta\">\r\n" + 
						"			<div class=\"slabel\">\r\n" + 
						"				<input type=\"checkbox\" onClick=\"recibeDocumentoCuentaPrincipal(this)\" value=\" "+ doc.getMonto() +"  \" id=\"D"+doc.getIdDocumento()+"C"+cuentaSeleccionada+"\" name=\"check\" checked=\"\">\r\n" + 
						"				<label for=\"D"+doc.getIdDocumento()+"C"+cuentaSeleccionada+"\"></label>\r\n" + 
						"			</div>\r\n" + 
						"			<div class=\"stext\">\r\n" + 
						"				<div class=\"s1\">\r\n" + 
						"					<p>\r\n" + 
						"						<span>Boleta </span>\r\n" + 
						"						<strong class=\"txt-1 hidden-xs\">N° "+ doc.getNumeroBoleta() +"</strong>\r\n" + 
						"						<strong class=\"txt-1 visible-xs\">"+ doc.getNumeroBoleta() +"</strong>\r\n" + 
						"						<strong class=\"txt-2\"></strong>\r\n" + 
						"					</p>\r\n" + 
						"				</div>\r\n" + 
						"				<div class=\"s2\">\r\n" + 
						"					<p>\r\n" + 
						"						<span class=\"hidden-xs\">Fecha de vencimiento</span>\r\n" + 
						"						<span class=\"visible-xs\">Vencimiento</span>\r\n" + 
						"						<strong>"+ doc.getFecha()+"</strong>\r\n" + 
						"					</p>\r\n" + 
						"				</div>\r\n" + 
						"				<div class=\"s3\">\r\n" + 
						"					<p>\r\n" + 
						"						<span class=\"hidden-xs\">Subtotal</span>\r\n" + 
						"						<span class=\"visible-xs\">Monto</span>\r\n" + 
						"						<em><i>$</i>" + UtilesPago.montoEnFormatoFront(doc.getMonto()) +"</em>\r\n" + 
						"					</p>\r\n" + 
						"				</div>\r\n" + 
						"			</div>\r\n" + 
						"			<div class=\"slink\">\r\n" + 
						"				<a href=\"javascript:void(0);\" onClick="+metodoDetalleServicio+" id=\"detalle-servicios-1\" class=\"link-simple\" data-toggle=\"modal\" data-target=\"#modal_detalle_servicios\">\r\n" + 
						"					<input type=\"hidden\" id=\"documento_marcado\" value=\""+ doc.getIdDocumento() +"\"/>\r\n"+
						"					<input type=\"hidden\" id=\"cuenta_padre\" value=\""+ cuentaSeleccionada +"\"/>"+
						"					<img src=\"/mo-theme/images/icon-light-detalle.png\">\r\n" + 
						"					<span>Detalle de servicios</span>\r\n" + 
						"				</a>\r\n" + 
						"			</div>\r\n" + 
						"		</div>\r\n" + 
						
						"<div class=\"mens1 mensaje\" style=\"display: none;\">\r\n" + 
						"                                <i class=\"MCSS-alerta\"></i>\r\n" + 
						"                                <span class=\"txt1\">Has modificado los servicios a pagar de esta boleta</span>\r\n" + 
						"                                <span class=\"txt2\">El monto total de esta boleta fue \r\n" + 
						"                                    cambiado porque has modificado los \r\n" + 
						"                                    servicios a pagar.</span>\r\n" + 
						"                            </div>"+
						
						"	</li>");
			}
			divDetalleServicios += "<div class=\"marcadorContServ\" id=\"contServicios"+"D"+doc.getIdDocumento()+"C"+cuentaSeleccionada +"\" ></div>";			
		}
		cuerpo.append("</ul>");
		
		cuerpo.append(divDetalleServicios);
		return cuerpo.toString();
	}

public String generaDetalleServicios(RespuestaFindAccount resp, String documento , String cuenta){
	
	StringBuilder retorno = new StringBuilder();
	
	Iterator<ServicioFA> it = resp.getBillingCustomer().get(0).getCuentasFinancieras().get(0).getDocumentos().get(0).getServicios().iterator();
	
	retorno.append("");
	
	int montoTotal = 0;
	int total = 0;
	String idCuentaPadre = cuenta;
	String idDocumentoPadre = documento;
	StringBuilder montoCuentas= new StringBuilder();
	while(it.hasNext()){
		ServicioFA serv = it.next();
		log.debug("Servicio obtenido : " + serv.getNombre());
		montoCuentas.append("<li>\r\n" + 
				"	<input type=\"checkbox\" class=\"regulado\"  value=\" "+ serv.getMonto() +"  \" onClick=\"recibeServicio(this)\" id=\"S"+ serv.getIdServicio() +"D"+documento+"C"+cuenta+"\" name=\"check\" checked />\r\n" + 
				"	<label for=\"S"+serv.getIdServicio()+"D"+documento+"C"+cuenta+"\">" + serv.getNombre() + "</label>\r\n" + 
				"	<span>$ "+ UtilesPago.montoEnFormatoFront(serv.getMonto()) +"</span>\r\n" +
				"	</li>\r\n");
		
		
		try{
			montoTotal += Integer.parseInt(serv.getMonto());
		}
		catch(NumberFormatException e){
			log.error("Error al parsear el monto total de los servicios...");
		}
		
	}
	total = montoTotal;
	
	
	retorno.append("<!-- MODAL DETALLE DE SERVICIOS #1 -->\r\n" + 
			"<div id=\"modal_detalle_servicios_D"+idDocumentoPadre+"C"+idCuentaPadre +"\" class=\"modal-mo modal-cont-servicios\">\r\n" + 
			"	<input type=\"hidden\" id=\"documentoPadre\" value=\""+  idDocumentoPadre  +"\"/>"+
			"	<input type=\"hidden\" id=\"cuentaPadre\" value=\""+  idCuentaPadre  +"\"/>"+
			"	<div class=\"modal-dialog detalle_de_servicios\">\r\n" + 
			"		<!-- Modal content-->\r\n" + 
			"		<div class=\"modal-content\">\r\n" + 
			"			<div class=\"modal-body\">\r\n" + 
			"				<div class=\"caja-dialogo inline-block width-100 bg-white\">\r\n" + 
			"					<div class=\"cabecera\">\r\n" + 
			"						<span>\r\n" + 
			"							Boleta N°"+ documento +"\r\n" + 
			"						</span>\r\n" + 
			"						<i class=\"MCSS-cancel bt-close-modal-mo\" onClick=\"cierraModalServicios('"+"D"+idDocumentoPadre+"C"+idCuentaPadre+"')\"></i>\r\n" + 
			"					</div>\r\n" + 
			"					<div class=\"cont-modal\">\r\n" + 
			"						<h4>Detalle de servicios</h4>\r\n" + 
			"						<p>A continuación podrás ver los servicios asociados a esta boleta.\r\n" + 
			"						</p>\r\n" + 
			"						<div class=\"importante\">\r\n" + 
			"							<div class=\"cont-alerta\">\r\n" + 
			"								<i class=\"MCSS-alerta_v2\"></i>\r\n" + 
			"							</div>\r\n" + 
			"							<div class=\"cont-importante\">\r\n" + 
			"								<h5>Importante</h5>\r\n" + 
			"								<p>Puedes elegir no pagar un servicio este mes desmarcando su casilla.\r\n" + 
			"								</p>\r\n" + 
			"								<p>Sin embargo, esto solo lo puedes realizar 2 veces al año</p>\r\n" + 
			"							</div>\r\n" + 
			"						</div>\r\n" + 
			"						<form action=\"\">\r\n" + 
			"							<div id=\"detalle-servicios\" class=\"cont-servicios\">\r\n" + 
			"								<ul>\r\n" + 
												montoCuentas+
			"									<div class=\"monto-total\">\r\n" + 
			"										<span class=\"txt-total\">Total a pagar</span>\r\n" + 
			"										<span class=\"txt-pagar\" id='txtpagar'>$ "+UtilesPago.montoEnFormatoFront(String.valueOf(total))+"</span>\r\n" + 
			"									</div>\r\n" + 
			"									<button id=\"confirmar-cambio-en-servicios\" type=\"button\" onClick=\"muestraModalDesmarcarServicio()\" class=\"btn-verde bt-close-modal-mo\" disabled>Confirmar cambio en monto a pagar de esta boleta</button>\r\n" + 
			"								</ul>\r\n" + 
			"							</div>\r\n" + 
			"						</form>\r\n" + 
			"					</div>\r\n" + 
			"				</div>\r\n" + 
			"			</div>\r\n" + 
			"		</div>\r\n" + 
			"	</div>\r\n" + 
			"</div>");
	
	return retorno.toString();
}



}

