package cl.ev.mo.portalcorporate.utiles;

/**
 * 
 * @author ev
 *
 */
public class Constantes {

	public static final String PDP_SESSION_ID = "PDP_SESSION_ID" ;

	/** ----CanalesPDP ----- */
	public static final String PAGO_DE_DEUDAS_PUBLICO = "32";
	public static final String PAGO_DE_DEUDAS_PRIVADO = "30";

	/** ---- Servicios y BD ----- */
	public static final String _session_en_db = "session_en_db";
	public static final String _endpoint_find_account = "endpoint_find_account";
	public static final String _biller_find_account = "biller_find_account"; 
	public static final String _biller_find_account_corporate_publico = "biller_find_account_corporate_publico"; 
	public static final String _biller_find_account_corporate_privado = "biller_find_account_corporate_privado"; 

	/** Operaciones de ajax*/
	public static final String SEND_NOTIFICATION = "notifica";
	public static final String IMPRIMIR = "imprime";
	public static final String DOWNLOAD_PDF = "download_pdf";
	public static final String GET_DOCUMENTOS="getDocumentos";
	public static final String GET_SERVICIOS="gets";
	
	
	/**Codigos de respuesta del WS*/
	public static final String CONSULTA_EXITOSA = "0";
	public static final String ERROR_AL_CONSULTAR = "-1";
	public static final String RUT_SIN_DEUDA = "9401"; //13 en virtualizado  //9401 en RDP
	public static final String NO_EXISTE = "9005"; //validar. datos no encontrados, no existe fa
	public static final String FA_NO_ENCONTRADA = "1"; //datos no encontrados
	
	
	/** valida acceso*/
	public static final String _validaAcceso_idEmpresa = "devetel_id_empresa";
	public static final String _validaAcceso_idCanalPrivado = "devetel_id_canal_privado";
	public static final String _validaAcceso_idCanalPublico = "devetel_id_canal_publico";
	public static final String _validaAcceso_contCanalPrivado = "devetel_password_canal_privado";
	public static final String _validaAcceso_contCanalPublico = "devetel_password_canal_publico";
	public static final String _validaAcceso_paginaRespGateway = "devetel_pagina_respuesta_gateway";
	
	/**Endpoint de WS*/
	public static final String _endpoint_valida_acceso = "endpoint_valida_acceso";
	public static final String _endpoint_create_ticket = "endpoint_create_ticket";
	public static final String _endpoint_create_debt_payment = "endpoint_create_debt_payment";
	public static final String _endpoint_find_ticket = "endpoint_find_ticket";
	public static final String _endpoint_send_legacy_event = "endpoint_send_legacy_event";
	public static final String _endpoint_billing_customer_service_local = "endpoint_billing_customer_service_local";
	public static final String _endpoint_collection_customer_service = "endpoint_collection_customer_service";

	
	/** correo*/
	public static final String NOTIFICACION_VENTA = "N-296";
	public static final String NOTIFICACION_PAGO = "000701";
	public static final String NOTIFICACION_TICKET_PRESENCIAL = "N-295";
	
	/**Otros*/
	public static final String _kit_simulado = "kit_simulado";
	public static final String CONSULTA_MOVIL_CUENTA="ConsultaMovilCuenta";
	public static final String CONSULTA_MOVIL_DOCUMENTO="ConsultaMovilDocumento";
	public static final String CONSULTA_MOVIL_SERVICIOS="ConsultaMovilServicios";
	
	
	/**Variables de session*/
	public static final String IDENTIFICADOR="identificador";
	public static final String FA_CLIENTE="fa_cliente";
	public static final String DOCUMENTO="documento";
	public static final String PROCEDENCIA = "procedencia";
	public static final String PROCEDENCIA_PUBLICA = "publico_corporate";
	public static final String PROCEDENCIA_PRIVADA = "privado_corporate_SMB";
	public static final String ES_PRIMERA_CARGA = "primera_carga";
	public static final String TOKEN_INTEGRACION_PDP = "token_integracion_pdp";
	public static final String TIPO_PAGO = "tipo_pago";
	public static final String CUENTA_FINANCIERA = "cuenta_financiera"; //validar
	public static final String MEDIO_PAGO = "medio_pago";
	public static final String ID_TRX_DEVETEL = "id_trx_devetel";
	public static final String ID_CANAL_DE_PAGO = "id_canal_de_pago";
	public static final String NOMBRE_CLIENTE = "nombre_cliente";
	public static final String NUMERO_CLIENTE = "numero_cliente";
	public static final String CORREO_CLIENTE = "correo_cliente";
	public static final String FECHA_VENCIMIENTO_DEUDA = "fecha_vencimiento_deuda";
	public static final String MONTO_TOTAL_A_PAGAR = "monto_total_pago";
	public static final String NUMERO_DE_ORDEN = "numero_orden_de_compra";
	public static final String FA_DOC_SERV_A_PAGAR = "identificador_de_pago";
	public static final String ID_TICKET_RDP = "id_ticket_rdp";
	public static final String TOKEN_DE_PAGO = "token_de_pago";
	public static final String PAYMENT_SYSTEM_ID = "payment_system_id";
	public static final String detalle_deuda = "detalle_deuda";
	public static final String detalle_deuda_documentos="detalle_deuda_documentos";
	public static final String nombre_cliente = "nombre_cliente";

	//errores
	public static final String FA_SIN_DEUDA = "fa_sin_deuda";
	public static final String FA_NO_EXISTE = "fa_no_existe";
	public static final String TIPO_ERROR= "tipo_error";
	
	//Ventas
	public static final String VENTAS_NUMERO_DE_ORDEN = "ventas_numero_orden";
	public static final String VENTAS_CUENTA_FINANCIERA = "ventas_cuenta_financiera";
	public static final String VENTAS_URL_RETORNO = "ventas_url_retorno";
	public static final String CARRO_COMPRAS = "carro_compras";
	/**fin Variables de session*/
	
	
	//Abono
	public static final String MONTO_ABONO = "monto_abono";
	public static final String TIPO_PAGO_ABONO = "tipo_pago_abono";
	
	
	
	/**nombres medios de pagos*/
	public static final String CUPON_PRESENCIAL = "coupon_presential";
/*	public static final String webPay = "Webpay";
	public static final String servipag = "Servipag";
	public static final String bancoEstado = "Banco Estado";
	public static final String bci = "BCI";
	*/
	/**fin nombres medios de pagos*/
	

}
