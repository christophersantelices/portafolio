
package cl.ev.mo.portalcorporate.devetelWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para autentificacion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="autentificacion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="COD_PORTADOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EMPRESA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ID_CANAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PAG_RESP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PASSWORD_CANAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SERVICIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TIPO_REGISTRO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OPTIONAL1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OPTIONAL2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "autentificacion", propOrder = {
    "codportador",
    "empresa",
    "idcanal",
    "pagresp",
    "passwordcanal",
    "servicio",
    "tiporegistro",
    "optional1",
    "optional2"
})
public class Autentificacion {

    @XmlElement(name = "COD_PORTADOR")
    protected String codportador;
    @XmlElement(name = "EMPRESA")
    protected String empresa;
    @XmlElement(name = "ID_CANAL")
    protected String idcanal;
    @XmlElement(name = "PAG_RESP")
    protected String pagresp;
    @XmlElement(name = "PASSWORD_CANAL")
    protected String passwordcanal;
    @XmlElement(name = "SERVICIO")
    protected String servicio;
    @XmlElement(name = "TIPO_REGISTRO")
    protected String tiporegistro;
    @XmlElement(name = "OPTIONAL1")
    protected String optional1;
    @XmlElement(name = "OPTIONAL2")
    protected String optional2;

    /**
     * Obtiene el valor de la propiedad codportador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODPORTADOR() {
        return codportador;
    }

    /**
     * Define el valor de la propiedad codportador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODPORTADOR(String value) {
        this.codportador = value;
    }

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMPRESA() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMPRESA(String value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad idcanal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDCANAL() {
        return idcanal;
    }

    /**
     * Define el valor de la propiedad idcanal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDCANAL(String value) {
        this.idcanal = value;
    }

    /**
     * Obtiene el valor de la propiedad pagresp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAGRESP() {
        return pagresp;
    }

    /**
     * Define el valor de la propiedad pagresp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAGRESP(String value) {
        this.pagresp = value;
    }

    /**
     * Obtiene el valor de la propiedad passwordcanal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPASSWORDCANAL() {
        return passwordcanal;
    }

    /**
     * Define el valor de la propiedad passwordcanal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPASSWORDCANAL(String value) {
        this.passwordcanal = value;
    }

    /**
     * Obtiene el valor de la propiedad servicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERVICIO() {
        return servicio;
    }

    /**
     * Define el valor de la propiedad servicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERVICIO(String value) {
        this.servicio = value;
    }

    /**
     * Obtiene el valor de la propiedad tiporegistro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOREGISTRO() {
        return tiporegistro;
    }

    /**
     * Define el valor de la propiedad tiporegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOREGISTRO(String value) {
        this.tiporegistro = value;
    }

    /**
     * Obtiene el valor de la propiedad optional1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOPTIONAL1() {
        return optional1;
    }

    /**
     * Define el valor de la propiedad optional1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOPTIONAL1(String value) {
        this.optional1 = value;
    }

    /**
     * Obtiene el valor de la propiedad optional2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOPTIONAL2() {
        return optional2;
    }

    /**
     * Define el valor de la propiedad optional2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOPTIONAL2(String value) {
        this.optional2 = value;
    }

}
