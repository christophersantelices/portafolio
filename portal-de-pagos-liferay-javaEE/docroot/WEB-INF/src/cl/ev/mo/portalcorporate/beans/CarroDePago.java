package cl.ev.mo.portalcorporate.beans;

import java.util.ArrayList;
import java.util.Iterator;

public class CarroDePago {

	
	private boolean valido;
	private ArrayList<ObjetoDePago> objetosDePago;
	
	public CarroDePago(){
		valido = true;
	}

	public boolean isValido() {
		return valido;
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public ArrayList<ObjetoDePago> getObjetosDePago() {
		return objetosDePago;
	}

	public void setObjetosDePago(ArrayList<ObjetoDePago> objetosDePago) {
		this.objetosDePago = objetosDePago;
	}
	
	public String getMontoTotalPago(){
		int montoTotal = 0;
		Iterator<ObjetoDePago> it = getObjetosDePago().iterator();
		while(it.hasNext()){
			ObjetoDePago obj = it.next();
			montoTotal += Integer.parseInt(obj.getMontoAPagar());
		}
		return String.valueOf(montoTotal);
	}
}
