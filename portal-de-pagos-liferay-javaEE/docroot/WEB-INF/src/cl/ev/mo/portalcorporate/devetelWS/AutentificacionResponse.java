
package cl.ev.mo.portalcorporate.devetelWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para autentificacionResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="autentificacionResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="COD_RESPUESTA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ID_CANAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ID_TRX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "autentificacionResponse", propOrder = {
    "codrespuesta",
    "idcanal",
    "idtrx"
})
public class AutentificacionResponse {

    @XmlElement(name = "COD_RESPUESTA")
    protected String codrespuesta;
    @XmlElement(name = "ID_CANAL")
    protected String idcanal;
    @XmlElement(name = "ID_TRX")
    protected String idtrx;

    /**
     * Obtiene el valor de la propiedad codrespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODRESPUESTA() {
        return codrespuesta;
    }

    /**
     * Define el valor de la propiedad codrespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODRESPUESTA(String value) {
        this.codrespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad idcanal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDCANAL() {
        return idcanal;
    }

    /**
     * Define el valor de la propiedad idcanal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDCANAL(String value) {
        this.idcanal = value;
    }

    /**
     * Obtiene el valor de la propiedad idtrx.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDTRX() {
        return idtrx;
    }

    /**
     * Define el valor de la propiedad idtrx.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDTRX(String value) {
        this.idtrx = value;
    }

}
