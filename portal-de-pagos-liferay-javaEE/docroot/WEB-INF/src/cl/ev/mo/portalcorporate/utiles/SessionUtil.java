package cl.ev.mo.portalcorporate.utiles;

import javax.portlet.ActionRequest;
import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.beans.RespuestaFindAccount;
import cl.ev.mo.portalcorporate.utiles.DBSupport;



public class SessionUtil {
	
	static Log log = LogFactoryUtil.getLog(SessionUtil.class);
	private static final String DETALLE_DEUDA = "detalle_deuda";
	
	private static String getCookieValue(HttpServletRequest request){
		
		log.debug("Oteniendo valor de la cookie");
		Cookie[] cookies = request.getCookies();
		String valor = "";
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				
				//log.debug("Nombre : " + cookie.getName() + " , valor : " + cookie.getValue());
				
				if (cookie.getName().equals(Constantes.PDP_SESSION_ID)) {
					String getCookie=cookie.getValue();
					valor =  getCookie;
					break;
				}
			}
		}
		log.debug("Valor : " + valor);
		return valor;
		
	}

	public static void setVariable( ResourceRequest request2 , String clave , String valor){
		
		boolean sessionEnDb = (UtilesContenidos.obtenerContenidoTexto(request2, Constantes._session_en_db).equals("true"))? true :false;
		
		if (sessionEnDb){
			HttpServletRequest request = PortalUtil.getHttpServletRequest(request2);
			String pdpSession = getCookieValue(request);
			//log.debug("Session PDP obtenida : " + pdpSession);
			DBSupport db = new DBSupport();
			db.insertaDatoSession(pdpSession, clave, valor);
		}
		else{
			log.debug("setVariable local");
			PortletSession psession = request2.getPortletSession();
			psession.setAttribute(clave, valor, PortletSession.APPLICATION_SCOPE);
		}
	}
	
	public static void setVariable( ActionRequest actionRequest , String clave , String valor){
		
		boolean sessionEnDb = (UtilesContenidos.obtenerContenidoTexto(actionRequest, Constantes._session_en_db).equals("true"))? true :false;
		
		if (sessionEnDb){
			HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
			String pdpSession = getCookieValue(request);
			//log.debug("Session PDP obtenida : " + pdpSession);
			DBSupport db = new DBSupport();
			db.insertaDatoSession(pdpSession, clave, valor);
		}
		else{
			log.debug("setVariable local");
			PortletSession psession = actionRequest.getPortletSession();
			psession.setAttribute(clave, valor, PortletSession.APPLICATION_SCOPE);
		}
	}
	
	public static RespuestaFindAccount getDetalleDeuda(ActionRequest actionRequest){
		
		boolean sessionEnDb = (UtilesContenidos.obtenerContenidoTexto(actionRequest, Constantes._session_en_db).equals("true"))? true :false;
		
		if(sessionEnDb){
			RespuestaFindAccount resp = null;	
			Gson gson = new Gson();
			DBSupport db = new DBSupport();
			HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
			String pdpSession = getCookieValue(request);
			//log.debug("Session PDP obtenida : " + pdpSession);
			String deuda = db.getDetalleDeuda(pdpSession);
			resp =  gson.fromJson(deuda , RespuestaFindAccount.class);
			return resp;
		}
		else{
			RespuestaFindAccount resp = null;	
			Gson gson = new Gson();	
			resp =  gson.fromJson(getVariable( actionRequest, DETALLE_DEUDA), RespuestaFindAccount.class);
			return resp;
		}
	}
	
	public static RespuestaFindAccount getDetalleDeuda(ResourceRequest resourceRequest){
		boolean sessionEnDb = (UtilesContenidos.obtenerContenidoTexto(resourceRequest, Constantes._session_en_db).equals("true"))? true :false;
		
		if(sessionEnDb){
			RespuestaFindAccount resp = null;	
			Gson gson = new Gson();
			DBSupport db = new DBSupport();
			HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
			String pdpSession = getCookieValue(request);
			//log.debug("Session PDP obtenida : " + pdpSession);
			String deuda = db.getDetalleDeuda(pdpSession);
			resp =  gson.fromJson(deuda , RespuestaFindAccount.class);
			return resp;
		}
		else{
			RespuestaFindAccount resp = null;	
			Gson gson = new Gson();	
			resp =  gson.fromJson(getVariable( resourceRequest, DETALLE_DEUDA), RespuestaFindAccount.class);
			return resp;
		}
	}
	
	public static String getVariable( ActionRequest actionRequest , String clave){
		
		boolean sessionEnDb = (UtilesContenidos.obtenerContenidoTexto(actionRequest, Constantes._session_en_db).equals("true"))? true :false;
		
		if(sessionEnDb){
			HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
			String pdpSession = getCookieValue(request);
			//log.debug("Session PDP obtenida : " + pdpSession);
			DBSupport dbs = new DBSupport();
			String valor = dbs.getDatoSession(pdpSession, clave);
			return valor;
		}
		else{
			String valor = "";
			PortletSession psession = actionRequest.getPortletSession();
			valor = (String) psession.getAttribute(clave, PortletSession.APPLICATION_SCOPE);
			return valor;
		}
	}
	
	public static String getVariable( ResourceRequest resourceRequest , String clave){
		boolean sessionEnDb = (UtilesContenidos.obtenerContenidoTexto(resourceRequest, Constantes._session_en_db).equals("true"))? true :false;
		
		if(sessionEnDb){
			HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
			String pdpSession = getCookieValue(request);
			//log.debug("Session PDP obtenida : " + pdpSession);
			DBSupport dbs = new DBSupport();
			String valor = dbs.getDatoSession(pdpSession, clave);
			return valor;
		}
		else{
			String valor = "";
			PortletSession psession = resourceRequest.getPortletSession();
			valor = (String) psession.getAttribute(clave, PortletSession.APPLICATION_SCOPE);
			return valor;
		}
	}

	public static void guardaDetalleDeuda(ActionRequest actionRequest , RespuestaFindAccount resp){
		
		boolean sessionEnDb = (UtilesContenidos.obtenerContenidoTexto(actionRequest, Constantes._session_en_db).equals("true"))? true :false;
		
		Gson gson = new Gson();
		String json = gson.toJson(resp);
		log.debug("El objeto en JSON es : " + json);
		
		if(sessionEnDb){
			HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
			String pdpSession = getCookieValue(request);
			//log.debug("Session PDP obtenida : " + pdpSession);
			DBSupport db = new DBSupport();
			boolean guardado = db.insertaDetalleDeuda(pdpSession, json);
		}
		else{
			PortletSession psession = actionRequest.getPortletSession();
			psession.setAttribute(DETALLE_DEUDA, json, PortletSession.APPLICATION_SCOPE);
		}
	}
	
	public static void guardaDetalleDeuda(ResourceRequest resourceRequest , RespuestaFindAccount resp){
		boolean sessionEnDb = (UtilesContenidos.obtenerContenidoTexto(resourceRequest, Constantes._session_en_db).equals("true"))? true :false;
		
		Gson gson = new Gson();
		String json = gson.toJson(resp);
		log.debug("El objeto en JSON es : " + json);
		
		if(sessionEnDb){
			HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
			String pdpSession = getCookieValue(request);
			//log.debug("Session PDP obtenida : " + pdpSession);
			DBSupport db = new DBSupport();
			boolean guardado = db.insertaDetalleDeuda(pdpSession, json);
		}
		else{
			PortletSession psession = resourceRequest.getPortletSession();
			psession.setAttribute(DETALLE_DEUDA, json, PortletSession.APPLICATION_SCOPE);
		}
	}

	

}
