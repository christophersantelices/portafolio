package cl.ev.mo.portalcorporate.serviciosHelper;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.model.JournalArticleDisplay;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.journalcontent.util.JournalContentUtil;

import cl.ev.mo.portalcorporate.beans.DocumentoFA;
import cl.ev.mo.portalcorporate.beans.ServicioFA;
import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.utiles.SessionUtil;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderRequest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ResourceRequest;

public class UtilServicios {
	
	static Log log = LogFactoryUtil.getLog(UtilServicios.class);
	
	public static TEFHeaderRequest fillFindAccountDebtsHeader(TEFHeaderRequest _findAccountDebts_findAccountDebtsRequestHeader , String serviceName){

		try{
			/*Este es el legal !!!*/
			_findAccountDebts_findAccountDebtsRequestHeader.setUserLogin("fpagos");
			_findAccountDebts_findAccountDebtsRequestHeader.setServiceChannel("Otros");
			_findAccountDebts_findAccountDebtsRequestHeader.setSessionCode("123456");
			_findAccountDebts_findAccountDebtsRequestHeader.setApplication("Front de pagos COL");
			_findAccountDebts_findAccountDebtsRequestHeader.setIdMessage("1234");
			_findAccountDebts_findAccountDebtsRequestHeader.setIpAddress("1.1.1.1");
			_findAccountDebts_findAccountDebtsRequestHeader.setFunctionalityCode("Anything");
			_findAccountDebts_findAccountDebtsRequestHeader.setTransactionTimestamp(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(getTransactionTimestamp()));
			_findAccountDebts_findAccountDebtsRequestHeader.setServiceName(serviceName);
			_findAccountDebts_findAccountDebtsRequestHeader.setVersion("2.3");
			return _findAccountDebts_findAccountDebtsRequestHeader;
			
		}
		catch(Exception e){
			log.error("Error al llenar la cabecera de findAccount >> " + e);
			return null;
		}
	}	
	
	public static String getTransactionTimestamp(){		 
		String completo = "";
		try{
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//Entrada 
			SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");//Entrada 
			Date hoy = new Date();
			completo = sdf1.format(hoy) + "T" + sdf2.format(hoy);
		}
		catch(Exception e){
			log.error("Error al llenar getTransactionTimestamp " + e);
			e.printStackTrace();
		}		
		return completo;
	}
	
	public static String getOperationDate(){

		String completo = "";
		try{
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//Entrada 
			SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");//Entrada 
			Date hoy = new Date();
			completo = sdf1.format(hoy) + "T" + sdf2.format(hoy) +".340-03:00";
		}
		catch(Exception e){
			log.error("Error al llenar getOperationDate " + e);
			e.printStackTrace();
		}
		
		return completo;
	}
	
	public static String[] getDatosDocumento(ActionRequest actionRequest,String idDocumento){
		String[] datos=new String[2];
		String fecha="11-11-1111";
		String invoiceType="B";
		try{
			ArrayList<DocumentoFA> listaDocumentos=SessionUtil.getDetalleDeuda(actionRequest).getBillingCustomer().get(0).getCuentasFinancieras().get(0).getDocumentos(); 
			for(DocumentoFA doc : listaDocumentos) {
				if(doc.getIdDocumento().equals(idDocumento)){
					fecha=doc.getFecha();
					invoiceType=doc.getTypoDocumento();
					break;
				}
			}
		}
		catch(Exception e){
			log.error("Error al llenar getFechaVencimiento " + e);
			e.printStackTrace();
		}
		
		datos[0]=fecha;
		datos[1]=invoiceType;
		return datos;
	}
	public static String[] getDatosDocumento(ActionRequest actionRequest,String idDocumento,String idServicio){
		String[] datos=new String[3];
		String fecha="11-11-1111";
		String invoiceType="B";
		String nombreServ="";
		try{
			ArrayList<DocumentoFA> listaDocumentos=SessionUtil.getDetalleDeuda(actionRequest).getBillingCustomer().get(0).getCuentasFinancieras().get(0).getDocumentos(); 
			for(DocumentoFA doc : listaDocumentos) {
				if(doc.getIdDocumento().equals(idDocumento)){
					fecha=doc.getFecha();
					invoiceType=doc.getTypoDocumento();
					for(ServicioFA serv : doc.getServicios()) {
						if(serv.getIdServicio().equals(idServicio)){
							nombreServ=serv.getNombre();
							break;
						}
					}
					break;
				}
			}
		}
		catch(Exception e){
			log.error("Error al llenar getFechaVencimiento " + e);
			e.printStackTrace();
		}
		
		datos[0]=fecha;
		datos[1]=invoiceType;
		datos[3]=nombreServ;
		return datos;
	}

	/*
	public static String getContenidoTextoPorNombre(ActionRequest request ,String nombreContenido){
		
		String valor = "";
		try{
			String articleName = nombreContenido;
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), articleName);
			String articleId = journalArticle.getArticleId();
			JournalArticleDisplay articleDisplay = JournalContentUtil.getDisplay(themeDisplay.getScopeGroupId(),articleId, "","",themeDisplay);
			valor = articleDisplay.getContent();
		} 
		catch (Exception e){
			valor = "Default";
			log.error("Error al obtener el contenido web con nombre : " + nombreContenido);
			e.printStackTrace();
		}
		return valor;
	}
	
	public static String getContenidoTextoPorNombre(ResourceRequest request ,String nombreContenido){
		
		String valor = "";
		try{
			String articleName = nombreContenido;
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), articleName);
			String articleId = journalArticle.getArticleId();
			JournalArticleDisplay articleDisplay = JournalContentUtil.getDisplay(themeDisplay.getScopeGroupId(),articleId, "","",themeDisplay);
			valor = articleDisplay.getContent();
		}
		catch (Exception e){
			valor = "Default";
			log.error("Error al obtener el contenido web con nombre : " + nombreContenido);
		}
		return valor;
	}
	
*/

}