
package cl.ev.mo.portalcorporate.devetelWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para cliente complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="cliente"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DIRECCION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ID_NUM_CELULAR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ID_RUT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ID_TEL_FIJO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NOMBRE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NUM_CLIENTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="REGION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cliente", propOrder = {
    "direccion",
    "email",
    "idnumcelular",
    "idrut",
    "idtelfijo",
    "nombre",
    "numcliente",
    "region"
})
public class Cliente {

    @XmlElement(name = "DIRECCION")
    protected String direccion;
    @XmlElement(name = "EMAIL")
    protected String email;
    @XmlElement(name = "ID_NUM_CELULAR")
    protected String idnumcelular;
    @XmlElement(name = "ID_RUT")
    protected String idrut;
    @XmlElement(name = "ID_TEL_FIJO")
    protected String idtelfijo;
    @XmlElement(name = "NOMBRE")
    protected String nombre;
    @XmlElement(name = "NUM_CLIENTE")
    protected String numcliente;
    @XmlElement(name = "REGION")
    protected String region;

    /**
     * Obtiene el valor de la propiedad direccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDIRECCION() {
        return direccion;
    }

    /**
     * Define el valor de la propiedad direccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDIRECCION(String value) {
        this.direccion = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMAIL() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMAIL(String value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad idnumcelular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDNUMCELULAR() {
        return idnumcelular;
    }

    /**
     * Define el valor de la propiedad idnumcelular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDNUMCELULAR(String value) {
        this.idnumcelular = value;
    }

    /**
     * Obtiene el valor de la propiedad idrut.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDRUT() {
        return idrut;
    }

    /**
     * Define el valor de la propiedad idrut.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDRUT(String value) {
        this.idrut = value;
    }

    /**
     * Obtiene el valor de la propiedad idtelfijo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDTELFIJO() {
        return idtelfijo;
    }

    /**
     * Define el valor de la propiedad idtelfijo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDTELFIJO(String value) {
        this.idtelfijo = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOMBRE() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOMBRE(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad numcliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMCLIENTE() {
        return numcliente;
    }

    /**
     * Define el valor de la propiedad numcliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMCLIENTE(String value) {
        this.numcliente = value;
    }

    /**
     * Obtiene el valor de la propiedad region.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREGION() {
        return region;
    }

    /**
     * Define el valor de la propiedad region.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREGION(String value) {
        this.region = value;
    }

}
