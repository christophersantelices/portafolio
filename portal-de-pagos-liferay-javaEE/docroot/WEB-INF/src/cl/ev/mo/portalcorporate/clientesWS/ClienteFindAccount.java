package cl.ev.mo.portalcorporate.clientesWS;

import java.math.BigInteger;
import java.net.URL;

import javax.portlet.ActionRequest;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cl.ev.mo.portalcorporate.serviciosHelper.UtilServicios;
import cl.ev.mo.portalcorporate.utiles.Constantes; 
import cl.ev.mo.portalcorporate.utiles.UtilesContenidos;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.FindAccountDebtsBindingQSService;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.FindAccountDebtsPortType;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.FindAccountDebtsRequest;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.FindAccountDebtsRequestData;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.FindAccountDebtsResponse;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.ObjectFactory;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.Paging;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.Party;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.ThirdPartyPayeeAgency;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderRequest;



public class ClienteFindAccount extends UtilServicios{

	private String billerFindAccount = "";
	
	Log log = LogFactoryUtil.getLog(ClienteFindAccount.class);
	
	//private String endpoint = "http://10.186.31.15:9081/moESBRDPAD/FindAccountDebts";
	private String endpoint = "http://esb.tchile.local:8011/ accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1";
	//private static final String endpoint = "http://pca2635.ctc.cl:9084/ accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1";
	private static final QName SERVICE_NAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1", "FindAccountDebtsBindingQSService");
	URL wsdlURL;
	FindAccountDebtsBindingQSService ss;
	FindAccountDebtsPortType port;
	
	public ClienteFindAccount(ActionRequest request){
		
		//wsdlURL = FindAccountDebts.WSDL_LOCATION;
		this.endpoint = UtilesContenidos.obtenerContenidoTexto(request, Constantes._endpoint_find_account);
		this.billerFindAccount = "";
		//this.getClass().getResource("/wsdl/FindAccountDebts.wsdl");
		try{
			wsdlURL=getClass().getResource("/wsdl/FindAccountDebts.wsdl");
			//log.debug("url del wsdl generada con exito ");
		}catch(Exception e){
			log.error("Error al generar la url del wsdl");
		}

		ss = new FindAccountDebtsBindingQSService(wsdlURL, SERVICE_NAME);
		port = ss.getFindAccountDebtsBindingQSPort();  
		BindingProvider provider = (BindingProvider) port;
		provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY , endpoint); 
	}
	
	public ClienteFindAccount(String endpoint , String billerFindAccount){
		this.endpoint = endpoint;
		this.billerFindAccount = billerFindAccount;
		wsdlURL = this.getClass().getResource("/wsdl/FindAccountDebts.wsdl");
		if(wsdlURL==null){
			log.error("URL null");
		}
		ss = new FindAccountDebtsBindingQSService(wsdlURL, SERVICE_NAME);
		port = ss.getFindAccountDebtsBindingQSPort();  
		BindingProvider provider = (BindingProvider) port;
		provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY , endpoint);
		//provider.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 100000);
		//provider.getRequestContext().put(BindingProviderProperties.CONNECT_TIMEOUT, 100000);
	}
	

	public FindAccountDebtsResponse consultaOrden(String cuentaFinanciera, String numeroOrden){
		
		
		log.info("Llamando a findAccount >> consulta de orden >> FA : " +cuentaFinanciera + " , Nº orden : " + numeroOrden);
		log.info("Endpoint utilizado : " + this.endpoint);
		
		String serviceName="ConsultaMovilOrden";
		String idType = "IdCuenta";
		String idValue = cuentaFinanciera;
		String secundaryIdType = "IdOrden";
		String secundaryIdValue = numeroOrden;
		String biller = billerFindAccount;
		String partyRoleId = "902002";
		String partyId = "1";
		String operationDate = getOperationDate();
		
		FindAccountDebtsResponse _findAccountDebts__return = null;
		ObjectFactory factory = new ObjectFactory();
		
		try{
			log.info("Invoking findAccountDebts...");
			FindAccountDebtsRequest _findAccountDebts_findAccountDebtsRequest = new FindAccountDebtsRequest();
			TEFHeaderRequest _findAccountDebts_findAccountDebtsRequestHeader = new TEFHeaderRequest();
			// Lleno la cabecera
			_findAccountDebts_findAccountDebtsRequestHeader = fillFindAccountDebtsHeader(_findAccountDebts_findAccountDebtsRequestHeader, serviceName);
			//Seteo la cabecera
			_findAccountDebts_findAccountDebtsRequest.setHeader(_findAccountDebts_findAccountDebtsRequestHeader);
			
			
			// Llenando Data
			FindAccountDebtsRequestData _findAccountDebts_findAccountDebtsRequestData = new FindAccountDebtsRequestData();
			_findAccountDebts_findAccountDebtsRequestData.setServiceName(serviceName);
			_findAccountDebts_findAccountDebtsRequestData.setIDType(idType);
			_findAccountDebts_findAccountDebtsRequestData.setIDValue(idValue);
			
			javax.xml.bind.JAXBElement<java.lang.String> _findAccountDebts_findAccountDebtsRequestDataSecondaryIDType = factory.createFindAccountDebtsRequestDataSecondaryIDType(secundaryIdType);
			_findAccountDebts_findAccountDebtsRequestData.setSecondaryIDType(_findAccountDebts_findAccountDebtsRequestDataSecondaryIDType);
			javax.xml.bind.JAXBElement<java.lang.String> _findAccountDebts_findAccountDebtsRequestDataSecondaryIDValue = factory.createFindAccountDebtsRequestDataSecondaryIDValue(secundaryIdValue);
			_findAccountDebts_findAccountDebtsRequestData.setSecondaryIDValue(_findAccountDebts_findAccountDebtsRequestDataSecondaryIDValue);
			
			
			javax.xml.bind.JAXBElement<java.lang.String> _findAccountDebts_findAccountDebtsRequestDataBiller = factory.createFindAccountDebtsRequestDataBiller(biller);
			_findAccountDebts_findAccountDebtsRequestData.setBiller(_findAccountDebts_findAccountDebtsRequestDataBiller);
			
			
			ThirdPartyPayeeAgency _findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee = new ThirdPartyPayeeAgency();
			_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee.setPartyRoleId(partyRoleId);
			Party _findAccountDebts_findAccountDebtsRequestDataThirdPartyPayeeParty = new Party();
			_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayeeParty.setPartyId(partyId);
			_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee.setParty(_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayeeParty);
			_findAccountDebts_findAccountDebtsRequestData.setThirdPartyPayee(_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee);
			_findAccountDebts_findAccountDebtsRequestData.setOperationDate(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(operationDate));
			
						
			Paging paginador = new Paging();
			paginador.setPageSize(factory.createPagingPageSize(BigInteger.ZERO));
			paginador.setNumberOfRows(factory.createPagingNumberOfRows(BigInteger.ZERO));
			paginador.setPageNumber(factory.createPagingPageNumber(BigInteger.ONE));
			paginador.setHasMore(factory.createPagingHasMore(false));
			
			
			javax.xml.bind.JAXBElement<Paging> _findAccountDebts_findAccountDebtsRequestDataPaging = factory.createFindAccountDebtsRequestDataPaging(paginador);
			_findAccountDebts_findAccountDebtsRequestData.setPaging(_findAccountDebts_findAccountDebtsRequestDataPaging);
			_findAccountDebts_findAccountDebtsRequest.setData(_findAccountDebts_findAccountDebtsRequestData);
			log.info("El input de findAccount es : " );
			log.info("User login : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getUserLogin());
			log.info("Service channel : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getServiceChannel());
			log.info("Session code : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getSessionCode());
			log.info("Aplication : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getApplication());
			log.info("iD MENSAJE : " +_findAccountDebts_findAccountDebtsRequest.getHeader().getIdMessage());
			log.info("ip address : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getIpAddress());
			log.info("Functionality code : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getFunctionalityCode());
			log.info("Transaction timestamp : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getTransactionTimestamp());
			log.info("Service name : " +_findAccountDebts_findAccountDebtsRequest.getHeader().getServiceName());
			log.info("Version : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getVersion());
			log.info("************************");
			log.info("Service name : " + _findAccountDebts_findAccountDebtsRequest.getData().getServiceName() );
			log.info("Id type : " + _findAccountDebts_findAccountDebtsRequest.getData().getIDType());
			log.info("IdValue : " + _findAccountDebts_findAccountDebtsRequest.getData().getIDValue());
			log.info("Biller : " + _findAccountDebts_findAccountDebtsRequest.getData().getBiller().getValue());
			log.info("Party role ID : " + _findAccountDebts_findAccountDebtsRequest.getData().getThirdPartyPayee().getPartyRoleId());
			log.info("Party ID : " + _findAccountDebts_findAccountDebtsRequest.getData().getThirdPartyPayee().getParty().getPartyId());
			log.info("Operation date : " + _findAccountDebts_findAccountDebtsRequest.getData().getOperationDate().toString());
			log.info("**********************");
			//log.info("Page size : " + _findAccountDebts_findAccountDebtsRequest.getData().getPaging().getValue().getPageSize().getValue());
			_findAccountDebts__return = port.findAccountDebts(_findAccountDebts_findAccountDebtsRequest);
			log.info("El codigo respuesta de findAccount es : " + _findAccountDebts__return.getData().getStatus().getStatusCode());
			log.info("La descripcion de la respuesta de findAccount es : " + _findAccountDebts__return.getData().getStatus().getStatusDescription());
			return _findAccountDebts__return;
		}
		catch(Exception e){
			log.error("Error en la llamada al WS findAccount : " + e);

			return null;
		}
		
	}
	
	public FindAccountDebtsResponse consultaPorCuentaFinanciera(String fa,String sn){
		
		log.debug("Entro a llamar a findAccount, consulta por FA : " + fa);
		String serviceName=sn;
		String idType = "IdCuenta";
		String idValue = fa;
		String secundaryIdType = "";
		String secundaryIdValue = "";
		String biller = billerFindAccount;
		String partyRoleId = "902002";
		String partyId = "1";
		String operationDate = getOperationDate();
		
		FindAccountDebtsResponse _findAccountDebts__return = null;
		ObjectFactory factory = new ObjectFactory();
		
		try{
			log.debug("Invoking findAccountDebts...");
			FindAccountDebtsRequest _findAccountDebts_findAccountDebtsRequest = new FindAccountDebtsRequest();
			TEFHeaderRequest _findAccountDebts_findAccountDebtsRequestHeader = new TEFHeaderRequest();
			// Lleno la cabecera
			_findAccountDebts_findAccountDebtsRequestHeader = fillFindAccountDebtsHeader(_findAccountDebts_findAccountDebtsRequestHeader, serviceName);
			//Seteo la cabecera
			_findAccountDebts_findAccountDebtsRequest.setHeader(_findAccountDebts_findAccountDebtsRequestHeader);
			// Llenando Data
			FindAccountDebtsRequestData _findAccountDebts_findAccountDebtsRequestData = new FindAccountDebtsRequestData();
			_findAccountDebts_findAccountDebtsRequestData.setServiceName(serviceName);
			_findAccountDebts_findAccountDebtsRequestData.setIDType(idType);
			_findAccountDebts_findAccountDebtsRequestData.setIDValue(idValue);
			
			javax.xml.bind.JAXBElement<java.lang.String> _findAccountDebts_findAccountDebtsRequestDataSecondaryIDType = factory.createFindAccountDebtsRequestDataSecondaryIDType(secundaryIdType);
			_findAccountDebts_findAccountDebtsRequestData.setSecondaryIDType(_findAccountDebts_findAccountDebtsRequestDataSecondaryIDType);
			javax.xml.bind.JAXBElement<java.lang.String> _findAccountDebts_findAccountDebtsRequestDataSecondaryIDValue = factory.createFindAccountDebtsRequestDataSecondaryIDValue(secundaryIdValue);
			_findAccountDebts_findAccountDebtsRequestData.setSecondaryIDValue(_findAccountDebts_findAccountDebtsRequestDataSecondaryIDValue);
			javax.xml.bind.JAXBElement<java.lang.String> _findAccountDebts_findAccountDebtsRequestDataBiller = factory.createFindAccountDebtsRequestDataBiller(biller);
			_findAccountDebts_findAccountDebtsRequestData.setBiller(_findAccountDebts_findAccountDebtsRequestDataBiller);
			
			
			ThirdPartyPayeeAgency _findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee = new ThirdPartyPayeeAgency();
			_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee.setPartyRoleId(partyRoleId);
			Party _findAccountDebts_findAccountDebtsRequestDataThirdPartyPayeeParty = new Party();
			_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayeeParty.setPartyId(partyId);
			_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee.setParty(_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayeeParty);
			_findAccountDebts_findAccountDebtsRequestData.setThirdPartyPayee(_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee);
			_findAccountDebts_findAccountDebtsRequestData.setOperationDate(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(operationDate));
			
			Paging paginador = new Paging();
			paginador.setPageSize(factory.createPagingPageSize(BigInteger.ZERO));
			paginador.setNumberOfRows(factory.createPagingNumberOfRows(BigInteger.ZERO));
			paginador.setPageNumber(factory.createPagingPageNumber(BigInteger.ONE));
			paginador.setHasMore(factory.createPagingHasMore(false));
			
			javax.xml.bind.JAXBElement<Paging> _findAccountDebts_findAccountDebtsRequestDataPaging = factory.createFindAccountDebtsRequestDataPaging(paginador);
			_findAccountDebts_findAccountDebtsRequestData.setPaging(_findAccountDebts_findAccountDebtsRequestDataPaging);
			_findAccountDebts_findAccountDebtsRequest.setData(_findAccountDebts_findAccountDebtsRequestData);
			log.debug("El input de findAccount es ... " );
			log.debug("User login : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getUserLogin());
			log.debug("Service channel : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getServiceChannel());
			log.debug("Session code : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getSessionCode());
			log.debug("Aplication : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getApplication());
			log.debug("iD MENSAJE : " +_findAccountDebts_findAccountDebtsRequest.getHeader().getIdMessage());
			log.debug("ip address : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getIpAddress());
			log.debug("Functionality code : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getFunctionalityCode());
			log.debug("Transaction timestamp : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getTransactionTimestamp());
			log.debug("Service name : " +_findAccountDebts_findAccountDebtsRequest.getHeader().getServiceName());
			log.debug("Version : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getVersion());
			log.debug("************************");
			log.debug("Service name : " + _findAccountDebts_findAccountDebtsRequest.getData().getServiceName() );
			log.debug("Id type : " + _findAccountDebts_findAccountDebtsRequest.getData().getIDType());
			log.debug("IdValue : " + _findAccountDebts_findAccountDebtsRequest.getData().getIDValue());
			log.debug("Biller : " + _findAccountDebts_findAccountDebtsRequest.getData().getBiller().getValue());
			log.debug("Party role ID : " + _findAccountDebts_findAccountDebtsRequest.getData().getThirdPartyPayee().getPartyRoleId());
			log.debug("Party ID : " + _findAccountDebts_findAccountDebtsRequest.getData().getThirdPartyPayee().getParty().getPartyId());
			log.debug("Operation date : " + _findAccountDebts_findAccountDebtsRequest.getData().getOperationDate().toString());
			log.debug("**********************");
			//log.debug("Page size : " + _findAccountDebts_findAccountDebtsRequest.getData().getPaging().getValue().getPageSize().getValue());
			_findAccountDebts__return = port.findAccountDebts(_findAccountDebts_findAccountDebtsRequest);
			log.debug("El codigo respuesta de findAccount es : " + _findAccountDebts__return.getData().getStatus().getStatusCode());
			log.debug("La descripcion de la respuesta de findAccount es : " + _findAccountDebts__return.getData().getStatus().getStatusDescription());
			return _findAccountDebts__return;
		}
		catch(Exception e){
			log.error("Error en la llamada al WS findAccount : " + e);
			return null;
		}
	}
	
public FindAccountDebtsResponse consultaPorDocumento(String fa, String documento,String nombreServicio){
		
		log.debug("Entro a llamar a findAccount, consulta por documento > FA : " + fa + " Documento : " + documento);

		//String serviceName="ConsultaMovilServicios";
		String serviceName=nombreServicio;
		String idType = "IdCuenta";
		String idValue = fa;
		String secundaryIdType = "IdInvoice";
		String secundaryIdValue = documento;
		String biller = billerFindAccount;
		String partyRoleId = "902002";
		String partyId = "1";
		String operationDate = getOperationDate();
		
		FindAccountDebtsResponse _findAccountDebts__return = null;
		ObjectFactory factory = new ObjectFactory();
		
		try{
			log.debug("Invoking findAccountDebts...");
			FindAccountDebtsRequest _findAccountDebts_findAccountDebtsRequest = new FindAccountDebtsRequest();
			TEFHeaderRequest _findAccountDebts_findAccountDebtsRequestHeader = new TEFHeaderRequest();
			// Lleno la cabecera
			_findAccountDebts_findAccountDebtsRequestHeader = fillFindAccountDebtsHeader(_findAccountDebts_findAccountDebtsRequestHeader, serviceName);
			//Seteo la cabecera
			_findAccountDebts_findAccountDebtsRequest.setHeader(_findAccountDebts_findAccountDebtsRequestHeader);
			
			
			// Llenando Data
			FindAccountDebtsRequestData _findAccountDebts_findAccountDebtsRequestData = new FindAccountDebtsRequestData();
			_findAccountDebts_findAccountDebtsRequestData.setServiceName(serviceName);
			_findAccountDebts_findAccountDebtsRequestData.setIDType(idType);
			_findAccountDebts_findAccountDebtsRequestData.setIDValue(idValue);
			
			javax.xml.bind.JAXBElement<java.lang.String> _findAccountDebts_findAccountDebtsRequestDataSecondaryIDType = factory.createFindAccountDebtsRequestDataSecondaryIDType(secundaryIdType);
			_findAccountDebts_findAccountDebtsRequestData.setSecondaryIDType(_findAccountDebts_findAccountDebtsRequestDataSecondaryIDType);
			javax.xml.bind.JAXBElement<java.lang.String> _findAccountDebts_findAccountDebtsRequestDataSecondaryIDValue = factory.createFindAccountDebtsRequestDataSecondaryIDValue(secundaryIdValue);
			_findAccountDebts_findAccountDebtsRequestData.setSecondaryIDValue(_findAccountDebts_findAccountDebtsRequestDataSecondaryIDValue);
			
			
			javax.xml.bind.JAXBElement<java.lang.String> _findAccountDebts_findAccountDebtsRequestDataBiller = factory.createFindAccountDebtsRequestDataBiller(biller);
			_findAccountDebts_findAccountDebtsRequestData.setBiller(_findAccountDebts_findAccountDebtsRequestDataBiller);
			
			
			
			
			ThirdPartyPayeeAgency _findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee = new ThirdPartyPayeeAgency();
			_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee.setPartyRoleId(partyRoleId);
			Party _findAccountDebts_findAccountDebtsRequestDataThirdPartyPayeeParty = new Party();
			_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayeeParty.setPartyId(partyId);
			_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee.setParty(_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayeeParty);
			_findAccountDebts_findAccountDebtsRequestData.setThirdPartyPayee(_findAccountDebts_findAccountDebtsRequestDataThirdPartyPayee);
			_findAccountDebts_findAccountDebtsRequestData.setOperationDate(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(operationDate));
			
			
			
			Paging paginador = new Paging();
			paginador.setPageSize(factory.createPagingPageSize(BigInteger.ZERO));
			paginador.setNumberOfRows(factory.createPagingNumberOfRows(BigInteger.ZERO));
			paginador.setPageNumber(factory.createPagingPageNumber(BigInteger.ONE));
			paginador.setHasMore(factory.createPagingHasMore(false));
			
			
			javax.xml.bind.JAXBElement<Paging> _findAccountDebts_findAccountDebtsRequestDataPaging = factory.createFindAccountDebtsRequestDataPaging(paginador);
			_findAccountDebts_findAccountDebtsRequestData.setPaging(_findAccountDebts_findAccountDebtsRequestDataPaging);
			_findAccountDebts_findAccountDebtsRequest.setData(_findAccountDebts_findAccountDebtsRequestData);
			log.debug("El input de findAccount es : " );
			log.debug("User login : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getUserLogin());
			log.debug("Service channel : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getServiceChannel());
			log.debug("Session code : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getSessionCode());
			log.debug("Aplication : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getApplication());
			log.debug("iD MENSAJE : " +_findAccountDebts_findAccountDebtsRequest.getHeader().getIdMessage());
			log.debug("ip address : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getIpAddress());
			log.debug("Functionality code : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getFunctionalityCode());
			log.debug("Transaction timestamp : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getTransactionTimestamp());
			log.debug("Service name : " +_findAccountDebts_findAccountDebtsRequest.getHeader().getServiceName());
			log.debug("Version : " + _findAccountDebts_findAccountDebtsRequest.getHeader().getVersion());
			log.debug("************************");
			log.debug("Service name : " + _findAccountDebts_findAccountDebtsRequest.getData().getServiceName() );
			log.debug("Id type : " + _findAccountDebts_findAccountDebtsRequest.getData().getIDType());
			log.debug("IdValue : " + _findAccountDebts_findAccountDebtsRequest.getData().getIDValue());
			
			log.debug("Secundary Id type : " + _findAccountDebts_findAccountDebtsRequest.getData().getSecondaryIDType().getValue());
			log.debug("Secundary IdValue : " + _findAccountDebts_findAccountDebtsRequest.getData().getSecondaryIDValue().getValue());
			
			log.debug("Biller : " + _findAccountDebts_findAccountDebtsRequest.getData().getBiller().getValue());
			log.debug("Party role ID : " + _findAccountDebts_findAccountDebtsRequest.getData().getThirdPartyPayee().getPartyRoleId());
			log.debug("Party ID : " + _findAccountDebts_findAccountDebtsRequest.getData().getThirdPartyPayee().getParty().getPartyId());
			log.debug("Operation date : " + _findAccountDebts_findAccountDebtsRequest.getData().getOperationDate().toString());
			log.debug("**********************");
			//log.debug("Page size : " + _findAccountDebts_findAccountDebtsRequest.getData().getPaging().getValue().getPageSize().getValue());
			_findAccountDebts__return = port.findAccountDebts(_findAccountDebts_findAccountDebtsRequest);
			log.debug("El codigo respuesta de findAccount es : " + _findAccountDebts__return.getData().getStatus().getStatusCode());
			log.debug("La descripcion de la respuesta de findAccount es : " + _findAccountDebts__return.getData().getStatus().getStatusDescription());
			return _findAccountDebts__return;
		}
		catch(Exception e){
			log.error("Error en la llamada al WS findAccount : " + e);
			return null; 
		}
	}
	
	
}