package cl.ev.mo.portalcorporate.clientesWS;


import java.net.URL;

import javax.portlet.ActionRequest;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;




import cl.ev.mo.portalcorporate.serviciosHelper.UtilesClientesWS;
import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.utiles.SessionUtil;
import cl.ev.mo.portalcorporate.utiles.UtilesPago;
import cl.ev.mo.portalcorporate.utiles.UtilesContenidos;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.CreateDebtPayment;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.CreateDebtPaymentPortType;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentRequest;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentRequestData;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentResponse;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreatePaymentMethod;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.Money;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.ObjectFactory;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.ThirdPartyCollector;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ClienteCreateDebtPayment extends UtilesClientesWS{
	
	
	Log log = LogFactoryUtil.getLog(ClienteCreateDebtPayment.class);
	private String endpoint = "http://10.186.31.15:9081/moESBRDPAD/CreateDebtPayment";
	
	
	private static final QName SERVICE_NAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1", "CreateDebtPayment");
	
	URL wsdlURL;
	CreateDebtPayment ss;
	CreateDebtPaymentPortType port;
	ObjectFactory factory;
	ActionRequest request;
	
	
	public ClienteCreateDebtPayment(ActionRequest request){
		
		this.endpoint = UtilesContenidos.obtenerContenidoTexto(request, Constantes._endpoint_create_debt_payment);
		wsdlURL = this.getClass().getResource("/wsdl/CreateDebtPayment.wsdl");
		ss = new CreateDebtPayment(wsdlURL, SERVICE_NAME);
        port = ss.getCreateDebtPaymentPort();
        BindingProvider provider = (BindingProvider) port;
		provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY , endpoint);
		factory = new ObjectFactory();
		this.request = request;

	}
	
	public CreateDebtPaymentResponse callCreateDebtPayment(boolean forzado){
		
		CreateDebtPaymentResponse response = null ;
		log.debug("Ingreso a llamar a createDebtPayment");
		log.debug("Endpoint utilizado : " + this.endpoint);
		log.debug("Es pago forzado: "+forzado);
		try{
			CreateDebtPaymentRequest _createDebtPayment_createDebtPaymentRequest = new CreateDebtPaymentRequest();
	        TEFHeaderRequest _createDebtPayment_createDebtPaymentRequestHeader = new TEFHeaderRequest();
	        //Llenando cabezera
	        _createDebtPayment_createDebtPaymentRequestHeader = fillCreateDebtPaymentHeader(_createDebtPayment_createDebtPaymentRequestHeader);
	        //Seteando cabezera
	        _createDebtPayment_createDebtPaymentRequest.setHeader(_createDebtPayment_createDebtPaymentRequestHeader);
	        
	        
	        /*Llenando Data*/
	        
	        String [] datosPagador = UtilesPago.obtienePartyRoleIdYAgencyId(request);
			log.debug("Datos de pagador segun medio de pago, Party role ID : " + datosPagador [0] + ", agencyID : " + datosPagador[1]);
			
			String partyRoleId = datosPagador [0];
			String agencyId = datosPagador [1];
	        
	        String shopID = "1";
	        String operationDateTime = getOperationDate();
	        String operationType = (!forzado)? "F" : "F";
	        String token = SessionUtil.getVariable(request, Constantes.TOKEN_DE_PAGO);
	        String rdpId = SessionUtil.getVariable(request, Constantes.PAYMENT_SYSTEM_ID);
	        String amountAmount = SessionUtil.getVariable(request, Constantes.MONTO_TOTAL_A_PAGAR);

	        String paymentDate = getOperationDate();
	        String accountingDate = getOperationDate();
	        String paymentMethodType = "E";
	        String paymentMethodAmount = SessionUtil.getVariable(request, Constantes.MONTO_TOTAL_A_PAGAR);
	        
	        
	        log.debug("token : " + token);
	        log.debug("rdpId : " + rdpId);
	        log.debug("operation date time : " + operationDateTime);
	        log.debug("paymentDate : " + paymentDate);
	        log.debug("accountingDate : " + accountingDate);
	        log.debug("paymentMethodAmount : " + paymentMethodAmount);
	        log.debug("operationType : " + operationType);
	        log.debug("paymentMethodType : " + paymentMethodType);

	        
	        /*Fin data dinamica*/
	        CreateDebtPaymentRequestData _createDebtPayment_createDebtPaymentRequestData = new CreateDebtPaymentRequestData();
	        ThirdPartyCollector _createDebtPayment_createDebtPaymentRequestDataThirdPartyCollector = new ThirdPartyCollector();
	        _createDebtPayment_createDebtPaymentRequestDataThirdPartyCollector.setPartyRoleId(partyRoleId);
	        _createDebtPayment_createDebtPaymentRequestDataThirdPartyCollector.setAgencyID(new java.math.BigInteger(agencyId));
	        _createDebtPayment_createDebtPaymentRequestDataThirdPartyCollector.setShopID(new java.math.BigInteger(shopID));
	        _createDebtPayment_createDebtPaymentRequestData.setThirdPartyCollector(_createDebtPayment_createDebtPaymentRequestDataThirdPartyCollector);
	        _createDebtPayment_createDebtPaymentRequestData.setOperationDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(operationDateTime));
	        _createDebtPayment_createDebtPaymentRequestData.setOperationType(operationType);
	        _createDebtPayment_createDebtPaymentRequestData.setToken(token);
	        _createDebtPayment_createDebtPaymentRequestData.setRDPID(rdpId);
	        	        
	        Money _createDebtPayment_createDebtPaymentRequestDataAmount = new Money();
	        _createDebtPayment_createDebtPaymentRequestDataAmount.setUnits("");
	        _createDebtPayment_createDebtPaymentRequestDataAmount.setAmount(new java.math.BigDecimal(amountAmount));
	        _createDebtPayment_createDebtPaymentRequestData.setAmount(_createDebtPayment_createDebtPaymentRequestDataAmount);
	        _createDebtPayment_createDebtPaymentRequestData.setPaymentDate(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(paymentDate));
	        _createDebtPayment_createDebtPaymentRequestData.setAccountingDate(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(accountingDate));
	            
	        CreatePaymentMethod _createDebtPayment_createDebtPaymentRequestDataPaymentMethod = new CreatePaymentMethod();
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethod.setPaymentMethodType(paymentMethodType);
	        javax.xml.bind.JAXBElement<java.lang.Long> _createDebtPayment_createDebtPaymentRequestDataPaymentMethodBankID = null;
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethod.setBankID(_createDebtPayment_createDebtPaymentRequestDataPaymentMethodBankID);
	        javax.xml.bind.JAXBElement<java.lang.String> _createDebtPayment_createDebtPaymentRequestDataPaymentMethodBankAccount = null;
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethod.setBankAccount(_createDebtPayment_createDebtPaymentRequestDataPaymentMethodBankAccount);
	        javax.xml.bind.JAXBElement<java.lang.String> _createDebtPayment_createDebtPaymentRequestDataPaymentMethodSerialNumber = null;
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethod.setSerialNumber(_createDebtPayment_createDebtPaymentRequestDataPaymentMethodSerialNumber);
	        javax.xml.bind.JAXBElement<javax.xml.datatype.XMLGregorianCalendar> _createDebtPayment_createDebtPaymentRequestDataPaymentMethodChequeDate = null;
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethod.setChequeDate(_createDebtPayment_createDebtPaymentRequestDataPaymentMethodChequeDate);
	        javax.xml.bind.JAXBElement<java.lang.String> _createDebtPayment_createDebtPaymentRequestDataPaymentMethodWebPayID = null;
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethod.setWebPayID(_createDebtPayment_createDebtPaymentRequestDataPaymentMethodWebPayID);
	        javax.xml.bind.JAXBElement<java.lang.String> _createDebtPayment_createDebtPaymentRequestDataPaymentMethodVerificationCode = null;
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethod.setVerificationCode(_createDebtPayment_createDebtPaymentRequestDataPaymentMethodVerificationCode);
	        javax.xml.bind.JAXBElement<java.lang.String> _createDebtPayment_createDebtPaymentRequestDataPaymentMethodAuthorizationCode = null;
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethod.setAuthorizationCode(_createDebtPayment_createDebtPaymentRequestDataPaymentMethodAuthorizationCode);
	        
	        Money _createDebtPayment_createDebtPaymentRequestDataPaymentMethodAmount = new Money();
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethodAmount.setUnits("");
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethodAmount.setAmount(new java.math.BigDecimal(paymentMethodAmount));
	        _createDebtPayment_createDebtPaymentRequestDataPaymentMethod.setAmount(_createDebtPayment_createDebtPaymentRequestDataPaymentMethodAmount);
	        _createDebtPayment_createDebtPaymentRequestData.setPaymentMethod(_createDebtPayment_createDebtPaymentRequestDataPaymentMethod);
	        javax.xml.bind.JAXBElement<java.lang.String> _createDebtPayment_createDebtPaymentRequestDataFiller1 = null;
	        _createDebtPayment_createDebtPaymentRequestData.setFiller1(_createDebtPayment_createDebtPaymentRequestDataFiller1);
	        javax.xml.bind.JAXBElement<java.lang.String> _createDebtPayment_createDebtPaymentRequestDataFiller2 = null;
	        _createDebtPayment_createDebtPaymentRequestData.setFiller2(_createDebtPayment_createDebtPaymentRequestDataFiller2);
	        _createDebtPayment_createDebtPaymentRequest.setData(_createDebtPayment_createDebtPaymentRequestData);
	         this.printDataRequest(_createDebtPayment_createDebtPaymentRequestData);
	        response = port.createDebtPayment(_createDebtPayment_createDebtPaymentRequest); 
		}		
		catch(Exception e){
			log.error("Error en el llamado de CreateDebtPayment : " + e);
			e.printStackTrace();
		}
		
		return response;
	}
	
	
	/**
	 * Imprimir Request
	 * @param data
	 */
	private void printDataRequest(CreateDebtPaymentRequestData data){
		
		try{
			log.debug("**Datos de CreateDebtPaymentRequestData**");
			log.debug("PartyRoleId: " + data.getThirdPartyCollector().getPartyRoleId());
			log.debug("AgencyId: " + data.getThirdPartyCollector().getAgencyID());
			log.debug("ShopId: " + data.getThirdPartyCollector().getShopID());
			log.debug("Amount Units: " + data.getAmount().getUnits());
			log.debug("Amount: " + data.getAmount().getAmount());
			log.debug("OperationType: " + data.getOperationType());
			log.debug("Token: " + data.getToken());
			log.debug("RDPID (RedDePagoID): " + data.getRDPID());
			log.debug("PaymentMethodType: " + data.getPaymentMethod().getPaymentMethodType());
			log.debug("PaymentMethod Amount: " + data.getPaymentMethod().getAmount().getAmount());
			log.debug("PaymentMethod AuthorizationCode: " + data.getPaymentMethod().getAuthorizationCode());
			log.debug("PaymentMethod BankAccount: " + data.getPaymentMethod().getBankAccount());
			log.debug("PaymentMethod BankID: " + data.getPaymentMethod().getBankID());
			log.debug("PaymentMethod ChequeDate: " + data.getPaymentMethod().getChequeDate());				
			log.debug("*****************************************");
		}catch(NullPointerException e){
			log.error("Error al imprimir request", e);
		}
	}
	
	
	
	
	
	
	
}
