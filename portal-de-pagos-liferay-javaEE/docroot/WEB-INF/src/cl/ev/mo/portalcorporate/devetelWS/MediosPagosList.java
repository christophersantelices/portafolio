
package cl.ev.mo.portalcorporate.devetelWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para mediosPagosList complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="mediosPagosList"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MEDIO_PAGO" type="{http://ws/}medioPago" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mediosPagosList", propOrder = {
    "mediopago"
})
public class MediosPagosList {

    @XmlElement(name = "MEDIO_PAGO", nillable = true)
    protected List<MedioPago> mediopago;

    /**
     * Gets the value of the mediopago property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mediopago property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMEDIOPAGO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MedioPago }
     * 
     * 
     */
    public List<MedioPago> getMEDIOPAGO() {
        if (mediopago == null) {
            mediopago = new ArrayList<MedioPago>();
        }
        return this.mediopago;
    }

}
