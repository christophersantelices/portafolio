package cl.ev.mo.portalcorporate.serviciosHelper;

import java.util.ArrayList;
import java.util.Iterator; 

import javax.portlet.ActionRequest;
import javax.portlet.ResourceRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cl.ev.mo.portalcorporate.beans.BillingCustomerFA;
import cl.ev.mo.portalcorporate.beans.CarroDePago;
import cl.ev.mo.portalcorporate.beans.CuentaFinancieraFA;
import cl.ev.mo.portalcorporate.beans.DocumentoFA;
import cl.ev.mo.portalcorporate.beans.RespuestaFindAccount;
import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.utiles.UtilesPago;
import cl.ev.mo.portalcorporate.utiles.SessionUtil;
import cl.ev.mo.portalcorporate.utiles.DBSupport;
import cl.ev.mo.portalcorporate.clientesWS.ClienteCreateDebtPayment;
import cl.ev.mo.portalcorporate.devetelWS.ValidaAccesoRespuesta;
import cl.ev.mo.portalcorporate.clientesWS.ClienteFindAccount;
import cl.ev.mo.portalcorporate.clientesWS.ClienteValidaAcceso;
import cl.ev.mo.portalcorporate.clientesWS.ClienteCreateTicket;
import cl.ev.mo.portalcorporate.clientesWS.ClienteSendLegacyEvent;
import cl.ev.mo.portalcorporate.beans.ServicioFA;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentResponse;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.CreateTicketResponse;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.BillingCustomer;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.CustomerAccountBalance;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.CustomerBill;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types.FindAccountDebtsResponse;
import cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.SendLegacyEventResponse;

public class WSHelper {
	Log log = LogFactoryUtil.getLog(WSHelper.class);
	ActionRequest request;
	ResourceRequest resRequest;
	ClienteFindAccount findAccount;
	ClienteCreateTicket createTicket;
	ClienteValidaAcceso validaAcceso;
	ClienteCreateDebtPayment debtPayment;
	ClienteSendLegacyEvent sendLegacy;

	public WSHelper(ActionRequest actionRequest) {
		this.request = actionRequest;
		findAccount = new ClienteFindAccount(request);
		validaAcceso = new ClienteValidaAcceso(request);
		createTicket = new ClienteCreateTicket(request);
		debtPayment = new ClienteCreateDebtPayment(request);
		
	}
	
	/**
	 * Este constructor se utiliza en llamadas ajax a notificador
	 */
	public WSHelper(String endpoint, ResourceRequest request) {
		this.resRequest = request;
		sendLegacy = new ClienteSendLegacyEvent(endpoint);
	}
	
	/**
	 * Este constructor se utiliza en llamadas ajax de find account
	 * 
	 */
	public WSHelper(String endpointFindAccount, String billerFindAccount) {
		findAccount = new ClienteFindAccount(endpointFindAccount,
				billerFindAccount);
	}
		
	public String callCreateTicket(ActionRequest actionRequest,CarroDePago carro, String tipoTicket) {
		String idTicketRDP = "";
		try {
			CreateTicketResponse resp = createTicket.callCrearTicket(actionRequest,carro,tipoTicket);
			SessionUtil.setVariable(request,Constantes.PAYMENT_SYSTEM_ID, resp.getHeader().getTransactionID());
			SessionUtil.setVariable(request, Constantes.TOKEN_DE_PAGO,resp.getData().getTicket().getToken());
			idTicketRDP = String.valueOf(resp.getData().getTicket().getTicketID());
			log.debug("El id obtenido desde create ticket es : " + idTicketRDP);
			log.debug("Transaction ID obtenido al crear el ticket : "
					+ SessionUtil.getVariable(request,Constantes.PAYMENT_SYSTEM_ID));

			if (tipoTicket.equals(ClienteCreateTicket.TICKET_DIFERIDO_SIN_RUT)) {
				log.debug("El ticket es diferido, obteniendo fecha de vencimiento...");
				String fechaValidez = resp.getData().getTicket().getInvoiceDueDate().toString();
				log.debug("Fecha de validez ticket : " + fechaValidez);
				SessionUtil.setVariable(request,Constantes.FECHA_VENCIMIENTO_DEUDA, fechaValidez);
			}

		} catch (Exception e) {
			log.error("Error al llamar a createTicket : " + e);
			e.printStackTrace();
			idTicketRDP = "0";
		}
		return idTicketRDP;
	}

	public ValidaAccesoRespuesta callValidaAcceso(String procedencia) {
		ValidaAccesoRespuesta resp = validaAcceso.callValidaAcceso(procedencia);
		return resp;
	}
	
	public boolean callCreateDebtPayment(boolean forzado, ActionRequest actionRequest) {
		boolean correcto = false;
		boolean pagoinyectado = false;
		DBSupport db = new DBSupport();
		try {
			pagoinyectado = db.getDatosInyeccion(SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP));
		} catch (Exception e1) {
			log.error("Error al obtener datos de inyección "+e1);
		}
		if (!pagoinyectado) {
			try {
				CreateDebtPaymentResponse resp = debtPayment
						.callCreateDebtPayment(forzado);
				String codError = resp.getData().getStatus().getCode();
				log.debug("El codigo de respuesta de createDebtPayment es : "
						+ codError);
				log.debug("La descripcion de la respuesta de createDebtPayment es : "
						+ resp.getData().getStatus().getDescription());
				
				db.guardaRespuestaCreateDebtPayment(SessionUtil.getVariable(actionRequest, Constantes.ID_TICKET_RDP),
						codError, resp.getData().getStatus().getDescription());
				
				if (codError.trim().equals("0")) {
					correcto = true;
				}
			} catch (Exception e) {
				log.error("Error al llamar a createDebtPayment : " + e);
				e.printStackTrace();
			}
		}
		else {
			correcto = true;
			log.debug("Pago inyectado por servicio");
		}
		return correcto;
	}
	
	public boolean callCreateDebtPaymentSimulado(boolean forzado, ActionRequest actionRequest) {
		boolean correcto = false;
			try {
				CreateDebtPaymentResponse resp = debtPayment.callCreateDebtPayment(forzado);
				log.debug(SessionUtil.getVariable( actionRequest, "detalle_deuda"));
				String codError = resp.getData().getStatus().getCode();
				log.debug("El codigo de respuesta de createDebtPayment es : "
						+ codError);
				log.debug("La descripcion de la respuesta de createDebtPayment es : "
						+ resp.getData().getStatus().getDescription());
				
				if (codError.trim().equals("0")) {
					correcto = true;
				}
			} catch (Exception e) {
				log.error("Error al llamar a createDebtPayment : " + e);
			}
		
		return correcto;
	}
	
	public boolean callSendLegacyEvent(String correo, boolean simulado,
			String tipoNotificacion) {
		boolean notificado = false;

		try {
			log.debug("Iniciando llamado a SendLegacyEvent");
			String ordenDeCompra = SessionUtil.getVariable(resRequest,Constantes.ID_TICKET_RDP);
			SendLegacyEventResponse response = sendLegacy
					.enviaNotificacionPago(correo, ordenDeCompra, simulado,
							tipoNotificacion, resRequest);

			String codError = response.getData().getStatus().getCode()
					.getValue();
			String descripcion = response.getData().getStatus()
					.getDescription().getValue();
			String codNotificacion = response.getData().getNotificationID()
					.toString();
			log.debug("Respuesta del WS notificador > ID notificacion : "
					+ codNotificacion + " > Cod error : " + codError
					+ " > Descripcion : " + descripcion);
			if (codError.trim().equals("0")) {
				notificado = true;
			} else {
				notificado = false;
			}
		} catch (Exception e) {

			log.debug("Error al ejecutar el llamado a sendLegacyEvent : " + e);
			notificado = false;
		}
		return notificado;
	}

	public RespuestaFindAccount callFindAccount_publico(String fa_cliente) throws Exception {
		log.debug("Llego a callFindAccount_publico ");

		RespuestaFindAccount respuesta = new RespuestaFindAccount();
			FindAccountDebtsResponse response = findAccount.consultaPorCuentaFinanciera(fa_cliente,"ConsultaMovilCuenta");

			if (null != response) {	
				respuesta.setCodigoError(response.getData().getStatus().getStatusCode());
				
				if (!respuesta.getCodigoError().equals(Constantes.CONSULTA_EXITOSA)) {
					log.error("La respuesta de find account es "+respuesta.getCodigoError());
					respuesta.setEjecutadoCorrectamente(false);
				} else {
					respuesta.setEjecutadoCorrectamente(true);			
					respuesta.setFA(response.getData().getBillingCustomerList().getValue().getBillingCustomer().get(0).getBillingCustomer());
					 
					Iterator<BillingCustomer> itBillingCustomerList = response.getData().getBillingCustomerList().getValue().getBillingCustomer().iterator();
					
					ArrayList<BillingCustomerFA> billings = new ArrayList<BillingCustomerFA>();
					while (itBillingCustomerList.hasNext()) {
						BillingCustomer bill = itBillingCustomerList.next();
						BillingCustomerFA bcfa = new BillingCustomerFA();
						ArrayList<CuentaFinancieraFA> cuentasFinancieras = new ArrayList<CuentaFinancieraFA>();
						Iterator<CustomerAccountBalance> itCustomerAccountBalance = bill.getCustomerAccountBalanceList().getCustomerAccountBalance().iterator();
						
						while (itCustomerAccountBalance.hasNext()) {
							CustomerAccountBalance balance = itCustomerAccountBalance.next();
							CuentaFinancieraFA cuenta = new CuentaFinancieraFA();
							
							cuenta.setIdCuentaFinanciera(balance.getCustomerAccountBalanceID());
							cuenta.setMontoTotal(balance.getPaymentItem().getValue().getCustomerPayment().getAmount().getAmount().toString());
							
							if (cuenta.getMontoTotal().equals("0")) {
								log.debug("El monto de la cuenta es 0, la ignoro");
								continue;
							}
							try {
								cuenta.setMontoVencido(balance.getPaymentItem().getValue().getCustomerPayment().getDueAmount().getValue().getAmount().toString());
							} catch (NullPointerException e) {
								log.debug("Monto vencido null");
								cuenta.setMontoVencido("");
							}
							try {
								cuenta.setMontoPorVencer(balance.getPaymentItem().getValue().getCustomerPayment().getRemainingAmount().getValue().getAmount().toString());
							} catch (NullPointerException e) {
								log.debug("Monto por vencer null");
								cuenta.setMontoPorVencer(balance.getPaymentItem().getValue().getCustomerPayment().getAmount().getAmount().toString());
							}
							try {
								cuenta.setMontoEnDisputa(balance.getPaymentItem().getValue().getCustomerPayment().getDisputedAmount().getValue().getAmount().toString());
							} catch (NullPointerException e) {
								log.debug("Monto en disputa null");
								cuenta.setMontoEnDisputa("");
							}
							cuenta.setTokenDePago(balance.getPaymentItem().getValue().getPaymentToken().getValue());
							cuenta.setSystemID(balance.getPaymentItem().getValue().getPaymentSystemID().getValue());
							cuenta.setNombreCliente(balance.getCustomerAccount().getValue().getCustomerInformation().getValue().getName().getValue().getGivenNames().toString());
							respuesta.setNombreCliente(cuenta.getNombreCliente());
							//no se la diferencia
							cuenta.setTipoCliente(balance.getCustomerAccount().getValue().getCustomerType());
							cuenta.setTipoCuenta(balance.getCustomerAccount().getValue().getAccountType());
							cuenta.setSubTipoCliente(balance.getCustomerAccount().getValue().getCustomerSubType());
							try {
								cuenta.setTienePagoPorServicio(balance.getCustomerAccount().getValue().getServicePaymentIndicator().getValue().equals("S") ? true : false);
							} catch (Exception e) {
								cuenta.setTienePagoPorServicio(false);
							}
							try {
								//estro trae una N pero aqui se valida una S ?????
								cuenta.setTienePat(balance.getCustomerAccount().getValue().getPacPatIndicator().getValue().equals("S") ? true : false);
							} catch (Exception e) {
								cuenta.setTienePat(false);
							}
							cuentasFinancieras.add(cuenta);
						}
						bcfa.setCuentasFinancieras(cuentasFinancieras);
						billings.add(bcfa);
					}
					respuesta.setBillingCustomer(billings);		
					/**validar que el cliente tenga deduda*/
					if(!respuesta.getTotalDeuda().equals("0")){
						respuesta.setTieneDeuda(true);
					}
				}
			} else {
				log.error("Consulta por cuenta financiera es nula");
			}
		
		return respuesta;
	}

	public RespuestaFindAccount callFindAccount_privado(String fa_cliente, String tipo) throws Exception {
		log.debug("Llego a callFindAccount_privado "+tipo);
		RespuestaFindAccount respuesta = new RespuestaFindAccount();
		
		if(tipo.equals(Constantes.CONSULTA_MOVIL_CUENTA)){
			//consulta la informacion de la cuenta asociada al fa
			FindAccountDebtsResponse response = findAccount.consultaPorCuentaFinanciera(fa_cliente,Constantes.CONSULTA_MOVIL_CUENTA);

			if (null != response) {	
				respuesta.setCodigoError(response.getData().getStatus().getStatusCode());
				
				if (!respuesta.getCodigoError().equals(Constantes.CONSULTA_EXITOSA)) {
					log.error("La respuesta de find account es "+respuesta.getCodigoError());
					respuesta.setEjecutadoCorrectamente(false);
				} else {			
					respuesta.setFA(response.getData().getBillingCustomerList().getValue().getBillingCustomer().get(0).getBillingCustomer());
					 
					Iterator<BillingCustomer> itBillingCustomerList = response.getData().getBillingCustomerList().getValue().getBillingCustomer().iterator();
					
					ArrayList<BillingCustomerFA> billings = new ArrayList<BillingCustomerFA>();
					while (itBillingCustomerList.hasNext()) {
						BillingCustomer bill = itBillingCustomerList.next();
						BillingCustomerFA bcfa = new BillingCustomerFA();
						ArrayList<CuentaFinancieraFA> cuentasFinancieras = new ArrayList<CuentaFinancieraFA>();
						Iterator<CustomerAccountBalance> itCustomerAccountBalance = bill.getCustomerAccountBalanceList().getCustomerAccountBalance().iterator();
						
						while (itCustomerAccountBalance.hasNext()) {
							CustomerAccountBalance balance = itCustomerAccountBalance.next();
							CuentaFinancieraFA cuenta = new CuentaFinancieraFA();
							
							cuenta.setIdCuentaFinanciera(balance.getCustomerAccountBalanceID());
							cuenta.setMontoTotal(balance.getPaymentItem().getValue().getCustomerPayment().getAmount().getAmount().toString());
							
							if (cuenta.getMontoTotal().equals("0")) {
								log.debug("El monto de la cuenta es 0, la ignoro");
								continue;
							}
							try {
								cuenta.setMontoVencido(balance.getPaymentItem().getValue().getCustomerPayment().getDueAmount().getValue().getAmount().toString());
							} catch (NullPointerException e) {
								log.debug("Monto vencido null");
								cuenta.setMontoVencido("");
							}
							try {
								cuenta.setMontoPorVencer(balance.getPaymentItem().getValue().getCustomerPayment().getRemainingAmount().getValue().getAmount().toString());
							} catch (NullPointerException e) {
								log.debug("Monto por vencer null");
								cuenta.setMontoPorVencer(balance.getPaymentItem().getValue().getCustomerPayment().getAmount().getAmount().toString());
							}
							try {
								cuenta.setMontoEnDisputa(balance.getPaymentItem().getValue().getCustomerPayment().getDisputedAmount().getValue().getAmount().toString());
							} catch (NullPointerException e) {
								log.debug("Monto en disputa null");
								cuenta.setMontoEnDisputa("");
							}
							cuenta.setTokenDePago(balance.getPaymentItem().getValue().getPaymentToken().getValue());
							cuenta.setSystemID(balance.getPaymentItem().getValue().getPaymentSystemID().getValue());
							cuenta.setNombreCliente(balance.getCustomerAccount().getValue().getCustomerInformation().getValue().getName().getValue().getGivenNames().toString());
							respuesta.setNombreCliente(cuenta.getNombreCliente());
							//no se la diferencia
							cuenta.setTipoCliente(balance.getCustomerAccount().getValue().getCustomerType());
							cuenta.setTipoCuenta(balance.getCustomerAccount().getValue().getAccountType());
							cuenta.setSubTipoCliente(balance.getCustomerAccount().getValue().getCustomerSubType());
							try {
								cuenta.setTienePagoPorServicio(balance.getCustomerAccount().getValue().getServicePaymentIndicator().getValue().equals("S") ? true : false);
							} catch (Exception e) {
								cuenta.setTienePagoPorServicio(false);
							}
							try {
								//estro trae una N pero aqui se valida una S ?????
								cuenta.setTienePat(balance.getCustomerAccount().getValue().getPacPatIndicator().getValue().equals("S") ? true : false);
							} catch (Exception e) {
								cuenta.setTienePat(false);
							}
							cuentasFinancieras.add(cuenta);
						}
						bcfa.setCuentasFinancieras(cuentasFinancieras);
						billings.add(bcfa);
					}
					respuesta.setBillingCustomer(billings);		
					/**validar que el cliente tenga deduda*/
					if(!respuesta.getTotalDeuda().equals("0")){
						respuesta.setTieneDeuda(true);
					}
					respuesta.setEjecutadoCorrectamente(true);
				}
			} else {
				log.error("Consulta por cuenta financiera es nula");
			}
		}else if(tipo.equals(Constantes.CONSULTA_MOVIL_DOCUMENTO)){
			//consulta todos los docuemntos asociados al fa
			FindAccountDebtsResponse response = findAccount.consultaPorCuentaFinanciera(fa_cliente,Constantes.CONSULTA_MOVIL_DOCUMENTO);			
			if (null != response) {
				respuesta.setCodigoError(response.getData().getStatus().getStatusCode());

					
					if (!response.getData().getStatus().getStatusCode().equals(Constantes.CONSULTA_EXITOSA)) {
						log.debug("codigo de respuesta find acount: "+response.getData().getStatus().getStatusCode()+ " - Mensaje: "+response.getData().getStatus().getStatusDescription());
						respuesta.setEjecutadoCorrectamente(false);
					} else {
						respuesta.setTieneDeuda(true);
						ArrayList<BillingCustomerFA> billings = new ArrayList<BillingCustomerFA>();
						ArrayList<CuentaFinancieraFA> cuentas = new ArrayList<CuentaFinancieraFA>();
						BillingCustomerFA billing = new BillingCustomerFA();
						CuentaFinancieraFA cuentaSeleccionada = new CuentaFinancieraFA();
						ArrayList<DocumentoFA> documentos = new ArrayList<DocumentoFA>();

						Iterator<CustomerBill> it = response.getData()
								.getBillingCustomerList().getValue()
								.getBillingCustomer().get(0)
								.getCustomerAccountBalanceList()
								.getCustomerAccountBalance().get(0)
								.getCustomerAccount().getValue().getActualBill()
								.getValue().getCustomerBill().iterator();
						int contador = 0;

						while (it.hasNext()) {

							log.info("Iterando documentos");

							DocumentoFA doc = new DocumentoFA();
							CustomerBill bill = it.next();

							String idDoc = bill.getBillingInvoiceNumber().getValue();
							String numBoleta = bill.getExternalLegalNumber().getValue();
							String monto = ""+ bill.getPaymentItemList().getValue().getPaymentItem().get(0)
											   .getCustomerPayment().getAmount().getAmount();
							String tokenDePago = bill.getPaymentItemList().getValue().getPaymentItem().get(0)
												 .getPaymentToken().getValue();
							String systemIdPago = bill.getPaymentItemList().getValue().getPaymentItem().get(0)
												 .getPaymentSystemID().getValue();
							String documentType = bill.getDocumentType().getValue();
							
							String fecha;
							try {
								  fecha = "" + bill.getBillSpec().getValue().getValidFrom().getValue();
								  doc.setVencido(UtilesPago.esDocumentoVencido(fecha));
							} catch (Exception e) {
								// TODO: validar la fecha por defecto en caso de
								// error de parseo
							  	  fecha = ":/";
								  doc.setVencido(true);
							}
							
							doc.setTypoDocumento(documentType);
							doc.setIdDocumento(idDoc);
							doc.setNumeroBoleta(numBoleta);
							doc.setMonto(monto);
							doc.setFecha(UtilesPago.fechaVencimientoEnFormatoFront(fecha));
							doc.setTokenDePago(tokenDePago);
							doc.setSystemID(systemIdPago);
							documentos.add(doc);
							contador++;
						}
						cuentaSeleccionada.setDocumentos(documentos);
						cuentas.add(cuentaSeleccionada);
						billing.setCuentasFinancieras(cuentas);
						billings.add(billing);
						respuesta.setBillingCustomer(billings);	
						
						if(!respuesta.getTotalDeuda().equals("0")){
							respuesta.setTieneDeuda(true);
						}
						respuesta.setEjecutadoCorrectamente(true);
					}
			} else {
				log.error("Consulta por cuenta financiera es nula");
			}
		}
		return respuesta;
	}

	public RespuestaFindAccount callFindAccount_privado(String fa_cliente,String documento, String tipo) throws Exception {
		log.debug("Llego a callFindAccount_privado "+tipo);
		RespuestaFindAccount respuesta = new RespuestaFindAccount();	
		if(tipo.equals(Constantes.CONSULTA_MOVIL_DOCUMENTO)){
			FindAccountDebtsResponse response = findAccount.consultaPorDocumento(fa_cliente,documento,Constantes.CONSULTA_MOVIL_DOCUMENTO);
			
			if (null != response) {	
				respuesta.setCodigoError(response.getData().getStatus().getStatusCode());
				if (!respuesta.getCodigoError().equals(Constantes.CONSULTA_EXITOSA)) {
					log.error("La respuesta de find account es "+respuesta.getCodigoError());
					respuesta.setEjecutadoCorrectamente(false);
				} else {
					DocumentoFA docFa = new DocumentoFA();
					CuentaFinancieraFA cuentaSeleccionada = new CuentaFinancieraFA();
					
					cuentaSeleccionada.setIdCuentaFinanciera(fa_cliente);
					
					String paymentToken=String.valueOf(response
							.getData().getBillingCustomerList().getValue()
							.getBillingCustomer().get(0).getCustomerAccountBalanceList()
							.getCustomerAccountBalance().get(0).getCustomerAccount().getValue()
							.getActualBill().getValue().getCustomerBill().get(0).getPaymentItemList().getValue().getPaymentItem()
							.get(0).getPaymentToken().getValue());
					docFa.setTokenDePago(paymentToken);
					log.debug("paymentToken:"+paymentToken);
					
					cuentaSeleccionada.setTokenDePago(paymentToken);
					
					String billingInvoiceNumber=response
					.getData().getBillingCustomerList().getValue()
					.getBillingCustomer().get(0).getCustomerAccountBalanceList()
					.getCustomerAccountBalance().get(0).getCustomerAccount().getValue()
					.getActualBill().getValue().getCustomerBill().get(0).getBillingInvoiceNumber().getValue();
					docFa.setIdDocumento(billingInvoiceNumber);
					log.debug("billingInvoiceNumber:"+billingInvoiceNumber);
					
					String externalLegalNumber=String.valueOf(response
							.getData().getBillingCustomerList().getValue()
							.getBillingCustomer().get(0).getCustomerAccountBalanceList()
							.getCustomerAccountBalance().get(0).getCustomerAccount().getValue()
							.getActualBill().getValue().getCustomerBill().get(0).getExternalLegalNumber().getValue());
					docFa.setNumeroBoleta(externalLegalNumber);
					log.debug("externalLegalNumber:"+ externalLegalNumber);
					
					String validFrom=String.valueOf(response
							.getData().getBillingCustomerList().getValue()
							.getBillingCustomer().get(0).getCustomerAccountBalanceList()
							.getCustomerAccountBalance().get(0).getCustomerAccount().getValue()
							.getActualBill().getValue().getCustomerBill().get(0).getBillSpec().getValue().getValidFrom().getValue());
					docFa.setFecha(validFrom);
					log.debug("validFrom:"+validFrom);

					String amount=String.valueOf(response
							.getData().getBillingCustomerList().getValue()
							.getBillingCustomer().get(0).getCustomerAccountBalanceList()
							.getCustomerAccountBalance().get(0).getCustomerAccount().getValue()
							.getActualBill().getValue().getCustomerBill().get(0).getPaymentItemList().getValue().getPaymentItem()
							.get(0).getCustomerPayment().getAmount().getAmount());
					docFa.setMonto(amount);
					log.debug("amount:"+ amount );
					
					ArrayList<BillingCustomerFA> billings = new ArrayList<BillingCustomerFA>();
					ArrayList<CuentaFinancieraFA> cuentas = new ArrayList<CuentaFinancieraFA>();
					BillingCustomerFA billing = new BillingCustomerFA();
					ArrayList<DocumentoFA> documentos = new ArrayList<DocumentoFA>();
					
					documentos.add(docFa);
					cuentaSeleccionada.setDocumentos(documentos);
					cuentas.add(cuentaSeleccionada);
					billing.setCuentasFinancieras(cuentas);
					billings.add(billing);
					respuesta.setBillingCustomer(billings);
					
					respuesta.setEjecutadoCorrectamente(true);
					respuesta.setTieneDeuda(true);
				}
			}
		}
		return respuesta;
	}

	
	public RespuestaFindAccount callFindAccountDetalleServicio_privado(String fa_cliente, String documento) throws Exception {
		log.debug("Consultando detalle de servicio para  "+fa_cliente+" "+documento);
		RespuestaFindAccount respuesta = new RespuestaFindAccount();
		FindAccountDebtsResponse response = findAccount.consultaPorDocumento(fa_cliente,documento,Constantes.CONSULTA_MOVIL_SERVICIOS);
		
		if (null != response) {	
			respuesta.setCodigoError(response.getData().getStatus().getStatusCode());
			if (!respuesta.getCodigoError().equals(Constantes.CONSULTA_EXITOSA)) {
				log.error("La respuesta de find account es "+respuesta.getCodigoError());
				respuesta.setEjecutadoCorrectamente(false);
			} else {
				ArrayList<BillingCustomerFA> billings = new ArrayList<BillingCustomerFA>();
				ArrayList<CuentaFinancieraFA> cuentas = new ArrayList<CuentaFinancieraFA>();
				BillingCustomerFA billing = new BillingCustomerFA();
				CuentaFinancieraFA cuentaSeleccionada = new CuentaFinancieraFA();
				ArrayList<DocumentoFA> documentos = new ArrayList<DocumentoFA>();
				ArrayList<ServicioFA> servicios = new ArrayList<ServicioFA>();

				Iterator<CustomerAccountBalance> itServicios = response
						.getData().getBillingCustomerList().getValue()
						.getBillingCustomer().get(0)
						.getCustomerAccountBalanceList()
						.getCustomerAccountBalance().iterator();

				DocumentoFA docFa = new DocumentoFA();
				while (itServicios.hasNext()) {
					ServicioFA servFa = new ServicioFA();
					CustomerAccountBalance balance = itServicios.next();
					servFa.setMonto(String.valueOf(balance.getPaymentItem()
							.getValue().getCustomerPayment().getAmount()
							.getAmount()));
					log.debug("Amount"
							+ balance.getPaymentItem().getValue()
									.getCustomerPayment().getAmount()
									.getAmount());
					servFa.setNombre(balance.getPaymentItem().getValue()
							.getName().getValue());
					servFa.setIdServicio(balance.getPaymentItem()
							.getValue().getID().getValue());
					// servFa.setSystemID(balance.getPaymentItem().getValue().getPaymentSystemID().getValue()); ???
					servFa.setTokenDePago(balance.getPaymentItem()
							.getValue().getPaymentToken().getValue());
					servicios.add(servFa);
				}
				docFa.setServicios(servicios);
				documentos.add(docFa);
				cuentaSeleccionada.setDocumentos(documentos);
				cuentas.add(cuentaSeleccionada);
				billing.setCuentasFinancieras(cuentas);
				billings.add(billing);
				respuesta.setBillingCustomer(billings);
			
				respuesta.setEjecutadoCorrectamente(true);
				respuesta.setTieneDeuda(true);

			}
		}
		
		
		
		return respuesta;
	}

}
