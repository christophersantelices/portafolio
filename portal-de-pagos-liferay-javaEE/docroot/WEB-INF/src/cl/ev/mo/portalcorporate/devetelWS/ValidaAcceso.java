
package cl.ev.mo.portalcorporate.devetelWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para validaAcceso complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="validaAcceso"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AUTENTIFICACION" type="{http://ws/}autentificacion" minOccurs="0"/&gt;
 *         &lt;element name="CLIENTE" type="{http://ws/}cliente" minOccurs="0"/&gt;
 *         &lt;element name="DETALLE_COMPRA" type="{http://ws/}detalleCompra" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validaAcceso", propOrder = {
    "autentificacion",
    "cliente",
    "detallecompra"
})
public class ValidaAcceso {

    @XmlElement(name = "AUTENTIFICACION")
    protected Autentificacion autentificacion;
    @XmlElement(name = "CLIENTE")
    protected Cliente cliente;
    @XmlElement(name = "DETALLE_COMPRA")
    protected DetalleCompra detallecompra;

    /**
     * Obtiene el valor de la propiedad autentificacion.
     * 
     * @return
     *     possible object is
     *     {@link Autentificacion }
     *     
     */
    public Autentificacion getAUTENTIFICACION() {
        return autentificacion;
    }

    /**
     * Define el valor de la propiedad autentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link Autentificacion }
     *     
     */
    public void setAUTENTIFICACION(Autentificacion value) {
        this.autentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad cliente.
     * 
     * @return
     *     possible object is
     *     {@link Cliente }
     *     
     */
    public Cliente getCLIENTE() {
        return cliente;
    }

    /**
     * Define el valor de la propiedad cliente.
     * 
     * @param value
     *     allowed object is
     *     {@link Cliente }
     *     
     */
    public void setCLIENTE(Cliente value) {
        this.cliente = value;
    }

    /**
     * Obtiene el valor de la propiedad detallecompra.
     * 
     * @return
     *     possible object is
     *     {@link DetalleCompra }
     *     
     */
    public DetalleCompra getDETALLECOMPRA() {
        return detallecompra;
    }

    /**
     * Define el valor de la propiedad detallecompra.
     * 
     * @param value
     *     allowed object is
     *     {@link DetalleCompra }
     *     
     */
    public void setDETALLECOMPRA(DetalleCompra value) {
        this.detallecompra = value;
    }

}
