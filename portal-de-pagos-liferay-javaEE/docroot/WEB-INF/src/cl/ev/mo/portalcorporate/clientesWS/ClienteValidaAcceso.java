package cl.ev.mo.portalcorporate.clientesWS;


import java.net.URL;

import javax.portlet.ActionRequest;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import cl.ev.mo.portalcorporate.devetelWS.ValidaAccesoRespuesta;
import cl.ev.mo.portalcorporate.devetelWS.WSAcceso;
import cl.ev.mo.portalcorporate.devetelWS.WSAcceso_Service;
import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.utiles.SessionUtil;
import cl.ev.mo.portalcorporate.utiles.UtilesPago;
import cl.ev.mo.portalcorporate.utiles.UtilesContenidos;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

//import ws.Autentificacion;
//import ws.Cliente;
//import ws.DetalleCompra;
//import ws.Producto;
//import ws.ValidaAccesoRespuesta;
//import ws.WSAccesoBindingStub;

public class ClienteValidaAcceso {

	Log log = LogFactoryUtil.getLog(ClienteValidaAcceso.class);
	ActionRequest request;
	
	
	private static final QName SERVICE_NAME = new QName("http://ws/", "WSAcceso");
	//String endpoint = "https://gmppre.dvtel.cl/ws_acc_gmp/WSAcceso";
	String endpoint = "http://localhost:80/ws_acc_gmp/WSAcceso";
	
	
	public ClienteValidaAcceso(ActionRequest req){
		this.request = req;
		String endpoint = UtilesContenidos.obtenerContenidoTexto(request, "endpoint_valida_acceso");
		//log.debug("Endpoint obtenido para valida acceso : " + endpoint);
		this.endpoint = endpoint;
	}
	
	
	public ValidaAccesoRespuesta callValidaAcceso(String procedencia){

		/*Configuracion de la llamada*/
		URL wsdlURL = this.getClass().getResource("/wsdl/WSAcceso.wsdl");
		if(wsdlURL==null){
			return null;
		}
		WSAcceso_Service ss = new WSAcceso_Service(wsdlURL, SERVICE_NAME);
		WSAcceso port = ss.getWSAccesoPort();  
		BindingProvider provider = (BindingProvider) port;
		provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);

		
		/*Datos de entrada*/

		/* Nodo Autentificacion*/
		String cod_portador = ""; //En blanco
		String empresa = UtilesContenidos.obtenerContenidoTexto(request, Constantes._validaAcceso_idEmpresa);
		
		//String pag_respuesta = UtilesContenidos.obtenerContenidoTexto(request , UtilesContenidos._validaAcceso_paginaRespGateway);
		String pag_respuesta = "http://cpo2589.ctc.cl:10039/WScierrePagoGateway/CierrePago";
		
		
		String id_canal = "";
		String password_canal = "";
		String servicio = ""; //En blanco
		String tipo_registro = ""; //En blanco
		String opcional1 = ""; //En blanco ?? , en gateway enviamos el folio del documento aquí
		String opcional2 = ""; //En blanco
		
		
		id_canal = (procedencia.equals(Constantes.PROCEDENCIA_PRIVADA) ? UtilesContenidos.obtenerContenidoTexto(request, Constantes._validaAcceso_idCanalPrivado) : UtilesContenidos.obtenerContenidoTexto(request, Constantes._validaAcceso_idCanalPublico));
		password_canal = (procedencia.equals(Constantes.PROCEDENCIA_PRIVADA) ? UtilesContenidos.obtenerContenidoTexto(request, Constantes._validaAcceso_contCanalPrivado) : UtilesContenidos.obtenerContenidoTexto(request, Constantes._validaAcceso_contCanalPublico));

		log.debug("Nodo autentificacion llenado correctamente");
		/* Nodo cliente */

		String direccion = ""; //En blanco
		String email = SessionUtil.getVariable(request, Constantes.CORREO_CLIENTE);
		log.debug("Paso" + email);
		//String id_num_celular = SessionUtil.getVariable(request, Constantes.NUMERO_TELEFONO);
		//log.debug("Paso" + id_num_celular);
		//String id_rut = SessionUtil.getVariable(request, Constantes.RUT_CLIENTE);
		//TODO: RUT en duro en input validaAcceso, cambiar !
		//String id_rut = (SessionUtil.getVariable(request, Constantes.TIPO_PAGO).equals(Constantes.RUT_CLIENTE)? SessionUtil.getVariable(request, Constantes.RUT_CLIENTE):"11111111-1");
		//log.debug("Paso" + id_rut);
		String id_tel_fijo = ""; //En blanco por ahora ??
		log.debug("Paso" + id_tel_fijo);
		String nombre = SessionUtil.getVariable(request, Constantes.NOMBRE_CLIENTE);
		log.debug("Paso" + nombre);
		String num_cliente = SessionUtil.getVariable(request, Constantes.NUMERO_CLIENTE);
		log.debug("Paso"+num_cliente);
		String region = ""; //En blanco
		log.debug("Nodo cliente llenado correctamente");

		/* Nodo detalle compra */
		
		int montoaux = Integer.parseInt(SessionUtil.getVariable(request, Constantes.MONTO_TOTAL_A_PAGAR));
		
		String cant_productos = "01";
		String fecha_vencimiento = SessionUtil.getVariable(request, Constantes.FECHA_VENCIMIENTO_DEUDA);
		String monto_total = UtilesPago.formatoMontoDevetel(String.valueOf(montoaux));
		String num_orden = SessionUtil.getVariable(request, Constantes.ID_TICKET_RDP);
		String param_resp = ""; //En blanco
		log.debug("Nodo compra llenado correctamente");

		/* Nodo productos */

		String cant_productos_producto = "01";
		String fecha_vencimiento_producto = SessionUtil.getVariable(request, Constantes.FECHA_VENCIMIENTO_DEUDA);
		String glosa = ""; //En blanco
		String id_producto = "01"; 
		String id_tipo_producto = "01";
		String marca = ""; //En blanco
		
		String monto_unitario = UtilesPago.formatoMontoDevetel(String.valueOf(montoaux));
		String tipo_doc_producto = ""; //En blanco

		/**/

		String tipo_doc = ""; // En blanco
		
		log.debug("Cod portador : " + cod_portador);
		log.debug("empresa : " + empresa);
		log.debug("pag_respuesta : " + pag_respuesta);
		log.debug("id_canal : " + id_canal);
		log.debug("password_canal : " + password_canal);
		log.debug("servicio : " + servicio);
		log.debug("tipo_registro : " + tipo_registro);
		log.debug("opcional1 : " + opcional1);
		log.debug("opcional2 : " + opcional2);
		log.debug("direccion : " + direccion);
		log.debug("email : " + email);
		//log.debug("id_num_celular : " + id_num_celular);
		//log.debug("id_rut : " + id_rut);
		log.debug("id_tel_fijo : " + id_tel_fijo);
		log.debug("nombre : " + nombre);
		log.debug("num_cliente : " + num_cliente);
		log.debug("region : " + region);
		log.debug("cant_productos : " + cant_productos);
		log.debug("fecha_vencimiento : " + fecha_vencimiento);
		log.debug("monto_total : " + monto_total);
		log.debug("num_orden : " + num_orden);
		log.debug("param_resp : " + param_resp);
		
		log.debug("cant_productos_producto : " + cant_productos_producto);
		log.debug("fecha_vencimiento_producto : " + fecha_vencimiento_producto);
		log.debug("glosa : " + glosa);
		log.debug("id_producto : " + id_producto);
		log.debug("id_tipo_producto : " + id_tipo_producto);
		log.debug("marca : " + marca);
		log.debug("monto_unitario : " + monto_unitario);
		log.debug("tipo_doc_producto : " + tipo_doc_producto);
		

		try{
			log.debug("LLamando a validaAcceso...");
			cl.ev.mo.portalcorporate.devetelWS.Autentificacion _validaAcceso_autentificacion = new cl.ev.mo.portalcorporate.devetelWS.Autentificacion();
			_validaAcceso_autentificacion.setCODPORTADOR(cod_portador);
			_validaAcceso_autentificacion.setEMPRESA(empresa);
			_validaAcceso_autentificacion.setIDCANAL(id_canal);
			_validaAcceso_autentificacion.setPAGRESP(pag_respuesta);
			_validaAcceso_autentificacion.setPASSWORDCANAL(password_canal);
			_validaAcceso_autentificacion.setSERVICIO(servicio);
			_validaAcceso_autentificacion.setTIPOREGISTRO(tipo_registro);
			_validaAcceso_autentificacion.setOPTIONAL1(opcional1);
			_validaAcceso_autentificacion.setOPTIONAL2(opcional2);
			cl.ev.mo.portalcorporate.devetelWS.Cliente _validaAcceso_cliente = new cl.ev.mo.portalcorporate.devetelWS.Cliente();
			_validaAcceso_cliente.setDIRECCION(direccion);
			_validaAcceso_cliente.setEMAIL(email);
			//_validaAcceso_cliente.setIDNUMCELULAR(id_num_celular);
			//_validaAcceso_cliente.setIDRUT(id_rut);
			//_validaAcceso_cliente.setIDTELFIJO(id_tel_fijo);
			_validaAcceso_cliente.setNOMBRE(nombre);
			_validaAcceso_cliente.setNUMCLIENTE(num_cliente);
			_validaAcceso_cliente.setREGION(region);
			cl.ev.mo.portalcorporate.devetelWS.DetalleCompra _validaAcceso_detalleCOMPRA = new cl.ev.mo.portalcorporate.devetelWS.DetalleCompra();
			_validaAcceso_detalleCOMPRA.setCANTPRODUCTOS(cant_productos);
			_validaAcceso_detalleCOMPRA.setFECHAVENC(fecha_vencimiento);
			_validaAcceso_detalleCOMPRA.setMONTOTOTAL(monto_total);
			_validaAcceso_detalleCOMPRA.setNUMORDEN(num_orden);
			_validaAcceso_detalleCOMPRA.setPARAMRESP(param_resp);
			java.util.List<cl.ev.mo.portalcorporate.devetelWS.Producto> _validaAcceso_detalleCOMPRAProductos = new java.util.ArrayList<cl.ev.mo.portalcorporate.devetelWS.Producto>();
			cl.ev.mo.portalcorporate.devetelWS.Producto _validaAcceso_detalleCOMPRAProductosVal1 = new cl.ev.mo.portalcorporate.devetelWS.Producto();
			_validaAcceso_detalleCOMPRAProductosVal1.setCANTIDADPRODUCTOS(cant_productos_producto);
			_validaAcceso_detalleCOMPRAProductosVal1.setFECHAVENC(fecha_vencimiento_producto);
			_validaAcceso_detalleCOMPRAProductosVal1.setGLOSA(glosa);
			_validaAcceso_detalleCOMPRAProductosVal1.setIDPRODUCTO(id_producto);
			_validaAcceso_detalleCOMPRAProductosVal1.setIDTIPOPRODUCTO(id_tipo_producto);
			_validaAcceso_detalleCOMPRAProductosVal1.setMARCA(marca);
			_validaAcceso_detalleCOMPRAProductosVal1.setMONTOUNITARIO(monto_unitario);
			_validaAcceso_detalleCOMPRAProductosVal1.setTIPODOC(tipo_doc_producto);
			_validaAcceso_detalleCOMPRAProductos.add(_validaAcceso_detalleCOMPRAProductosVal1);
			_validaAcceso_detalleCOMPRA.getProductos().addAll(_validaAcceso_detalleCOMPRAProductos);
			_validaAcceso_detalleCOMPRA.setTIPODOC(tipo_doc);
			cl.ev.mo.portalcorporate.devetelWS.ValidaAccesoRespuesta _validaAcceso__return = port.validaAcceso(_validaAcceso_autentificacion, _validaAcceso_cliente, _validaAcceso_detalleCOMPRA);

			log.debug("Cod respuesta valida acceso : " + _validaAcceso__return.getAUTENTIFICACION().getCODRESPUESTA());	
			return _validaAcceso__return;
		}
		catch(Exception e){
			log.error("Error en la llamada de ValidaAcceso : " + e);
			e.printStackTrace();
			return null;
		}
	}
}
