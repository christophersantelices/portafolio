package cl.ev.mo.portalcorporate.beans;

public class ObjetoDePago {
	
	public static final String CUENTA = "cuenta";
	public static final String DOCUMENTO = "documento";
	public static final String SERVICIO = "servicio";
	public static final String ORDEN = "orden";
	public static final String ABONO = "abono";

	private String tipoObjeto ;
	
	private String idCuentaFinanciera ;
	private String idDocumento;
	private String idServicio;
	private String tokenDePago;
	private String systemID;
	private String montoAPagar;
	private String nombreCliente;
	
	public ObjetoDePago(){
		tipoObjeto = CUENTA;
	}
	
	public String getIdCuentaFinanciera() {
		return idCuentaFinanciera;
	}
	public void setIdCuentaFinanciera(String idCuentaFinanciera) {
		this.idCuentaFinanciera = idCuentaFinanciera;
	}
	public String getTokenDePago() {
		return tokenDePago;
	}
	public void setTokenDePago(String tokenDePago) {
		this.tokenDePago = tokenDePago;
	}
	public String getSystemID() {
		return systemID;
	}
	public void setSystemID(String systemID) {
		this.systemID = systemID;
	}

	public String getMontoAPagar() {
		return montoAPagar;
	}

	public void setMontoAPagar(String montoAPagar) {
		this.montoAPagar = montoAPagar;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getTipoObjeto() {
		return tipoObjeto;
	}

	public void setTipoObjeto(String tipoObjeto) {
		this.tipoObjeto = tipoObjeto;
	}
}
