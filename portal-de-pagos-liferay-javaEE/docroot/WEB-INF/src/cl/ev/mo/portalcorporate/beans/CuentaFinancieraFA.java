package cl.ev.mo.portalcorporate.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class CuentaFinancieraFA extends BaseFindAccount implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idCuentaFinanciera;
	private ArrayList<DocumentoFA> documentos;
	
	
	
	/*Estos campos debiesen ser parte de una clase de más bajo nivel??*/
	private String montoVencido;
	private String montoPorVencer;
	private String montoEnDisputa;
	private String nombreCliente;

	private boolean tienePat;
	private boolean tienePagoPorServicio;
	private String tipoCliente;
	private String tipoCuenta;
	private String subTipoCliente;	
	
	public CuentaFinancieraFA(){
		idCuentaFinanciera = "";
		documentos = new ArrayList<DocumentoFA>();
		tokenDePago = "";
		systemID = "";
		montoVencido = "";
		montoPorVencer = "";
		montoTotal = "";
		montoEnDisputa = "";
		nombreCliente = "";
		tienePat = false;
		tienePagoPorServicio = false;
		tipoCliente = "";
		tipoCuenta="";
		subTipoCliente = "";
	}

	public String getIdCuentaFinanciera() {
		return idCuentaFinanciera;
	}

	public void setIdCuentaFinanciera(String idCuentaFinanciera) {
		this.idCuentaFinanciera = idCuentaFinanciera;
	}

	public ArrayList<DocumentoFA> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(ArrayList<DocumentoFA> documentos) {
		this.documentos = documentos;
	}

	public String getMontoVencido() {
		return montoVencido;
	}

	public void setMontoVencido(String montoVencido) {
		this.montoVencido = montoVencido;
	}

	public String getMontoPorVencer() {
		return montoPorVencer;
	}

	public void setMontoPorVencer(String montoPorVencer) {
		this.montoPorVencer = montoPorVencer;
	}


	public String getMontoEnDisputa() {
		return montoEnDisputa;
	}

	public void setMontoEnDisputa(String montoEnDisputa) {
		this.montoEnDisputa = montoEnDisputa;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public boolean isTienePat() {
		return tienePat;
	}

	public void setTienePat(boolean tienePat) {
		this.tienePat = tienePat;
	}

	public boolean isTienePagoPorServicio() {
		return tienePagoPorServicio;
	}

	public void setTienePagoPorServicio(boolean tienePagoPorServicio) {
		this.tienePagoPorServicio = tienePagoPorServicio;
	}

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta (String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public String getSubTipoCliente() {
		return subTipoCliente;
	}

	public void setSubTipoCliente(String subTipoCliente) {
		this.subTipoCliente = subTipoCliente;
	}

	public String getMontoTotal() {
		return this.montoTotal;
	}

	public void setMontoTotal(String montoTotal) {
		this.montoTotal = montoTotal;
	}

	
}
