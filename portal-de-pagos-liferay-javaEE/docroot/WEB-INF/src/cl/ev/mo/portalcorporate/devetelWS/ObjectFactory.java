
package cl.ev.mo.portalcorporate.devetelWS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.ev.mo.portalcorporate.devetelWS package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ValidaAcceso_QNAME = new QName("http://ws/", "validaAcceso");
    private final static QName _ValidaAccesoResponse_QNAME = new QName("http://ws/", "validaAccesoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.ev.mo.portalcorporate.devetelWS
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValidaAcceso }
     * 
     */
    public ValidaAcceso createValidaAcceso() {
        return new ValidaAcceso();
    }

    /**
     * Create an instance of {@link ValidaAccesoResponse }
     * 
     */
    public ValidaAccesoResponse createValidaAccesoResponse() {
        return new ValidaAccesoResponse();
    }

    /**
     * Create an instance of {@link Autentificacion }
     * 
     */
    public Autentificacion createAutentificacion() {
        return new Autentificacion();
    }

    /**
     * Create an instance of {@link Cliente }
     * 
     */
    public Cliente createCliente() {
        return new Cliente();
    }

    /**
     * Create an instance of {@link DetalleCompra }
     * 
     */
    public DetalleCompra createDetalleCompra() {
        return new DetalleCompra();
    }

    /**
     * Create an instance of {@link Producto }
     * 
     */
    public Producto createProducto() {
        return new Producto();
    }

    /**
     * Create an instance of {@link ValidaAccesoRespuesta }
     * 
     */
    public ValidaAccesoRespuesta createValidaAccesoRespuesta() {
        return new ValidaAccesoRespuesta();
    }

    /**
     * Create an instance of {@link AutentificacionResponse }
     * 
     */
    public AutentificacionResponse createAutentificacionResponse() {
        return new AutentificacionResponse();
    }

    /**
     * Create an instance of {@link MediosPagosList }
     * 
     */
    public MediosPagosList createMediosPagosList() {
        return new MediosPagosList();
    }

    /**
     * Create an instance of {@link MedioPago }
     * 
     */
    public MedioPago createMedioPago() {
        return new MedioPago();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaAcceso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "validaAcceso")
    public JAXBElement<ValidaAcceso> createValidaAcceso(ValidaAcceso value) {
        return new JAXBElement<ValidaAcceso>(_ValidaAcceso_QNAME, ValidaAcceso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaAccesoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "validaAccesoResponse")
    public JAXBElement<ValidaAccesoResponse> createValidaAccesoResponse(ValidaAccesoResponse value) {
        return new JAXBElement<ValidaAccesoResponse>(_ValidaAccesoResponse_QNAME, ValidaAccesoResponse.class, null, value);
    }

}
