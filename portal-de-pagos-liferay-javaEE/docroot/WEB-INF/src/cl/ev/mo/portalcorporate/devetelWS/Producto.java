
package cl.ev.mo.portalcorporate.devetelWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para producto complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="producto"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CANTIDAD_PRODUCTOS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FECHA_VENC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GLOSA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ID_PRODUCTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ID_TIPO_PRODUCTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MARCA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MONTO_UNITARIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TIPO_DOC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "producto", propOrder = {
    "cantidadproductos",
    "fechavenc",
    "glosa",
    "idproducto",
    "idtipoproducto",
    "marca",
    "montounitario",
    "tipodoc"
})
public class Producto {

    @XmlElement(name = "CANTIDAD_PRODUCTOS")
    protected String cantidadproductos;
    @XmlElement(name = "FECHA_VENC")
    protected String fechavenc;
    @XmlElement(name = "GLOSA")
    protected String glosa;
    @XmlElement(name = "ID_PRODUCTO")
    protected String idproducto;
    @XmlElement(name = "ID_TIPO_PRODUCTO")
    protected String idtipoproducto;
    @XmlElement(name = "MARCA")
    protected String marca;
    @XmlElement(name = "MONTO_UNITARIO")
    protected String montounitario;
    @XmlElement(name = "TIPO_DOC")
    protected String tipodoc;

    /**
     * Obtiene el valor de la propiedad cantidadproductos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCANTIDADPRODUCTOS() {
        return cantidadproductos;
    }

    /**
     * Define el valor de la propiedad cantidadproductos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCANTIDADPRODUCTOS(String value) {
        this.cantidadproductos = value;
    }

    /**
     * Obtiene el valor de la propiedad fechavenc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFECHAVENC() {
        return fechavenc;
    }

    /**
     * Define el valor de la propiedad fechavenc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFECHAVENC(String value) {
        this.fechavenc = value;
    }

    /**
     * Obtiene el valor de la propiedad glosa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLOSA() {
        return glosa;
    }

    /**
     * Define el valor de la propiedad glosa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLOSA(String value) {
        this.glosa = value;
    }

    /**
     * Obtiene el valor de la propiedad idproducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDPRODUCTO() {
        return idproducto;
    }

    /**
     * Define el valor de la propiedad idproducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDPRODUCTO(String value) {
        this.idproducto = value;
    }

    /**
     * Obtiene el valor de la propiedad idtipoproducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDTIPOPRODUCTO() {
        return idtipoproducto;
    }

    /**
     * Define el valor de la propiedad idtipoproducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDTIPOPRODUCTO(String value) {
        this.idtipoproducto = value;
    }

    /**
     * Obtiene el valor de la propiedad marca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMARCA() {
        return marca;
    }

    /**
     * Define el valor de la propiedad marca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMARCA(String value) {
        this.marca = value;
    }

    /**
     * Obtiene el valor de la propiedad montounitario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMONTOUNITARIO() {
        return montounitario;
    }

    /**
     * Define el valor de la propiedad montounitario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMONTOUNITARIO(String value) {
        this.montounitario = value;
    }

    /**
     * Obtiene el valor de la propiedad tipodoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPODOC() {
        return tipodoc;
    }

    /**
     * Define el valor de la propiedad tipodoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPODOC(String value) {
        this.tipodoc = value;
    }

}
