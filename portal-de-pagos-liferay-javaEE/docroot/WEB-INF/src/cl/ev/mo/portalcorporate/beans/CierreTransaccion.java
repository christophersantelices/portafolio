package cl.ev.mo.portalcorporate.beans;

public class CierreTransaccion {
	
	private String monto;
	private String identificadorDePago;
	private String medioDePago;
	private String numeroDeOrden;
	private String codigoTransaccion;
	private String ultimosDigitosTarjeta;
	private String numeroCuotas;
	private String codigoAutorizacion;
	private String tipoTransaccion;
	private String tipoTarjeta;
	private String tipoCuotas;
	private String fechaPago;
	
	
	public CierreTransaccion(){
		
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getIdentificadorDePago() {
		return identificadorDePago;
	}
	public void setIdentificadorDePago(String identificadorDePago) {
		this.identificadorDePago = identificadorDePago;
	}
	public String getMedioDePago() {
		return medioDePago;
	}
	public void setMedioDePago(String medioDePago) {
		this.medioDePago = medioDePago;
	}
	public String getNumeroDeOrden() {
		return numeroDeOrden;
	}
	public void setNumeroDeOrden(String numeroDeOrden) {
		this.numeroDeOrden = numeroDeOrden;
	}
	public String getCodigoTransaccion() {
		return codigoTransaccion;
	}
	public void setCodigoTransaccion(String codigoTransaccion) {
		this.codigoTransaccion = codigoTransaccion;
	}
	public String getUltimosDigitosTarjeta() {
		return ultimosDigitosTarjeta;
	}
	public void setUltimosDigitosTarjeta(String ultimosDigitosTarjeta) {
		this.ultimosDigitosTarjeta = ultimosDigitosTarjeta;
	}
	public String getNumeroCuotas() {
		return numeroCuotas;
	}
	public void setNumeroCuotas(String numeroCuotas) {
		this.numeroCuotas = numeroCuotas;
	}
	public String getCodigoAutorizacion() {
		return codigoAutorizacion;
	}
	public void setCodigoAutorizacion(String codigoAutorizacion) {
		this.codigoAutorizacion = codigoAutorizacion;
	}
	public String getTipoTransaccion() {
		return tipoTransaccion;
	}
	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getTipoCuotas() {
		return tipoCuotas;
	}
	public void setTipoCuotas(String tipoCuotas) {
		this.tipoCuotas = tipoCuotas;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
}
