package cl.ev.mo.portalcorporate.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class DocumentoFA extends BaseFindAccount implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idDocumento;
	private String numeroBoleta;
	private String monto;
	private String fecha;
	private boolean vencido;
	private String typoDocumento;
	
	private ArrayList<ServicioFA> servicios;
	
	public DocumentoFA(){
		systemID = "";
		tokenDePago = "";
	}

	public String getTypoDocumento() {
		return typoDocumento;
	}

	public void setTypoDocumento(String typoDocumento) {
		this.typoDocumento = typoDocumento;
	}
	
	public String getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}

	public ArrayList<ServicioFA> getServicios() {
		return servicios;
	}

	public void setServicios(ArrayList<ServicioFA> servicios) {
		this.servicios = servicios;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public boolean isVencido() {
		return vencido;
	}

	public void setVencido(boolean vencido) {
		this.vencido = vencido;
	}

	public String getNumeroBoleta() {
		return numeroBoleta;
	}

	public void setNumeroBoleta(String numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}
}
