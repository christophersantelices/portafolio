package cl.ev.mo.portalcorporate.utiles;

import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticleDisplay;
import com.liferay.portlet.journalcontent.util.JournalContentUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;

import javax.portlet.ActionRequest;
import javax.portlet.ResourceRequest;
import javax.servlet.http.HttpServletRequest;


public class UtilesContenidos {

	private static Log log = LogFactoryUtil.getLog(UtilesContenidos.class);
	
	public static String obtenerContenidoTexto(HttpServletRequest request , String nombreContenido){

		String valor = "";

		try{
			String articleName = nombreContenido;
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), articleName);
			String articleId = journalArticle.getArticleId();
			JournalArticleDisplay articleDisplay = JournalContentUtil.getDisplay(themeDisplay.getScopeGroupId(),articleId, "","",themeDisplay);
			valor = articleDisplay.getContent();
		}
		catch (Exception e){
			valor = "Default";
			log.error("Error al obtener el contenido web con nombre : " + nombreContenido);
		}
		return valor;
	}
	
	public static String obtenerContenidoTexto(ActionRequest request , String nombreContenido){

		String valor = "";

		try{
			String articleName = nombreContenido;
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), articleName);
			String articleId = journalArticle.getArticleId();
			JournalArticleDisplay articleDisplay = JournalContentUtil.getDisplay(themeDisplay.getScopeGroupId(),articleId, "","",themeDisplay);
			valor = articleDisplay.getContent();
		}
		catch (Exception e){
			valor = "Default";
			log.error("Error al obtener el contenido web con nombre : " + nombreContenido);
		}
		return valor;
	}

	public static String obtenerContenidoTexto(ResourceRequest request ,String nombreContenido){
		
		String valor = "";
		try{
			String articleName = nombreContenido;
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), articleName);
			String articleId = journalArticle.getArticleId();
			JournalArticleDisplay articleDisplay = JournalContentUtil.getDisplay(themeDisplay.getScopeGroupId(),articleId, "","",themeDisplay);
			valor = articleDisplay.getContent();
		}
		catch (Exception e){
			valor = "Default";
			log.error("Error al obtener el contenido web con nombre : " + nombreContenido);
		}
		return valor;
	}
	
}
