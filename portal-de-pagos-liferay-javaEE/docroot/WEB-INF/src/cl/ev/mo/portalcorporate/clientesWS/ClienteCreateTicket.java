package cl.ev.mo.portalcorporate.clientesWS;


import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import cl.ev.mo.portalcorporate.beans.CarroDePago;
import cl.ev.mo.portalcorporate.beans.DocumentoFA;
import cl.ev.mo.portalcorporate.beans.ObjetoDePago;
import cl.ev.mo.portalcorporate.serviciosHelper.UtilServicios;
import cl.ev.mo.portalcorporate.serviciosHelper.UtilesClientesWS;
import cl.ev.mo.portalcorporate.utiles.SessionUtil;
import cl.ev.mo.portalcorporate.utiles.UtilesPago;
import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.utiles.UtilesContenidos;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.CreateTicket;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.CreateTicketPortType;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.CreatableContext;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.CreatableCustomerAccount;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.CreatableService;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.CreatableTicket;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.CreatableTicketList;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.CreateTicketRequest;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.CreateTicketRequestData;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.CreateTicketResponse;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.Money;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.ObjectFactory;
import cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types.ThirdPartyCollector;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderRequest;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ClienteCreateTicket extends UtilesClientesWS{
	
	
	public static final String TICKET_DIFERIDO_SIN_RUT = "diferido_sin_rut";
	public static final String TICKET_INMEDIATO_SIN_RUT= "inmediato_sin_rut";

	Log log = LogFactoryUtil.getLog(ClienteCreateTicket.class);
	
	ActionRequest request;
	
	
	private String endpoint = "http://10.186.31.15:9081/moESBRDPAD/CreateTicket";
	//private static final String endpoint = "http://pca2635.ctc.cl:9039/ accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1";

	private static final QName SERVICE_NAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1", "CreateTicket");

	URL wsdlURL;
	CreateTicket ss ;
	CreateTicketPortType port;
	
	cl.telefonica.enterpriseapplicationintegration.tefheader.v1.ObjectFactory factoryIdentification;

	public ClienteCreateTicket(ActionRequest request){

		this.endpoint = UtilesContenidos.obtenerContenidoTexto(request, Constantes._endpoint_create_ticket);
		wsdlURL = this.getClass().getResource("/wsdl/CreateTicket.wsdl");
		if(wsdlURL==null){
			System.out.println("URL null");
		}
		ss = new CreateTicket(wsdlURL, SERVICE_NAME);
		port = ss.getCreateTicketPort();
		BindingProvider provider = (BindingProvider) port;
		provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY , endpoint);
		factoryIdentification = new  cl.telefonica.enterpriseapplicationintegration.tefheader.v1.ObjectFactory();
		this.request = request;
		
	}
	
	
	public CreateTicketResponse callCrearTicket(ActionRequest actionRequest,CarroDePago carro, String tipoTicket){
		log.debug("Ingreso a callCrearTicket");
		log.debug("Ingreso a crear el ticket del tipo : " + tipoTicket + " , el largo del carro es " + carro.getObjetosDePago().size() + ", el monto total es : " + carro.getMontoTotalPago());
		log.debug("Endpoint utilizado : " + this.endpoint);
		log.debug("carro : " + carro);
		log.debug("detalles_documentos"+SessionUtil.getVariable(actionRequest, Constantes.detalle_deuda_documentos));
		try{

			ObjectFactory factory = new ObjectFactory();
			
			String [] datosPagador = UtilesPago.obtienePartyRoleIdYAgencyId(request);
			log.debug("Datos de pagador segun medio de pago, Party role ID : " + datosPagador [0] + ", agencyID : " + datosPagador[1]);
			
			String partyRoleId = datosPagador [0];
			String agencyId = datosPagador [1];
			String fechaPago = generaFechaPago();
			
			//TODO:validar
			String shopId = "1";
			String operationType ="C";
			String identificationType = "RUT";
			String identificactionNumber = "0";
			String identificationCode = "";
			String ticketType = "D";
			
			log.debug("Invocando a createTicket...");
			CreateTicketRequest _createTicket_createTicketRequest = new CreateTicketRequest();
			TEFHeaderRequest _createTicket_createTicketRequestHeader = new TEFHeaderRequest();
			// Lleno la cabecera
			_createTicket_createTicketRequestHeader = fillCreateTicketHeader(_createTicket_createTicketRequestHeader);
			//Seteo la cabecera
			_createTicket_createTicketRequest.setHeader(_createTicket_createTicketRequestHeader);
			log.debug("Cabecera creada correctamente...");
			_createTicket_createTicketRequest.getHeader().setTransactionTimestamp(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(fechaPago)); 
			
			// Llenando Data
			log.debug("Llenando data ticket.");
			CreateTicketRequestData _createTicket_createTicketRequestData = new CreateTicketRequestData();
			ThirdPartyCollector _createTicket_createTicketRequestDataThirdPartyCollector = new ThirdPartyCollector();
			_createTicket_createTicketRequestDataThirdPartyCollector.setPartyRoleId(partyRoleId);
			_createTicket_createTicketRequestDataThirdPartyCollector.setAgencyID(new java.math.BigInteger(agencyId));
			_createTicket_createTicketRequestDataThirdPartyCollector.setShopID(new java.math.BigInteger(shopId));
			_createTicket_createTicketRequestData.setThirdPartyCollector(_createTicket_createTicketRequestDataThirdPartyCollector);
			_createTicket_createTicketRequestData.setOperationDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(fechaPago)); 
			_createTicket_createTicketRequestData.setOperationType(operationType);
			log.debug("Llenado data ticket ThirdPartyCollector.");
			
			CreatableContext _createTicket_createTicketRequestDataContext = new CreatableContext();
			TEFChileanIdentificationNumber _createTicket_createTicketRequestDataContextCustomerIdentification = new TEFChileanIdentificationNumber();
			_createTicket_createTicketRequestDataContextCustomerIdentification.setIdentificationType(identificationType);
			_createTicket_createTicketRequestDataContextCustomerIdentification.setIdentificationNumber(identificactionNumber);
			
			log.debug("Llenado data ticket CreatableContext.");
			//_createTicket_createTicketRequestDataContextCustomerIdentification.se
			
			JAXBElement<java.lang.String> _createTicket_createTicketRequestDataContextCustomerIdentificationIdentificationCode = factoryIdentification.createTEFChileanIdentificationNumberIdentificationCode(identificationCode);
			_createTicket_createTicketRequestDataContextCustomerIdentification.setIdentificationCode(_createTicket_createTicketRequestDataContextCustomerIdentificationIdentificationCode);
			_createTicket_createTicketRequestDataContext.setCustomerIdentification(_createTicket_createTicketRequestDataContextCustomerIdentification);
			_createTicket_createTicketRequestDataContext.setTicketType(ticketType);
			
			log.debug("Comienzo a crear lista de ticket.");
			
			CreatableTicketList _createTicket_createTicketRequestDataContextTicketList = new CreatableTicketList();
			List<CreatableTicket> _createTicket_createTicketRequestDataContextTicketListTicket = new ArrayList<CreatableTicket>();
			
			Iterator<ObjetoDePago> it = carro.getObjetosDePago().iterator();
			log.debug("****While*****");
			while(it.hasNext()){
				
				ObjetoDePago obj = it.next();
				CreatableTicket ticket = new CreatableTicket();
				/*Lista de ticket*/
				//Datos básicos
				String marketType = "1";
				String customerAccountID = obj.getIdCuentaFinanciera();
				log.debug("Nombre antes "+obj.getNombreCliente());
				String customerAccountName;
				
				if (obj.getNombreCliente().length()>30) {
					customerAccountName = obj.getNombreCliente().substring(0,30);
				}
				else {
					customerAccountName = obj.getNombreCliente();
				}
				log.debug("Nombre dsps"+customerAccountName);
				String monto = obj.getMontoAPagar();		
				if (obj.getTipoObjeto().equals(ObjetoDePago.CUENTA)){
					log.debug("El tipo es cuenta");
//					ticket.setInvoiceID(factory.createCreatableTicketInvoiceID(Long.parseLong("0")));
//					ticket.setInvoiceDueDate(factory.createCreatableTicketInvoiceDueDate(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar("")));
//					ticket.setInvoiceType(factory.createCreatableTicketInvoiceType(""));
//					ticket.setDocumentFormat(factory.createCreatableTicketDocumentFormat(""));
//					ticket.setInvoiceNumber(factory.createCreatableTicketInvoiceNumber(Long.parseLong("")));
					
//					String serviceId = "";
//					String serviceDescription = "";
//					CreatableService serv = new CreatableService();
//					serv.setID(serviceId);
//					serv.setDescription(serviceDescription);
//					ticket.setService(factory.createCreatableTicketService(serv));
					
				}
				
				
				if (obj.getTipoObjeto().equals(ObjetoDePago.DOCUMENTO)){
					log.debug("El tipo es documento "+obj.getIdDocumento());
					String invoiceId = obj.getIdDocumento();
					//String invoiceNumber = "123456789012";
					String[] datosDocumento=UtilServicios.getDatosDocumento(actionRequest,invoiceId);
					String invoiceDueDate=datosDocumento[0].split("-")[2]+"-"+datosDocumento[0].split("-")[1]+"-"+datosDocumento[0].split("-")[0];
					log.debug("Fecha de vencimiento "+invoiceDueDate);
					String invoiceType = datosDocumento[1];
					String documentFormat = "B";
					
					ticket.setInvoiceID(factory.createCreatableTicketInvoiceID(Long.parseLong(invoiceId)));
					ticket.setInvoiceDueDate(factory.createCreatableTicketInvoiceDueDate(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(invoiceDueDate)));
					ticket.setInvoiceType(factory.createCreatableTicketInvoiceType(invoiceType));
					ticket.setDocumentFormat(factory.createCreatableTicketDocumentFormat(documentFormat));
					//ticket.setInvoiceNumber(factory.createCreatableTicketInvoiceNumber(Long.parseLong(invoiceNumber)));

//					CreatableService serv = new CreatableService();
//					serv.setID("");
//					serv.setDescription("");
//					ticket.setService(factory.createCreatableTicketService(serv));
					
				}
				
				
				if(obj.getTipoObjeto().equals(ObjetoDePago.SERVICIO)){
					log.debug("El tipo es servicio");
					String invoiceId = obj.getIdDocumento();
					String serviceId = obj.getIdServicio();
					//String invoiceNumber = "123456789012";
					String[] datosDocumento=UtilServicios.getDatosDocumento(actionRequest,invoiceId,serviceId);
					String invoiceDueDate=datosDocumento[0].split("-")[2]+"-"+datosDocumento[0].split("-")[1]+"-"+datosDocumento[0].split("-")[0];
					log.debug("Fecha de vencimiento "+invoiceDueDate);
					String invoiceType = datosDocumento[1];
					String documentFormat = "B";
					
					ticket.setInvoiceID(factory.createCreatableTicketInvoiceID(Long.parseLong(invoiceId)));
					ticket.setInvoiceDueDate(factory.createCreatableTicketInvoiceDueDate(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(invoiceDueDate)));
					ticket.setInvoiceType(factory.createCreatableTicketInvoiceType(invoiceType));
					ticket.setDocumentFormat(factory.createCreatableTicketDocumentFormat(documentFormat));
					//ticket.setInvoiceNumber(factory.createCreatableTicketInvoiceNumber(Long.parseLong(invoiceNumber)));
					
					String serviceDescription = datosDocumento[3];
					//String orderId = "0";
					CreatableService serv = new CreatableService();
					serv.setID(serviceId);
					serv.setDescription(serviceDescription);
					ticket.setService(factory.createCreatableTicketService(serv));
				}
				else{
					log.debug("El tipo es otro... : " + obj.getTipoObjeto());
					if(  obj.getTipoObjeto().trim().equals("orden") ){
						String orderId = SessionUtil.getVariable(request, Constantes.VENTAS_NUMERO_DE_ORDEN);
						log.debug("Numero de orden al crear el ticket  : " + orderId);
						ticket.setOrderID(factory.createCreatableTicketOrderID(Long.parseLong(orderId)));
					}
				}
				ticket.setMarketType(factory.createCreatableTicketMarketType(Long.parseLong(marketType)));
				String token = obj.getTokenDePago();
				CreatableCustomerAccount cusAccount = new CreatableCustomerAccount();
				cusAccount.setID(customerAccountID);
				cusAccount.setName(customerAccountName);
				ticket.setCustomerAccount(factory.createCreatableTicketCustomerAccount(cusAccount));
				log.debug("Creado el ambito Creatable customer account");
				Money money = new Money();
				money.setAmount(new BigDecimal(monto));
				ticket.setBillAmount(money);
				log.debug("Creado el monto en el ticket");
				//TODO: Validar como crear ticket para orden
				
				ticket.setToken(factory.createCreatableTicketToken(token));
				//_createTicket_createTicketRequestDataContextTicketListTicket.
				_createTicket_createTicketRequestDataContextTicketList.getTicket().add(ticket);
				/*Hasta aqui debiese estar dentro de un for*/
				/*******************************************/
				/*******************************************/	
				log.debug("Fin al crear el ticket ");
			}
			log.debug("****fin While*****");
			_createTicket_createTicketRequestDataContext.setTicketList(_createTicket_createTicketRequestDataContextTicketList);
			_createTicket_createTicketRequestData.setContext(_createTicket_createTicketRequestDataContext);
			_createTicket_createTicketRequest.setData(_createTicket_createTicketRequestData);
			
			log.debug("User login : " +_createTicket_createTicketRequest.getHeader().getUserLogin());
			log.debug("service channel : " + _createTicket_createTicketRequest.getHeader().getServiceChannel());
			log.debug("session code : " + _createTicket_createTicketRequest.getHeader().getSessionCode());
			log.debug("aplication : " + _createTicket_createTicketRequest.getHeader().getApplication());
			log.debug("idMessage : " + _createTicket_createTicketRequest.getHeader().getIdMessage());
			log.debug("ipAddress : " + _createTicket_createTicketRequest.getHeader().getIpAddress());
			log.debug("functionality code : " +_createTicket_createTicketRequest.getHeader().getFunctionalityCode());
			log.debug("transaction timestamp : " +_createTicket_createTicketRequest.getHeader().getTransactionTimestamp());
			log.debug("service name : " + _createTicket_createTicketRequest.getHeader().getServiceName());
			log.debug("version : " + _createTicket_createTicketRequest.getHeader().getVersion());
			log.debug("party role id : " + _createTicket_createTicketRequest.getData().getThirdPartyCollector().getPartyRoleId());
			log.debug("agency id : " + _createTicket_createTicketRequest.getData().getThirdPartyCollector().getAgencyID());
			log.debug("shop id : " + _createTicket_createTicketRequest.getData().getThirdPartyCollector().getShopID());
			log.debug("operation date time : " + _createTicket_createTicketRequest.getData().getOperationDateTime());
			log.debug("operation type : " + _createTicket_createTicketRequest.getData().getOperationType());
			log.debug("identification type : " + _createTicket_createTicketRequest.getData().getContext().getCustomerIdentification().getIdentificationType());
			log.debug("identification number : " + _createTicket_createTicketRequest.getData().getContext().getCustomerIdentification().getIdentificationNumber());
			log.debug("identification code : " + _createTicket_createTicketRequest.getData().getContext().getCustomerIdentification().getIdentificationCode().getValue());
			log.debug("ticket type : " + _createTicket_createTicketRequest.getData().getContext().getTicketType());
			
			
			
			for(CreatableTicket t : _createTicket_createTicketRequest.getData().getContext().getTicketList().getTicket()){
				
				log.debug("####################################");
				log.debug("Market type : " +t.getMarketType().getValue());
				log.debug("Customer account id : " + t.getCustomerAccount().getValue().getID());
				log.debug("Customer account name : " + t.getCustomerAccount().getValue().getName());
				log.debug("Ammount : " + t.getBillAmount().getAmount());
				log.debug("Token : " + t.getToken().getValue());

				log.debug("-");
				try{
					log.debug("Invoice id : " + t.getInvoiceID().getValue());
					//log.debug("Invoice number : " + t.getInvoiceNumber().getValue());
					log.debug("Invoice due date : " + t.getInvoiceDueDate().getValue());
					log.debug("Invoice type : " + t.getInvoiceType().getValue());
					log.debug("Document format : " + t.getDocumentFormat().getValue());
					log.debug("-");
					log.debug("Service id : " + t.getService().getValue().getID());
					log.debug("Service description : "+ t.getService().getValue().getDescription());
					log.debug("####################################");
				}
				catch(Exception e){
					log.debug("xxxxxxxx");
				}
			}

			CreateTicketResponse _createTicket__return = port.createTicket(_createTicket_createTicketRequest);
			log.debug("El codigo de respuesta de create ticket es : " + _createTicket__return.getData().getStatus().getCode());
			log.debug("La descripcion de la respuesta de create ticket es : " + _createTicket__return.getData().getStatus().getDescription());
			return _createTicket__return;
			
		}
		catch (DatatypeConfigurationException e) {
			log.error("Expected exception: CreateTicketFault has occurred.");
			log.error(e);
			return null;
		}
		catch (Exception e) { 
			log.error("Expected exception: CreateTicketFault has occurred.");
			log.error(e);
			return null;
		} 
	}
	
	private String generaFechaPago(){
		//2015-07-09T00:00:00
		String fecha = "";
		try{
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//Entrada 
			SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");//Entrada 
			Date hoy = new Date();
			fecha = sdf1.format(hoy) + "T" + sdf2.format(hoy);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return fecha;
	}
	
}


