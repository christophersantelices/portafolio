package cl.ev.mo.portalcorporate.beans;

import java.io.Serializable;

public class ServicioFA extends BaseFindAccount implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idServicio;
	
	private String nombre;
	private String monto;
	
	
	public ServicioFA() {
		systemID = "";
		tokenDePago = "";
	}


	public String getIdServicio() {
		return idServicio;
	}


	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getMonto() {
		return monto;
	}


	public void setMonto(String monto) {
		this.monto = monto;
	}	
}
