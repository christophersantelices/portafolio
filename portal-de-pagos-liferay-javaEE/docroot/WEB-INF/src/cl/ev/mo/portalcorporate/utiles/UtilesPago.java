package cl.ev.mo.portalcorporate.utiles;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.model.JournalArticleDisplay;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.journalcontent.util.JournalContentUtil;

import cl.ev.mo.portalcorporate.utiles.Constantes;
import cl.ev.mo.portalcorporate.beans.BillingCustomerFA;
import cl.ev.mo.portalcorporate.beans.CarroDePago;
import cl.ev.mo.portalcorporate.beans.CuentaFinancieraFA;
import cl.ev.mo.portalcorporate.beans.IntegracionPDP;
import cl.ev.mo.portalcorporate.beans.IntegrateJSONObject;
import cl.ev.mo.portalcorporate.beans.ObjetoDePago;
import cl.ev.mo.portalcorporate.beans.RespuestaFindAccount;
import cl.ev.mo.portalcorporate.utiles.EncriptadorUtil;
import cl.ev.mo.portalcorporate.beans.DocumentoFA;
import cl.ev.mo.portalcorporate.beans.ServicioFA;
import cl.ev.mo.portalcorporate.utiles.SessionUtil;


public class UtilesPago {

	private static Log log = LogFactoryUtil.getLog(UtilesPago.class);
	
	public static CarroDePago validaCarroDeuda(ActionRequest actionRequest ,String carro){
		log.debug("Validando el carro de deuda obtenido : " +carro);		
		String [] arrCarroBase = carro.split(";");
		String strCarroLimpio ="";
		for (int i = 0 ; i < arrCarroBase.length ; i++){
			String linea = arrCarroBase[i];
			int indice = 0;
			String tipoPago = "";
			for(int j = 0 ; j < linea.length() ; j++){
				String caracter = linea.substring(j , j+1);
				if(caracter.equals("T") || caracter.equals("V")){
					indice = j;
					tipoPago = caracter;
					break;
				}
			}
			String idCuenta = linea.substring(0 , indice);
			if(tipoPago.equals("T")){
				//Si paga el total lo agrego de una
				strCarroLimpio += linea+";";
			}
			else{
				//Si paga vencida, debo validar de que pague solo vencida y no total
				boolean pagaAmbas = false;
				for(int k = 0 ; k < arrCarroBase.length ; k++){
					String val = arrCarroBase[k];
					log.debug("Comparando : " + val + " con : " + idCuenta);
					if (val.contains(idCuenta+"T")){
						log.debug("Paga ambas !!!");
						pagaAmbas = true;
					}
				}
				if(!pagaAmbas){
					log.debug("Agrego la vencida");
					strCarroLimpio += linea+";";
				}
			}
		}
		log.debug("El carro final es : " + strCarroLimpio);
		RespuestaFindAccount resp = SessionUtil.getDetalleDeuda(actionRequest);
		CarroDePago respuesta = new CarroDePago();
		//Valido que el carro no venga vacio
		if(strCarroLimpio.length() == 0){
			respuesta.setValido(false);
			return respuesta;
		}
		ArrayList<ObjetoDePago> objetosDePago = new ArrayList<ObjetoDePago>();
		String [] arrCarro = strCarroLimpio.split(";");
		Iterator<BillingCustomerFA> it = resp.getBillingCustomer().iterator();
		while(it.hasNext()){
			BillingCustomerFA bc = it.next();
			Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
			while(it2.hasNext()){
				CuentaFinancieraFA fa = it2.next();
				log.debug("##########################################");
				log.debug("######### El ID de la FA es : " + fa.getIdCuentaFinanciera());
				for (int i = 0 ; i < arrCarro.length ; i++){
					String linea = arrCarro[i];
					int indice = 0;
					for(int j = 0 ; j < linea.length() ; j++){
						String caracter = linea.substring(j , j+1);
						if(caracter.equals("T") || caracter.equals("V")){
							indice = j;
							break;
						}
					}
					String idCuenta = linea.substring(0 , indice);
					String monto = linea.substring(indice+1 , linea.length());
					String tipoPago = linea.substring(indice,indice+1);
					log.debug("ID cuenta en carro : " + idCuenta + " > monto : " + monto + " > tipo pago : " + tipoPago);
					if(tipoPago.equals("T")){
						log.debug("TOTAL");
						log.debug("### Comparo " + idCuenta + " con " + fa.getIdCuentaFinanciera());
						log.debug("### Comparo " + monto + " con " + fa.getMontoPorVencer());
						if(idCuenta.equals(fa.getIdCuentaFinanciera()) && monto.equals(fa.getMontoTotal())){
							log.debug("Agregando cuenta total...");
							ObjetoDePago objPago = new ObjetoDePago();
							objPago.setTipoObjeto(ObjetoDePago.CUENTA);
							objPago.setIdCuentaFinanciera(fa.getIdCuentaFinanciera());
							objPago.setTokenDePago(fa.getTokenDePago());
							objPago.setSystemID(fa.getSystemID());
							objPago.setMontoAPagar(fa.getMontoTotal());
							objPago.setNombreCliente(fa.getNombreCliente());
							objetosDePago.add(objPago);
							break;
						}
						else{
							log.debug("No coincide id cuenta o monto en T");
						}
					}
					else if(tipoPago.equals("V")){
						log.debug("VENCIDA");
						log.debug("### Comparo " + idCuenta + " con " + fa.getIdCuentaFinanciera());
						log.debug("### Comparo " + monto + " con " + fa.getMontoVencido());
						if(idCuenta.equals(fa.getIdCuentaFinanciera()) && monto.equals(fa.getMontoVencido())){
							log.debug("Agregando cuenta vencida...");
							ObjetoDePago objPago = new ObjetoDePago();
							objPago.setIdCuentaFinanciera(fa.getIdCuentaFinanciera());
							objPago.setTokenDePago(fa.getTokenDePago());
							objPago.setSystemID(fa.getSystemID());
							objPago.setMontoAPagar(fa.getMontoVencido());
							objPago.setNombreCliente(fa.getNombreCliente());
							objetosDePago.add(objPago);
							break;
						}
						else{
							log.debug("No coincide id cuenta o monto en V");
						}
					}
				}
				log.debug("##########################################");
			}
		}
		if(objetosDePago.size() != arrCarro.length){
			respuesta.setValido(false);
		}
		else{
			respuesta.setObjetosDePago(objetosDePago);
		}
		log.debug("El largo del carro recibido es : " + arrCarro.length);
		log.debug("La cantidad de objetos es : " + objetosDePago.size());
		return respuesta;
	}

	public static String[] splitByLargo(String s, int largo){
	    int contador = (s.length() / largo) + (s.length() % largo == 0 ? 0 : 1);
	    String[] retorno = new String[contador];
	    for(int i=0;i<contador;i++){
	        retorno[i] = s.substring(i*largo, Math.min((i+1)*largo-1, s.length()));
	    }
	    return retorno;
	}

	public static String [] obtienePartyRoleIdYAgencyId (ActionRequest request){
		
		String [] datos = new String [2];
		try {
			// Party role ID , agencyID 

		    //170 008: Creación TICKET y Recaudación Servipag Web (rinde Servipag Web)
		    //008 007: Creación TICKET y Recaudación Banco Estado (rinde Banco Estado)
		    //021 006: Creación TICKET y Recaudación BCI (rinde Banco BCI)
		    //013 007: Creación TICKET y Recaudación TBK Web Pay (Mantener para Operaciones Gateway actual)
			String medioPago = SessionUtil.getVariable(request, Constantes.MEDIO_PAGO);
			log.debug("El medio de pago en session es : " + medioPago);
			
			if (medioPago.equals("Servipag")){
				datos[0] = "170";
				datos[1] = "008";
			}
			else if ("WebPay".equals(medioPago) || "WebpayWS".equalsIgnoreCase(medioPago)){
				datos[0] = "013";
				datos[1] = "007";
			}
			else if (medioPago.equals("Bci")){
				datos[0] = "021";
				datos[1] = "006";
			}
			else if (medioPago.equalsIgnoreCase("Banco Estado")){
				datos[0] = "008";
				datos[1] = "007";
			}
			else{
				datos[0] = "170";
				datos[1] = "008";
			}
		}
		catch(Exception e){
			log.error("Error al generar el party ID y agency ID : " + e);
			log.error("Retorno por defecto party id : 170 , agency id : 008");
			e.printStackTrace();
			datos[0] = "170";
			datos[1] = "008";
		}
		return datos;
	}
	
	public static String encriptaSolicitudDeCredito(String idCanal, String idMedioPago, String idTRX){
		String respuesta = "";
		StringBuilder xml = new StringBuilder();
		xml.append("<solicitarCredito>");
		xml.append("<ID_CANAL>"+idCanal.trim()+"</ID_CANAL>)");
		xml.append("<ID_MEDIO_PAGO>"+idMedioPago.trim()+"</ID_MEDIO_PAGO>");
		xml.append("<ID_TRX>"+idTRX.trim()+"</ID_TRX>");
		xml.append("</solicitarCredito>");
		String aux = EncriptadorUtil.encripta(xml.toString());
		respuesta = aux.replaceAll("\n", "");
		log.debug("Encriptado en UTIL : " + respuesta);
		return respuesta;
	}
	
	public static String montoEnFormatoFront(String monto){
		log.debug("El monto inicial es : " + monto);
		String montoFinal = "";
		long montoParcial = 0;
		try{
			montoParcial = Integer.parseInt(monto);
			//montoParcial = montoParcial / 100;
			montoFinal += montoParcial;
		}
		catch(NumberFormatException e){
			montoFinal = monto;
		}
		log.debug("El monto final es : " + separadorMiles(montoFinal));
		return separadorMiles(montoFinal);
	}
	
	public static String separadorMiles(String monto){
		double value;
		String numberFormat = "###,###,###,###";
		DecimalFormat formatter = new DecimalFormat(numberFormat);
		try {
			value = Double.parseDouble(monto);
		} catch (Throwable t) {
			return null;
		}
		String valor = formatter.format(value);
		valor = valor.replaceAll(",",".");
		return valor;

	}
	
	public static String getFechaInicioTransaccion(){
		String fecha = "";
		Date fechaActual = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY hh:mm:ss");
		fecha = sdf.format(fechaActual);
		return fecha;
	}
	
	public static String formatoMontoDevetel(String monto){
		log.debug("El monto a formatear es : " + monto);
		String montoFormateado = "";
		String montoAFormatear = monto;
		for(int i = monto.length() ; i < 15 ; i ++){
			montoAFormatear = "0"+montoAFormatear;
		}
		montoFormateado = montoAFormatear+".0000";
		log.debug("El monto formateado es : " + montoFormateado);
		return montoFormateado;
	}
	
	public static String getFechaActualComprobantePDF(){
		log.debug("Ingresa metodo getFechaActualComprobantePDF");
		try{
			String fecha = "";
			Date fechaActual = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm", new Locale("es", "ES"));
			fecha += sdf.format(fechaActual);
			String [] fechaArr = fecha.split(" ");
			String mes = fechaArr[2];
			String mesMayuscula = mes.substring(0,1).toUpperCase() + mes.substring(1,mes.length());
			String fechaAux = fechaArr[0] + " "+ fechaArr[1] + " "+mesMayuscula + " " +fechaArr[3];
			log.debug("La fecha generada es :" + fechaAux);
			return fechaAux;
		}
		catch(Exception e){
			String fecha = "";
			Date fechaActual = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm", new Locale("es", "ES"));
			fecha += sdf.format(fechaActual);
			log.debug("La fecha generada es :" + fecha);
			return fecha;
		}
	}
	
	public static String obtieneFechaPago(String fechaDevetel){
		
		String fechaFinal = "";
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");//Entrada 
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy HH:mm");//Salida
		try {
			Date fechaVencimiento = sdf1.parse(fechaDevetel);
			Date fechaActual = new Date();
			
			fechaFinal = sdf2.format(fechaVencimiento);
		} 
		catch (ParseException e) {
			log.error("Error al formatear la fecha : " + fechaDevetel + " > " + e);
			fechaFinal = fechaDevetel;
		}
		return fechaFinal;
	}
	
	public static String formateaFechaValizTicket(String fechaServicio){
		
		String fechaFinal = "";
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//Entrada 
		SimpleDateFormat sdf2 = new SimpleDateFormat("YYYY-MM-dd");//Salida, este lo cambie
		try {
			Date fechaVencimiento = sdf1.parse(fechaServicio);
			fechaFinal = sdf2.format(fechaVencimiento);
		} 
		catch (ParseException e) {
			log.error("Error al formatear la fecha : " + fechaServicio + " > " + e);
			fechaFinal = fechaServicio;
		}
		return fechaFinal + "T00:00:00-03:00";
	}
	
	public static boolean validaPagoVencido(String fechaDocumento){
		
		boolean pagoVencido=false;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//Entrada 
		try {
			Date fechaVencimiento = sdf1.parse(fechaDocumento);
			Date fechaActual = new Date();
			if(fechaActual.after(fechaVencimiento)){
				pagoVencido=true;
			}		} 
		catch (ParseException e) {
			log.error("Error al validar fecha Pago Vencido : " +e);
		}
		return pagoVencido;
	}
	
	public static IntegracionPDP getDatosIntegracion(String token){

		log.debug("Obteniendo objeto integracionPDP...");
		IntegracionPDP inte = new IntegracionPDP();
		String desencriptado = EncriptadorUtil.desencripta(token);
		log.debug("El parametro desencriptado en portlet es : " + desencriptado);
		String canalIntegracion = "";
		if(!desencriptado.equals("error")){
			log.error("Parametro desencriptado correctamente :)");
			Gson gson = new Gson();
			IntegrateJSONObject json = gson.fromJson(desencriptado, IntegrateJSONObject.class);
			canalIntegracion = json.getPayment().getTransactionId();
			
			if(canalIntegracion.equals(Constantes.PAGO_DE_DEUDAS_PRIVADO)){
				inte.setProcedencia(Constantes.PAGO_DE_DEUDAS_PRIVADO);
				inte.setFa(json.getPayment().getSearchParameters().getFinancialAccount());
				inte.setDoc(json.getPayment().getSearchParameters().getDocumentType());

			}else{
				inte.setProcedencia(Constantes.PAGO_DE_DEUDAS_PUBLICO);
			}
			inte.setJsonIntegracion(json);
			return inte;
		}
		else{
			log.error("Error al desencriptar el parametro de integracion, por defecto me voy a flujo publico");
			inte.setProcedencia(Constantes.PAGO_DE_DEUDAS_PUBLICO);
			return inte;
		}
	}	
	
	public static String generaIdentificadorUnicoSinDB(ActionResponse actionResponse)throws Exception{
		Random random = new Random();
		int x = random.nextInt(1000);
		String identificador = ""+System.currentTimeMillis()+x;
		log.info("Identificador de session : " + identificador);
		return identificador;
	}
	
	public String obtenerContenidoTexto(HttpServletRequest request , String nombreContenido){		
		String valor = "";		
		try{
			String articleName = nombreContenido;
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), articleName);
			String articleId = journalArticle.getArticleId();
			JournalArticleDisplay articleDisplay = JournalContentUtil.getDisplay(themeDisplay.getScopeGroupId(),articleId, "","",themeDisplay);
			valor = articleDisplay.getContent();
		}
		catch (Exception e){
			valor = "Default";
		}
		return valor;
	}
	
	public static String getFechaActualEnFormatoFront(){
		log.debug("Ingresa metodo formatoFechaHora");
		try{
			String fecha = "Al ";
			Date fechaActual = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", new Locale("es", "ES"));
			fecha += sdf.format(fechaActual);
			String [] fechaArr = fecha.split(" ");
			String mes = fechaArr[2];
			String mesMayuscula = mes.substring(0,1).toUpperCase() + mes.substring(1,mes.length());
			String fechaAux = fechaArr[0] + " "+ fechaArr[1] + " "+mesMayuscula + " " +fechaArr[3];
			log.debug("La fecha generada es :" + fechaAux);
			return fechaAux;
		}
		catch(Exception e){
			String fecha = "Al ";
			Date fechaActual = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", new Locale("es", "ES"));
			fecha += sdf.format(fechaActual);
			log.debug("La fecha generada es :" + fecha);
			return fecha;
		}
	}
	
	public static String fechaVencimientoEnFormatoFront(String fecha){
		String fechaFinal = "";
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//Entrada 
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");//Salida
		try {
			Date fechaVencimiento = sdf1.parse(fecha);
			fechaFinal = sdf2.format(fechaVencimiento);
		}
		catch (ParseException e) {
			log.error("Error al formatear la fecha : " + fecha + " > " + e);
			fechaFinal = fecha;
		}
		return fechaFinal;
	}
	
	public static boolean esDocumentoVencido(String fecha){
		boolean vencido = false;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date hoy = new Date();
			Date fechaVencimiento = formatter.parse(fecha);
			String strHoy = formatter.format(hoy);
			hoy = formatter.parse(strHoy);
			if( fechaVencimiento.before(hoy)){
				log.debug("La fecha de vencimiento del documento es menor a hoy : Documento vencido !");
				vencido = true;
			}
		}  
		catch(ParseException ex){
				log.error("Error al parsear la fecha de vencimiento del documento \n Se espera formato 'yyyy-MM-dd' , fecha recibida : " + fecha );
		}
		return vencido;
	}

	
	public static CarroDePago validaCarroDeudaPrivado(ActionRequest actionRequest ,String carro){
		CarroDePago respuesta = new CarroDePago();
		ArrayList<ObjetoDePago> objPago = new ArrayList<ObjetoDePago>();
		RespuestaFindAccount deudaCuentaPrincipal = SessionUtil.getDetalleDeuda(actionRequest);
		
		String [] objetosAPagar = carro.split(";");
		log.debug("Hay un total de " + objetosAPagar.length + " objetos seleccionados para pagar");
		
		if(objetosAPagar.length < 1){
			respuesta.setValido(false);
			log.debug("El carro privado recibido es inválido : " + carro);
			return respuesta;
		}
		
		//TODO: Validar condiciones de falla y setear ejecutado correctamente como falso en la respuesta
		
		// Si paga solo la deuda de la cuenta principal
		if(objetosAPagar.length == 1 && (objetosAPagar[0].substring(0,1).equals("V") || objetosAPagar[0].substring(0,1).equals("T"))){
			log.debug("Paga solo la cuenta principal T o V");
			String deudaPrincipal = objetosAPagar[0];
			String totalOVencida = deudaPrincipal.substring(0, 1);
			String numeroCuentaAPagar = deudaPrincipal.substring(1, deudaPrincipal.length());
			boolean pagaTotal = true;
			if (totalOVencida.equals("T")){
				log.debug("Paga el total de la deuda");
				pagaTotal = true;
			}
			else if(totalOVencida.equals("V")){
				log.debug("Paga solo la deuda vencida");
				pagaTotal = false;
			}
			
			String montoAPagar = "";
			
			
			Iterator<BillingCustomerFA> it = deudaCuentaPrincipal.getBillingCustomer().iterator();
			while(it.hasNext()){
				BillingCustomerFA bc = it.next();
				Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
				while(it2.hasNext()){
					CuentaFinancieraFA fa = it2.next();
					
					log.debug("Comparando cuenta recibida en carro : " + numeroCuentaAPagar + " con cuenta en session : " + fa.getIdCuentaFinanciera());
					
					if(numeroCuentaAPagar.equals(fa.getIdCuentaFinanciera())){
						
						
						//Agregar objetos de pago !
						
						
						ObjetoDePago obj = new ObjetoDePago();
						obj.setTipoObjeto(ObjetoDePago.CUENTA);
						obj.setIdCuentaFinanciera(numeroCuentaAPagar);
						obj.setSystemID(fa.getSystemID());
						obj.setTokenDePago(fa.getTokenDePago());
						obj.setNombreCliente(fa.getNombreCliente());
						if(pagaTotal){
							montoAPagar = fa.getMontoTotal();
						}
						else{
							montoAPagar = fa.getMontoVencido();
						}
						obj.setMontoAPagar(montoAPagar);
						objPago.add(obj);
						respuesta.setObjetosDePago(objPago);
						respuesta.setValido(true);
						break;
					}
				}
			}
			log.debug("El monto a pagar es : " + montoAPagar);
		}
		// Si paga mas de un item
		else{
			log.debug("Paga mas de un item...");
			
			String cuentaPrincipal = "";
			
			for (int i = 0 ; i < objetosAPagar.length ; i++){
				String aux = objetosAPagar[i];
				
				log.debug("Procesando el objeto " + aux);
				
				if(aux.substring(0, 1).equals("V") || aux.substring(0, 1).equals("T") ){
					log.debug("El objeto es T o V de la cuenta principal");
					cuentaPrincipal = aux;
					String totalOVencida = cuentaPrincipal.substring(0, 1);
					String numeroCuentaAPagar = cuentaPrincipal.substring(1, cuentaPrincipal.length());
					boolean pagaTotal = true;
					if (totalOVencida.equals("T")){
						log.debug("Paga el total de la deuda de la cuenta principal");
						pagaTotal = true;
					}
					else if(totalOVencida.equals("V")){
						log.debug("Paga solo la deuda vencida de la cuenta principal");
						pagaTotal = false;
					}
					String montoAPagar = "";
				
					Iterator<BillingCustomerFA> it = deudaCuentaPrincipal.getBillingCustomer().iterator();
					while(it.hasNext()){
						BillingCustomerFA bc = it.next();
						Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
						while(it2.hasNext()){
							CuentaFinancieraFA fa = it2.next();
							log.debug("Comparando cuenta recibida en carro : " + numeroCuentaAPagar + " con cuenta en session : " + fa.getIdCuentaFinanciera());
							if(numeroCuentaAPagar.equals(fa.getIdCuentaFinanciera())){
								
								ObjetoDePago obj = new ObjetoDePago();
								obj.setTipoObjeto(ObjetoDePago.CUENTA);
								obj.setIdCuentaFinanciera(numeroCuentaAPagar);
								obj.setSystemID(fa.getSystemID());
								obj.setTokenDePago(fa.getTokenDePago());
								obj.setNombreCliente(fa.getNombreCliente());
								if(pagaTotal){
									montoAPagar = fa.getMontoTotal();
								}
								else{
									montoAPagar = fa.getMontoVencido();
								}
								obj.setMontoAPagar(montoAPagar);
								objPago.add(obj);
								respuesta.setValido(true);
								break;
							}
						}
					}
				}
				else{
					log.debug("El objeto no es deuda T o V de cuenta principal");
					if(esCuenta(aux)){
						log.debug("El elemento " + aux + " es cuenta");
						String idCuenta = aux.substring(1, aux.length());
						Iterator<BillingCustomerFA> it = deudaCuentaPrincipal.getBillingCustomer().iterator();
						while(it.hasNext()){
							BillingCustomerFA bc = it.next();
							Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
							while(it2.hasNext()){
								CuentaFinancieraFA fa = it2.next();
								if (fa.getIdCuentaFinanciera().equals(idCuenta)){
									ObjetoDePago obj = new ObjetoDePago();
									obj.setTipoObjeto(ObjetoDePago.CUENTA);
									obj.setIdCuentaFinanciera(idCuenta);
									obj.setSystemID(fa.getSystemID());
									obj.setTokenDePago(fa.getTokenDePago());
									obj.setNombreCliente(fa.getNombreCliente());
									obj.setMontoAPagar(fa.getMontoTotal());
									objPago.add(obj);
									respuesta.setValido(true);
								}
							}
						}
					}
					else if(esDocumento(aux)){
						log.debug("El elemento " + aux + " es documento");
						String idCuenta = aux.substring(aux.indexOf("C")+1, aux.length());
						String idDocumento = aux.substring(1, aux.indexOf("C"));
						log.debug("El ID de la cuenta es " + idCuenta + " y el id de documento es " + idDocumento);

						Iterator<BillingCustomerFA> it = deudaCuentaPrincipal.getBillingCustomer().iterator();
						log.debug("0");
						while(it.hasNext()){
							log.debug("1");
							BillingCustomerFA bc = it.next();
							Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
							while(it2.hasNext()){
								log.debug("2");
								CuentaFinancieraFA fa = it2.next();
								log.debug("cuenta fa1  "+fa.getIdCuentaFinanciera());
								if (fa.getIdCuentaFinanciera().equals(idCuenta)){
									log.debug("cuenta fa2  "+fa.getIdCuentaFinanciera());
									Iterator <DocumentoFA> it3 = fa.getDocumentos().iterator();
									while(it3.hasNext()){
										DocumentoFA doc = it3.next();
										log.debug("documento  "+doc.getIdDocumento());
										if(doc.getIdDocumento().equals(idDocumento)){
											ObjetoDePago obj = new ObjetoDePago();
											obj.setTipoObjeto(ObjetoDePago.DOCUMENTO);
											obj.setIdCuentaFinanciera(idCuenta);
											obj.setIdDocumento(idDocumento);
											obj.setSystemID(doc.getSystemID());
											obj.setTokenDePago(doc.getTokenDePago());
											obj.setNombreCliente(fa.getNombreCliente());
											obj.setMontoAPagar(doc.getMonto());
											objPago.add(obj);
											respuesta.setValido(true);
											break;
										}
									}
								}
							}
						}
					}
					else if(esServicio(aux)){
						log.debug("El elemento " + aux + " es servicio");
						
						int indiceCuenta = aux.indexOf("C");
						int indiceDocumento = aux.indexOf("D");
						int indiceServicio = aux.indexOf("S");
						
						String idCuenta = aux.substring(indiceCuenta + 1 , aux.length());
						String idDocumento = aux.substring(indiceDocumento + 1 , indiceCuenta);
						String idServicio = aux.substring(indiceServicio + 1 , indiceDocumento);
						
						Iterator<BillingCustomerFA> it = deudaCuentaPrincipal.getBillingCustomer().iterator();
						while(it.hasNext()){
							BillingCustomerFA bc = it.next();
							Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
							while(it2.hasNext()){
								CuentaFinancieraFA fa = it2.next();
								if (fa.getIdCuentaFinanciera().equals(idCuenta)){
									Iterator <DocumentoFA> it3 = fa.getDocumentos().iterator();
									while(it3.hasNext()){
										DocumentoFA doc = it3.next();
										if(doc.getIdDocumento().equals(idDocumento)){			
											Iterator<ServicioFA> it4 = doc.getServicios().iterator();									
											while(it4.hasNext()){
												ServicioFA serv = it4.next();
												if ( serv.getIdServicio().equals(idServicio)  ){
													
													ObjetoDePago obj = new ObjetoDePago();
													obj.setTipoObjeto(ObjetoDePago.SERVICIO);
													obj.setIdCuentaFinanciera(idCuenta);
													obj.setIdDocumento(idDocumento);
													obj.setIdServicio(idServicio);
													obj.setSystemID(serv.getSystemID());
													obj.setTokenDePago(serv.getTokenDePago());
													obj.setNombreCliente(fa.getNombreCliente());
													obj.setMontoAPagar(serv.getMonto());
													objPago.add(obj);
													respuesta.setValido(true);
													break;

												}
											}
										}
									}
								}
							}
						}
						log.debug("El ID de la cuenta es " + idCuenta + " y el id de documento es " + idDocumento + " y el id del servicio es " + idServicio);
					}
				}
			}
		}
		respuesta.setObjetosDePago(objPago);
		log.debug("La cantidad de objetos a pagar despues de validar el carro es : " + respuesta.getObjetosDePago().size());
		return respuesta;
	}
	
	private static boolean esCuenta(String elemento){
		return (elemento.indexOf("C") == 0  && elemento.indexOf("D") == -1  && elemento.indexOf("S") == -1) ? true : false;
	}
	
	private static boolean esDocumento(String elemento){
		return (elemento.indexOf("C") > 0  && elemento.indexOf("D") == 0  && elemento.indexOf("S") == -1) ? true : false;
	}
	
	private static boolean esServicio(String elemento){
		log.debug("C : " + elemento.indexOf("C"));
		log.debug("D : " + elemento.indexOf("D"));
		log.debug("S : " + elemento.indexOf("S"));
		return (elemento.indexOf("C") > 0  && elemento.indexOf("D") > 0  && elemento.indexOf("S") == 0) ? true : false ;
	}
	
	public static CarroDePago validaCarroAbono(ActionRequest actionRequest , String cuenta , String monto){
		CarroDePago respuesta = new CarroDePago();
		log.debug("Validando carro de pago abono");		
		log.debug("El monto a pagar es : " + monto + " , la cuenta es : " + cuenta);
		if(monto.trim().length() < 1  || cuenta.trim().length() < 1){
			respuesta.setValido(false);
			return respuesta;
		}
		RespuestaFindAccount resp = SessionUtil.getDetalleDeuda(actionRequest);
		ArrayList<ObjetoDePago> objetosDePago = new ArrayList<ObjetoDePago>();
		Iterator<BillingCustomerFA> it = resp.getBillingCustomer().iterator();
		while(it.hasNext()){
			BillingCustomerFA bc = it.next();
			Iterator<CuentaFinancieraFA> it2 = bc.getCuentasFinancieras().iterator();
			while(it2.hasNext()){
				CuentaFinancieraFA fa = it2.next();
				log.debug("######### El ID de la FA es : " + fa.getIdCuentaFinanciera());
				if(fa.getIdCuentaFinanciera().equals(cuenta)){
					ObjetoDePago obj = new ObjetoDePago();
					obj.setIdCuentaFinanciera(cuenta);
					obj.setMontoAPagar(monto);
					obj.setSystemID(fa.getSystemID());
					obj.setTokenDePago(fa.getTokenDePago());
					obj.setTipoObjeto(ObjetoDePago.ABONO);
					obj.setNombreCliente(fa.getNombreCliente());
					objetosDePago.add(obj);
					break;
				}
			}
		}
		respuesta.setObjetosDePago(objetosDePago);
		if(respuesta.getObjetosDePago().size() < 1){
			respuesta.setValido(false);
		}
		return respuesta;
	}
	
	public static String formateaFechaValizTicketFormatoFront(String fechaServicio){
		
		String fechaFinal = "";
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//Entrada 
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");//Salida
		try {
			Date fechaVencimiento = sdf1.parse(fechaServicio);
			fechaFinal = sdf2.format(fechaVencimiento);
		} 
		catch (ParseException e) {
			log.error("Error al formatear la fecha : " + fechaServicio + " > " + e);
			fechaFinal = fechaServicio;
		}
		return fechaFinal ;
	}
}