package cl.ev.mo.portalcorporate.utiles;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Properties;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import cl.devetel.bpc.encriptacion.Encriptacion;

public class EncriptadorUtil {

	static Log log = LogFactoryUtil.getLog(EncriptadorUtil.class);
	

	public EncriptadorUtil() {

	}

	public static String encripta(String cadena){
		Properties properties = cargarProperties();
		Encriptacion ec = new Encriptacion();
		String encrip = ec.encrip_desenc_canal_tres_des(cadena, ENCRIPTA, properties.getProperty("KEY_1"), 
						properties.getProperty("KEY_2"), properties.getProperty("KEY_3"));
		return encrip;
	}

	public static String desencripta(String cadena){ 
		Properties properties = cargarProperties();
		try{
			Encriptacion ec = new Encriptacion();
			log.debug(properties.getProperty("KEY_1"));
			log.debug(properties.getProperty("KEY_2"));
			log.debug(properties.getProperty("KEY_3"));
			log.debug("cadena: " + cadena);
			String desencrip = ec.encrip_desenc_canal_tres_des(cadena, "des", properties.getProperty("KEY_1"), 
							   properties.getProperty("KEY_2"), properties.getProperty("KEY_3"));			 
			return desencrip;
		}catch(Exception e){
			log.info("Error al ejecutar desencripta");
			return null;
		}
	}
	public static Properties cargarProperties() {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	    InputStream input2 = null;
	    Properties properties = new Properties();
		try {
			input2 = new FileInputStream("/opt/liferay/app/portlet.corporate.pdp.properties");
			properties.clear();
			properties.load(input2);
			KEY_1 = properties.getProperty("KEY_1");
			KEY_2 = properties.getProperty("KEY_2");
			KEY_3 = properties.getProperty("KEY_3");
			log.debug("KEY 1 "+KEY_1);
			log.debug("KEY 2 "+KEY_2);
			log.debug("KEY 3 "+KEY_3);
		} catch (IOException e) {
			log.info("Error al cargar properties"+e);
		}
    	finally{
    		try {
    			if (input2 != null) {
    				input2.close();
    			}
    		} 
    		catch (IOException e) {
    			log.error("Error al cerrar archivo properties "+e);
    		}
    	}
		return properties;
	}
	
	public static String desencriptaBlowfish(byte[] plaintext) {
		SecretKeySpec _key = new SecretKeySpec(keyBlow.getBytes(), "Blowfish");
		plaintext = Base64.decodeBase64(plaintext); 
		if (plaintext.length % 8 != 0) {  
			byte[] padded = new byte[plaintext.length + 8 - (plaintext.length % 8)]; 
			System.arraycopy(plaintext, 0, padded, 0, plaintext.length);
			plaintext = padded;
		}
		try { 
			Cipher cipher = Cipher.getInstance("Blowfish/ECB/NoPadding");  
			cipher.init(Cipher.DECRYPT_MODE, _key); 
			byte[] cipherText = cipher.doFinal(plaintext); 
			String value = new String(cipherText); 
			return value;
		} 
		catch (Exception e) {
			log.error("Error al desencriptar blowfish"+e);
			return null;
		}
	}
	
	public static String encriptaBlowfish(byte[] plaintext) {
		SecretKeySpec _key = new SecretKeySpec(keyBlow.getBytes(), "Blowfish"); 
		if (plaintext.length % 8 != 0) {  
			byte[] padded = new byte[plaintext.length + 8
			    - (plaintext.length % 8)];
				System.arraycopy(plaintext, 0, padded, 0, plaintext.length);
				plaintext = padded;
		}
		try {
			Cipher cipher = Cipher.getInstance("Blowfish/ECB/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, _key);
			byte[] cipherText = cipher.doFinal(plaintext);
			cipherText = Base64.encodeBase64(cipherText);
			String value = new String(cipherText);
			return value;
		} 
		catch (Exception e) {
			log.error("Error al encriptar "+e);
			return null;
		}
	}
	public static void main(String []args){
		
		String asdf = "{\r\n" + 
				"    \"payment\": {\r\n" + 
				"        \"transactionId\": \"30\",   \r\n" + 
				"        \"returnBackURL\": \"www.mo.cl\",\r\n" + 
				"        \"searchParameters\": {\r\n" + 
				"            \"financialAccount\": \"100003765\",\r\n" +
				"            \"billingCustomer\": \"\",\r\n" + 
				"            \"legalInvoiceNumber\": \"16052811-7\",\r\n" + 
				"            \"documentType\": \"\"\r\n" + 
				"        },\r\n" + 
				"        \"orderParameters\": {\r\n" + 
				"            \"orderId\": \"237575\",\r\n" + 
				"            \"financialAccount\": \"100014244\",\r\n" + 
				"            \"totalInmediateCharges\": \"65445\"\r\n" + 
				"        }\r\n" + 
				"    }\r\n" +
				"}";
		
		String mobile= "{\r\n" + 
				"    \"payment\": {\r\n" + 
				"        \"transactionId\": \"37\",\r\n" + 
				"        \"returnBackURL\": \"\",\r\n" + 
				"        \"searchParameters\": {\r\n" + 
				"            \"financialAccount\": \"\",\r\n" + 
				"            \"billingCustomer\": \"\",\r\n" + 
				"            \"legalInvoiceNumber\": \"\",\r\n" + 
				"            \"documentType\": \"\"\r\n" + 
				"        },\r\n" + 
				"        \"orderParameters\": {\r\n" + 
				"            \"orderId\": \"\",\r\n" + 
				"            \"financialAccount\": \"\",\r\n" + 
				"            \"totalInmediateCharges\": \"\"\r\n" + 
				"        },\r\n" + 
				"        \"appIntegration\": {\r\n" + 
				"                \"dni\": \"34567\",\r\n" +  
				"                \"financialAccount\": \"34657\",\r\n" + 
				"                \"msisdn\": \"993243781\",\r\n" + 
				"                \"email\": \"a@a.cl\"\r\n" + 
				"        }\r\n" + 
				"    }\r\n" + 
				"}";
		//mobile= "pony";
		String par = encripta(asdf);
		//System.out.println(par);
		try {
			URLEncoder.encode(par,"UTF-8"); 
			System.out.println(par);
			//System.out.println(desencripta("DJkMDKuctw9OL5Lr0nMXqy78CATD4LaPTh5m5pahBukfeFSvuJByNuFpDdJwNjUY7DMw7U6SoMcOBRxhN+oeiToVoj1R86Ap9RExHja5YqUKRh8JxsakdqiUTh1mnIX2J6iOjk8m0qcyq89h+0uiSmGz3OZnZAGd8dPBbE7OIP8Bs5tTeNq45rDpGXIYMPXWRb5FU0f5sQRVP5QoJq1CcHBv9d58oKSsQ5YE89n+jndX5Cv9tKUjqI3EwdYnkoeja58PeuY3MeGhYYtQDnZytsjbKkwU4joRoqub1PGXrfw3uxTvPi5M8PywWj3oZ+SpJx8pnmo10mf6g/zR+AKju4xQq8F/0K20ryyxhmu2cYRLxrZKiN9o22SSkGExhTDwG3XfXcrzLpP3NDz+MJwTV0OvOWBGU9cjhJEiH8mjxGQ3tsEs/gWZZjESi1XluLFn4RvskEUUC9lbL2FirAleE1cD5o+ZhKL20Yzdg8QuJeBP5x45eA5RZDnv6w0XKDZHFfQkJGGtDN4ccO63pN4uCmcuhQUajvt2KSn2LGhlEXFv76OnI89KEXMSwAD1242QnXjR0T9gwFNxJaRP3IsyNNRdQkLr1C/1IFg+paP5qVJWJCjvmT9o/VoB4/bKIfq5BZKMpkRfz4pb4BmLLz69Vt399iJ01PAeB2rijK9xqBj8lyNJr+LiR6Lf6f3ECrdOoDfDU0RTn+UjxDQ5UzUdxFUii52T6+XLPzrrSlyfaoX8ODprrgKg75gBs/9/Ug6NzFVgc/v5zdMPrehaqFH2RCYJMVf26eLy8WUBnXGQpR1eYSX66z+0IJF8g65eZXNZKsnLvnF2yssNvDJCW/JeG/OlBL3W36PAHJXiA/MuHhw+bArTNWLZfyUy3L2Zl29JK+xxMy9JCxdSEUOAOnQRDKa1qcXotYirnkzUC/OGnBWOd2jZSSD6eGJtqHoXULIL+vmbb5Y2xOx2Otv8m9faZjfog2VBPhmMNeUR5k+gOVn5NGPwUSJOTHx9cjOF7LbU2R561xHbVwC/Tn8ex4Ey+9bQ59MrHynO+0KOsSByVV8x/UQjhq8BLLlv5BDIpEC2gAwvrq4MgXy1QbKHlmpDYa4EYkqdS5LOs9gunPM2PHac2LcMA82x26ZqOF0BNhdNvmYvZ7AZhLRcRjzgNkKd1aDwo8oWczHU1j3pqz1378sAAL3kPmvirD7K9pyfUFyOw3HI9i4CjAoMlb6MewhMJ2qh47w+iyutKCEZXxeIePytfX6T0aiCOoPuwrG1leyhuVIY0FRowv8BP/TULuNzRUALXM+XpfFd/lkQtUVc5zvFtIWEHPU5oh+eKAuiwCjltRYQFsKmRWOgjtVp+q/vZFHYxki7DSP2LhrTtJSGyDUScSjZKyDGsYTZ/QKjtG2TSY6t3NtrpOKl+xtqmXmc5/sZ7S1H4lD6uXhG5dx259eVQaiUughqG2SBq4b1rJ0JD9fJBH8m8MQVX2vO6B//5qoAb8mIUjcIa8kjS94SzH+QWAhiQIVUSJW800aUPd8GrxJyKb654MIWqHQcFtU+2CvzJm7tVi"));
		} 
		catch (UnsupportedEncodingException e) {
			log.error("Error de codificacion  "+e);
		}
		
		
	}
}
