package cl.ev.mo.portalcorporate.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class BillingCustomerFA implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<CuentaFinancieraFA> cuentasFinancieras;
	
	
	public BillingCustomerFA(){
		cuentasFinancieras = new ArrayList<CuentaFinancieraFA>();
	}


	public ArrayList<CuentaFinancieraFA> getCuentasFinancieras() {
		return cuentasFinancieras;
	}


	public void setCuentasFinancieras(ArrayList<CuentaFinancieraFA> cuentasFinancieras) {
		this.cuentasFinancieras = cuentasFinancieras;
	}
	
	
	
}
