package cl.ev.mo.portalcorporate.beans;

public class IntegrateJSONObject {

	private Payment payment;

	public IntegrateJSONObject(){
		
	}
	
	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	
	public class Payment{
		
		String transactionId;
		String returnBackURL;
		SearchParameters searchParameters;
		OrderParameters orderParameters;
		AppIntegration appIntegration;
		
		public Payment(){
			
		}
		
		public String getTransactionId() {
			return transactionId;
		}
		public void setTransactionId(String transactionId) {
			this.transactionId = transactionId;
		}
		public String getReturnBackURL() {
			return returnBackURL;
		}
		public void setReturnBackURL(String returnBackURL) {
			this.returnBackURL = returnBackURL;
		}
		public SearchParameters getSearchParameters() {
			return searchParameters;
		}
		public void setSearchParameters(SearchParameters searchParameters) {
			this.searchParameters = searchParameters;
		}
		public OrderParameters getOrderParameters() {
			return orderParameters;
		}
		public void setOrderParameters(OrderParameters orderParameters) {
			this.orderParameters = orderParameters;
		}
		
		public AppIntegration getAppIntegration(){
			return appIntegration;
		}
		public void setAppIntegration(AppIntegration appIntegration){
			this.appIntegration = appIntegration;
		}
		
		
		public class SearchParameters{		
			String financialAccount;
			String billingCustomer;
			String legalInvoiceNumber;
			String documentType;
			
			public SearchParameters(){
				
			}
			
			public String getFinancialAccount() {
				return financialAccount;
			}
			public void setFinancialAccount(String financialAccount) {
				this.financialAccount = financialAccount;
			}
			public String getBillingCustomer() {
				return billingCustomer;
			}
			public void setBillingCustomer(String billingCustomer) {
				this.billingCustomer = billingCustomer;
			}
			public String getLegalInvoiceNumber() {
				return legalInvoiceNumber;
			}
			public void setLegalInvoiceNumber(String legalInvoiceNumber) {
				this.legalInvoiceNumber = legalInvoiceNumber;
			}
			public String getDocumentType() {
				return documentType;
			}
			public void setDocumentType(String documentType) {
				this.documentType = documentType;
			}
		}
		public class OrderParameters{
			String orderId;
			String financialAccount;
			String totalInmediateCharges;
			
			public OrderParameters(){
				
			}
			
			public String getOrderId() {
				return orderId;
			}
			public void setOrderId(String orderId) {
				this.orderId = orderId;
			}
			public String getFinancialAccount() {
				return financialAccount;
			}
			public void setFinancialAccount(String financialAccount) {
				this.financialAccount = financialAccount;
			}
			public String getTotalInmediateCharges() {
				return totalInmediateCharges;
			}
			public void setTotalInmediateCharges(String totalInmediateCharges) {
				this.totalInmediateCharges = totalInmediateCharges;
			}
		}
		
		
		public class AppIntegration{
			
			String dni;
			String financialAccount;
			String msisdn;
			String email;
			
			public AppIntegration(){}
			
			
			public String getDni() {
				return dni;
			}
			public void setDni(String dni) {
				this.dni = dni;
			}
			public String getFinancialAccount() {
				return financialAccount;
			}
			public void setFinancialAccount(String financialAccount) {
				this.financialAccount = financialAccount;
			}
			public String getMsisdn() {
				return msisdn;
			}
			public void setMsisdn(String msisdn) {
				this.msisdn = msisdn;
			}
			public String getEmail() {
				return email;
			}
			public void setEmail(String email) {
				this.email = email;
			}
		}
	}
	
}

//
//{
//    "payment": {
//        "transactionId": "31",
//         "returnBackURL": "MCSS_ORDER_SUMMARY_URL",
//        "searchParameters": {
//            "financialAccount": "",
//            "billingCustomer": "",
//            "legalInvoiceNumber": "",
//            "documentType": ""
//        },
//        "orderParameters": {
//            "orderId": "34567",
//            "financialAccount": "34657",
//            "totalInmediateCharges": "23900"
//        }
//        "appIntegration"{
//        	"":"",
//        	"":"",
//        	"":"",
//        	"":""
//        }
//    }
//}