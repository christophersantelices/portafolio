package cl.ev.mo.portalcorporate.serviciosHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderRequest;

public class UtilesClientesWS {

	Log log = LogFactoryUtil.getLog(UtilesClientesWS.class);

	protected TEFHeaderRequest fillCreateTicketHeader(TEFHeaderRequest _findAccountDebts_findAccountDebtsRequestHeader){

		try{
			_findAccountDebts_findAccountDebtsRequestHeader.setUserLogin("fpagos");
			_findAccountDebts_findAccountDebtsRequestHeader.setServiceChannel("Otros");
			_findAccountDebts_findAccountDebtsRequestHeader.setSessionCode("123");
			_findAccountDebts_findAccountDebtsRequestHeader.setApplication("Front de pagos COL");
			_findAccountDebts_findAccountDebtsRequestHeader.setIdMessage("1234");
			_findAccountDebts_findAccountDebtsRequestHeader.setIpAddress("1.1.1.1");
			_findAccountDebts_findAccountDebtsRequestHeader.setFunctionalityCode("functionalityCode");
			_findAccountDebts_findAccountDebtsRequestHeader.setTransactionTimestamp(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(getTransactionTimestamp()));
			_findAccountDebts_findAccountDebtsRequestHeader.setServiceName("CreateTicket");
			_findAccountDebts_findAccountDebtsRequestHeader.setVersion("1.0");
			return _findAccountDebts_findAccountDebtsRequestHeader;
		}
		catch(Exception e){
			log.error("Error al llenar la cabecera de findAccount >> " + e);
			return null;
		}
	}

	protected String getTransactionTimestamp(){
		
		//TODO: se debe pasar el valor de este metodo a la cabecera??
		String completo = "";
		try{
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//Entrada 
			SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");//Entrada 
			Date hoy = new Date();
			completo = sdf1.format(hoy) + "T" + sdf2.format(hoy);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return completo;
	}
	
	protected TEFHeaderRequest fillCreateDebtPaymentHeader(TEFHeaderRequest _findAccountDebts_findAccountDebtsRequestHeader){

		try{
			_findAccountDebts_findAccountDebtsRequestHeader.setUserLogin("fpagos");
			_findAccountDebts_findAccountDebtsRequestHeader.setServiceChannel("Otros");
			_findAccountDebts_findAccountDebtsRequestHeader.setSessionCode("123");
			_findAccountDebts_findAccountDebtsRequestHeader.setApplication("Front de pagos COL");
			_findAccountDebts_findAccountDebtsRequestHeader.setIdMessage("1234");
			_findAccountDebts_findAccountDebtsRequestHeader.setIpAddress("1.1.1.1");
			_findAccountDebts_findAccountDebtsRequestHeader.setFunctionalityCode("functionalityCode");
			_findAccountDebts_findAccountDebtsRequestHeader.setTransactionTimestamp(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(getTransactionTimestamp()));
			_findAccountDebts_findAccountDebtsRequestHeader.setServiceName("CreatePayment");
			_findAccountDebts_findAccountDebtsRequestHeader.setVersion("1.0");
			return _findAccountDebts_findAccountDebtsRequestHeader;
		}
		catch(Exception e){
			log.error("Error al llenar la cabecera de createDebtPayment >> " + e);
			return null;
		}
	}
	protected static String getOperationDate(){

		String completo = "";
		try{
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");//Entrada 
			SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");//Entrada 
			Date hoy = new Date();
			completo = sdf1.format(hoy) + "T" + sdf2.format(hoy) +".340-03:00";
		}
		catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("getOperationDate "+completo);
		return completo;
	}
	
	protected TEFHeaderRequest fillSendLegacyEventHeader(TEFHeaderRequest _sendLegacyEvent_sendLegacyEventRequestHeader){
		
		try{
			 	_sendLegacyEvent_sendLegacyEventRequestHeader.setUserLogin("internal");
		        _sendLegacyEvent_sendLegacyEventRequestHeader.setServiceChannel("OSB");
		        _sendLegacyEvent_sendLegacyEventRequestHeader.setSessionCode("0");
		        _sendLegacyEvent_sendLegacyEventRequestHeader.setApplication("FRONT DE PAGOS");
		        _sendLegacyEvent_sendLegacyEventRequestHeader.setIdMessage("local");
		        _sendLegacyEvent_sendLegacyEventRequestHeader.setIpAddress("127.0.0.1");
		        _sendLegacyEvent_sendLegacyEventRequestHeader.setFunctionalityCode("CreateSubscription");
		        _sendLegacyEvent_sendLegacyEventRequestHeader.setTransactionTimestamp(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(getTransactionTimestamp()));
                _sendLegacyEvent_sendLegacyEventRequestHeader.setServiceName("SendLegacyEvent");
		        _sendLegacyEvent_sendLegacyEventRequestHeader.setVersion("1.0.0");
		        return _sendLegacyEvent_sendLegacyEventRequestHeader;
		}
		catch(Exception e){
			log.error("Error al llenar la cabecera de Send legacy event >> " + e);
			return null;
		}
		
	}
	
}
