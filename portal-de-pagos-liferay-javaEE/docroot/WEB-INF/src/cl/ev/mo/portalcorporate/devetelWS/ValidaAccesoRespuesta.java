
package cl.ev.mo.portalcorporate.devetelWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para validaAccesoRespuesta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="validaAccesoRespuesta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AUTENTIFICACION" type="{http://ws/}autentificacionResponse" minOccurs="0"/&gt;
 *         &lt;element name="MEDIOS_PAGO_LIST" type="{http://ws/}mediosPagosList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validaAccesoRespuesta", propOrder = {
    "autentificacion",
    "mediospagolist"
})
public class ValidaAccesoRespuesta {

    @XmlElement(name = "AUTENTIFICACION")
    protected AutentificacionResponse autentificacion;
    @XmlElement(name = "MEDIOS_PAGO_LIST")
    protected MediosPagosList mediospagolist;

    /**
     * Obtiene el valor de la propiedad autentificacion.
     * 
     * @return
     *     possible object is
     *     {@link AutentificacionResponse }
     *     
     */
    public AutentificacionResponse getAUTENTIFICACION() {
        return autentificacion;
    }

    /**
     * Define el valor de la propiedad autentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link AutentificacionResponse }
     *     
     */
    public void setAUTENTIFICACION(AutentificacionResponse value) {
        this.autentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad mediospagolist.
     * 
     * @return
     *     possible object is
     *     {@link MediosPagosList }
     *     
     */
    public MediosPagosList getMEDIOSPAGOLIST() {
        return mediospagolist;
    }

    /**
     * Define el valor de la propiedad mediospagolist.
     * 
     * @param value
     *     allowed object is
     *     {@link MediosPagosList }
     *     
     */
    public void setMEDIOSPAGOLIST(MediosPagosList value) {
        this.mediospagolist = value;
    }

}
