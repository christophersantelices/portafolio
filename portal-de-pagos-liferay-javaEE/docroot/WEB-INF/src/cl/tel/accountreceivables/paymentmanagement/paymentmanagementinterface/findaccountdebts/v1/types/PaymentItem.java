
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PaymentItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PaymentItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="24"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="name" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="30"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="paymentToken" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="250"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="paymentSystemID" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="24"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="isEditable" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="printLineList" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}LineList" minOccurs="0"/&gt;
 *         &lt;element name="displayLineList" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}LineList" minOccurs="0"/&gt;
 *         &lt;element name="customerPayment" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}CustomerPayment"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentItem", propOrder = {
    "id",
    "name",
    "paymentToken",
    "paymentSystemID",
    "isEditable",
    "printLineList",
    "displayLineList",
    "customerPayment"
})
public class PaymentItem {

    @XmlElementRef(name = "ID", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> id;
    @XmlElementRef(name = "name", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "paymentToken", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentToken;
    @XmlElementRef(name = "paymentSystemID", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paymentSystemID;
    @XmlElementRef(name = "isEditable", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> isEditable;
    @XmlElementRef(name = "printLineList", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<LineList> printLineList;
    @XmlElementRef(name = "displayLineList", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<LineList> displayLineList;
    @XmlElement(required = true)
    protected CustomerPayment customerPayment;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getID() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setID(JAXBElement<String> value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentToken.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentToken() {
        return paymentToken;
    }

    /**
     * Define el valor de la propiedad paymentToken.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentToken(JAXBElement<String> value) {
        this.paymentToken = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentSystemID.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentSystemID() {
        return paymentSystemID;
    }

    /**
     * Define el valor de la propiedad paymentSystemID.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentSystemID(JAXBElement<String> value) {
        this.paymentSystemID = value;
    }

    /**
     * Obtiene el valor de la propiedad isEditable.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIsEditable() {
        return isEditable;
    }

    /**
     * Define el valor de la propiedad isEditable.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIsEditable(JAXBElement<String> value) {
        this.isEditable = value;
    }

    /**
     * Obtiene el valor de la propiedad printLineList.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LineList }{@code >}
     *     
     */
    public JAXBElement<LineList> getPrintLineList() {
        return printLineList;
    }

    /**
     * Define el valor de la propiedad printLineList.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LineList }{@code >}
     *     
     */
    public void setPrintLineList(JAXBElement<LineList> value) {
        this.printLineList = value;
    }

    /**
     * Obtiene el valor de la propiedad displayLineList.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LineList }{@code >}
     *     
     */
    public JAXBElement<LineList> getDisplayLineList() {
        return displayLineList;
    }

    /**
     * Define el valor de la propiedad displayLineList.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LineList }{@code >}
     *     
     */
    public void setDisplayLineList(JAXBElement<LineList> value) {
        this.displayLineList = value;
    }

    /**
     * Obtiene el valor de la propiedad customerPayment.
     * 
     * @return
     *     possible object is
     *     {@link CustomerPayment }
     *     
     */
    public CustomerPayment getCustomerPayment() {
        return customerPayment;
    }

    /**
     * Define el valor de la propiedad customerPayment.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerPayment }
     *     
     */
    public void setCustomerPayment(CustomerPayment value) {
        this.customerPayment = value;
    }

}
