
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.w3.org/2001/XMLSchema" xmlns:impl=" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tef=" /EnterpriseApplicationIntegration/TEFHeader/V1" xmlns:tns=" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;Extends: SID Models::Customer Domain::Customer Bill ABE::CustomerBill&lt;/p&gt;
 * </pre>
 * 
 * 
 * <p>Clase Java para CreatableTicket complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CreatableTicket"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="marketType" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="customerAccount" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}CreatableCustomerAccount" minOccurs="0"/&gt;
 *         &lt;element name="billAmount" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}Money"/&gt;
 *         &lt;element name="invoiceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="invoiceDueDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="invoiceType" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="3"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="documentFormat" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="invoiceNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="service" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}CreatableService" minOccurs="0"/&gt;
 *         &lt;element name="orderID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="token" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="250"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreatableTicket", propOrder = {
    "marketType",
    "customerAccount",
    "billAmount",
    "invoiceID",
    "invoiceDueDate",
    "invoiceType",
    "documentFormat",
    "invoiceNumber",
    "service",
    "orderID",
    "token"
})
public class CreatableTicket {

    @XmlElementRef(name = "marketType", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> marketType;
    @XmlElementRef(name = "customerAccount", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<CreatableCustomerAccount> customerAccount;
    @XmlElement(required = true)
    protected Money billAmount;
    @XmlElementRef(name = "invoiceID", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> invoiceID;
    @XmlElementRef(name = "invoiceDueDate", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> invoiceDueDate;
    @XmlElementRef(name = "invoiceType", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> invoiceType;
    @XmlElementRef(name = "documentFormat", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> documentFormat;
    @XmlElementRef(name = "invoiceNumber", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> invoiceNumber;
    @XmlElementRef(name = "service", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<CreatableService> service;
    @XmlElementRef(name = "orderID", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> orderID;
    @XmlElementRef(name = "token", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> token;

    /**
     * Obtiene el valor de la propiedad marketType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getMarketType() {
        return marketType;
    }

    /**
     * Define el valor de la propiedad marketType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setMarketType(JAXBElement<Long> value) {
        this.marketType = value;
    }

    /**
     * Obtiene el valor de la propiedad customerAccount.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreatableCustomerAccount }{@code >}
     *     
     */
    public JAXBElement<CreatableCustomerAccount> getCustomerAccount() {
        return customerAccount;
    }

    /**
     * Define el valor de la propiedad customerAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreatableCustomerAccount }{@code >}
     *     
     */
    public void setCustomerAccount(JAXBElement<CreatableCustomerAccount> value) {
        this.customerAccount = value;
    }

    /**
     * Obtiene el valor de la propiedad billAmount.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getBillAmount() {
        return billAmount;
    }

    /**
     * Define el valor de la propiedad billAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setBillAmount(Money value) {
        this.billAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceID.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getInvoiceID() {
        return invoiceID;
    }

    /**
     * Define el valor de la propiedad invoiceID.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setInvoiceID(JAXBElement<Long> value) {
        this.invoiceID = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceDueDate.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getInvoiceDueDate() {
        return invoiceDueDate;
    }

    /**
     * Define el valor de la propiedad invoiceDueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setInvoiceDueDate(JAXBElement<XMLGregorianCalendar> value) {
        this.invoiceDueDate = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInvoiceType() {
        return invoiceType;
    }

    /**
     * Define el valor de la propiedad invoiceType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInvoiceType(JAXBElement<String> value) {
        this.invoiceType = value;
    }

    /**
     * Obtiene el valor de la propiedad documentFormat.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocumentFormat() {
        return documentFormat;
    }

    /**
     * Define el valor de la propiedad documentFormat.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocumentFormat(JAXBElement<String> value) {
        this.documentFormat = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Define el valor de la propiedad invoiceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setInvoiceNumber(JAXBElement<Long> value) {
        this.invoiceNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad service.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreatableService }{@code >}
     *     
     */
    public JAXBElement<CreatableService> getService() {
        return service;
    }

    /**
     * Define el valor de la propiedad service.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreatableService }{@code >}
     *     
     */
    public void setService(JAXBElement<CreatableService> value) {
        this.service = value;
    }

    /**
     * Obtiene el valor de la propiedad orderID.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getOrderID() {
        return orderID;
    }

    /**
     * Define el valor de la propiedad orderID.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setOrderID(JAXBElement<Long> value) {
        this.orderID = value;
    }

    /**
     * Obtiene el valor de la propiedad token.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getToken() {
        return token;
    }

    /**
     * Define el valor de la propiedad token.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setToken(JAXBElement<String> value) {
        this.token = value;
    }

}
