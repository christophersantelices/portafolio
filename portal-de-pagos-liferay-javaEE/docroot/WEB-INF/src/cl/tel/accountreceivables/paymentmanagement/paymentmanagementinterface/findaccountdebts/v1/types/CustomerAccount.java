
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CustomerAccount complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CustomerAccount"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerAccountID" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="24"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="accountStatus" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="16"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="customerInformation" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}IndividualNamesResponse" minOccurs="0"/&gt;
 *         &lt;element name="isDefaulter" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="pacPatIndicator" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="servicePaymentIndicator" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="customerType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="customerSubType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="3"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="accountType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="paymentArrangementIndicator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="actualBill" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}CustomerBillList" minOccurs="0"/&gt;
 *         &lt;element name="previousBill" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}CustomerBillList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerAccount", propOrder = {
    "customerAccountID",
    "accountStatus",
    "customerInformation",
    "isDefaulter",
    "pacPatIndicator",
    "servicePaymentIndicator",
    "customerType",
    "customerSubType",
    "accountType",
    "paymentArrangementIndicator",
    "actualBill",
    "previousBill"
})
public class CustomerAccount {

    @XmlElementRef(name = "customerAccountID", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerAccountID;
    @XmlElementRef(name = "accountStatus", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accountStatus;
    @XmlElementRef(name = "customerInformation", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<IndividualNamesResponse> customerInformation;
    @XmlElementRef(name = "isDefaulter", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> isDefaulter;
    @XmlElementRef(name = "pacPatIndicator", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pacPatIndicator;
    @XmlElementRef(name = "servicePaymentIndicator", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servicePaymentIndicator;
    @XmlElement(required = true)
    protected String customerType;
    @XmlElement(required = true)
    protected String customerSubType;
    @XmlElement(required = true)
    protected String accountType;
    @XmlElement(required = true)
    protected String paymentArrangementIndicator;
    @XmlElementRef(name = "actualBill", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerBillList> actualBill;
    @XmlElementRef(name = "previousBill", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerBillList> previousBill;

    /**
     * Obtiene el valor de la propiedad customerAccountID.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerAccountID() {
        return customerAccountID;
    }

    /**
     * Define el valor de la propiedad customerAccountID.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerAccountID(JAXBElement<String> value) {
        this.customerAccountID = value;
    }

    /**
     * Obtiene el valor de la propiedad accountStatus.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccountStatus() {
        return accountStatus;
    }

    /**
     * Define el valor de la propiedad accountStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccountStatus(JAXBElement<String> value) {
        this.accountStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad customerInformation.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link IndividualNamesResponse }{@code >}
     *     
     */
    public JAXBElement<IndividualNamesResponse> getCustomerInformation() {
        return customerInformation;
    }

    /**
     * Define el valor de la propiedad customerInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link IndividualNamesResponse }{@code >}
     *     
     */
    public void setCustomerInformation(JAXBElement<IndividualNamesResponse> value) {
        this.customerInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad isDefaulter.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIsDefaulter() {
        return isDefaulter;
    }

    /**
     * Define el valor de la propiedad isDefaulter.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIsDefaulter(JAXBElement<String> value) {
        this.isDefaulter = value;
    }

    /**
     * Obtiene el valor de la propiedad pacPatIndicator.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPacPatIndicator() {
        return pacPatIndicator;
    }

    /**
     * Define el valor de la propiedad pacPatIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPacPatIndicator(JAXBElement<String> value) {
        this.pacPatIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad servicePaymentIndicator.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServicePaymentIndicator() {
        return servicePaymentIndicator;
    }

    /**
     * Define el valor de la propiedad servicePaymentIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServicePaymentIndicator(JAXBElement<String> value) {
        this.servicePaymentIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad customerType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerType() {
        return customerType;
    }

    /**
     * Define el valor de la propiedad customerType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerType(String value) {
        this.customerType = value;
    }

    /**
     * Obtiene el valor de la propiedad customerSubType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerSubType() {
        return customerSubType;
    }

    /**
     * Define el valor de la propiedad customerSubType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerSubType(String value) {
        this.customerSubType = value;
    }

    /**
     * Obtiene el valor de la propiedad accountType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Define el valor de la propiedad accountType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentArrangementIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentArrangementIndicator() {
        return paymentArrangementIndicator;
    }

    /**
     * Define el valor de la propiedad paymentArrangementIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentArrangementIndicator(String value) {
        this.paymentArrangementIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad actualBill.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerBillList }{@code >}
     *     
     */
    public JAXBElement<CustomerBillList> getActualBill() {
        return actualBill;
    }

    /**
     * Define el valor de la propiedad actualBill.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerBillList }{@code >}
     *     
     */
    public void setActualBill(JAXBElement<CustomerBillList> value) {
        this.actualBill = value;
    }

    /**
     * Obtiene el valor de la propiedad previousBill.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerBillList }{@code >}
     *     
     */
    public JAXBElement<CustomerBillList> getPreviousBill() {
        return previousBill;
    }

    /**
     * Define el valor de la propiedad previousBill.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerBillList }{@code >}
     *     
     */
    public void setPreviousBill(JAXBElement<CustomerBillList> value) {
        this.previousBill = value;
    }

}
