
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CustomerBill complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CustomerBill"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="billingInvoiceNumber" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="24"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="externalLegalNumber" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="16"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="documentType" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="18"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="documentTypeInd" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="18"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="billStatus" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="16"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="billNo" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="24"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="billSpec" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}CustomerBillSpec" minOccurs="0"/&gt;
 *         &lt;element name="paymentItemList" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}PaymentItemList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerBill", propOrder = {
    "billingInvoiceNumber",
    "externalLegalNumber",
    "documentType",
    "documentTypeInd",
    "billStatus",
    "billNo",
    "billSpec",
    "paymentItemList"
})
public class CustomerBill {

    @XmlElementRef(name = "billingInvoiceNumber", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> billingInvoiceNumber;
    @XmlElementRef(name = "externalLegalNumber", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalLegalNumber;
    @XmlElementRef(name = "documentType", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> documentType;
    @XmlElementRef(name = "documentTypeInd", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> documentTypeInd;
    @XmlElementRef(name = "billStatus", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> billStatus;
    @XmlElementRef(name = "billNo", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> billNo;
    @XmlElementRef(name = "billSpec", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerBillSpec> billSpec;
    @XmlElementRef(name = "paymentItemList", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<PaymentItemList> paymentItemList;

    /**
     * Obtiene el valor de la propiedad billingInvoiceNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBillingInvoiceNumber() {
        return billingInvoiceNumber;
    }

    /**
     * Define el valor de la propiedad billingInvoiceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBillingInvoiceNumber(JAXBElement<String> value) {
        this.billingInvoiceNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad externalLegalNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalLegalNumber() {
        return externalLegalNumber;
    }

    /**
     * Define el valor de la propiedad externalLegalNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalLegalNumber(JAXBElement<String> value) {
        this.externalLegalNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad documentType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocumentType() {
        return documentType;
    }

    /**
     * Define el valor de la propiedad documentType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocumentType(JAXBElement<String> value) {
        this.documentType = value;
    }

    /**
     * Obtiene el valor de la propiedad documentTypeInd.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocumentTypeInd() {
        return documentTypeInd;
    }

    /**
     * Define el valor de la propiedad documentTypeInd.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocumentTypeInd(JAXBElement<String> value) {
        this.documentTypeInd = value;
    }

    /**
     * Obtiene el valor de la propiedad billStatus.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBillStatus() {
        return billStatus;
    }

    /**
     * Define el valor de la propiedad billStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBillStatus(JAXBElement<String> value) {
        this.billStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad billNo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBillNo() {
        return billNo;
    }

    /**
     * Define el valor de la propiedad billNo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBillNo(JAXBElement<String> value) {
        this.billNo = value;
    }

    /**
     * Obtiene el valor de la propiedad billSpec.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerBillSpec }{@code >}
     *     
     */
    public JAXBElement<CustomerBillSpec> getBillSpec() {
        return billSpec;
    }

    /**
     * Define el valor de la propiedad billSpec.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerBillSpec }{@code >}
     *     
     */
    public void setBillSpec(JAXBElement<CustomerBillSpec> value) {
        this.billSpec = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentItemList.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PaymentItemList }{@code >}
     *     
     */
    public JAXBElement<PaymentItemList> getPaymentItemList() {
        return paymentItemList;
    }

    /**
     * Define el valor de la propiedad paymentItemList.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PaymentItemList }{@code >}
     *     
     */
    public void setPaymentItemList(JAXBElement<PaymentItemList> value) {
        this.paymentItemList = value;
    }

}
