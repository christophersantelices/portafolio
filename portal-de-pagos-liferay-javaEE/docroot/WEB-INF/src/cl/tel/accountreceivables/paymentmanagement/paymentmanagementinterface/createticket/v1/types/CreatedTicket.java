
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber;


/**
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.w3.org/2001/XMLSchema" xmlns:impl=" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tef=" /EnterpriseApplicationIntegration/TEFHeader/V1" xmlns:tns=" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;Extends: SID Models::Customer Domain::Customer Bill ABE::CustomerBill&lt;/p&gt;
 * </pre>
 * 
 * 
 * <p>Clase Java para CreatedTicket complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CreatedTicket"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ticketID" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="token"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="250"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="billAmount" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}Money"/&gt;
 *         &lt;element name="invoiceDueDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="ticketStatus"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="customerIdentification" type="{ /EnterpriseApplicationIntegration/TEFHeader/V1}TEFChileanIdentificationNumber"/&gt;
 *         &lt;element name="customerName" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}CreatedIndividualName"/&gt;
 *         &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="lineList" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}LineList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreatedTicket", propOrder = {
    "ticketID",
    "token",
    "billAmount",
    "invoiceDueDate",
    "ticketStatus",
    "customerIdentification",
    "customerName",
    "creationDate",
    "lineList"
})
public class CreatedTicket {

    protected long ticketID;
    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected Money billAmount;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar invoiceDueDate;
    @XmlElement(required = true)
    protected String ticketStatus;
    @XmlElement(required = true)
    protected TEFChileanIdentificationNumber customerIdentification;
    @XmlElement(required = true)
    protected CreatedIndividualName customerName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar creationDate;
    @XmlElement(required = true)
    protected LineList lineList;

    /**
     * Obtiene el valor de la propiedad ticketID.
     * 
     */
    public long getTicketID() {
        return ticketID;
    }

    /**
     * Define el valor de la propiedad ticketID.
     * 
     */
    public void setTicketID(long value) {
        this.ticketID = value;
    }

    /**
     * Obtiene el valor de la propiedad token.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Define el valor de la propiedad token.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Obtiene el valor de la propiedad billAmount.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getBillAmount() {
        return billAmount;
    }

    /**
     * Define el valor de la propiedad billAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setBillAmount(Money value) {
        this.billAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceDueDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvoiceDueDate() {
        return invoiceDueDate;
    }

    /**
     * Define el valor de la propiedad invoiceDueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvoiceDueDate(XMLGregorianCalendar value) {
        this.invoiceDueDate = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketStatus() {
        return ticketStatus;
    }

    /**
     * Define el valor de la propiedad ticketStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketStatus(String value) {
        this.ticketStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad customerIdentification.
     * 
     * @return
     *     possible object is
     *     {@link TEFChileanIdentificationNumber }
     *     
     */
    public TEFChileanIdentificationNumber getCustomerIdentification() {
        return customerIdentification;
    }

    /**
     * Define el valor de la propiedad customerIdentification.
     * 
     * @param value
     *     allowed object is
     *     {@link TEFChileanIdentificationNumber }
     *     
     */
    public void setCustomerIdentification(TEFChileanIdentificationNumber value) {
        this.customerIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad customerName.
     * 
     * @return
     *     possible object is
     *     {@link CreatedIndividualName }
     *     
     */
    public CreatedIndividualName getCustomerName() {
        return customerName;
    }

    /**
     * Define el valor de la propiedad customerName.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatedIndividualName }
     *     
     */
    public void setCustomerName(CreatedIndividualName value) {
        this.customerName = value;
    }

    /**
     * Obtiene el valor de la propiedad creationDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Define el valor de la propiedad creationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad lineList.
     * 
     * @return
     *     possible object is
     *     {@link LineList }
     *     
     */
    public LineList getLineList() {
        return lineList;
    }

    /**
     * Define el valor de la propiedad lineList.
     * 
     * @param value
     *     allowed object is
     *     {@link LineList }
     *     
     */
    public void setLineList(LineList value) {
        this.lineList = value;
    }

}
