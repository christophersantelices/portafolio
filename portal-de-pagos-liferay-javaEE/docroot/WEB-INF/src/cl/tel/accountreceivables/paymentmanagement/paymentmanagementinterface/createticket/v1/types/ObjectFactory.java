
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreatableTicketMarketType_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "marketType");
    private final static QName _CreatableTicketCustomerAccount_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "customerAccount");
    private final static QName _CreatableTicketInvoiceID_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "invoiceID");
    private final static QName _CreatableTicketInvoiceDueDate_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "invoiceDueDate");
    private final static QName _CreatableTicketInvoiceType_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "invoiceType");
    private final static QName _CreatableTicketDocumentFormat_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "documentFormat");
    private final static QName _CreatableTicketInvoiceNumber_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "invoiceNumber");
    private final static QName _CreatableTicketService_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "service");
    private final static QName _CreatableTicketOrderID_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "orderID");
    private final static QName _CreatableTicketToken_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", "token");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateTicketRequest }
     * 
     */
    public CreateTicketRequest createCreateTicketRequest() {
        return new CreateTicketRequest();
    }

    /**
     * Create an instance of {@link CreateTicketRequestData }
     * 
     */
    public CreateTicketRequestData createCreateTicketRequestData() {
        return new CreateTicketRequestData();
    }

    /**
     * Create an instance of {@link CreateTicketResponse }
     * 
     */
    public CreateTicketResponse createCreateTicketResponse() {
        return new CreateTicketResponse();
    }

    /**
     * Create an instance of {@link CreateTicketResponseData }
     * 
     */
    public CreateTicketResponseData createCreateTicketResponseData() {
        return new CreateTicketResponseData();
    }

    /**
     * Create an instance of {@link CreateTicketFault }
     * 
     */
    public CreateTicketFault createCreateTicketFault() {
        return new CreateTicketFault();
    }

    /**
     * Create an instance of {@link CreatableContext }
     * 
     */
    public CreatableContext createCreatableContext() {
        return new CreatableContext();
    }

    /**
     * Create an instance of {@link CreatableTicketList }
     * 
     */
    public CreatableTicketList createCreatableTicketList() {
        return new CreatableTicketList();
    }

    /**
     * Create an instance of {@link CreatableTicket }
     * 
     */
    public CreatableTicket createCreatableTicket() {
        return new CreatableTicket();
    }

    /**
     * Create an instance of {@link CreatableCustomerAccount }
     * 
     */
    public CreatableCustomerAccount createCreatableCustomerAccount() {
        return new CreatableCustomerAccount();
    }

    /**
     * Create an instance of {@link CreatableService }
     * 
     */
    public CreatableService createCreatableService() {
        return new CreatableService();
    }

    /**
     * Create an instance of {@link CreatedTicket }
     * 
     */
    public CreatedTicket createCreatedTicket() {
        return new CreatedTicket();
    }

    /**
     * Create an instance of {@link CreatedIndividualName }
     * 
     */
    public CreatedIndividualName createCreatedIndividualName() {
        return new CreatedIndividualName();
    }

    /**
     * Create an instance of {@link LineList }
     * 
     */
    public LineList createLineList() {
        return new LineList();
    }

    /**
     * Create an instance of {@link ThirdPartyCollector }
     * 
     */
    public ThirdPartyCollector createThirdPartyCollector() {
        return new ThirdPartyCollector();
    }

    /**
     * Create an instance of {@link Money }
     * 
     */
    public Money createMoney() {
        return new Money();
    }

    /**
     * Create an instance of {@link StatusAbstract }
     * 
     */
    public StatusAbstract createStatusAbstract() {
        return new StatusAbstract();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "marketType", scope = CreatableTicket.class)
    public JAXBElement<Long> createCreatableTicketMarketType(Long value) {
        return new JAXBElement<Long>(_CreatableTicketMarketType_QNAME, Long.class, CreatableTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatableCustomerAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "customerAccount", scope = CreatableTicket.class)
    public JAXBElement<CreatableCustomerAccount> createCreatableTicketCustomerAccount(CreatableCustomerAccount value) {
        return new JAXBElement<CreatableCustomerAccount>(_CreatableTicketCustomerAccount_QNAME, CreatableCustomerAccount.class, CreatableTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "invoiceID", scope = CreatableTicket.class)
    public JAXBElement<Long> createCreatableTicketInvoiceID(Long value) {
        return new JAXBElement<Long>(_CreatableTicketInvoiceID_QNAME, Long.class, CreatableTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "invoiceDueDate", scope = CreatableTicket.class)
    public JAXBElement<XMLGregorianCalendar> createCreatableTicketInvoiceDueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CreatableTicketInvoiceDueDate_QNAME, XMLGregorianCalendar.class, CreatableTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "invoiceType", scope = CreatableTicket.class)
    public JAXBElement<String> createCreatableTicketInvoiceType(String value) {
        return new JAXBElement<String>(_CreatableTicketInvoiceType_QNAME, String.class, CreatableTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "documentFormat", scope = CreatableTicket.class)
    public JAXBElement<String> createCreatableTicketDocumentFormat(String value) {
        return new JAXBElement<String>(_CreatableTicketDocumentFormat_QNAME, String.class, CreatableTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "invoiceNumber", scope = CreatableTicket.class)
    public JAXBElement<Long> createCreatableTicketInvoiceNumber(Long value) {
        return new JAXBElement<Long>(_CreatableTicketInvoiceNumber_QNAME, Long.class, CreatableTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatableService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "service", scope = CreatableTicket.class)
    public JAXBElement<CreatableService> createCreatableTicketService(CreatableService value) {
        return new JAXBElement<CreatableService>(_CreatableTicketService_QNAME, CreatableService.class, CreatableTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "orderID", scope = CreatableTicket.class)
    public JAXBElement<Long> createCreatableTicketOrderID(Long value) {
        return new JAXBElement<Long>(_CreatableTicketOrderID_QNAME, Long.class, CreatableTicket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types", name = "token", scope = CreatableTicket.class)
    public JAXBElement<String> createCreatableTicketToken(String value) {
        return new JAXBElement<String>(_CreatableTicketToken_QNAME, String.class, CreatableTicket.class, value);
    }

}
