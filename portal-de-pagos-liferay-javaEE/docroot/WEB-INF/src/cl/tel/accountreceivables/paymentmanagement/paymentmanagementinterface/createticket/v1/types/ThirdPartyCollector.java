
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;p xmlns="http://www.w3.org/2001/XMLSchema" xmlns:impl=" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tef=" /EnterpriseApplicationIntegration/TEFHeader/V1" xmlns:tns=" / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;Extends: SID Models::Customer Domain::Customer Bill Collection ABE::Customer Payment ABE::ThirdPartyPayeeAgency&lt;/p&gt;
 * </pre>
 * 
 * 
 * <p>Clase Java para ThirdPartyCollector complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ThirdPartyCollector"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partyRoleId"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="4"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="agencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="shopID" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ThirdPartyCollector", propOrder = {
    "partyRoleId",
    "agencyID",
    "shopID"
})
public class ThirdPartyCollector {

    @XmlElement(required = true)
    protected String partyRoleId;
    @XmlElement(required = true)
    protected BigInteger agencyID;
    @XmlElement(required = true)
    protected BigInteger shopID;

    /**
     * Obtiene el valor de la propiedad partyRoleId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyRoleId() {
        return partyRoleId;
    }

    /**
     * Define el valor de la propiedad partyRoleId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyRoleId(String value) {
        this.partyRoleId = value;
    }

    /**
     * Obtiene el valor de la propiedad agencyID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAgencyID() {
        return agencyID;
    }

    /**
     * Define el valor de la propiedad agencyID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAgencyID(BigInteger value) {
        this.agencyID = value;
    }

    /**
     * Obtiene el valor de la propiedad shopID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getShopID() {
        return shopID;
    }

    /**
     * Define el valor de la propiedad shopID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setShopID(BigInteger value) {
        this.shopID = value;
    }

}
