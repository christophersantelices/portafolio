
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CustomerPayment complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CustomerPayment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="amount" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}Money"/&gt;
 *         &lt;element name="remainingAmount" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}Money" minOccurs="0"/&gt;
 *         &lt;element name="dueAmount" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}Money" minOccurs="0"/&gt;
 *         &lt;element name="disputedAmount" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}Money" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerPayment", propOrder = {
    "amount",
    "remainingAmount",
    "dueAmount",
    "disputedAmount"
})
public class CustomerPayment {

    @XmlElement(required = true)
    protected Money amount;
    @XmlElementRef(name = "remainingAmount", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Money> remainingAmount;
    @XmlElementRef(name = "dueAmount", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Money> dueAmount;
    @XmlElementRef(name = "disputedAmount", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Money> disputedAmount;

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setAmount(Money value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad remainingAmount.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public JAXBElement<Money> getRemainingAmount() {
        return remainingAmount;
    }

    /**
     * Define el valor de la propiedad remainingAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public void setRemainingAmount(JAXBElement<Money> value) {
        this.remainingAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad dueAmount.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public JAXBElement<Money> getDueAmount() {
        return dueAmount;
    }

    /**
     * Define el valor de la propiedad dueAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public void setDueAmount(JAXBElement<Money> value) {
        this.dueAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad disputedAmount.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public JAXBElement<Money> getDisputedAmount() {
        return disputedAmount;
    }

    /**
     * Define el valor de la propiedad disputedAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public void setDisputedAmount(JAXBElement<Money> value) {
        this.disputedAmount = value;
    }

}
