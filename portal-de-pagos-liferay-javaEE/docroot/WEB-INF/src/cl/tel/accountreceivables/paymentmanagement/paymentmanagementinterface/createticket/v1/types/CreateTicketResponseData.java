
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CreateTicketResponseData complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CreateTicketResponseData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="status" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}StatusAbstract"/&gt;
 *         &lt;element name="ticket" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}CreatedTicket" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateTicketResponseData", propOrder = {
    "status",
    "ticket"
})
public class CreateTicketResponseData {

    @XmlElement(required = true)
    protected StatusAbstract status;
    protected CreatedTicket ticket;

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link StatusAbstract }
     *     
     */
    public StatusAbstract getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusAbstract }
     *     
     */
    public void setStatus(StatusAbstract value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad ticket.
     * 
     * @return
     *     possible object is
     *     {@link CreatedTicket }
     *     
     */
    public CreatedTicket getTicket() {
        return ticket;
    }

    /**
     * Define el valor de la propiedad ticket.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatedTicket }
     *     
     */
    public void setTicket(CreatedTicket value) {
        this.ticket = value;
    }

}
