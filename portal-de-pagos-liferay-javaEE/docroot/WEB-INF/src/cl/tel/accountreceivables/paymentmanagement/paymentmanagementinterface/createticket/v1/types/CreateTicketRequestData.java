
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para CreateTicketRequestData complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CreateTicketRequestData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="thirdPartyCollector" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}ThirdPartyCollector"/&gt;
 *         &lt;element name="operationDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="operationType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="context" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}CreatableContext"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateTicketRequestData", propOrder = {
    "thirdPartyCollector",
    "operationDateTime",
    "operationType",
    "context"
})
public class CreateTicketRequestData {

    @XmlElement(required = true)
    protected ThirdPartyCollector thirdPartyCollector;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar operationDateTime;
    @XmlElement(required = true)
    protected String operationType;
    @XmlElement(required = true)
    protected CreatableContext context;

    /**
     * Obtiene el valor de la propiedad thirdPartyCollector.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyCollector }
     *     
     */
    public ThirdPartyCollector getThirdPartyCollector() {
        return thirdPartyCollector;
    }

    /**
     * Define el valor de la propiedad thirdPartyCollector.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyCollector }
     *     
     */
    public void setThirdPartyCollector(ThirdPartyCollector value) {
        this.thirdPartyCollector = value;
    }

    /**
     * Obtiene el valor de la propiedad operationDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOperationDateTime() {
        return operationDateTime;
    }

    /**
     * Define el valor de la propiedad operationDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOperationDateTime(XMLGregorianCalendar value) {
        this.operationDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad operationType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationType() {
        return operationType;
    }

    /**
     * Define el valor de la propiedad operationType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationType(String value) {
        this.operationType = value;
    }

    /**
     * Obtiene el valor de la propiedad context.
     * 
     * @return
     *     possible object is
     *     {@link CreatableContext }
     *     
     */
    public CreatableContext getContext() {
        return context;
    }

    /**
     * Define el valor de la propiedad context.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatableContext }
     *     
     */
    public void setContext(CreatableContext value) {
        this.context = value;
    }

}
