
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreatePaymentMethodBankID_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "bankID");
    private final static QName _CreatePaymentMethodBankAccount_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "bankAccount");
    private final static QName _CreatePaymentMethodSerialNumber_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "serialNumber");
    private final static QName _CreatePaymentMethodChequeDate_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "chequeDate");
    private final static QName _CreatePaymentMethodWebPayID_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "webPayID");
    private final static QName _CreatePaymentMethodVerificationCode_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "verificationCode");
    private final static QName _CreatePaymentMethodAuthorizationCode_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "authorizationCode");
    private final static QName _CreateDebtPaymentResponseDataRDPID_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "RDPID");
    private final static QName _CreateDebtPaymentResponseDataLineList_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "lineList");
    private final static QName _CreateDebtPaymentRequestDataFiller1_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "filler1");
    private final static QName _CreateDebtPaymentRequestDataFiller2_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", "filler2");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateDebtPaymentRequest }
     * 
     */
    public CreateDebtPaymentRequest createCreateDebtPaymentRequest() {
        return new CreateDebtPaymentRequest();
    }

    /**
     * Create an instance of {@link CreateDebtPaymentRequestData }
     * 
     */
    public CreateDebtPaymentRequestData createCreateDebtPaymentRequestData() {
        return new CreateDebtPaymentRequestData();
    }

    /**
     * Create an instance of {@link CreateDebtPaymentResponse }
     * 
     */
    public CreateDebtPaymentResponse createCreateDebtPaymentResponse() {
        return new CreateDebtPaymentResponse();
    }

    /**
     * Create an instance of {@link CreateDebtPaymentResponseData }
     * 
     */
    public CreateDebtPaymentResponseData createCreateDebtPaymentResponseData() {
        return new CreateDebtPaymentResponseData();
    }

    /**
     * Create an instance of {@link CreateDebtPaymentFault }
     * 
     */
    public CreateDebtPaymentFault createCreateDebtPaymentFault() {
        return new CreateDebtPaymentFault();
    }

    /**
     * Create an instance of {@link CreatePaymentMethod }
     * 
     */
    public CreatePaymentMethod createCreatePaymentMethod() {
        return new CreatePaymentMethod();
    }

    /**
     * Create an instance of {@link LineList }
     * 
     */
    public LineList createLineList() {
        return new LineList();
    }

    /**
     * Create an instance of {@link ThirdPartyCollector }
     * 
     */
    public ThirdPartyCollector createThirdPartyCollector() {
        return new ThirdPartyCollector();
    }

    /**
     * Create an instance of {@link Money }
     * 
     */
    public Money createMoney() {
        return new Money();
    }

    /**
     * Create an instance of {@link StatusAbstract }
     * 
     */
    public StatusAbstract createStatusAbstract() {
        return new StatusAbstract();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "bankID", scope = CreatePaymentMethod.class)
    public JAXBElement<Long> createCreatePaymentMethodBankID(Long value) {
        return new JAXBElement<Long>(_CreatePaymentMethodBankID_QNAME, Long.class, CreatePaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "bankAccount", scope = CreatePaymentMethod.class)
    public JAXBElement<String> createCreatePaymentMethodBankAccount(String value) {
        return new JAXBElement<String>(_CreatePaymentMethodBankAccount_QNAME, String.class, CreatePaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "serialNumber", scope = CreatePaymentMethod.class)
    public JAXBElement<String> createCreatePaymentMethodSerialNumber(String value) {
        return new JAXBElement<String>(_CreatePaymentMethodSerialNumber_QNAME, String.class, CreatePaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "chequeDate", scope = CreatePaymentMethod.class)
    public JAXBElement<XMLGregorianCalendar> createCreatePaymentMethodChequeDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CreatePaymentMethodChequeDate_QNAME, XMLGregorianCalendar.class, CreatePaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "webPayID", scope = CreatePaymentMethod.class)
    public JAXBElement<String> createCreatePaymentMethodWebPayID(String value) {
        return new JAXBElement<String>(_CreatePaymentMethodWebPayID_QNAME, String.class, CreatePaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "verificationCode", scope = CreatePaymentMethod.class)
    public JAXBElement<String> createCreatePaymentMethodVerificationCode(String value) {
        return new JAXBElement<String>(_CreatePaymentMethodVerificationCode_QNAME, String.class, CreatePaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "authorizationCode", scope = CreatePaymentMethod.class)
    public JAXBElement<String> createCreatePaymentMethodAuthorizationCode(String value) {
        return new JAXBElement<String>(_CreatePaymentMethodAuthorizationCode_QNAME, String.class, CreatePaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "RDPID", scope = CreateDebtPaymentResponseData.class)
    public JAXBElement<String> createCreateDebtPaymentResponseDataRDPID(String value) {
        return new JAXBElement<String>(_CreateDebtPaymentResponseDataRDPID_QNAME, String.class, CreateDebtPaymentResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "lineList", scope = CreateDebtPaymentResponseData.class)
    public JAXBElement<LineList> createCreateDebtPaymentResponseDataLineList(LineList value) {
        return new JAXBElement<LineList>(_CreateDebtPaymentResponseDataLineList_QNAME, LineList.class, CreateDebtPaymentResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "filler1", scope = CreateDebtPaymentRequestData.class)
    public JAXBElement<String> createCreateDebtPaymentRequestDataFiller1(String value) {
        return new JAXBElement<String>(_CreateDebtPaymentRequestDataFiller1_QNAME, String.class, CreateDebtPaymentRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", name = "filler2", scope = CreateDebtPaymentRequestData.class)
    public JAXBElement<String> createCreateDebtPaymentRequestDataFiller2(String value) {
        return new JAXBElement<String>(_CreateDebtPaymentRequestDataFiller2_QNAME, String.class, CreateDebtPaymentRequestData.class, value);
    }

}
