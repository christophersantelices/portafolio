package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.1.12
 * 2018-02-27T12:22:09.135-03:00
 * Generated source version: 3.1.12
 * 
 */
@WebService(targetNamespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1", name = "CreateDebtPaymentPortType")
@XmlSeeAlso({cl.telefonica.enterpriseapplicationintegration.tefheader.v1.ObjectFactory.class, cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface CreateDebtPaymentPortType {

    @WebResult(name = "CreateDebtPaymentResponse", targetNamespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types", partName = "CreateDebtPaymentResponse")
    @WebMethod(action = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1")
    public cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentResponse createDebtPayment(
        @WebParam(partName = "CreateDebtPaymentRequest", name = "CreateDebtPaymentRequest", targetNamespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1/types")
        cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentRequest createDebtPaymentRequest
    ) throws CreateDebtPaymentFault;
}
