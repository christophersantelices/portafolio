
/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1;

import java.util.logging.Logger;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.1.12
 * 2018-02-27T12:22:09.120-03:00
 * Generated source version: 3.1.12
 * 
 */

@javax.jws.WebService(
                      serviceName = "CreateDebtPayment",
                      portName = "CreateDebtPaymentPort",
                      targetNamespace = " / accoun/PaymentManagement/PaymentManagementInterface/CreateDebtPayment/V1",
                      wsdlLocation = "file:/C:/liferay/liferay-developer-studio/liferay-plugins-sdk-6.2-ee-sp9/portlets/portal-de-pagos-corporate-portlet/docroot/WEB-INF/src/wsdl/CreateDebtPayment.wsdl",
                      endpointInterface = "cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.CreateDebtPaymentPortType")
                      
public class CreateDebtPaymentPortImpl implements CreateDebtPaymentPortType {

    private static final Logger LOG = Logger.getLogger(CreateDebtPaymentPortImpl.class.getName());

    /* (non-Javadoc)
     * @see cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.CreateDebtPaymentPortType#createDebtPayment(cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentRequest createDebtPaymentRequest)*
     */
    public cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentResponse createDebtPayment(cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentRequest createDebtPaymentRequest) throws CreateDebtPaymentFault   { 
        LOG.info("Executing operation createDebtPayment");
        System.out.println(createDebtPaymentRequest);
        try {
            cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentResponse _return = new cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentResponse();
            cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderResponse _returnHeader = new cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderResponse();
            _returnHeader.setIdMessage("IdMessage1702252115");
            _returnHeader.setServiceName("ServiceName-822456223");
            _returnHeader.setResponseDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar("2018-02-27T12:22:09.134-03:00"));
            _returnHeader.setTransactionID("TransactionID-1486835146");
            _return.setHeader(_returnHeader);
            cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentResponseData _returnData = new cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.CreateDebtPaymentResponseData();
            cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.StatusAbstract _returnDataStatus = new cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.StatusAbstract();
            _returnDataStatus.setCode("Code13714092");
            _returnDataStatus.setDescription("Description16203954");
            _returnData.setStatus(_returnDataStatus);
            javax.xml.bind.JAXBElement<java.lang.String> _returnDataRDPID = null;
            _returnData.setRDPID(_returnDataRDPID);
            javax.xml.bind.JAXBElement<cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createdebtpayment.v1.types.LineList> _returnDataLineList = null;
            _returnData.setLineList(_returnDataLineList);
            _return.setData(_returnData);
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new CreateDebtPaymentFault("CreateDebtPaymentFault...");
    }

}
