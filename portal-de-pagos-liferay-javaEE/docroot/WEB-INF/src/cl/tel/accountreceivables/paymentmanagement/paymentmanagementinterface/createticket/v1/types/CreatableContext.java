
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.createticket.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber;


/**
 * <p>Clase Java para CreatableContext complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CreatableContext"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerIdentification" type="{ /EnterpriseApplicationIntegration/TEFHeader/V1}TEFChileanIdentificationNumber"/&gt;
 *         &lt;element name="ticketType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ticketList" type="{ / accoun/PaymentManagement/PaymentManagementInterface/CreateTicket/V1/types}CreatableTicketList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreatableContext", propOrder = {
    "customerIdentification",
    "ticketType",
    "ticketList"
})
public class CreatableContext {

    @XmlElement(required = true)
    protected TEFChileanIdentificationNumber customerIdentification;
    @XmlElement(required = true)
    protected String ticketType;
    @XmlElement(required = true)
    protected CreatableTicketList ticketList;

    /**
     * Obtiene el valor de la propiedad customerIdentification.
     * 
     * @return
     *     possible object is
     *     {@link TEFChileanIdentificationNumber }
     *     
     */
    public TEFChileanIdentificationNumber getCustomerIdentification() {
        return customerIdentification;
    }

    /**
     * Define el valor de la propiedad customerIdentification.
     * 
     * @param value
     *     allowed object is
     *     {@link TEFChileanIdentificationNumber }
     *     
     */
    public void setCustomerIdentification(TEFChileanIdentificationNumber value) {
        this.customerIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketType() {
        return ticketType;
    }

    /**
     * Define el valor de la propiedad ticketType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketType(String value) {
        this.ticketType = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketList.
     * 
     * @return
     *     possible object is
     *     {@link CreatableTicketList }
     *     
     */
    public CreatableTicketList getTicketList() {
        return ticketList;
    }

    /**
     * Define el valor de la propiedad ticketList.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatableTicketList }
     *     
     */
    public void setTicketList(CreatableTicketList value) {
        this.ticketList = value;
    }

}
