
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BillingCustomer complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BillingCustomer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="billingCustomer"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="12"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="customerAccountBalanceList" type="{ / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types}CustomerAccountBalanceList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingCustomer", propOrder = {
    "billingCustomer",
    "customerAccountBalanceList"
})
public class BillingCustomer {

    @XmlElement(required = true)
    protected String billingCustomer;
    @XmlElement(required = true)
    protected CustomerAccountBalanceList customerAccountBalanceList;

    /**
     * Obtiene el valor de la propiedad billingCustomer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCustomer() {
        return billingCustomer;
    }

    /**
     * Define el valor de la propiedad billingCustomer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCustomer(String value) {
        this.billingCustomer = value;
    }

    /**
     * Obtiene el valor de la propiedad customerAccountBalanceList.
     * 
     * @return
     *     possible object is
     *     {@link CustomerAccountBalanceList }
     *     
     */
    public CustomerAccountBalanceList getCustomerAccountBalanceList() {
        return customerAccountBalanceList;
    }

    /**
     * Define el valor de la propiedad customerAccountBalanceList.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerAccountBalanceList }
     *     
     */
    public void setCustomerAccountBalanceList(CustomerAccountBalanceList value) {
        this.customerAccountBalanceList = value;
    }

}
