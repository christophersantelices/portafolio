
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Paging complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Paging"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pageSize" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="numberOfRows" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="hasMore" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Paging", propOrder = {
    "pageSize",
    "pageNumber",
    "numberOfRows",
    "hasMore"
})
public class Paging {

    @XmlElementRef(name = "pageSize", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> pageSize;
    @XmlElementRef(name = "pageNumber", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> pageNumber;
    @XmlElementRef(name = "numberOfRows", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> numberOfRows;
    @XmlElementRef(name = "hasMore", namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> hasMore;

    /**
     * Obtiene el valor de la propiedad pageSize.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getPageSize() {
        return pageSize;
    }

    /**
     * Define el valor de la propiedad pageSize.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setPageSize(JAXBElement<BigInteger> value) {
        this.pageSize = value;
    }

    /**
     * Obtiene el valor de la propiedad pageNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getPageNumber() {
        return pageNumber;
    }

    /**
     * Define el valor de la propiedad pageNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setPageNumber(JAXBElement<BigInteger> value) {
        this.pageNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfRows.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getNumberOfRows() {
        return numberOfRows;
    }

    /**
     * Define el valor de la propiedad numberOfRows.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setNumberOfRows(JAXBElement<BigInteger> value) {
        this.numberOfRows = value;
    }

    /**
     * Obtiene el valor de la propiedad hasMore.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getHasMore() {
        return hasMore;
    }

    /**
     * Define el valor de la propiedad hasMore.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setHasMore(JAXBElement<Boolean> value) {
        this.hasMore = value;
    }

}
