
package cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PagingPageSize_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "pageSize");
    private final static QName _PagingPageNumber_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "pageNumber");
    private final static QName _PagingNumberOfRows_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "numberOfRows");
    private final static QName _PagingHasMore_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "hasMore");
    private final static QName _CustomerPaymentRemainingAmount_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "remainingAmount");
    private final static QName _CustomerPaymentDueAmount_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "dueAmount");
    private final static QName _CustomerPaymentDisputedAmount_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "disputedAmount");
    private final static QName _PaymentItemID_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "ID");
    private final static QName _PaymentItemName_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "name");
    private final static QName _PaymentItemPaymentToken_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "paymentToken");
    private final static QName _PaymentItemPaymentSystemID_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "paymentSystemID");
    private final static QName _PaymentItemIsEditable_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "isEditable");
    private final static QName _PaymentItemPrintLineList_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "printLineList");
    private final static QName _PaymentItemDisplayLineList_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "displayLineList");
    private final static QName _CustomerBillSpecValidFor_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "validFor");
    private final static QName _CustomerBillSpecValidFrom_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "validFrom");
    private final static QName _CustomerBillBillingInvoiceNumber_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "billingInvoiceNumber");
    private final static QName _CustomerBillExternalLegalNumber_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "externalLegalNumber");
    private final static QName _CustomerBillDocumentType_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "documentType");
    private final static QName _CustomerBillDocumentTypeInd_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "documentTypeInd");
    private final static QName _CustomerBillBillStatus_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "billStatus");
    private final static QName _CustomerBillBillNo_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "billNo");
    private final static QName _CustomerBillBillSpec_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "billSpec");
    private final static QName _CustomerBillPaymentItemList_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "paymentItemList");
    private final static QName _IndividualNamesResponseTelephoneNumber_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "telephoneNumber");
    private final static QName _IndividualNamesResponseIdentification_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "identification");
    private final static QName _CustomerAccountCustomerAccountID_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "customerAccountID");
    private final static QName _CustomerAccountAccountStatus_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "accountStatus");
    private final static QName _CustomerAccountCustomerInformation_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "customerInformation");
    private final static QName _CustomerAccountIsDefaulter_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "isDefaulter");
    private final static QName _CustomerAccountPacPatIndicator_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "pacPatIndicator");
    private final static QName _CustomerAccountServicePaymentIndicator_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "servicePaymentIndicator");
    private final static QName _CustomerAccountActualBill_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "actualBill");
    private final static QName _CustomerAccountPreviousBill_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "previousBill");
    private final static QName _CustomerAccountBalancePaymentItem_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "paymentItem");
    private final static QName _CustomerAccountBalanceCustomerAccount_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "customerAccount");
    private final static QName _FindAccountDebtsResponseDataCompanyID_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "companyID");
    private final static QName _FindAccountDebtsResponseDataBiller_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "biller");
    private final static QName _FindAccountDebtsResponseDataBillingCustomerList_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "billingCustomerList");
    private final static QName _FindAccountDebtsResponseDataPaging_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "paging");
    private final static QName _FindAccountDebtsRequestDataSecondaryIDType_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "secondaryIDType");
    private final static QName _FindAccountDebtsRequestDataSecondaryIDValue_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "secondaryIDValue");
    private final static QName _FindAccountDebtsRequestDataCustomerTypeCode_QNAME = new QName(" / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", "customerTypeCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.telefonica. accoun.paymentmanagement.paymentmanagementinterface.findaccountdebts.v1.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindAccountDebtsRequest }
     * 
     */
    public FindAccountDebtsRequest createFindAccountDebtsRequest() {
        return new FindAccountDebtsRequest();
    }

    /**
     * Create an instance of {@link FindAccountDebtsRequestData }
     * 
     */
    public FindAccountDebtsRequestData createFindAccountDebtsRequestData() {
        return new FindAccountDebtsRequestData();
    }

    /**
     * Create an instance of {@link FindAccountDebtsResponse }
     * 
     */
    public FindAccountDebtsResponse createFindAccountDebtsResponse() {
        return new FindAccountDebtsResponse();
    }

    /**
     * Create an instance of {@link FindAccountDebtsResponseData }
     * 
     */
    public FindAccountDebtsResponseData createFindAccountDebtsResponseData() {
        return new FindAccountDebtsResponseData();
    }

    /**
     * Create an instance of {@link FindAccountDebtsFault }
     * 
     */
    public FindAccountDebtsFault createFindAccountDebtsFault() {
        return new FindAccountDebtsFault();
    }

    /**
     * Create an instance of {@link CustomerAccountBalanceList }
     * 
     */
    public CustomerAccountBalanceList createCustomerAccountBalanceList() {
        return new CustomerAccountBalanceList();
    }

    /**
     * Create an instance of {@link CustomerAccountBalance }
     * 
     */
    public CustomerAccountBalance createCustomerAccountBalance() {
        return new CustomerAccountBalance();
    }

    /**
     * Create an instance of {@link CustomerAccount }
     * 
     */
    public CustomerAccount createCustomerAccount() {
        return new CustomerAccount();
    }

    /**
     * Create an instance of {@link CustomerBillList }
     * 
     */
    public CustomerBillList createCustomerBillList() {
        return new CustomerBillList();
    }

    /**
     * Create an instance of {@link IndividualNamesResponse }
     * 
     */
    public IndividualNamesResponse createIndividualNamesResponse() {
        return new IndividualNamesResponse();
    }

    /**
     * Create an instance of {@link IndividualName }
     * 
     */
    public IndividualName createIndividualName() {
        return new IndividualName();
    }

    /**
     * Create an instance of {@link CustomerBill }
     * 
     */
    public CustomerBill createCustomerBill() {
        return new CustomerBill();
    }

    /**
     * Create an instance of {@link CustomerBillSpec }
     * 
     */
    public CustomerBillSpec createCustomerBillSpec() {
        return new CustomerBillSpec();
    }

    /**
     * Create an instance of {@link PaymentItemList }
     * 
     */
    public PaymentItemList createPaymentItemList() {
        return new PaymentItemList();
    }

    /**
     * Create an instance of {@link PaymentItem }
     * 
     */
    public PaymentItem createPaymentItem() {
        return new PaymentItem();
    }

    /**
     * Create an instance of {@link LineList }
     * 
     */
    public LineList createLineList() {
        return new LineList();
    }

    /**
     * Create an instance of {@link CustomerPayment }
     * 
     */
    public CustomerPayment createCustomerPayment() {
        return new CustomerPayment();
    }

    /**
     * Create an instance of {@link Paging }
     * 
     */
    public Paging createPaging() {
        return new Paging();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link Money }
     * 
     */
    public Money createMoney() {
        return new Money();
    }

    /**
     * Create an instance of {@link ThirdPartyPayeeAgency }
     * 
     */
    public ThirdPartyPayeeAgency createThirdPartyPayeeAgency() {
        return new ThirdPartyPayeeAgency();
    }

    /**
     * Create an instance of {@link Party }
     * 
     */
    public Party createParty() {
        return new Party();
    }

    /**
     * Create an instance of {@link TelephoneNumber }
     * 
     */
    public TelephoneNumber createTelephoneNumber() {
        return new TelephoneNumber();
    }

    /**
     * Create an instance of {@link BillingCustomerList }
     * 
     */
    public BillingCustomerList createBillingCustomerList() {
        return new BillingCustomerList();
    }

    /**
     * Create an instance of {@link BillingCustomer }
     * 
     */
    public BillingCustomer createBillingCustomer() {
        return new BillingCustomer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "pageSize", scope = Paging.class)
    public JAXBElement<BigInteger> createPagingPageSize(BigInteger value) {
        return new JAXBElement<BigInteger>(_PagingPageSize_QNAME, BigInteger.class, Paging.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "pageNumber", scope = Paging.class)
    public JAXBElement<BigInteger> createPagingPageNumber(BigInteger value) {
        return new JAXBElement<BigInteger>(_PagingPageNumber_QNAME, BigInteger.class, Paging.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "numberOfRows", scope = Paging.class)
    public JAXBElement<BigInteger> createPagingNumberOfRows(BigInteger value) {
        return new JAXBElement<BigInteger>(_PagingNumberOfRows_QNAME, BigInteger.class, Paging.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "hasMore", scope = Paging.class)
    public JAXBElement<Boolean> createPagingHasMore(Boolean value) {
        return new JAXBElement<Boolean>(_PagingHasMore_QNAME, Boolean.class, Paging.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "remainingAmount", scope = CustomerPayment.class)
    public JAXBElement<Money> createCustomerPaymentRemainingAmount(Money value) {
        return new JAXBElement<Money>(_CustomerPaymentRemainingAmount_QNAME, Money.class, CustomerPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "dueAmount", scope = CustomerPayment.class)
    public JAXBElement<Money> createCustomerPaymentDueAmount(Money value) {
        return new JAXBElement<Money>(_CustomerPaymentDueAmount_QNAME, Money.class, CustomerPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "disputedAmount", scope = CustomerPayment.class)
    public JAXBElement<Money> createCustomerPaymentDisputedAmount(Money value) {
        return new JAXBElement<Money>(_CustomerPaymentDisputedAmount_QNAME, Money.class, CustomerPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "ID", scope = PaymentItem.class)
    public JAXBElement<String> createPaymentItemID(String value) {
        return new JAXBElement<String>(_PaymentItemID_QNAME, String.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "name", scope = PaymentItem.class)
    public JAXBElement<String> createPaymentItemName(String value) {
        return new JAXBElement<String>(_PaymentItemName_QNAME, String.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "paymentToken", scope = PaymentItem.class)
    public JAXBElement<String> createPaymentItemPaymentToken(String value) {
        return new JAXBElement<String>(_PaymentItemPaymentToken_QNAME, String.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "paymentSystemID", scope = PaymentItem.class)
    public JAXBElement<String> createPaymentItemPaymentSystemID(String value) {
        return new JAXBElement<String>(_PaymentItemPaymentSystemID_QNAME, String.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "isEditable", scope = PaymentItem.class)
    public JAXBElement<String> createPaymentItemIsEditable(String value) {
        return new JAXBElement<String>(_PaymentItemIsEditable_QNAME, String.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "printLineList", scope = PaymentItem.class)
    public JAXBElement<LineList> createPaymentItemPrintLineList(LineList value) {
        return new JAXBElement<LineList>(_PaymentItemPrintLineList_QNAME, LineList.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "displayLineList", scope = PaymentItem.class)
    public JAXBElement<LineList> createPaymentItemDisplayLineList(LineList value) {
        return new JAXBElement<LineList>(_PaymentItemDisplayLineList_QNAME, LineList.class, PaymentItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "validFor", scope = CustomerBillSpec.class)
    public JAXBElement<XMLGregorianCalendar> createCustomerBillSpecValidFor(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CustomerBillSpecValidFor_QNAME, XMLGregorianCalendar.class, CustomerBillSpec.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "validFrom", scope = CustomerBillSpec.class)
    public JAXBElement<XMLGregorianCalendar> createCustomerBillSpecValidFrom(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CustomerBillSpecValidFrom_QNAME, XMLGregorianCalendar.class, CustomerBillSpec.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "billingInvoiceNumber", scope = CustomerBill.class)
    public JAXBElement<String> createCustomerBillBillingInvoiceNumber(String value) {
        return new JAXBElement<String>(_CustomerBillBillingInvoiceNumber_QNAME, String.class, CustomerBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "externalLegalNumber", scope = CustomerBill.class)
    public JAXBElement<String> createCustomerBillExternalLegalNumber(String value) {
        return new JAXBElement<String>(_CustomerBillExternalLegalNumber_QNAME, String.class, CustomerBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "documentType", scope = CustomerBill.class)
    public JAXBElement<String> createCustomerBillDocumentType(String value) {
        return new JAXBElement<String>(_CustomerBillDocumentType_QNAME, String.class, CustomerBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "documentTypeInd", scope = CustomerBill.class)
    public JAXBElement<String> createCustomerBillDocumentTypeInd(String value) {
        return new JAXBElement<String>(_CustomerBillDocumentTypeInd_QNAME, String.class, CustomerBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "billStatus", scope = CustomerBill.class)
    public JAXBElement<String> createCustomerBillBillStatus(String value) {
        return new JAXBElement<String>(_CustomerBillBillStatus_QNAME, String.class, CustomerBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "billNo", scope = CustomerBill.class)
    public JAXBElement<String> createCustomerBillBillNo(String value) {
        return new JAXBElement<String>(_CustomerBillBillNo_QNAME, String.class, CustomerBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerBillSpec }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "billSpec", scope = CustomerBill.class)
    public JAXBElement<CustomerBillSpec> createCustomerBillBillSpec(CustomerBillSpec value) {
        return new JAXBElement<CustomerBillSpec>(_CustomerBillBillSpec_QNAME, CustomerBillSpec.class, CustomerBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentItemList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "paymentItemList", scope = CustomerBill.class)
    public JAXBElement<PaymentItemList> createCustomerBillPaymentItemList(PaymentItemList value) {
        return new JAXBElement<PaymentItemList>(_CustomerBillPaymentItemList_QNAME, PaymentItemList.class, CustomerBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndividualName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "name", scope = IndividualNamesResponse.class)
    public JAXBElement<IndividualName> createIndividualNamesResponseName(IndividualName value) {
        return new JAXBElement<IndividualName>(_PaymentItemName_QNAME, IndividualName.class, IndividualNamesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "telephoneNumber", scope = IndividualNamesResponse.class)
    public JAXBElement<TelephoneNumber> createIndividualNamesResponseTelephoneNumber(TelephoneNumber value) {
        return new JAXBElement<TelephoneNumber>(_IndividualNamesResponseTelephoneNumber_QNAME, TelephoneNumber.class, IndividualNamesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "identification", scope = IndividualNamesResponse.class)
    public JAXBElement<TEFChileanIdentificationNumber> createIndividualNamesResponseIdentification(TEFChileanIdentificationNumber value) {
        return new JAXBElement<TEFChileanIdentificationNumber>(_IndividualNamesResponseIdentification_QNAME, TEFChileanIdentificationNumber.class, IndividualNamesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "customerAccountID", scope = CustomerAccount.class)
    public JAXBElement<String> createCustomerAccountCustomerAccountID(String value) {
        return new JAXBElement<String>(_CustomerAccountCustomerAccountID_QNAME, String.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "accountStatus", scope = CustomerAccount.class)
    public JAXBElement<String> createCustomerAccountAccountStatus(String value) {
        return new JAXBElement<String>(_CustomerAccountAccountStatus_QNAME, String.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndividualNamesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "customerInformation", scope = CustomerAccount.class)
    public JAXBElement<IndividualNamesResponse> createCustomerAccountCustomerInformation(IndividualNamesResponse value) {
        return new JAXBElement<IndividualNamesResponse>(_CustomerAccountCustomerInformation_QNAME, IndividualNamesResponse.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "isDefaulter", scope = CustomerAccount.class)
    public JAXBElement<String> createCustomerAccountIsDefaulter(String value) {
        return new JAXBElement<String>(_CustomerAccountIsDefaulter_QNAME, String.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "pacPatIndicator", scope = CustomerAccount.class)
    public JAXBElement<String> createCustomerAccountPacPatIndicator(String value) {
        return new JAXBElement<String>(_CustomerAccountPacPatIndicator_QNAME, String.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "servicePaymentIndicator", scope = CustomerAccount.class)
    public JAXBElement<String> createCustomerAccountServicePaymentIndicator(String value) {
        return new JAXBElement<String>(_CustomerAccountServicePaymentIndicator_QNAME, String.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerBillList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "actualBill", scope = CustomerAccount.class)
    public JAXBElement<CustomerBillList> createCustomerAccountActualBill(CustomerBillList value) {
        return new JAXBElement<CustomerBillList>(_CustomerAccountActualBill_QNAME, CustomerBillList.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerBillList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "previousBill", scope = CustomerAccount.class)
    public JAXBElement<CustomerBillList> createCustomerAccountPreviousBill(CustomerBillList value) {
        return new JAXBElement<CustomerBillList>(_CustomerAccountPreviousBill_QNAME, CustomerBillList.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "paymentItem", scope = CustomerAccountBalance.class)
    public JAXBElement<PaymentItem> createCustomerAccountBalancePaymentItem(PaymentItem value) {
        return new JAXBElement<PaymentItem>(_CustomerAccountBalancePaymentItem_QNAME, PaymentItem.class, CustomerAccountBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "customerAccount", scope = CustomerAccountBalance.class)
    public JAXBElement<CustomerAccount> createCustomerAccountBalanceCustomerAccount(CustomerAccount value) {
        return new JAXBElement<CustomerAccount>(_CustomerAccountBalanceCustomerAccount_QNAME, CustomerAccount.class, CustomerAccountBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "companyID", scope = FindAccountDebtsResponseData.class)
    public JAXBElement<String> createFindAccountDebtsResponseDataCompanyID(String value) {
        return new JAXBElement<String>(_FindAccountDebtsResponseDataCompanyID_QNAME, String.class, FindAccountDebtsResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "biller", scope = FindAccountDebtsResponseData.class)
    public JAXBElement<String> createFindAccountDebtsResponseDataBiller(String value) {
        return new JAXBElement<String>(_FindAccountDebtsResponseDataBiller_QNAME, String.class, FindAccountDebtsResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillingCustomerList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "billingCustomerList", scope = FindAccountDebtsResponseData.class)
    public JAXBElement<BillingCustomerList> createFindAccountDebtsResponseDataBillingCustomerList(BillingCustomerList value) {
        return new JAXBElement<BillingCustomerList>(_FindAccountDebtsResponseDataBillingCustomerList_QNAME, BillingCustomerList.class, FindAccountDebtsResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Paging }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "paging", scope = FindAccountDebtsResponseData.class)
    public JAXBElement<Paging> createFindAccountDebtsResponseDataPaging(Paging value) {
        return new JAXBElement<Paging>(_FindAccountDebtsResponseDataPaging_QNAME, Paging.class, FindAccountDebtsResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "secondaryIDType", scope = FindAccountDebtsRequestData.class)
    public JAXBElement<String> createFindAccountDebtsRequestDataSecondaryIDType(String value) {
        return new JAXBElement<String>(_FindAccountDebtsRequestDataSecondaryIDType_QNAME, String.class, FindAccountDebtsRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "secondaryIDValue", scope = FindAccountDebtsRequestData.class)
    public JAXBElement<String> createFindAccountDebtsRequestDataSecondaryIDValue(String value) {
        return new JAXBElement<String>(_FindAccountDebtsRequestDataSecondaryIDValue_QNAME, String.class, FindAccountDebtsRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "customerTypeCode", scope = FindAccountDebtsRequestData.class)
    public JAXBElement<String> createFindAccountDebtsRequestDataCustomerTypeCode(String value) {
        return new JAXBElement<String>(_FindAccountDebtsRequestDataCustomerTypeCode_QNAME, String.class, FindAccountDebtsRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "biller", scope = FindAccountDebtsRequestData.class)
    public JAXBElement<String> createFindAccountDebtsRequestDataBiller(String value) {
        return new JAXBElement<String>(_FindAccountDebtsResponseDataBiller_QNAME, String.class, FindAccountDebtsRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Paging }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " / accoun/PaymentManagement/PaymentManagementInterface/FindAccountDebts/V1/types", name = "paging", scope = FindAccountDebtsRequestData.class)
    public JAXBElement<Paging> createFindAccountDebtsRequestDataPaging(Paging value) {
        return new JAXBElement<Paging>(_FindAccountDebtsResponseDataPaging_QNAME, Paging.class, FindAccountDebtsRequestData.class, value);
    }

}
