
package cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber;


/**
 * <p>Clase Java para data_req complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="data_req"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="notificationId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="externalID" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="20"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="triggeringSystem"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="20"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="notificationType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="6"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="identification" type="{ /EnterpriseApplicationIntegration/TEFHeader/V1}TEFChileanIdentificationNumber"/&gt;
 *         &lt;element name="preferredContactMedia" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="SMS"/&gt;
 *               &lt;enumeration value="EMAIL"/&gt;
 *               &lt;enumeration value="CALLCENTER"/&gt;
 *               &lt;enumeration value="PUSH"/&gt;
 *               &lt;enumeration value="XTIFY"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="email" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}EmailContact" minOccurs="0"/&gt;
 *         &lt;element name="telephoneNumber" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}TelephoneNumber" minOccurs="0"/&gt;
 *         &lt;element name="templateParameters" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}TemplateParameters"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "data_req", propOrder = {
    "notificationId",
    "externalID",
    "triggeringSystem",
    "notificationType",
    "identification",
    "preferredContactMedia",
    "email",
    "telephoneNumber",
    "templateParameters"
})
public class DataReq {

    @XmlElementRef(name = "notificationId", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> notificationId;
    @XmlElementRef(name = "externalID", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalID;
    @XmlElement(required = true)
    protected String triggeringSystem;
    @XmlElement(required = true)
    protected String notificationType;
    @XmlElement(required = true)
    protected TEFChileanIdentificationNumber identification;
    @XmlElementRef(name = "preferredContactMedia", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> preferredContactMedia;
    @XmlElementRef(name = "email", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<EmailContact> email;
    @XmlElementRef(name = "telephoneNumber", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<TelephoneNumber> telephoneNumber;
    @XmlElement(required = true, nillable = true)
    protected TemplateParameters templateParameters;

    /**
     * Obtiene el valor de la propiedad notificationId.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getNotificationId() {
        return notificationId;
    }

    /**
     * Define el valor de la propiedad notificationId.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setNotificationId(JAXBElement<Long> value) {
        this.notificationId = value;
    }

    /**
     * Obtiene el valor de la propiedad externalID.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalID() {
        return externalID;
    }

    /**
     * Define el valor de la propiedad externalID.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalID(JAXBElement<String> value) {
        this.externalID = value;
    }

    /**
     * Obtiene el valor de la propiedad triggeringSystem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTriggeringSystem() {
        return triggeringSystem;
    }

    /**
     * Define el valor de la propiedad triggeringSystem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTriggeringSystem(String value) {
        this.triggeringSystem = value;
    }

    /**
     * Obtiene el valor de la propiedad notificationType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotificationType() {
        return notificationType;
    }

    /**
     * Define el valor de la propiedad notificationType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotificationType(String value) {
        this.notificationType = value;
    }

    /**
     * Obtiene el valor de la propiedad identification.
     * 
     * @return
     *     possible object is
     *     {@link TEFChileanIdentificationNumber }
     *     
     */
    public TEFChileanIdentificationNumber getIdentification() {
        return identification;
    }

    /**
     * Define el valor de la propiedad identification.
     * 
     * @param value
     *     allowed object is
     *     {@link TEFChileanIdentificationNumber }
     *     
     */
    public void setIdentification(TEFChileanIdentificationNumber value) {
        this.identification = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredContactMedia.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPreferredContactMedia() {
        return preferredContactMedia;
    }

    /**
     * Define el valor de la propiedad preferredContactMedia.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPreferredContactMedia(JAXBElement<String> value) {
        this.preferredContactMedia = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EmailContact }{@code >}
     *     
     */
    public JAXBElement<EmailContact> getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EmailContact }{@code >}
     *     
     */
    public void setEmail(JAXBElement<EmailContact> value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad telephoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}
     *     
     */
    public JAXBElement<TelephoneNumber> getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * Define el valor de la propiedad telephoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}
     *     
     */
    public void setTelephoneNumber(JAXBElement<TelephoneNumber> value) {
        this.telephoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad templateParameters.
     * 
     * @return
     *     possible object is
     *     {@link TemplateParameters }
     *     
     */
    public TemplateParameters getTemplateParameters() {
        return templateParameters;
    }

    /**
     * Define el valor de la propiedad templateParameters.
     * 
     * @param value
     *     allowed object is
     *     {@link TemplateParameters }
     *     
     */
    public void setTemplateParameters(TemplateParameters value) {
        this.templateParameters = value;
    }

}
