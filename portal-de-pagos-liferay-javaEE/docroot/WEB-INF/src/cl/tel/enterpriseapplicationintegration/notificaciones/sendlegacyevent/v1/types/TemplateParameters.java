
package cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber;


/**
 * <p>Clase Java para TemplateParameters complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TemplateParameters"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productOffering" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}ProductOffering" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}Product" minOccurs="0"/&gt;
 *         &lt;element name="productOld" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}Product" minOccurs="0"/&gt;
 *         &lt;element name="loyaltyBalance" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}LoyaltyBalance" minOccurs="0"/&gt;
 *         &lt;element name="service" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}Service" minOccurs="0"/&gt;
 *         &lt;element name="serviceCharacteristicValue" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}ServiceCharacteristicValue" minOccurs="0"/&gt;
 *         &lt;element name="resource" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}Resource" minOccurs="0"/&gt;
 *         &lt;element name="geographicAddress" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}GeographicAddress" minOccurs="0"/&gt;
 *         &lt;element name="userAbstract" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}UserAbstract" minOccurs="0"/&gt;
 *         &lt;element name="documentAbstract" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}DocumentAbstract" minOccurs="0"/&gt;
 *         &lt;element name="activityAbstract" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}ActivityAbstract" minOccurs="0"/&gt;
 *         &lt;element name="customerIdentification" type="{ /EnterpriseApplicationIntegration/TEFHeader/V1}TEFChileanIdentificationNumber" minOccurs="0"/&gt;
 *         &lt;element name="receiverIdentification" type="{ /EnterpriseApplicationIntegration/TEFHeader/V1}TEFChileanIdentificationNumber" minOccurs="0"/&gt;
 *         &lt;element name="contactIdentification" type="{ /EnterpriseApplicationIntegration/TEFHeader/V1}TEFChileanIdentificationNumber" minOccurs="0"/&gt;
 *         &lt;element name="individualName" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}IndividualName" minOccurs="0"/&gt;
 *         &lt;element name="contactMedium" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}ContactMedium" minOccurs="0"/&gt;
 *         &lt;element name="customer" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}Customer" minOccurs="0"/&gt;
 *         &lt;element name="customerAccount" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}CustomerAccount" minOccurs="0"/&gt;
 *         &lt;element name="customerCreditProfile" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}CustomerCreditProfile" minOccurs="0"/&gt;
 *         &lt;element name="customerOrder" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}CustomerOrder" minOccurs="0"/&gt;
 *         &lt;element name="customerOrderItem" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}CustomerOrderItem" minOccurs="0"/&gt;
 *         &lt;element name="paymentMethod" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}PaymentMethod" minOccurs="0"/&gt;
 *         &lt;element name="salesChannel" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}SalesChannel" minOccurs="0"/&gt;
 *         &lt;element name="contactNumber" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}TelephoneNumber" minOccurs="0"/&gt;
 *         &lt;element name="emailContact" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}EmailContact" minOccurs="0"/&gt;
 *         &lt;element name="timePeriod" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}TimePeriod" minOccurs="0"/&gt;
 *         &lt;element name="amount" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}Money" minOccurs="0"/&gt;
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="message" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="1000"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="url" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="200"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TemplateParameters", propOrder = {
    "productOffering",
    "product",
    "productOld",
    "loyaltyBalance",
    "service",
    "serviceCharacteristicValue",
    "resource",
    "geographicAddress",
    "userAbstract",
    "documentAbstract",
    "activityAbstract",
    "customerIdentification",
    "receiverIdentification",
    "contactIdentification",
    "individualName",
    "contactMedium",
    "customer",
    "customerAccount",
    "customerCreditProfile",
    "customerOrder",
    "customerOrderItem",
    "paymentMethod",
    "salesChannel",
    "contactNumber",
    "emailContact",
    "timePeriod",
    "amount",
    "date",
    "message",
    "url"
})
public class TemplateParameters {

    @XmlElementRef(name = "productOffering", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<ProductOffering> productOffering;
    @XmlElementRef(name = "product", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Product> product;
    @XmlElementRef(name = "productOld", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Product> productOld;
    @XmlElementRef(name = "loyaltyBalance", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<LoyaltyBalance> loyaltyBalance;
    @XmlElementRef(name = "service", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Service> service;
    @XmlElementRef(name = "serviceCharacteristicValue", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<ServiceCharacteristicValue> serviceCharacteristicValue;
    @XmlElementRef(name = "resource", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Resource> resource;
    @XmlElementRef(name = "geographicAddress", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<GeographicAddress> geographicAddress;
    @XmlElementRef(name = "userAbstract", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<UserAbstract> userAbstract;
    @XmlElementRef(name = "documentAbstract", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<DocumentAbstract> documentAbstract;
    @XmlElementRef(name = "activityAbstract", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<ActivityAbstract> activityAbstract;
    @XmlElementRef(name = "customerIdentification", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<TEFChileanIdentificationNumber> customerIdentification;
    @XmlElementRef(name = "receiverIdentification", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<TEFChileanIdentificationNumber> receiverIdentification;
    @XmlElementRef(name = "contactIdentification", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<TEFChileanIdentificationNumber> contactIdentification;
    @XmlElementRef(name = "individualName", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<IndividualName> individualName;
    @XmlElementRef(name = "contactMedium", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<ContactMedium> contactMedium;
    @XmlElementRef(name = "customer", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Customer> customer;
    @XmlElementRef(name = "customerAccount", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerAccount> customerAccount;
    @XmlElementRef(name = "customerCreditProfile", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerCreditProfile> customerCreditProfile;
    @XmlElementRef(name = "customerOrder", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerOrder> customerOrder;
    @XmlElementRef(name = "customerOrderItem", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerOrderItem> customerOrderItem;
    @XmlElementRef(name = "paymentMethod", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<PaymentMethod> paymentMethod;
    @XmlElementRef(name = "salesChannel", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<SalesChannel> salesChannel;
    @XmlElementRef(name = "contactNumber", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<TelephoneNumber> contactNumber;
    @XmlElementRef(name = "emailContact", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<EmailContact> emailContact;
    @XmlElementRef(name = "timePeriod", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<TimePeriod> timePeriod;
    @XmlElementRef(name = "amount", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Money> amount;
    @XmlElementRef(name = "date", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> date;
    @XmlElementRef(name = "message", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> message;
    @XmlElementRef(name = "url", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> url;

    /**
     * Obtiene el valor de la propiedad productOffering.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ProductOffering }{@code >}
     *     
     */
    public JAXBElement<ProductOffering> getProductOffering() {
        return productOffering;
    }

    /**
     * Define el valor de la propiedad productOffering.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ProductOffering }{@code >}
     *     
     */
    public void setProductOffering(JAXBElement<ProductOffering> value) {
        this.productOffering = value;
    }

    /**
     * Obtiene el valor de la propiedad product.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Product }{@code >}
     *     
     */
    public JAXBElement<Product> getProduct() {
        return product;
    }

    /**
     * Define el valor de la propiedad product.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Product }{@code >}
     *     
     */
    public void setProduct(JAXBElement<Product> value) {
        this.product = value;
    }

    /**
     * Obtiene el valor de la propiedad productOld.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Product }{@code >}
     *     
     */
    public JAXBElement<Product> getProductOld() {
        return productOld;
    }

    /**
     * Define el valor de la propiedad productOld.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Product }{@code >}
     *     
     */
    public void setProductOld(JAXBElement<Product> value) {
        this.productOld = value;
    }

    /**
     * Obtiene el valor de la propiedad loyaltyBalance.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LoyaltyBalance }{@code >}
     *     
     */
    public JAXBElement<LoyaltyBalance> getLoyaltyBalance() {
        return loyaltyBalance;
    }

    /**
     * Define el valor de la propiedad loyaltyBalance.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LoyaltyBalance }{@code >}
     *     
     */
    public void setLoyaltyBalance(JAXBElement<LoyaltyBalance> value) {
        this.loyaltyBalance = value;
    }

    /**
     * Obtiene el valor de la propiedad service.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Service }{@code >}
     *     
     */
    public JAXBElement<Service> getService() {
        return service;
    }

    /**
     * Define el valor de la propiedad service.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Service }{@code >}
     *     
     */
    public void setService(JAXBElement<Service> value) {
        this.service = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceCharacteristicValue.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ServiceCharacteristicValue }{@code >}
     *     
     */
    public JAXBElement<ServiceCharacteristicValue> getServiceCharacteristicValue() {
        return serviceCharacteristicValue;
    }

    /**
     * Define el valor de la propiedad serviceCharacteristicValue.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ServiceCharacteristicValue }{@code >}
     *     
     */
    public void setServiceCharacteristicValue(JAXBElement<ServiceCharacteristicValue> value) {
        this.serviceCharacteristicValue = value;
    }

    /**
     * Obtiene el valor de la propiedad resource.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Resource }{@code >}
     *     
     */
    public JAXBElement<Resource> getResource() {
        return resource;
    }

    /**
     * Define el valor de la propiedad resource.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Resource }{@code >}
     *     
     */
    public void setResource(JAXBElement<Resource> value) {
        this.resource = value;
    }

    /**
     * Obtiene el valor de la propiedad geographicAddress.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GeographicAddress }{@code >}
     *     
     */
    public JAXBElement<GeographicAddress> getGeographicAddress() {
        return geographicAddress;
    }

    /**
     * Define el valor de la propiedad geographicAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GeographicAddress }{@code >}
     *     
     */
    public void setGeographicAddress(JAXBElement<GeographicAddress> value) {
        this.geographicAddress = value;
    }

    /**
     * Obtiene el valor de la propiedad userAbstract.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UserAbstract }{@code >}
     *     
     */
    public JAXBElement<UserAbstract> getUserAbstract() {
        return userAbstract;
    }

    /**
     * Define el valor de la propiedad userAbstract.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UserAbstract }{@code >}
     *     
     */
    public void setUserAbstract(JAXBElement<UserAbstract> value) {
        this.userAbstract = value;
    }

    /**
     * Obtiene el valor de la propiedad documentAbstract.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentAbstract }{@code >}
     *     
     */
    public JAXBElement<DocumentAbstract> getDocumentAbstract() {
        return documentAbstract;
    }

    /**
     * Define el valor de la propiedad documentAbstract.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentAbstract }{@code >}
     *     
     */
    public void setDocumentAbstract(JAXBElement<DocumentAbstract> value) {
        this.documentAbstract = value;
    }

    /**
     * Obtiene el valor de la propiedad activityAbstract.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ActivityAbstract }{@code >}
     *     
     */
    public JAXBElement<ActivityAbstract> getActivityAbstract() {
        return activityAbstract;
    }

    /**
     * Define el valor de la propiedad activityAbstract.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ActivityAbstract }{@code >}
     *     
     */
    public void setActivityAbstract(JAXBElement<ActivityAbstract> value) {
        this.activityAbstract = value;
    }

    /**
     * Obtiene el valor de la propiedad customerIdentification.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}
     *     
     */
    public JAXBElement<TEFChileanIdentificationNumber> getCustomerIdentification() {
        return customerIdentification;
    }

    /**
     * Define el valor de la propiedad customerIdentification.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}
     *     
     */
    public void setCustomerIdentification(JAXBElement<TEFChileanIdentificationNumber> value) {
        this.customerIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad receiverIdentification.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}
     *     
     */
    public JAXBElement<TEFChileanIdentificationNumber> getReceiverIdentification() {
        return receiverIdentification;
    }

    /**
     * Define el valor de la propiedad receiverIdentification.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}
     *     
     */
    public void setReceiverIdentification(JAXBElement<TEFChileanIdentificationNumber> value) {
        this.receiverIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad contactIdentification.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}
     *     
     */
    public JAXBElement<TEFChileanIdentificationNumber> getContactIdentification() {
        return contactIdentification;
    }

    /**
     * Define el valor de la propiedad contactIdentification.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}
     *     
     */
    public void setContactIdentification(JAXBElement<TEFChileanIdentificationNumber> value) {
        this.contactIdentification = value;
    }

    /**
     * Obtiene el valor de la propiedad individualName.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link IndividualName }{@code >}
     *     
     */
    public JAXBElement<IndividualName> getIndividualName() {
        return individualName;
    }

    /**
     * Define el valor de la propiedad individualName.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link IndividualName }{@code >}
     *     
     */
    public void setIndividualName(JAXBElement<IndividualName> value) {
        this.individualName = value;
    }

    /**
     * Obtiene el valor de la propiedad contactMedium.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ContactMedium }{@code >}
     *     
     */
    public JAXBElement<ContactMedium> getContactMedium() {
        return contactMedium;
    }

    /**
     * Define el valor de la propiedad contactMedium.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ContactMedium }{@code >}
     *     
     */
    public void setContactMedium(JAXBElement<ContactMedium> value) {
        this.contactMedium = value;
    }

    /**
     * Obtiene el valor de la propiedad customer.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Customer }{@code >}
     *     
     */
    public JAXBElement<Customer> getCustomer() {
        return customer;
    }

    /**
     * Define el valor de la propiedad customer.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Customer }{@code >}
     *     
     */
    public void setCustomer(JAXBElement<Customer> value) {
        this.customer = value;
    }

    /**
     * Obtiene el valor de la propiedad customerAccount.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerAccount }{@code >}
     *     
     */
    public JAXBElement<CustomerAccount> getCustomerAccount() {
        return customerAccount;
    }

    /**
     * Define el valor de la propiedad customerAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerAccount }{@code >}
     *     
     */
    public void setCustomerAccount(JAXBElement<CustomerAccount> value) {
        this.customerAccount = value;
    }

    /**
     * Obtiene el valor de la propiedad customerCreditProfile.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerCreditProfile }{@code >}
     *     
     */
    public JAXBElement<CustomerCreditProfile> getCustomerCreditProfile() {
        return customerCreditProfile;
    }

    /**
     * Define el valor de la propiedad customerCreditProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerCreditProfile }{@code >}
     *     
     */
    public void setCustomerCreditProfile(JAXBElement<CustomerCreditProfile> value) {
        this.customerCreditProfile = value;
    }

    /**
     * Obtiene el valor de la propiedad customerOrder.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerOrder }{@code >}
     *     
     */
    public JAXBElement<CustomerOrder> getCustomerOrder() {
        return customerOrder;
    }

    /**
     * Define el valor de la propiedad customerOrder.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerOrder }{@code >}
     *     
     */
    public void setCustomerOrder(JAXBElement<CustomerOrder> value) {
        this.customerOrder = value;
    }

    /**
     * Obtiene el valor de la propiedad customerOrderItem.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerOrderItem }{@code >}
     *     
     */
    public JAXBElement<CustomerOrderItem> getCustomerOrderItem() {
        return customerOrderItem;
    }

    /**
     * Define el valor de la propiedad customerOrderItem.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerOrderItem }{@code >}
     *     
     */
    public void setCustomerOrderItem(JAXBElement<CustomerOrderItem> value) {
        this.customerOrderItem = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentMethod.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PaymentMethod }{@code >}
     *     
     */
    public JAXBElement<PaymentMethod> getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Define el valor de la propiedad paymentMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PaymentMethod }{@code >}
     *     
     */
    public void setPaymentMethod(JAXBElement<PaymentMethod> value) {
        this.paymentMethod = value;
    }

    /**
     * Obtiene el valor de la propiedad salesChannel.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SalesChannel }{@code >}
     *     
     */
    public JAXBElement<SalesChannel> getSalesChannel() {
        return salesChannel;
    }

    /**
     * Define el valor de la propiedad salesChannel.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SalesChannel }{@code >}
     *     
     */
    public void setSalesChannel(JAXBElement<SalesChannel> value) {
        this.salesChannel = value;
    }

    /**
     * Obtiene el valor de la propiedad contactNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}
     *     
     */
    public JAXBElement<TelephoneNumber> getContactNumber() {
        return contactNumber;
    }

    /**
     * Define el valor de la propiedad contactNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}
     *     
     */
    public void setContactNumber(JAXBElement<TelephoneNumber> value) {
        this.contactNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad emailContact.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EmailContact }{@code >}
     *     
     */
    public JAXBElement<EmailContact> getEmailContact() {
        return emailContact;
    }

    /**
     * Define el valor de la propiedad emailContact.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EmailContact }{@code >}
     *     
     */
    public void setEmailContact(JAXBElement<EmailContact> value) {
        this.emailContact = value;
    }

    /**
     * Obtiene el valor de la propiedad timePeriod.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimePeriod }{@code >}
     *     
     */
    public JAXBElement<TimePeriod> getTimePeriod() {
        return timePeriod;
    }

    /**
     * Define el valor de la propiedad timePeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimePeriod }{@code >}
     *     
     */
    public void setTimePeriod(JAXBElement<TimePeriod> value) {
        this.timePeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public JAXBElement<Money> getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public void setAmount(JAXBElement<Money> value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad date.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDate() {
        return date;
    }

    /**
     * Define el valor de la propiedad date.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDate(JAXBElement<XMLGregorianCalendar> value) {
        this.date = value;
    }

    /**
     * Obtiene el valor de la propiedad message.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMessage() {
        return message;
    }

    /**
     * Define el valor de la propiedad message.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMessage(JAXBElement<String> value) {
        this.message = value;
    }

    /**
     * Obtiene el valor de la propiedad url.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUrl() {
        return url;
    }

    /**
     * Define el valor de la propiedad url.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUrl(JAXBElement<String> value) {
        this.url = value;
    }

}
