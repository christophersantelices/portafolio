
package cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderResponse;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{ /EnterpriseApplicationIntegration/TEFHeader/V1}TEFHeaderResponse"/&gt;
 *         &lt;element name="data" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}data_res"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "data"
})
@XmlRootElement(name = "SendLegacyEventResponse")
public class SendLegacyEventResponse {

    @XmlElement(required = true)
    protected TEFHeaderResponse header;
    @XmlElement(required = true)
    protected DataRes data;

    /**
     * Obtiene el valor de la propiedad header.
     * 
     * @return
     *     possible object is
     *     {@link TEFHeaderResponse }
     *     
     */
    public TEFHeaderResponse getHeader() {
        return header;
    }

    /**
     * Define el valor de la propiedad header.
     * 
     * @param value
     *     allowed object is
     *     {@link TEFHeaderResponse }
     *     
     */
    public void setHeader(TEFHeaderResponse value) {
        this.header = value;
    }

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataRes }
     *     
     */
    public DataRes getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRes }
     *     
     */
    public void setData(DataRes value) {
        this.data = value;
    }

}
