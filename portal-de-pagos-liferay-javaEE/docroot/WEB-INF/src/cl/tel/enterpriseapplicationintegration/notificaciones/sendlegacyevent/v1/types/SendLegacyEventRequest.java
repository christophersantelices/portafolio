
package cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFHeaderRequest;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{ /EnterpriseApplicationIntegration/TEFHeader/V1}TEFHeaderRequest"/&gt;
 *         &lt;element name="data" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}data_req"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "data"
})
@XmlRootElement(name = "SendLegacyEventRequest")
public class SendLegacyEventRequest {

    @XmlElement(required = true)
    protected TEFHeaderRequest header;
    @XmlElement(required = true)
    protected DataReq data;

    /**
     * Obtiene el valor de la propiedad header.
     * 
     * @return
     *     possible object is
     *     {@link TEFHeaderRequest }
     *     
     */
    public TEFHeaderRequest getHeader() {
        return header;
    }

    /**
     * Define el valor de la propiedad header.
     * 
     * @param value
     *     allowed object is
     *     {@link TEFHeaderRequest }
     *     
     */
    public void setHeader(TEFHeaderRequest value) {
        this.header = value;
    }

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataReq }
     *     
     */
    public DataReq getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataReq }
     *     
     */
    public void setData(DataReq value) {
        this.data = value;
    }

}
