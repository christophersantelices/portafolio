
package cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para LoyaltyBalance complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="LoyaltyBalance"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="points" type="{ /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types}Money" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoyaltyBalance", propOrder = {
    "points"
})
public class LoyaltyBalance {

    @XmlElementRef(name = "points", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Money> points;

    /**
     * Obtiene el valor de la propiedad points.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public JAXBElement<Money> getPoints() {
        return points;
    }

    /**
     * Define el valor de la propiedad points.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Money }{@code >}
     *     
     */
    public void setPoints(JAXBElement<Money> value) {
        this.points = value;
    }

}
