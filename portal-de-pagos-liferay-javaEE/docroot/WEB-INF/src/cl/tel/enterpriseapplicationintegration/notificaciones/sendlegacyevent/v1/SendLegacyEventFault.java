
package cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.1.12
 * 2018-03-08T17:49:21.506-03:00
 * Generated source version: 3.1.12
 */

@WebFault(name = "SendLegacyEventFault", targetNamespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types")
public class SendLegacyEventFault extends Exception {
    
    private cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.SendLegacyEventFault sendLegacyEventFault;

    public SendLegacyEventFault() {
        super();
    }
    
    public SendLegacyEventFault(String message) {
        super(message);
    }
    
    public SendLegacyEventFault(String message, Throwable cause) {
        super(message, cause);
    }

    public SendLegacyEventFault(String message, cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.SendLegacyEventFault sendLegacyEventFault) {
        super(message);
        this.sendLegacyEventFault = sendLegacyEventFault;
    }

    public SendLegacyEventFault(String message, cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.SendLegacyEventFault sendLegacyEventFault, Throwable cause) {
        super(message, cause);
        this.sendLegacyEventFault = sendLegacyEventFault;
    }

    public cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types.SendLegacyEventFault getFaultInfo() {
        return this.sendLegacyEventFault;
    }
}
