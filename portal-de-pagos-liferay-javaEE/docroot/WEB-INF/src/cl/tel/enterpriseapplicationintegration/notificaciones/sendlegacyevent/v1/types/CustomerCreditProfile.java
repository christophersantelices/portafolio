
package cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CustomerCreditProfile complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CustomerCreditProfile"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="creditScore" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="creditClass" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="4"/&gt;
 *               &lt;whiteSpace value="preserve"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="creditRiskRating" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="creditLimit" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="creditLimitIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerCreditProfile", propOrder = {
    "creditScore",
    "creditClass",
    "creditRiskRating",
    "creditLimit",
    "creditLimitIndicator"
})
public class CustomerCreditProfile {

    @XmlElementRef(name = "creditScore", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> creditScore;
    @XmlElementRef(name = "creditClass", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditClass;
    @XmlElementRef(name = "creditRiskRating", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> creditRiskRating;
    @XmlElementRef(name = "creditLimit", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> creditLimit;
    @XmlElementRef(name = "creditLimitIndicator", namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> creditLimitIndicator;

    /**
     * Obtiene el valor de la propiedad creditScore.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getCreditScore() {
        return creditScore;
    }

    /**
     * Define el valor de la propiedad creditScore.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setCreditScore(JAXBElement<BigInteger> value) {
        this.creditScore = value;
    }

    /**
     * Obtiene el valor de la propiedad creditClass.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreditClass() {
        return creditClass;
    }

    /**
     * Define el valor de la propiedad creditClass.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreditClass(JAXBElement<String> value) {
        this.creditClass = value;
    }

    /**
     * Obtiene el valor de la propiedad creditRiskRating.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getCreditRiskRating() {
        return creditRiskRating;
    }

    /**
     * Define el valor de la propiedad creditRiskRating.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setCreditRiskRating(JAXBElement<BigInteger> value) {
        this.creditRiskRating = value;
    }

    /**
     * Obtiene el valor de la propiedad creditLimit.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getCreditLimit() {
        return creditLimit;
    }

    /**
     * Define el valor de la propiedad creditLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setCreditLimit(JAXBElement<Long> value) {
        this.creditLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad creditLimitIndicator.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getCreditLimitIndicator() {
        return creditLimitIndicator;
    }

    /**
     * Define el valor de la propiedad creditLimitIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setCreditLimitIndicator(JAXBElement<Boolean> value) {
        this.creditLimitIndicator = value;
    }

}
