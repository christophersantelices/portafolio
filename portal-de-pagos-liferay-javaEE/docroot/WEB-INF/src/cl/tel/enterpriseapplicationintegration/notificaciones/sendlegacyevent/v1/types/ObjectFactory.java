
package cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import cl.telefonica.enterpriseapplicationintegration.tefheader.v1.TEFChileanIdentificationNumber;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StatusAbstractCode_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "code");
    private final static QName _StatusAbstractDescription_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "description");
    private final static QName _MoneyUnits_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "units");
    private final static QName _SalesChannelName_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "name");
    private final static QName _PaymentMethodID_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "ID");
    private final static QName _PaymentMethodPaymentMethodType_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "paymentMethodType");
    private final static QName _PaymentMethodTransactionType_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "transactionType");
    private final static QName _PaymentMethodBankName_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "bankName");
    private final static QName _PaymentMethodAmount_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "amount");
    private final static QName _PaymentMethodTransactionCode_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "transactionCode");
    private final static QName _PaymentMethodQuotaType_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "quotaType");
    private final static QName _PaymentMethodNumberOfQuotas_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "numberOfQuotas");
    private final static QName _PaymentMethodQuotaNumber_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "quotaNumber");
    private final static QName _PaymentMethodAuthorizationCode_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "authorizationCode");
    private final static QName _PaymentMethodCreditCardType_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "creditCardType");
    private final static QName _PaymentMethodSecurityCode_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "securityCode");
    private final static QName _CustomerOrderItemOrderActionID_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "orderActionID");
    private final static QName _CustomerOrderOrderID_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "orderID");
    private final static QName _CustomerOrderCustomerOrderType_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "customerOrderType");
    private final static QName _CustomerOrderAction_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "action");
    private final static QName _CustomerOrderIsForPortability_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "isForPortability");
    private final static QName _CustomerCreditProfileCreditScore_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "creditScore");
    private final static QName _CustomerCreditProfileCreditClass_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "creditClass");
    private final static QName _CustomerCreditProfileCreditRiskRating_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "creditRiskRating");
    private final static QName _CustomerCreditProfileCreditLimit_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "creditLimit");
    private final static QName _CustomerCreditProfileCreditLimitIndicator_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "creditLimitIndicator");
    private final static QName _CustomerAccountBillCycle_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "billCycle");
    private final static QName _CustomerMsisdn_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "msisdn");
    private final static QName _IndividualNameGivenNames_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "givenNames");
    private final static QName _IndividualNameFamilyNames_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "familyNames");
    private final static QName _IndividualNameFullName_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "fullName");
    private final static QName _TelephoneNumberNumber_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "number");
    private final static QName _EmailContactEMailAddress_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "eMailAddress");
    private final static QName _ActivityAbstractStartTime_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "startTime");
    private final static QName _ActivityAbstractEndTime_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "endTime");
    private final static QName _ActivityAbstractDayOfMonth_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "dayOfMonth");
    private final static QName _ActivityAbstractReasonCode_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "reasonCode");
    private final static QName _ActivityAbstractDuration_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "duration");
    private final static QName _ActivityAbstractFrequency_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "frequency");
    private final static QName _DocumentAbstractDocumentID_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "documentID");
    private final static QName _DocumentAbstractDocumentType_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "documentType");
    private final static QName _DocumentAbstractDocumentFormat_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "documentFormat");
    private final static QName _UserAbstractUserName_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "userName");
    private final static QName _UserAbstractPassword_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "password");
    private final static QName _TimePeriodStartDateTime_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "startDateTime");
    private final static QName _TimePeriodEndDateTime_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "endDateTime");
    private final static QName _GeographicAddressStreetName_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "streetName");
    private final static QName _GeographicAddressStreetNrFirst_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "streetNrFirst");
    private final static QName _GeographicAddressAparment_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "aparment");
    private final static QName _GeographicAddressFloor_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "floor");
    private final static QName _GeographicAddressVillage_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "village");
    private final static QName _GeographicAddressCommune_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "commune");
    private final static QName _GeographicAddressRegion_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "region");
    private final static QName _GeographicAddressCity_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "city");
    private final static QName _GeographicAddressCountry_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "country");
    private final static QName _GeographicAddressComments_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "comments");
    private final static QName _GeographicAddressFullAddress_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "fullAddress");
    private final static QName _ResourceImei_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "imei");
    private final static QName _ResourceIccid_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "iccid");
    private final static QName _ServiceCharacteristicValueValue_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "value");
    private final static QName _LoyaltyBalancePoints_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "points");
    private final static QName _ProductId_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "id");
    private final static QName _ProductProductType_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "productType");
    private final static QName _ProductBrand_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "brand");
    private final static QName _ProductModel_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "model");
    private final static QName _ProductDiscount_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "discount");
    private final static QName _ProductOfferingPrice_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "price");
    private final static QName _TemplateParametersProductOffering_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "productOffering");
    private final static QName _TemplateParametersProduct_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "product");
    private final static QName _TemplateParametersProductOld_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "productOld");
    private final static QName _TemplateParametersLoyaltyBalance_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "loyaltyBalance");
    private final static QName _TemplateParametersService_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "service");
    private final static QName _TemplateParametersServiceCharacteristicValue_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "serviceCharacteristicValue");
    private final static QName _TemplateParametersResource_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "resource");
    private final static QName _TemplateParametersGeographicAddress_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "geographicAddress");
    private final static QName _TemplateParametersUserAbstract_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "userAbstract");
    private final static QName _TemplateParametersDocumentAbstract_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "documentAbstract");
    private final static QName _TemplateParametersActivityAbstract_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "activityAbstract");
    private final static QName _TemplateParametersCustomerIdentification_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "customerIdentification");
    private final static QName _TemplateParametersReceiverIdentification_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "receiverIdentification");
    private final static QName _TemplateParametersContactIdentification_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "contactIdentification");
    private final static QName _TemplateParametersIndividualName_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "individualName");
    private final static QName _TemplateParametersContactMedium_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "contactMedium");
    private final static QName _TemplateParametersCustomer_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "customer");
    private final static QName _TemplateParametersCustomerAccount_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "customerAccount");
    private final static QName _TemplateParametersCustomerCreditProfile_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "customerCreditProfile");
    private final static QName _TemplateParametersCustomerOrder_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "customerOrder");
    private final static QName _TemplateParametersCustomerOrderItem_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "customerOrderItem");
    private final static QName _TemplateParametersPaymentMethod_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "paymentMethod");
    private final static QName _TemplateParametersSalesChannel_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "salesChannel");
    private final static QName _TemplateParametersContactNumber_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "contactNumber");
    private final static QName _TemplateParametersEmailContact_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "emailContact");
    private final static QName _TemplateParametersTimePeriod_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "timePeriod");
    private final static QName _TemplateParametersDate_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "date");
    private final static QName _TemplateParametersMessage_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "message");
    private final static QName _TemplateParametersUrl_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "url");
    private final static QName _DataReqNotificationId_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "notificationId");
    private final static QName _DataReqExternalID_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "externalID");
    private final static QName _DataReqPreferredContactMedia_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "preferredContactMedia");
    private final static QName _DataReqEmail_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "email");
    private final static QName _DataReqTelephoneNumber_QNAME = new QName(" /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", "telephoneNumber");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.telefonica.enterpriseapplicationintegration.notificaciones.sendlegacyevent.v1.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendLegacyEventRequest }
     * 
     */
    public SendLegacyEventRequest createSendLegacyEventRequest() {
        return new SendLegacyEventRequest();
    }

    /**
     * Create an instance of {@link DataReq }
     * 
     */
    public DataReq createDataReq() {
        return new DataReq();
    }

    /**
     * Create an instance of {@link SendLegacyEventResponse }
     * 
     */
    public SendLegacyEventResponse createSendLegacyEventResponse() {
        return new SendLegacyEventResponse();
    }

    /**
     * Create an instance of {@link DataRes }
     * 
     */
    public DataRes createDataRes() {
        return new DataRes();
    }

    /**
     * Create an instance of {@link SendLegacyEventFault }
     * 
     */
    public SendLegacyEventFault createSendLegacyEventFault() {
        return new SendLegacyEventFault();
    }

    /**
     * Create an instance of {@link TemplateParameters }
     * 
     */
    public TemplateParameters createTemplateParameters() {
        return new TemplateParameters();
    }

    /**
     * Create an instance of {@link ProductOffering }
     * 
     */
    public ProductOffering createProductOffering() {
        return new ProductOffering();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link LoyaltyBalance }
     * 
     */
    public LoyaltyBalance createLoyaltyBalance() {
        return new LoyaltyBalance();
    }

    /**
     * Create an instance of {@link Service }
     * 
     */
    public Service createService() {
        return new Service();
    }

    /**
     * Create an instance of {@link ServiceCharacteristicValue }
     * 
     */
    public ServiceCharacteristicValue createServiceCharacteristicValue() {
        return new ServiceCharacteristicValue();
    }

    /**
     * Create an instance of {@link Resource }
     * 
     */
    public Resource createResource() {
        return new Resource();
    }

    /**
     * Create an instance of {@link GeographicAddress }
     * 
     */
    public GeographicAddress createGeographicAddress() {
        return new GeographicAddress();
    }

    /**
     * Create an instance of {@link TimePeriod }
     * 
     */
    public TimePeriod createTimePeriod() {
        return new TimePeriod();
    }

    /**
     * Create an instance of {@link UserAbstract }
     * 
     */
    public UserAbstract createUserAbstract() {
        return new UserAbstract();
    }

    /**
     * Create an instance of {@link DocumentAbstract }
     * 
     */
    public DocumentAbstract createDocumentAbstract() {
        return new DocumentAbstract();
    }

    /**
     * Create an instance of {@link ActivityAbstract }
     * 
     */
    public ActivityAbstract createActivityAbstract() {
        return new ActivityAbstract();
    }

    /**
     * Create an instance of {@link EmailContact }
     * 
     */
    public EmailContact createEmailContact() {
        return new EmailContact();
    }

    /**
     * Create an instance of {@link TelephoneNumber }
     * 
     */
    public TelephoneNumber createTelephoneNumber() {
        return new TelephoneNumber();
    }

    /**
     * Create an instance of {@link IndividualName }
     * 
     */
    public IndividualName createIndividualName() {
        return new IndividualName();
    }

    /**
     * Create an instance of {@link ContactMedium }
     * 
     */
    public ContactMedium createContactMedium() {
        return new ContactMedium();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link CustomerAccount }
     * 
     */
    public CustomerAccount createCustomerAccount() {
        return new CustomerAccount();
    }

    /**
     * Create an instance of {@link CustomerCreditProfile }
     * 
     */
    public CustomerCreditProfile createCustomerCreditProfile() {
        return new CustomerCreditProfile();
    }

    /**
     * Create an instance of {@link CustomerOrder }
     * 
     */
    public CustomerOrder createCustomerOrder() {
        return new CustomerOrder();
    }

    /**
     * Create an instance of {@link CustomerOrderItem }
     * 
     */
    public CustomerOrderItem createCustomerOrderItem() {
        return new CustomerOrderItem();
    }

    /**
     * Create an instance of {@link PaymentMethod }
     * 
     */
    public PaymentMethod createPaymentMethod() {
        return new PaymentMethod();
    }

    /**
     * Create an instance of {@link SalesChannel }
     * 
     */
    public SalesChannel createSalesChannel() {
        return new SalesChannel();
    }

    /**
     * Create an instance of {@link Money }
     * 
     */
    public Money createMoney() {
        return new Money();
    }

    /**
     * Create an instance of {@link StatusAbstract }
     * 
     */
    public StatusAbstract createStatusAbstract() {
        return new StatusAbstract();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "code", scope = StatusAbstract.class)
    public JAXBElement<String> createStatusAbstractCode(String value) {
        return new JAXBElement<String>(_StatusAbstractCode_QNAME, String.class, StatusAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "description", scope = StatusAbstract.class)
    public JAXBElement<String> createStatusAbstractDescription(String value) {
        return new JAXBElement<String>(_StatusAbstractDescription_QNAME, String.class, StatusAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "units", scope = Money.class)
    public JAXBElement<String> createMoneyUnits(String value) {
        return new JAXBElement<String>(_MoneyUnits_QNAME, String.class, Money.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "name", scope = SalesChannel.class)
    public JAXBElement<String> createSalesChannelName(String value) {
        return new JAXBElement<String>(_SalesChannelName_QNAME, String.class, SalesChannel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "ID", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodID(String value) {
        return new JAXBElement<String>(_PaymentMethodID_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "paymentMethodType", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodPaymentMethodType(String value) {
        return new JAXBElement<String>(_PaymentMethodPaymentMethodType_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "transactionType", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodTransactionType(String value) {
        return new JAXBElement<String>(_PaymentMethodTransactionType_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "bankName", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodBankName(String value) {
        return new JAXBElement<String>(_PaymentMethodBankName_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "amount", scope = PaymentMethod.class)
    public JAXBElement<Money> createPaymentMethodAmount(Money value) {
        return new JAXBElement<Money>(_PaymentMethodAmount_QNAME, Money.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "transactionCode", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodTransactionCode(String value) {
        return new JAXBElement<String>(_PaymentMethodTransactionCode_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "quotaType", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodQuotaType(String value) {
        return new JAXBElement<String>(_PaymentMethodQuotaType_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "numberOfQuotas", scope = PaymentMethod.class)
    public JAXBElement<BigInteger> createPaymentMethodNumberOfQuotas(BigInteger value) {
        return new JAXBElement<BigInteger>(_PaymentMethodNumberOfQuotas_QNAME, BigInteger.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "quotaNumber", scope = PaymentMethod.class)
    public JAXBElement<BigInteger> createPaymentMethodQuotaNumber(BigInteger value) {
        return new JAXBElement<BigInteger>(_PaymentMethodQuotaNumber_QNAME, BigInteger.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "authorizationCode", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodAuthorizationCode(String value) {
        return new JAXBElement<String>(_PaymentMethodAuthorizationCode_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "creditCardType", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodCreditCardType(String value) {
        return new JAXBElement<String>(_PaymentMethodCreditCardType_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "securityCode", scope = PaymentMethod.class)
    public JAXBElement<String> createPaymentMethodSecurityCode(String value) {
        return new JAXBElement<String>(_PaymentMethodSecurityCode_QNAME, String.class, PaymentMethod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "orderActionID", scope = CustomerOrderItem.class)
    public JAXBElement<String> createCustomerOrderItemOrderActionID(String value) {
        return new JAXBElement<String>(_CustomerOrderItemOrderActionID_QNAME, String.class, CustomerOrderItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "orderID", scope = CustomerOrder.class)
    public JAXBElement<String> createCustomerOrderOrderID(String value) {
        return new JAXBElement<String>(_CustomerOrderOrderID_QNAME, String.class, CustomerOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "customerOrderType", scope = CustomerOrder.class)
    public JAXBElement<String> createCustomerOrderCustomerOrderType(String value) {
        return new JAXBElement<String>(_CustomerOrderCustomerOrderType_QNAME, String.class, CustomerOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "action", scope = CustomerOrder.class)
    public JAXBElement<String> createCustomerOrderAction(String value) {
        return new JAXBElement<String>(_CustomerOrderAction_QNAME, String.class, CustomerOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "isForPortability", scope = CustomerOrder.class)
    public JAXBElement<Boolean> createCustomerOrderIsForPortability(Boolean value) {
        return new JAXBElement<Boolean>(_CustomerOrderIsForPortability_QNAME, Boolean.class, CustomerOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "creditScore", scope = CustomerCreditProfile.class)
    public JAXBElement<BigInteger> createCustomerCreditProfileCreditScore(BigInteger value) {
        return new JAXBElement<BigInteger>(_CustomerCreditProfileCreditScore_QNAME, BigInteger.class, CustomerCreditProfile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "creditClass", scope = CustomerCreditProfile.class)
    public JAXBElement<String> createCustomerCreditProfileCreditClass(String value) {
        return new JAXBElement<String>(_CustomerCreditProfileCreditClass_QNAME, String.class, CustomerCreditProfile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "creditRiskRating", scope = CustomerCreditProfile.class)
    public JAXBElement<BigInteger> createCustomerCreditProfileCreditRiskRating(BigInteger value) {
        return new JAXBElement<BigInteger>(_CustomerCreditProfileCreditRiskRating_QNAME, BigInteger.class, CustomerCreditProfile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "creditLimit", scope = CustomerCreditProfile.class)
    public JAXBElement<Long> createCustomerCreditProfileCreditLimit(Long value) {
        return new JAXBElement<Long>(_CustomerCreditProfileCreditLimit_QNAME, Long.class, CustomerCreditProfile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "creditLimitIndicator", scope = CustomerCreditProfile.class)
    public JAXBElement<Boolean> createCustomerCreditProfileCreditLimitIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_CustomerCreditProfileCreditLimitIndicator_QNAME, Boolean.class, CustomerCreditProfile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "ID", scope = CustomerAccount.class)
    public JAXBElement<String> createCustomerAccountID(String value) {
        return new JAXBElement<String>(_PaymentMethodID_QNAME, String.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "billCycle", scope = CustomerAccount.class)
    public JAXBElement<BigInteger> createCustomerAccountBillCycle(BigInteger value) {
        return new JAXBElement<BigInteger>(_CustomerAccountBillCycle_QNAME, BigInteger.class, CustomerAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "ID", scope = Customer.class)
    public JAXBElement<String> createCustomerID(String value) {
        return new JAXBElement<String>(_PaymentMethodID_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "msisdn", scope = Customer.class)
    public JAXBElement<String> createCustomerMsisdn(String value) {
        return new JAXBElement<String>(_CustomerMsisdn_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "name", scope = ContactMedium.class)
    public JAXBElement<String> createContactMediumName(String value) {
        return new JAXBElement<String>(_SalesChannelName_QNAME, String.class, ContactMedium.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "givenNames", scope = IndividualName.class)
    public JAXBElement<String> createIndividualNameGivenNames(String value) {
        return new JAXBElement<String>(_IndividualNameGivenNames_QNAME, String.class, IndividualName.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "familyNames", scope = IndividualName.class)
    public JAXBElement<String> createIndividualNameFamilyNames(String value) {
        return new JAXBElement<String>(_IndividualNameFamilyNames_QNAME, String.class, IndividualName.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "fullName", scope = IndividualName.class)
    public JAXBElement<String> createIndividualNameFullName(String value) {
        return new JAXBElement<String>(_IndividualNameFullName_QNAME, String.class, IndividualName.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "number", scope = TelephoneNumber.class)
    public JAXBElement<String> createTelephoneNumberNumber(String value) {
        return new JAXBElement<String>(_TelephoneNumberNumber_QNAME, String.class, TelephoneNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "eMailAddress", scope = EmailContact.class)
    public JAXBElement<String> createEmailContactEMailAddress(String value) {
        return new JAXBElement<String>(_EmailContactEMailAddress_QNAME, String.class, EmailContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "startTime", scope = ActivityAbstract.class)
    public JAXBElement<XMLGregorianCalendar> createActivityAbstractStartTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ActivityAbstractStartTime_QNAME, XMLGregorianCalendar.class, ActivityAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "endTime", scope = ActivityAbstract.class)
    public JAXBElement<XMLGregorianCalendar> createActivityAbstractEndTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ActivityAbstractEndTime_QNAME, XMLGregorianCalendar.class, ActivityAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "dayOfMonth", scope = ActivityAbstract.class)
    public JAXBElement<BigInteger> createActivityAbstractDayOfMonth(BigInteger value) {
        return new JAXBElement<BigInteger>(_ActivityAbstractDayOfMonth_QNAME, BigInteger.class, ActivityAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "reasonCode", scope = ActivityAbstract.class)
    public JAXBElement<String> createActivityAbstractReasonCode(String value) {
        return new JAXBElement<String>(_ActivityAbstractReasonCode_QNAME, String.class, ActivityAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "duration", scope = ActivityAbstract.class)
    public JAXBElement<BigInteger> createActivityAbstractDuration(BigInteger value) {
        return new JAXBElement<BigInteger>(_ActivityAbstractDuration_QNAME, BigInteger.class, ActivityAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "frequency", scope = ActivityAbstract.class)
    public JAXBElement<String> createActivityAbstractFrequency(String value) {
        return new JAXBElement<String>(_ActivityAbstractFrequency_QNAME, String.class, ActivityAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "documentID", scope = DocumentAbstract.class)
    public JAXBElement<String> createDocumentAbstractDocumentID(String value) {
        return new JAXBElement<String>(_DocumentAbstractDocumentID_QNAME, String.class, DocumentAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "documentType", scope = DocumentAbstract.class)
    public JAXBElement<String> createDocumentAbstractDocumentType(String value) {
        return new JAXBElement<String>(_DocumentAbstractDocumentType_QNAME, String.class, DocumentAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "documentFormat", scope = DocumentAbstract.class)
    public JAXBElement<String> createDocumentAbstractDocumentFormat(String value) {
        return new JAXBElement<String>(_DocumentAbstractDocumentFormat_QNAME, String.class, DocumentAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "userName", scope = UserAbstract.class)
    public JAXBElement<String> createUserAbstractUserName(String value) {
        return new JAXBElement<String>(_UserAbstractUserName_QNAME, String.class, UserAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "password", scope = UserAbstract.class)
    public JAXBElement<String> createUserAbstractPassword(String value) {
        return new JAXBElement<String>(_UserAbstractPassword_QNAME, String.class, UserAbstract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "startDateTime", scope = TimePeriod.class)
    public JAXBElement<XMLGregorianCalendar> createTimePeriodStartDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TimePeriodStartDateTime_QNAME, XMLGregorianCalendar.class, TimePeriod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "endDateTime", scope = TimePeriod.class)
    public JAXBElement<XMLGregorianCalendar> createTimePeriodEndDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TimePeriodEndDateTime_QNAME, XMLGregorianCalendar.class, TimePeriod.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "streetName", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressStreetName(String value) {
        return new JAXBElement<String>(_GeographicAddressStreetName_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "streetNrFirst", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressStreetNrFirst(String value) {
        return new JAXBElement<String>(_GeographicAddressStreetNrFirst_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "aparment", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressAparment(String value) {
        return new JAXBElement<String>(_GeographicAddressAparment_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "floor", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressFloor(String value) {
        return new JAXBElement<String>(_GeographicAddressFloor_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "village", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressVillage(String value) {
        return new JAXBElement<String>(_GeographicAddressVillage_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "commune", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressCommune(String value) {
        return new JAXBElement<String>(_GeographicAddressCommune_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "region", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressRegion(String value) {
        return new JAXBElement<String>(_GeographicAddressRegion_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "city", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressCity(String value) {
        return new JAXBElement<String>(_GeographicAddressCity_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "country", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressCountry(String value) {
        return new JAXBElement<String>(_GeographicAddressCountry_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "comments", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressComments(String value) {
        return new JAXBElement<String>(_GeographicAddressComments_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "fullAddress", scope = GeographicAddress.class)
    public JAXBElement<String> createGeographicAddressFullAddress(String value) {
        return new JAXBElement<String>(_GeographicAddressFullAddress_QNAME, String.class, GeographicAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "imei", scope = Resource.class)
    public JAXBElement<String> createResourceImei(String value) {
        return new JAXBElement<String>(_ResourceImei_QNAME, String.class, Resource.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "iccid", scope = Resource.class)
    public JAXBElement<String> createResourceIccid(String value) {
        return new JAXBElement<String>(_ResourceIccid_QNAME, String.class, Resource.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "name", scope = ServiceCharacteristicValue.class)
    public JAXBElement<String> createServiceCharacteristicValueName(String value) {
        return new JAXBElement<String>(_SalesChannelName_QNAME, String.class, ServiceCharacteristicValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "value", scope = ServiceCharacteristicValue.class)
    public JAXBElement<String> createServiceCharacteristicValueValue(String value) {
        return new JAXBElement<String>(_ServiceCharacteristicValueValue_QNAME, String.class, ServiceCharacteristicValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "code", scope = Service.class)
    public JAXBElement<String> createServiceCode(String value) {
        return new JAXBElement<String>(_StatusAbstractCode_QNAME, String.class, Service.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "name", scope = Service.class)
    public JAXBElement<String> createServiceName(String value) {
        return new JAXBElement<String>(_SalesChannelName_QNAME, String.class, Service.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "points", scope = LoyaltyBalance.class)
    public JAXBElement<Money> createLoyaltyBalancePoints(Money value) {
        return new JAXBElement<Money>(_LoyaltyBalancePoints_QNAME, Money.class, LoyaltyBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "id", scope = Product.class)
    public JAXBElement<String> createProductId(String value) {
        return new JAXBElement<String>(_ProductId_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "name", scope = Product.class)
    public JAXBElement<String> createProductName(String value) {
        return new JAXBElement<String>(_SalesChannelName_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "description", scope = Product.class)
    public JAXBElement<String> createProductDescription(String value) {
        return new JAXBElement<String>(_StatusAbstractDescription_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "productType", scope = Product.class)
    public JAXBElement<String> createProductProductType(String value) {
        return new JAXBElement<String>(_ProductProductType_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "brand", scope = Product.class)
    public JAXBElement<String> createProductBrand(String value) {
        return new JAXBElement<String>(_ProductBrand_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "model", scope = Product.class)
    public JAXBElement<String> createProductModel(String value) {
        return new JAXBElement<String>(_ProductModel_QNAME, String.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "discount", scope = Product.class)
    public JAXBElement<Money> createProductDiscount(Money value) {
        return new JAXBElement<Money>(_ProductDiscount_QNAME, Money.class, Product.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "id", scope = ProductOffering.class)
    public JAXBElement<String> createProductOfferingId(String value) {
        return new JAXBElement<String>(_ProductId_QNAME, String.class, ProductOffering.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "name", scope = ProductOffering.class)
    public JAXBElement<String> createProductOfferingName(String value) {
        return new JAXBElement<String>(_SalesChannelName_QNAME, String.class, ProductOffering.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "description", scope = ProductOffering.class)
    public JAXBElement<String> createProductOfferingDescription(String value) {
        return new JAXBElement<String>(_StatusAbstractDescription_QNAME, String.class, ProductOffering.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "discount", scope = ProductOffering.class)
    public JAXBElement<Money> createProductOfferingDiscount(Money value) {
        return new JAXBElement<Money>(_ProductDiscount_QNAME, Money.class, ProductOffering.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "duration", scope = ProductOffering.class)
    public JAXBElement<BigInteger> createProductOfferingDuration(BigInteger value) {
        return new JAXBElement<BigInteger>(_ActivityAbstractDuration_QNAME, BigInteger.class, ProductOffering.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "price", scope = ProductOffering.class)
    public JAXBElement<Money> createProductOfferingPrice(Money value) {
        return new JAXBElement<Money>(_ProductOfferingPrice_QNAME, Money.class, ProductOffering.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductOffering }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "productOffering", scope = TemplateParameters.class)
    public JAXBElement<ProductOffering> createTemplateParametersProductOffering(ProductOffering value) {
        return new JAXBElement<ProductOffering>(_TemplateParametersProductOffering_QNAME, ProductOffering.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Product }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "product", scope = TemplateParameters.class)
    public JAXBElement<Product> createTemplateParametersProduct(Product value) {
        return new JAXBElement<Product>(_TemplateParametersProduct_QNAME, Product.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Product }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "productOld", scope = TemplateParameters.class)
    public JAXBElement<Product> createTemplateParametersProductOld(Product value) {
        return new JAXBElement<Product>(_TemplateParametersProductOld_QNAME, Product.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoyaltyBalance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "loyaltyBalance", scope = TemplateParameters.class)
    public JAXBElement<LoyaltyBalance> createTemplateParametersLoyaltyBalance(LoyaltyBalance value) {
        return new JAXBElement<LoyaltyBalance>(_TemplateParametersLoyaltyBalance_QNAME, LoyaltyBalance.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Service }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "service", scope = TemplateParameters.class)
    public JAXBElement<Service> createTemplateParametersService(Service value) {
        return new JAXBElement<Service>(_TemplateParametersService_QNAME, Service.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceCharacteristicValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "serviceCharacteristicValue", scope = TemplateParameters.class)
    public JAXBElement<ServiceCharacteristicValue> createTemplateParametersServiceCharacteristicValue(ServiceCharacteristicValue value) {
        return new JAXBElement<ServiceCharacteristicValue>(_TemplateParametersServiceCharacteristicValue_QNAME, ServiceCharacteristicValue.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Resource }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "resource", scope = TemplateParameters.class)
    public JAXBElement<Resource> createTemplateParametersResource(Resource value) {
        return new JAXBElement<Resource>(_TemplateParametersResource_QNAME, Resource.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeographicAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "geographicAddress", scope = TemplateParameters.class)
    public JAXBElement<GeographicAddress> createTemplateParametersGeographicAddress(GeographicAddress value) {
        return new JAXBElement<GeographicAddress>(_TemplateParametersGeographicAddress_QNAME, GeographicAddress.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserAbstract }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "userAbstract", scope = TemplateParameters.class)
    public JAXBElement<UserAbstract> createTemplateParametersUserAbstract(UserAbstract value) {
        return new JAXBElement<UserAbstract>(_TemplateParametersUserAbstract_QNAME, UserAbstract.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentAbstract }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "documentAbstract", scope = TemplateParameters.class)
    public JAXBElement<DocumentAbstract> createTemplateParametersDocumentAbstract(DocumentAbstract value) {
        return new JAXBElement<DocumentAbstract>(_TemplateParametersDocumentAbstract_QNAME, DocumentAbstract.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityAbstract }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "activityAbstract", scope = TemplateParameters.class)
    public JAXBElement<ActivityAbstract> createTemplateParametersActivityAbstract(ActivityAbstract value) {
        return new JAXBElement<ActivityAbstract>(_TemplateParametersActivityAbstract_QNAME, ActivityAbstract.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "customerIdentification", scope = TemplateParameters.class)
    public JAXBElement<TEFChileanIdentificationNumber> createTemplateParametersCustomerIdentification(TEFChileanIdentificationNumber value) {
        return new JAXBElement<TEFChileanIdentificationNumber>(_TemplateParametersCustomerIdentification_QNAME, TEFChileanIdentificationNumber.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "receiverIdentification", scope = TemplateParameters.class)
    public JAXBElement<TEFChileanIdentificationNumber> createTemplateParametersReceiverIdentification(TEFChileanIdentificationNumber value) {
        return new JAXBElement<TEFChileanIdentificationNumber>(_TemplateParametersReceiverIdentification_QNAME, TEFChileanIdentificationNumber.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEFChileanIdentificationNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "contactIdentification", scope = TemplateParameters.class)
    public JAXBElement<TEFChileanIdentificationNumber> createTemplateParametersContactIdentification(TEFChileanIdentificationNumber value) {
        return new JAXBElement<TEFChileanIdentificationNumber>(_TemplateParametersContactIdentification_QNAME, TEFChileanIdentificationNumber.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndividualName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "individualName", scope = TemplateParameters.class)
    public JAXBElement<IndividualName> createTemplateParametersIndividualName(IndividualName value) {
        return new JAXBElement<IndividualName>(_TemplateParametersIndividualName_QNAME, IndividualName.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactMedium }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "contactMedium", scope = TemplateParameters.class)
    public JAXBElement<ContactMedium> createTemplateParametersContactMedium(ContactMedium value) {
        return new JAXBElement<ContactMedium>(_TemplateParametersContactMedium_QNAME, ContactMedium.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "customer", scope = TemplateParameters.class)
    public JAXBElement<Customer> createTemplateParametersCustomer(Customer value) {
        return new JAXBElement<Customer>(_TemplateParametersCustomer_QNAME, Customer.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "customerAccount", scope = TemplateParameters.class)
    public JAXBElement<CustomerAccount> createTemplateParametersCustomerAccount(CustomerAccount value) {
        return new JAXBElement<CustomerAccount>(_TemplateParametersCustomerAccount_QNAME, CustomerAccount.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerCreditProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "customerCreditProfile", scope = TemplateParameters.class)
    public JAXBElement<CustomerCreditProfile> createTemplateParametersCustomerCreditProfile(CustomerCreditProfile value) {
        return new JAXBElement<CustomerCreditProfile>(_TemplateParametersCustomerCreditProfile_QNAME, CustomerCreditProfile.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "customerOrder", scope = TemplateParameters.class)
    public JAXBElement<CustomerOrder> createTemplateParametersCustomerOrder(CustomerOrder value) {
        return new JAXBElement<CustomerOrder>(_TemplateParametersCustomerOrder_QNAME, CustomerOrder.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerOrderItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "customerOrderItem", scope = TemplateParameters.class)
    public JAXBElement<CustomerOrderItem> createTemplateParametersCustomerOrderItem(CustomerOrderItem value) {
        return new JAXBElement<CustomerOrderItem>(_TemplateParametersCustomerOrderItem_QNAME, CustomerOrderItem.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "paymentMethod", scope = TemplateParameters.class)
    public JAXBElement<PaymentMethod> createTemplateParametersPaymentMethod(PaymentMethod value) {
        return new JAXBElement<PaymentMethod>(_TemplateParametersPaymentMethod_QNAME, PaymentMethod.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesChannel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "salesChannel", scope = TemplateParameters.class)
    public JAXBElement<SalesChannel> createTemplateParametersSalesChannel(SalesChannel value) {
        return new JAXBElement<SalesChannel>(_TemplateParametersSalesChannel_QNAME, SalesChannel.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "contactNumber", scope = TemplateParameters.class)
    public JAXBElement<TelephoneNumber> createTemplateParametersContactNumber(TelephoneNumber value) {
        return new JAXBElement<TelephoneNumber>(_TemplateParametersContactNumber_QNAME, TelephoneNumber.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "emailContact", scope = TemplateParameters.class)
    public JAXBElement<EmailContact> createTemplateParametersEmailContact(EmailContact value) {
        return new JAXBElement<EmailContact>(_TemplateParametersEmailContact_QNAME, EmailContact.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TimePeriod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "timePeriod", scope = TemplateParameters.class)
    public JAXBElement<TimePeriod> createTemplateParametersTimePeriod(TimePeriod value) {
        return new JAXBElement<TimePeriod>(_TemplateParametersTimePeriod_QNAME, TimePeriod.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "amount", scope = TemplateParameters.class)
    public JAXBElement<Money> createTemplateParametersAmount(Money value) {
        return new JAXBElement<Money>(_PaymentMethodAmount_QNAME, Money.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "date", scope = TemplateParameters.class)
    public JAXBElement<XMLGregorianCalendar> createTemplateParametersDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TemplateParametersDate_QNAME, XMLGregorianCalendar.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "message", scope = TemplateParameters.class)
    public JAXBElement<String> createTemplateParametersMessage(String value) {
        return new JAXBElement<String>(_TemplateParametersMessage_QNAME, String.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "url", scope = TemplateParameters.class)
    public JAXBElement<String> createTemplateParametersUrl(String value) {
        return new JAXBElement<String>(_TemplateParametersUrl_QNAME, String.class, TemplateParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "notificationId", scope = DataReq.class)
    public JAXBElement<Long> createDataReqNotificationId(Long value) {
        return new JAXBElement<Long>(_DataReqNotificationId_QNAME, Long.class, DataReq.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "externalID", scope = DataReq.class)
    public JAXBElement<String> createDataReqExternalID(String value) {
        return new JAXBElement<String>(_DataReqExternalID_QNAME, String.class, DataReq.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "preferredContactMedia", scope = DataReq.class)
    public JAXBElement<String> createDataReqPreferredContactMedia(String value) {
        return new JAXBElement<String>(_DataReqPreferredContactMedia_QNAME, String.class, DataReq.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "email", scope = DataReq.class)
    public JAXBElement<EmailContact> createDataReqEmail(EmailContact value) {
        return new JAXBElement<EmailContact>(_DataReqEmail_QNAME, EmailContact.class, DataReq.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TelephoneNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/Notificaciones/SendLegacyEvent/V1/types", name = "telephoneNumber", scope = DataReq.class)
    public JAXBElement<TelephoneNumber> createDataReqTelephoneNumber(TelephoneNumber value) {
        return new JAXBElement<TelephoneNumber>(_DataReqTelephoneNumber_QNAME, TelephoneNumber.class, DataReq.class, value);
    }

}
