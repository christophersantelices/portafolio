
package cl.telefonica.enterpriseapplicationintegration.tefheader.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.telefonica.enterpriseapplicationintegration.tefheader.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TEFChileanIdentificationNumberIdentificationCode_QNAME = new QName(" /EnterpriseApplicationIntegration/TEFHeader/V1", "identificationCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.telefonica.enterpriseapplicationintegration.tefheader.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TEFHeaderRequest }
     * 
     */
    public TEFHeaderRequest createTEFHeaderRequest() {
        return new TEFHeaderRequest();
    }

    /**
     * Create an instance of {@link TEFHeaderResponse }
     * 
     */
    public TEFHeaderResponse createTEFHeaderResponse() {
        return new TEFHeaderResponse();
    }

    /**
     * Create an instance of {@link TEFErrorCode }
     * 
     */
    public TEFErrorCode createTEFErrorCode() {
        return new TEFErrorCode();
    }

    /**
     * Create an instance of {@link TEFErrorDetail }
     * 
     */
    public TEFErrorDetail createTEFErrorDetail() {
        return new TEFErrorDetail();
    }

    /**
     * Create an instance of {@link TEFChileanIdentificationNumber }
     * 
     */
    public TEFChileanIdentificationNumber createTEFChileanIdentificationNumber() {
        return new TEFChileanIdentificationNumber();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = " /EnterpriseApplicationIntegration/TEFHeader/V1", name = "identificationCode", scope = TEFChileanIdentificationNumber.class)
    public JAXBElement<String> createTEFChileanIdentificationNumberIdentificationCode(String value) {
        return new JAXBElement<String>(_TEFChileanIdentificationNumberIdentificationCode_QNAME, String.class, TEFChileanIdentificationNumber.class, value);
    }

}
