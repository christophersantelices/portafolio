$(document).ready(function() {
		inicializaPago();
		
	});
	function inicializaPago() {
		window.setTimeout("iniciaPago();", 1000);
		$('#errorButton').click(function() {
			$('#formError').submit();
			return false;
		});
		$('#cierreButton').click(function() {
			$('#formCierre').submit();
			return false;
		});
	}

	function iniciaPago() {
		var solicitud = document.getElementById('SolicitudCredito');
		var valueSolicitud = solicitud.value;
		var aux = valueSolicitud.replace(/\n|\r/g, "");
		var sani = '';
		for (i = 0; i < aux.length; i++) {
			var a = aux.substring(i, i + 1);
			if (a == '' || a == ' ' || a == '\n') {
				//alert ("Caracter extraño");
			} else {
				sani += a;
			}
		}
		solicitud.value = sani;
		var div = document.getElementById('divPago');
		//document.body.appendChild(div);
		div.innerHTML = '<iframe id="iframe-pago" name="iframe-pago" frameborder="0" allowTransparency="true" style="height: 840px; width: 100%;"></iframe>';
		var iframe = document.getElementById('iframe-pago');
		try {
			iframe.addEventListener('load', function() {
			}, false);
		} catch (err) {
			iframe.attachEvent('onload', function() {
			});
		}
		var form = document.getElementById('formularioSolicitudCredito');
		form.target = iframe.id;
		form.submit();
		sendMessage();
		cargaAjaxPrimeraLLamada();
		return false;
	}

	var contador = 0;
	var stopLoop = 99;
	var cantidadRequest = 0;
	var cantidadDeConsultas = 60; // Debiese ser 60 para hacer un total de 5 minutos de espera ( 5 seg entre cada consulta * 60 consultas = 300 seg = 5 min)
	function consultar() {
		Liferay.Service(
				'/portal-de-pagos-core-portlet.estadopago/get-estado-pago', {
					ordenDeCompra : $("#numeroOrden").val()
				}, function(obj) {
					stopLoop = obj.estado;
				});

		if (contador < cantidadDeConsultas && stopLoop == 99) {
			var id = window.setTimeout("consultar();", 5000);
			contador++;
		} else if (stopLoop != 0 && stopLoop != 99) {
			$('#errorButton').click();
		} else if (contador >= cantidadDeConsultas) {
			$('#errorButton').click();
		} else if (stopLoop == 0) {
			$('#cierreButton').click();
		}
		cantidadRequest++;
	}

	function cargaAjaxPrimeraLLamada() {
		window.setTimeout("consultar();", 5000);
	}