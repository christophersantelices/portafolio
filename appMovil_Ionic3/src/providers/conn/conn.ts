import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

/*
  Generated class for the ConProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConProvider {

  
  private baseUrl:string = 'http://www';

  private isLogged:boolean=false;
  private access_token:any;
  private divice_token:string;

  constructor(public http: Http,private storage: Storage) {
    //console.log('Hello Auth Provider');
  }
  public isLoggedin(){
    return this.isLogged;
  }

  public setAccesToken(token){
    this.access_token= token;
    this.isLogged=true;
    this.storage.set('access_token', token);
  }
  public getAccesToken(){
    return this.access_token;
  }

  public getDT(){
    return this.divice_token;
  }
  public setDT(token){
    this.divice_token=token;
  }

//login
  public login(data){
    let  headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let  body='grant_type=password&scope=*&'+'email=' + data.username.trim() + '&password=' + data.password.trim();
    let result=this.http.post(this.baseUrl+'/api/login', body, {headers: headers}).map(res => res.json());
    return result;
  }
  public loginFace(uid,token){
    let  headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let  body='provider=facebook'+'&client_id=' + uid + '&access_token=' +token;
    let result=this.http.post(this.baseUrl+'/api/login/social', body, {headers: headers}).map(res => res.json());
    return result;
  }

  //loguot
  public logOut(){
    //this.access_token=null;
    //this.storage.clear();
    let  headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Bearer '+this.access_token);
    let  body='devicetoken='+this.divice_token;    
    console.log('body ',body);
    let result=this.http.post(this.baseUrl+'/api/logout',body, {headers: headers}).map(res => res.json());
    return result;
  }
  //Home
    public HomePage(){
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      //console.log('Bearer '+this.access_token);
      let result= this.http.get(this.baseUrl+'/api/home', {headers: headers}).map(res => res.json());
      return result;
    }
    //AsistenciaPage
    public AsistenciaPageFechas(){
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let result= this.http.get(this.baseUrl+'/api/asistencia', {headers: headers}).map(res => res.json());
      return result;
    }
    public getHorariosActividad(actividad, fecha){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='actividad='+actividad +'&fecha='+fecha;
      let result=this.http.post(this.baseUrl+'/api/asistencia/getHorariosActividad', body, {headers: headers}).map(res => res.json());
      return result;
    }
    public cancelarActividad(token_registro){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='token='+token_registro;
      let result=this.http.post(this.baseUrl+'/api/asistencia/cancelarReserva', body, {headers: headers}).map(res => res.json());
      return result;
    }
    public reservarActividad(token_registro){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='token='+token_registro;
      let result=this.http.post(this.baseUrl+'/api/asistencia', body, {headers: headers}).map(res => res.json());
      return result;
    }
    //historico asistencia
    public HistoricoAsistenciaPageLista(){
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let result= this.http.get(this.baseUrl+'/api/asistencia/historialUsuario', {headers: headers}).map(res => res.json());
      return result;
    }
    //historial de pagos
    public HistorialPagosLista(){
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let result= this.http.get(this.baseUrl+'/api/pago/historialUsuario', {headers: headers}).map(res => res.json());
      return result;
    }
    //historial de pagos
    public planesAccesos(){
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let result= this.http.get(this.baseUrl+'/api/plan/detalle', {headers: headers}).map(res => res.json());
      return result;
    }
    //crear CrearCuentaPage
    public crearCuenta(FormUsuario){
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      let  body='correo='+FormUsuario.correo+'&nombre='+FormUsuario.nombre.trim()+'&apellido_p='+FormUsuario.apellido_p.trim()+'&apellido_m='+FormUsuario.apellido_m.trim()+'&fecha_nacimiento='+FormUsuario.fecha_nacimiento+'&fono_contacto='+FormUsuario.fono_contacto+'&pais_id='+FormUsuario.pais_id+'&region_id='+FormUsuario.region_id+'&provincia_id='+FormUsuario.provincia_id+'&comuna_id='+FormUsuario.comuna_id+'&sexo='+FormUsuario.sexo+'&talla='+FormUsuario.talla_polera+'&box='+FormUsuario.box+'&password='+FormUsuario.password.trim()+'&password_confirm='+FormUsuario.password_confirm.trim()+'&perfilprivado='+FormUsuario.perfilprivado; 
      //let  body= 'nombre='+f.nombre+'&apellido_p='+f.apellido_p+'&apellido_m='+f.apellido_m+'&sexo='+f.sexo+'&talla='+f.talla+'&correo='+f.correo+'&fecha_nacimiento='+this.formatoFecha(f.fecha_nacimiento)+'&fono_contacto='+f.fono_contacto+'&box='+f.box+'&password='+f.password+'&password_confirm='+f.password_confirm;
      let result=this.http.post(this.baseUrl+'/api/usuario/create', body, {headers: headers}).map(res => res.json());
      return result;
    }
    public boxs(){
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      let result= this.http.get(this.baseUrl+'/api/usuario/create', {headers: headers}).map(res => res.json());
      return result;
    }
    //divice token
    public setDiviceToken(os){
      let  headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='devicetoken='+this.divice_token+"&so="+os;
      let result=this.http.post(this.baseUrl+'/api/deviceToken', body, {headers: headers}).map(res => res.json());
      return result;
    }
  //imagen perdil
    public guardarImagenPerfil(imageBase64:string){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='imagen='+imageBase64+'&encode=true';
      let result=this.http.post(this.baseUrl+'/api/usuario/modificarImagenPerfil', body, {headers: headers}).map(res => res.json());
      return result;
    }
  //cambiar contraseña
    public cambiarContrasena(contraseña){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='contrasena='+contraseña;
      let result=this.http.post(this.baseUrl+'/api/usuario/modificarContrasena', body, {headers: headers}).map(res => res.json());
      return result;
    }
  //perdil edit
    public getUsuario(){
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let result= this.http.get(this.baseUrl+'/api/usuario', {headers: headers}).map(res => res.json());
      return result;
    }
    public setUsuario(FormUsuario){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='nombre='+FormUsuario.nombre.trim()+'&apellido_p='+FormUsuario.apellido_p.trim()+'&apellido_m='+FormUsuario.apellido_m.trim()+'&sexo='+FormUsuario.sexo+'&talla_polera='+FormUsuario.talla_polera+'&fecha_nacimiento='+this.formatoFecha(FormUsuario.fecha_nacimiento)+'&fono_contacto='+FormUsuario.fono_contacto+'&pais_id='+FormUsuario.pais_id+'&region_id='+FormUsuario.region_id+'&provincia_id='+FormUsuario.provincia_id+'&comuna_id='+FormUsuario.comuna_id+'&perfilprivado='+FormUsuario.perfilprivado;
      let result=this.http.post(this.baseUrl+'/api/usuario', body, {headers: headers}).map(res => res.json());
      return result;
    }
    public setContactoEmergencia(FormUsuario){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body="nombreemergencia="+FormUsuario.nombreemergencia+"&fonoemergencia="+FormUsuario.fonoemergencia+"&direccionemergencia="+FormUsuario.direccionemergencia+"&previsionsalud="+FormUsuario.previsionsalud;
      let result=this.http.post(this.baseUrl+'/api/usuario/contactoEmergencia', body, {headers: headers}).map(res => res.json());
      return result;
    }
    //restablecer contraseña
    public restablecerContrasena(correo){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      let body="correo="+correo;
      let result=this.http.post(this.baseUrl+'/api/reestablecerContrasena', body, {headers: headers}).map(res => res.json());
      return result;
    }
    //combobox region, comuna, y provincia
    public getRegion(pais){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      let body="pais="+pais;
      let result=this.http.post(this.baseUrl+'/api/usuario/getRegion', body, {headers: headers}).map(res => res.json());
      return result;
    }
    public getProvincia(region){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      let body="region="+region;
      let result=this.http.post(this.baseUrl+'/api/usuario/getProvincia', body, {headers: headers}).map(res => res.json());
      return result;
    }
    public getComuna(provincia){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      let body="provincia="+provincia;
      let result=this.http.post(this.baseUrl+'/api/usuario/getComuna', body, {headers: headers}).map(res => res.json());
      return result;
    }
    //completar perfil 
    public setCompletarPerfil(idUsuario,FormUsuario,checkSolicitud){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let body;
      if(checkSolicitud){
        body='idUsuario='+idUsuario+'&correo='+FormUsuario.correo+'&nombre='+FormUsuario.nombre+'&apellido_p='+FormUsuario.apellido_p+'&apellido_m='+FormUsuario.apellido_m+'&fecha_nacimiento='+this.formatoFecha(FormUsuario.fecha_nacimiento)+'&fono_contacto='+FormUsuario.fono_contacto+'&pais_id='+FormUsuario.pais_id+'&region_id='+FormUsuario.region_id+'&provincia_id='+FormUsuario.provincia_id+'&comuna_id='+FormUsuario.comuna_id+'&sexo='+FormUsuario.sexo+'&perfilprivado='+FormUsuario.perfilprivado+'&talla_polera='+FormUsuario.talla_polera+'&box='+FormUsuario.box; 
      }else{
        body='idUsuario='+idUsuario+'&correo='+FormUsuario.correo+'&nombre='+FormUsuario.nombre+'&apellido_p='+FormUsuario.apellido_p+'&apellido_m='+FormUsuario.apellido_m+'&fecha_nacimiento='+this.formatoFecha(FormUsuario.fecha_nacimiento)+'&fono_contacto='+FormUsuario.fono_contacto+'&pais_id='+FormUsuario.pais_id+'&region_id='+FormUsuario.region_id+'&provincia_id='+FormUsuario.provincia_id+'&comuna_id='+FormUsuario.comuna_id+'&sexo='+FormUsuario.sexo+'&perfilprivado='+FormUsuario.perfilprivado+'&talla_polera='+FormUsuario.talla_polera; 
      }
      let result=this.http.post(this.baseUrl+'/api/usuario/completarPerfil', body, {headers: headers}).map(res => res.json());
      return result;
    }
    //usuario sin comunidad
    public getSolicitud(){
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let result= this.http.get(this.baseUrl+'/api/solicitud', {headers: headers}).map(res => res.json());
      return result;
    }
    public CancelarSolicitud(solicitud){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='solicitud='+solicitud;
      let result=this.http.post(this.baseUrl+'/api/solicitud/cancelar', body, {headers: headers}).map(res => res.json());
      return result;
    }
    public MandarSolicitud(organismo){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='organismo='+organismo;
      let result=this.http.post(this.baseUrl+'/api/solicitud/enviar', body, {headers: headers}).map(res => res.json());
      return result;
    }
    //salir de la comunidad 
    public salirComunidad(){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='';
      let result=this.http.post(this.baseUrl+'/api/usuario/salirComunidad',body, {headers: headers}).map(res => res.json());
      return result;
    }
     //aceptar terminos y condiciones
     public aceptarTerminos(perfil,id_condiciones_gym){
      let  headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Accept', 'application/json');
      headers.append('Authorization', 'Bearer '+this.access_token);
      let  body='perfil='+perfil+'&id_condiciones_gym='+id_condiciones_gym;
      let result=this.http.post(this.baseUrl+'/api/aceptarTyc', body, {headers: headers}).map(res => res.json());
      return result;
    }

    public formatoFecha(fecha){
        var info = fecha.split('-');
        return info[2] + '-' + info[1] + '-' + info[0];
    }

}