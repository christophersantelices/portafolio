import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

/*
  Generated class for the SessionProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SessionProvider {

  private session={
    perfilCompleto:false,
    esAdmin:false,
    esUsuario:false,
    esUsuarioOrg:false,
    esAdminOrg:false,
    esProfesor:false,
    esSubAdmin:false,
    esTester:false,
    perfil:null,
    id:null,
    checkTyc:null
  };

  //datos por si el api no carga. 
  private usuario={
        "imagenPerfil": "https://s3.amazonaws.com//logos/logo-200.png",
        "nombrePlan": "No disponible",
        "nombre": "No disponible",
        "apellido": "No disponible",
        "fechaNacimiento": "No disponible",
        "fonoContacto": "No disponible",
        "tallaPolera": "No disponible",
        "fechaIngreso": "No disponible",
        "comuna": "No disponible",
        "provincia": "No disponible",
        "contactoEmergencia": "No disponible",
        "telefonoEmergencia": "No disponible",
        "direccionEmergencia": "No disponible",
        "entidadPrevisional": "No disponible",
        "direccion": "No disponible"
    }
  private facebook:boolean=null;

  constructor(private storage: Storage) {
    //console.log(this.session);
  }

  /** Inicio de session por facebook. */

  public getfacebook(){
    return this.facebook;
  }
  public setfacebook(f){
    this.facebook=f;
  }
  public setStoragefacebook(f){
    this.facebook=f;
    this.storage.set('facebook', f);
  }

    /** Get y Set de Session */
  public setSession(session){
    this.session=session;
  }
  public getSession(){
    return this.session;
  }
  public getperfilCompleto(){
    return this.session.perfilCompleto;
  }
  public setperfilCompleto(p){
    this.session.perfilCompleto=p;
  }
  public getEsUsuarioOrg(){
    return this.session.esUsuarioOrg;
  }
  public getEsUsuario(){
    return this.session.esUsuario;
  }
  public getCheckTyc(){
    return this.session.checkTyc;
  }
  public setCheckTyc(check){
     this.session.checkTyc.check=check;
  }
  public getPerfil(){
    return this.session.perfil;
  }


 /** Get y Set de Usuario */
  public getNombre(){
    return this.usuario.nombre;
  }
  public getApellido(){
    return this.usuario.apellido;
  }
  public getImagenPerfil(){
    return this.usuario.imagenPerfil;
  }
  public setImagenPerfil(foto){
    this.usuario.imagenPerfil=foto;
  }
  public getComuna(){
    return this.usuario.comuna;
  }
  public getProvincia(){
    return this.usuario.provincia;
  }
  public setComuna(comuna){
    this.usuario.comuna=comuna;
  }
  public setProvincia(provincia){
    this.usuario.provincia=provincia;
  }

  /**
   * Cierre de session
   */
  public logOut(){
    this.session={
      perfilCompleto:false,
      esAdmin:false,
      esUsuario:false,
      esUsuarioOrg:false,
      esAdminOrg:false,
      esProfesor:false,
      esSubAdmin:false,
      esTester:false,
      perfil:null,
      id:null,
      checkTyc:null
    };
  }
}
