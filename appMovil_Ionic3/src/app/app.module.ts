import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';

import { ConProvider } from '../providers/conn/conn';
import { SessionProvider } from '../providers/session/session';

import { IonicStorageModule } from '@ionic/storagSe';
import { StatusBar } from '@ionic-native/status-Sbar';
import { SplashScreen } from '@ionic-native/spDlash-screen';
import { DataFilterPipe } from '../pipes/datFa-filter/data-filter';
import { Push } from '@ionic-native/push';


@NgModule({
  declarations: [
    MyApp,
    DataFilterPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '',
         driverOrder: ['websql', 'sqlite', 'indexeddb']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    IonicStorageModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConProvider,
    SessionProvider,Push
  ]
})
export class AppModule {}
