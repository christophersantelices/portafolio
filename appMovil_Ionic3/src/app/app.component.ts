import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,ModalController,AlertController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
//providers
import { ConProvider } from '../providers/conn/conn';
import { SessionProvider } from '../providers/session/session';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  rootPage: string;
  loader:any;
  shownGroup = false;
  pagesUsuarioOrg = [
    { title: 'Reserva de clases', component: ["AsistenciaPage","HistoricoAsistenciaPage"],icon:'md-create' },
    { title: 'Pagos y planes', component:["PlanesAccesosPage","HistorialPagosPage"],icon:'bookmark'}
  ];
  pagesEsUsuario= [
    { title: 'Solicitudes', component:["IngresarComunidadPage"],icon:'mail'}
  ];


  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,private session:SessionProvider,
    public modalCtrl: ModalController,private storage: Storage,private con:ConProvider,
      private alertCtrl: AlertController,private push: Push) {
  
        this.getAcccesToken();
        this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is('ios')) {
        this.statusBar.hide();
      }else{
        this.statusBar.styleDefault;
      }
      // to check if we have permission
      this.push.hasPermission()
      .then((res: any) => {
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
        } else {
          console.log('We do not have permission to send push notifications');
        }
      });
      // push notifications
      const options: PushOptions = {
        android: {
          senderID: "",
          icon: "notificacion",
          iconColor: "#2D97CF"
        },
          ios: {
            alert: "true",
            badge: "true",
            sound: "true",
            clearBadge: "true"
          }
      };
      
      const pushObject: PushObject = this.push.init(options);
      
      pushObject.on('notification').subscribe(
        (notification: any) => {
          console.log('Received a notification', notification);
          if(notification.additionalData.foreground){
            this.presentAlert(notification.title,notification.message);
          }
          
        });
      
      pushObject.on('registration').subscribe(
        (registration: any) => {
          //console.log('Device registered:',registration);
          console.log('Device registered');
          console.log(registration.registrationId);
          this.con.setDT(registration.registrationId);
        });
    
      pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
      
    });
  }

  private presentAlert(titulo,texto) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: texto,
      buttons: ['Ok']
    });
    alert.present();
  }
  private async getAcccesToken(){
    await this.storage.get('access_token').then((token) => {
        if(token){
          this.con.setAccesToken(token);
          this.getAccces();
          this.rootPage="HomePage";
        }else{
          this.rootPage="LoginPage";
        }
    });
  }
  private async getAccces(){
    await this.storage.get('facebook').then((face) => {
       if(face){
         this.session.setfacebook(face);
       }else{
        this.session.setfacebook(null);
       }
   });
 }

  openPage(page:string) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page=='AsistenciaPage'){
      try {
        if(this.session.getCheckTyc().check){
          this.nav.setRoot("TerminosycondicionesPage");
        }else{
          this.nav.setRoot(page);
        }
      }
      catch(Exception) {
          console.log("Error al mirar terminos");
          this.nav.setRoot("HomePage");
      }
    }else{
      this.nav.setRoot(page);
    }
    
  }
  public modalTerminos(){
    let contactModal = this.modalCtrl.create("TerminosycondicionesPage");
    contactModal.present();
}

  openHome() {
    this.nav.setRoot("HomePage");
  }
  openPerfil() {
    this.nav.setRoot("PerfilPage");
  }

  logOut(){
    this.con.logOut().subscribe(
      data => {
        console.log("logout",data);
      },
      err => {
         console.log("ERROR logout!: "+err.status, err);
      });

    this.session.logOut();
    this.storage.clear();
    this.nav.setRoot("LoginPage");
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
  }
  isGroupShown(group) {
      return this.shownGroup === group;
  }


}
