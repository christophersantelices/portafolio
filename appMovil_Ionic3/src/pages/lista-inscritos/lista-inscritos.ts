import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ListaInscritosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-lista-inscritos',
  templateUrl: 'lista-inscritos.html',
})
export class ListaInscritosPage {

  lista: Array<{nombre: string,apellido_p:string,imagenperfil:string}>;


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrol: ViewController ) {
    this.lista=this.navParams.get('lista');
    console.log(' ListaInscritosPage', this.lista);
    for (let entry of this.lista) {
      if(entry.imagenperfil==null){
        entry.imagenperfil="https://s3.amazonaws.com/logos/unknown_user.png"
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaInscritosPage');
  }

  close(){
    this.viewCtrol.dismiss();
  }
}
