import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaInscritosPage } from './lista-inscritos';

@NgModule({
  declarations: [
    ListaInscritosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaInscritosPage),
  ],
})
export class ListaInscritosPageModule {}
