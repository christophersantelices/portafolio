import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController,MenuController,Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ConProvider } from '../../providers/conn/conn';
import { SessionProvider } from '../../providers/session/session';


import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loader:any;
  userID:string;
  accessToken:string;
  email:string;

  constructor(public platform: Platform,public navCtrl: NavController, public navParams: NavParams,
    private con: ConProvider,private session:SessionProvider, public alertCtrl:AlertController,
    public loadingCtrl: LoadingController, public menuCtrl: MenuController,
    private fb: Facebook, private splashScreen:SplashScreen ) {
      //this.splashScreen.show();
     this.menuCtrl.enable(false);

  }
  ionViewDidEnter() {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.splashScreen.hide();
  }

  login(FormLogin){
      this.presentLoading();
      this.con.login(FormLogin.value).subscribe(
        data => {
          console.log("login succes");
          this.con.setAccesToken(data.access_token);
          this.session.setSession(data.session);
          this.session.setStoragefacebook(false);
          this.sendDiviceToken();
          this.loader.dismiss();
          if(data.session.perfilCompleto){
            this.navCtrl.setRoot("HomePage");
          }else{
            this.navCtrl.setRoot("CompletarPerfilPage");
          }
        },
        err => {
          this.loader.dismiss();
          console.log("ERROR login  : "+err.status, err);
          if(err.status==401){
            this.mensaje("No Autorizado","Usuario o Contraseña incorrecta");
          } else if(err.status==422){
            this.mensaje("Correo no valido","El campo usuario debe ser un correo valido");
          } else if(err.status==500){
            this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }else{
            this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo nuevamente.");
          }
        });

    }

  async loginFAcebook(){
    console.log("------------------------------------------");
    let fa=false;
    await this.fb.login(['public_profile']).then(
        (res: FacebookLoginResponse) => {
          console.log('Logged into Facebook!', res);
           this.userID=res.authResponse.userID;
           this.accessToken=res.authResponse.accessToken
           fa=true; // se logueo con exito
        }).catch(e => {
          console.log('Error logging into Facebook', e);
          this.mensaje("Error Login Facebook",e.errorMessage);
        });

      if(fa){
        this.fb.logout();
        this.presentLoading();
        //console.log(this.userID);
        //console.log(this.accessToken);
        this.con.loginFace(this.userID,this.accessToken).subscribe(
          data => {
            console.log("login facebook succes");
            //console.log(data.access_token);
            this.con.setAccesToken(data.access_token);
            this.session.setSession(data.session);
            this.session.setStoragefacebook(true);
            this.sendDiviceToken();
            this.loader.dismiss();
            if(data.session.perfilCompleto){
              this.navCtrl.setRoot("HomePage");
            }else{
              this.navCtrl.setRoot("CompletarPerfilPage");
            }
          },
          err => {
            console.log("ERROR login f  : "+err.status, err);
            this.loader.dismiss();
            if(err.status==500){
              this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
            }else{
              this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo nuevamente.");
            }
          });
      }
      
  }
  
  public sendDiviceToken(){
    let os="null";
    if (this.platform.is('ios')) {
      os="ios";
    } else if (this.platform.is('android')) {
      os="android";
    }
    this.con.setDiviceToken(os).subscribe(
      data => {
        console.log("divice token set succes",os);
      },
      err => {
          console.log("divice token set ERROR : "+err.status, err);
      }
    );
  }

  
  private mensaje(t,s) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando"
    });
    this.loader.present();
  }

  loadRecuPass(){
    this.navCtrl.push("RecuPassPage");
  }
  loadCrearCuenta(){
    this.navCtrl.push("CrearCuentaPage");
  }
  loadDatosPerfil(){
    this.navCtrl.push("DatosPerfilPage");
  }
  loadHome(){
    this.navCtrl.setRoot("HomePage");
  }

}
