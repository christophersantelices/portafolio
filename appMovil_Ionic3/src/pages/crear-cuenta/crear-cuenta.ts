import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,AlertController } from 'ionic-angular';
import { ConProvider } from '../../providers/conn/conn';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
/**
 * Generated class for the CrearCuentaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-crear-cuenta',
  templateUrl: 'crear-cuenta.html',
})
export class CrearCuentaPage {
  loader:any;
  boxs:Array<{}>;
  paises:Array<{pais_id:number,pais_nombre:string}>;
  regiones:Array<{region_id:number,region_nombre:string,pais_id:number}>;
  provincias:Array<{provincia_id:number,provincia_nombre:string}>;
  comunas:Array<{comuna_id:number,comuna_nombre:string}>;
  FormCreateUser:FormGroup;
  submitAttempt:boolean=false;


  constructor(public formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController, public alertCtrl:AlertController,private con:ConProvider) {
    this.cargarBoxs();
    this.paises=[{pais_id:1,pais_nombre:'Chile'}];

    this.FormCreateUser = formBuilder.group({
      nombre:     ['', Validators.compose([Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ\\s]{2,}'), Validators.required])],
      apellido_p: ['', Validators.compose([Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ\\s]{2,}'), Validators.required])],
      apellido_m: ['', Validators.compose([Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ\\s]{2,}'), Validators.required])],
      sexo : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      talla_polera : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      correo : ['', Validators.compose([Validators.minLength(2), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'), Validators.required])],
      fecha_nacimiento : ['',Validators.required], 
      fono_contacto: ['', Validators.compose([Validators.minLength(8), Validators.pattern('([0-9]{8})'), Validators.required])],
      pais_id: ['', Validators.compose([Validators.minLength(1), Validators.required])], 
      region_id : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      provincia_id : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      comuna_id : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      box : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      perfilprivado : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      password : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      password_confirm : ['', Validators.compose([Validators.minLength(1), Validators.required])]
      /*passwords: formBuilder.group({
        password: ['', Validators.required],
        password_confirm: ['', Validators.required]}, {validator: this.areEqual})*/
      },{validator: this.areEqual});
      
  }

  areEqual(control: FormControl): any {
    console.log(control);
    if(control.value.password != control.value.password_confirm){
        console.log('contraseñas no iguales');
        return {
            "areEqual": true
        };
    }
    return null;
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrearCuentaPage');
  }

   loadLogin(){
    this.navCtrl.pop();
  }

  crearUsuario(formValues){
    this.submitAttempt = true;
    if(this.FormCreateUser.valid){
      this.presentLoading();
      formValues.fecha_nacimiento= this.formatoFecha(formValues.fecha_nacimiento);
      this.con.crearCuenta(formValues).subscribe(
        data => {
          this.loader.dismiss();
          this.mensaje("Usuario creado","Usuario creado con éxito!");
          this.navCtrl.pop();
        },
        err => {
          this.loader.dismiss();
          console.log("ERROR crearUsuario  : "+err.status, err);
          if(err.status==400){
            //console.log(err._body);
            let obj = JSON.parse(err._body);
            let mensaje="";

            if (obj.nombre){
              mensaje=mensaje+obj.nombre[0]+'<br>';
            }
            if(obj.apellido_p){
              mensaje=mensaje+obj.apellido_p[0]+'<br>';
            }
            if(obj.apellido_m){
              mensaje=mensaje+obj.apellido_m[0]+'<br>';
            }
            if(obj.sexo){
              mensaje=mensaje+obj.sexo[0]+'<br>';
            }
            if(obj.talla){
              mensaje=mensaje+obj.talla[0]+'<br>';
            }
            if(obj.correo){
              mensaje=mensaje+obj.correo[0]+'<br>';
            }
            if(obj.fecha_nacimiento){
              mensaje=mensaje+obj.fecha_nacimiento[0]+'<br>';
            }
            if(obj.fono_contacto){
              mensaje=mensaje+obj.fono_contacto[0]+'<br>';
            }
            if(obj.pais_id){
              mensaje=mensaje+obj.pais_id[0]+'<br>';
            }
            if(obj.region_id){
              mensaje=mensaje+obj.region_id[0]+'<br>';
            }
            if(obj.provincia_id){
              mensaje=mensaje+obj.provincia_id[0]+'<br>';
            }
            if(obj.comuna_id){
              mensaje=mensaje+obj.comuna_id[0]+'<br>';
            }
            if(obj.password){
              mensaje=mensaje+obj.password[0]+'<br>';
            }
            if(obj.password_confirm){
              mensaje=mensaje+obj.password_confirm+'<br>';
            }
            if(obj.perfilprivado){
              mensaje=mensaje+obj.perfilprivado[0]+'<br>';
            }
            this.mensaje("Error en el formulario",mensaje);
          } else if(err.status==500){
            this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }else{
            this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }
        });
    }
      

  }
  cargarBoxs(){
      this.presentLoading();
      this.con.boxs().subscribe(
        data => {
          this.boxs=data.organismo;
          this.loader.dismiss();
        },
        err => {
          console.log("ERROR cargarBoxs  : "+err.status, err);
          this.loader.dismiss();
          if(err.status==500){
            this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }else{
            this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }
        });
        
  }

  public cargarRegion(pais_id){
    console.log("cargar regions",pais_id);
    this.con.getRegion(pais_id).subscribe(
     data => {
       this.regiones=data;
       this.provincias=[];
       this.comunas=[];
     },
     err => {
         console.log("getRegion set ERROR : "+err.status, err);
         if(err.status==500){
           this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
         }else{
           this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
         }
     }
   );
   } 
   
     public cargarProvincia(region_id){
       this.con.getProvincia(region_id).subscribe(
         data => {
           this.provincias=data;
           this.comunas=[];
           console.log("getProvincia succes");
         },
         err => {
             console.log("getProvincia set ERROR : "+err.status, err);
             if(err.status==500){
               this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
             }else{
               this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
             }
         }
       );
     } 

     public cargarComuna(provincia_id){
      this.con.getComuna(provincia_id).subscribe(
       data => {
         this.comunas=data;
         console.log("getComuna succes");
       },
       err => {
          console.log("getComuna set ERROR : "+err.status, err);
          if(err.status==500){
            this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }else{
            this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }
       }
      );
      } 

  public formatoFecha(fecha){
      var info = fecha.split('-');
      return info[2] + '-' + info[1] + '-' + info[0];
  }

  private mensaje(t,s) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();
  }
   private presentLoading() {
      this.loader = this.loadingCtrl.create({
        content: "Conectando"
      });
      this.loader.present();
    }
}
