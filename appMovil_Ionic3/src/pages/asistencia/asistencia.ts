import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ModalController,LoadingController,ToastController,MenuController} from 'ionic-angular';
import { ConProvider } from '../../providers/conn/conn';
import { SessionProvider } from '../../providers/session/session';

@IonicPage()
@Component({
  selector: 'page-asistencia',
  templateUrl: 'asistencia.html',
})
export class AsistenciaPage {

  loader:any;
  toast:any;
  tieneAccesos:boolean;
  dataAsistencia:boolean=false;
  datafechas:boolean=false;
  pagoActivo:boolean;
  fechasActividades : any;
  actividad:any;
  seleccionarFechas:boolean=true; //si esta en false datachosen no funciona
  //actividad: {nombre: string,id_actividad:string,horariosAct: Array<any>} | any;

  fecha:string=null; //timestrap
  fechas: any;
  horariosActividad:any;
  horas:any;

  pagos:any;


  constructor(public navCtrl: NavController, public navParams: NavParams,
      public alertCtrl: AlertController,public modalCtrl: ModalController,
      private con: ConProvider,public loadingCtrl: LoadingController, 
      public toastCtrl: ToastController,private session:SessionProvider,public menuCtrl: MenuController) {
    
        this.asistenciaFecha();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AsistenciaPage');
  }

///// Fechas de asistencia a las actividades
 public asistenciaFecha(){
  this.presentLoading();
   this.con.AsistenciaPageFechas().subscribe(
     data => {
       console.log("asistencia fechas succes",data);
       this.session.setSession(data.session);
       this.tieneAccesos=data.tieneAccesos;
       if(this.tieneAccesos){
         //console.log(data);
       //this.pagos=data.pagos;
        if(data.fechasActividades){
          this.fechasActividades=data.fechasActividades;
          this.actividad=this.fechasActividades[0];
          this.fechas=this.actividad.horariosAct;
          this.fecha=this.actividad.horariosAct[0].timeStamp;
          this.logChosen();
          //console.log(this.fechasActividades);
        }else{
          this.fechasActividades=[{
            "id_actividad": data.actividades.id_actividad,
            "nombre": data.actividades.nombre,
            "horariosAct": data.actividades.fechasActividad}];
          this.actividad=this.fechasActividades[0];
          this.fechas=this.actividad.horariosAct;
          this.fecha=this.actividad.horariosAct[0].timeStamp;
          this.logChosen();
        }
       }else{
        this.fechasActividades=[{nombre:"No hay actividades disponibles"}];
        this.fechas=[{diaS:"No hay fechas disponibles",dia:""}]
        this.seleccionarFechas=false;
       }
       this.loader.dismiss();
     },
     err => {
        console.log("ERROR ASISTENCIA!: "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }
     });
 }

 //elegir actividad
  public logChosen(): void {
    //console.log('logChosen');
    //console.log(this.actividad);
    this.dataAsistencia=false;
    if(this.actividad.horariosAct[0].fecha=="No hay fechas disponibles"){
      this.datafechas=true;
      this.fechas=[{diaS:"Sin fechas",dia:""}];
    }else{
      this.datafechas=false;
      this.fechas=this.actividad.horariosAct;
      this.fecha=this.actividad.horariosAct[0].timeStamp;
      this.dateChosen();
    }
  }
  //elegir fecha de actividad
  public dateChosen(): void {
    //console.log('dateChosen');
    if(this.seleccionarFechas){
      this.con.getHorariosActividad(this.actividad.id_actividad, this.fecha).subscribe(
        data => {
          this.session.setSession(data.session);
          console.log("HorariosActividad succes");
          //console.log(data);
          
          this.horariosActividad=data.actividad;
          this.horas=data.actividad.horas;
          this.pagos=data.actividad.pagos;
          this.pagoActivo=data.actividad.pagoActivo;
            //console.log(this.horas);
            this.dataAsistencia=true;
          try {
            this.loader.dismiss();
          } catch (error) {
            
          }

        },
        err => {
          console.log("ERROR ASISTENCIA! dateChosen: "+err.status, err);
          this.loader.dismiss();
          if(err.status==500){
            this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }else{
            this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }
        });
    }else{
      console.log("seleccionarFechas es false ");
    }
    
  }

  //reservar clases
  showReservar(tipo,token_registro) {
    let mensaje="";
    if(tipo==1){
      mensaje="<br><br>Si reservas esta hora, posteriormente no la podrás cancelar";
    }
    let confirm = this.alertCtrl.create({
      title: '¿Estás seguro?',
      message: '¿Está seguro de querer agendar en este horario?'+mensaje,
      buttons: [
        {
          text: 'No gracias',
          handler: () => {
            console.log('No gracias');
          }
        },
        {
          text: 'Si, Gracias!',
          handler: () => {
            console.log('Si, Gracias!');
            this.reservarAct(token_registro);
          }
        }
      ]
    });
    confirm.present();
  }
  reservarAct(token_registro): void {
    this.presentLoading()
    this.con.reservarActividad(token_registro).subscribe(
      data => {
        this.session.setSession(data.session);
        console.log("reservarAct succes");
        this.loader.dismiss();
        this.showConfirmacion(data.titulo,data.mensaje);
      },
      err => {
        console.log("ERROR ASISTENCIA! reservarAct: "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }
      });
  }

  //cancelar clases
  showCancelar(token_registro) {
      let confirm = this.alertCtrl.create({
      title: '¿Estás seguro?',
      message: '¿Está seguro de querer cancelar esta reserva?',
      buttons: [
        {
          text: 'No gracias',
          handler: () => {
            console.log('No gracias');
          }
        },
        {
          text: 'Cancelar!',
          handler: () => {
            console.log('Si, cancelar!');
            this.cancelarAct(token_registro);
          }
        }
      ]
    });
    confirm.present();
  }
  cancelarAct(token_registro): void {
    this.presentLoading()
    this.con.cancelarActividad(token_registro).subscribe(
      data => {
        this.session.setSession(data.session);
        console.log("cancelarAct succes");
        this.loader.dismiss();
        this.showConfirmacion(data.titulo,data.mensaje);
      },
      err => {
        console.log("ERROR ASISTENCIA! cancelarAct: "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }
      });
  }
  showConfirmacion(titulo,mensaje) {
      let confirm = this.alertCtrl.create({
      title: titulo,
      message: mensaje,
      buttons: [
        {
          text: 'Continuar',
          handler: () => {
            console.log('Continuar');
            this.dateChosen();
          }
        }
      ]
    });
    confirm.present();
  }

////// Lista de inscritos
  public presentModal(alumnos) {
    let modal = this.modalCtrl.create("ListaInscritosPage",{lista:alumnos});
    modal.present();
  }
  //tostada
  public showToastWithHC(mensaje) {
    this.toast = this.toastCtrl.create({
      message: 'Esta clase ha sido cancelada por el siguiente motivo: '+mensaje,
      duration: 4000
    });
    this.toast.present();
 }
 public showToast(mensaje) {
   let contenido="";

   switch(mensaje) {
    case 1:
        contenido='No puedes cancelar esta reserva ya que se ha alcanzado el tiempo máximo para hacerlo.';
        break;
    case 2:
        contenido="No puedes hacer una reserva a esta hora porque haz alcanzado el límite diario según tu plan.";
        break;
    case 3:
        contenido="No puedes hacer una reserva a esta hora porque no tienes un pago activo para esta fecha.";
        break;
    case 4:
        contenido="No puedes hacer una reserva a esta hora porque haz alcanzado el límite diario según tu plan.";
        break;
    default:
      break;
      }

   this.toast = this.toastCtrl.create({
     message: contenido,
     duration: 4000
   });
   this.toast.present();
}
  //formato fecha
  public subString(string) {
    if(string){
      return string.substring(0, 10);
    }else{
      return string;
    }
  }

  private mensaje(t,s) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();
  }

  //LOADINGBOX
  public presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando"
    });
    this.loader.present();
  }

  public valueConverter(value:number,max:number){
      //console.log("value convert1",value,max);
      let res=(value*100)/max;
      if(res<10&&res!=0){
        res=10;
      }
      //console.log("value convert2",res);
      return res;
  }

  //recargar asistencia
  public recargar(){
    console.log(this.fecha);
    if(this.fecha=="No hay fechas disponibles"||this.fecha==null){
      this.asistenciaFecha();
    }else{
      this.presentLoading();
      var temp=this.fecha;
      this.dateChosen();
      this.fecha=temp;
      this.loader.dismiss();
    }

  }
}
