import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController ,AlertController} from 'ionic-angular';
import { ConProvider } from '../../providers/conn/conn';
import { SessionProvider } from '../../providers/session/session';

@IonicPage()
@Component({
  selector: 'page-historico-asistencia',
  templateUrl: 'historico-asistencia.html',
})
export class HistoricoAsistenciaPage {
  loader:any;

  @ViewChild('myTable') table: any;
  rows: any[] = [];
  expanded: any = {};
  timeout: any;
  loadingIndicator: boolean = true;
  temp = [];
  limit:any;

  constructor(public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController,private con:ConProvider,private session:SessionProvider) {
    this.getHistorial();
    this.loadingIndicator = false;
    this.limit=6;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Test2Page');
  }

  onPage(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      //console.log('paged!', event);
    }, 100);
  }


  public getHistorial(): void {
  this.presentLoading();
  this.con.HistoricoAsistenciaPageLista().subscribe(
    data => {
      this.session.setSession(data.session);
      this.rows=data.asistencia;
      this.temp=data.asistencia;
      this.loader.dismiss();
    },
    err => {
      console.log("ERROR getHistorial: "+err.status, err);
      this.loader.dismiss();
      if(err.status==500){
        this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
      }else{
        this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
      }
    });

}

  dataFormat(data){
      var temp=[{comunidad:String,plan:String}];
      for(var i =0; i<data.length;i++){
        temp.push({comunidad:data.nombre_sucursal,plan:data.nombre_plan});
      }
      return temp;
  }

  toggleExpandRow(row) {
    //console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }
  isGroupShown(row) {
      return row.$$expanded;
  }

  onDetailToggle(event) {
    //console.log('Detail Toggled', event);
  }
  /*updateFilter(event) {
    var val = event.target.value.trim().toUpperCase();
    // filter our data
    var temp = this.temp.filter(function(d) {
      return d.nombre_sucursal.toUpperCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }*/

  public updateFilter(event) {
    var val = event.target.value.trim().toUpperCase();
    // filter our data
    var temp = this.temp.filter(function(d) {
       if(d.nombre.toUpperCase().indexOf(val) !== -1){
          return d;
       } else if(d.fecha_actividad.toUpperCase().indexOf(val) !== -1){
          return d;
       }
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  public col1(val){


  }

  //formato fecha
  public subString(string) {
  if(string){
    return string.substring(0, 10);
  }else{
    return string;
  }
  }
  private mensaje(t,s) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();
  }

  //LOADINGBOX
  public presentLoading() {
  this.loader = this.loadingCtrl.create({
    content: "Conectando"
  });
  this.loader.present();
  }

  }
