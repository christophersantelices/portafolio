import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoricoAsistenciaPage } from './historico-asistencia';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
@NgModule({
  declarations: [
    HistoricoAsistenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoricoAsistenciaPage),NgxDatatableModule
  ]
})
export class HistoricoAsistenciaPageModule {}
