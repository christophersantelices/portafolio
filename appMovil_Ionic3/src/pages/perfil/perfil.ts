import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,AlertController,ToastController,ModalController,LoadingController  } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { Base64 } from '@ionic-native/base64';
//import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
import { Platform } from 'ionic-angular';

import { SessionProvider } from '../../providers/session/session';
import { ConProvider } from '../../providers/conn/conn';

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  //private imageSrc="assets/img/logo_escudo_texto_128x136.png";
  loader:any;
  private options: CameraOptions  = {
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.FILE_URI,
    quality: 100,
    targetWidth: 1000,
    targetHeight: 1000,
    encodingType: this.camera.EncodingType.JPEG,
    correctOrientation: true,
    allowEdit: true,
    mediaType: this.camera.MediaType.ALLMEDIA

  }


  constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, public viewCtrol: ViewController,private camera: Camera,private crop: Crop,public platform: Platform,
      public modalCtrl: ModalController,private session:SessionProvider,private base64: Base64,private con:ConProvider,public alertCtrl: AlertController,public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }
  close(){
    this.viewCtrol.dismiss();
  }

  public cambiarFoto(){
    this.getMedia().then((path) => {
      //console.log('lo que final mente sale es1:' + path);

      this.getBase64(path).then((base64) => {
        base64=base64.replace("data:image/*;charset=utf-8;base64","data:image/jpeg;base64");
        base64=encodeURIComponent(base64);
        this.presentLoading();
        this.con.guardarImagenPerfil(base64).subscribe(
          data => {
            console.log("Base64 savefoto succes");
            data.url=data.url.replace("\/\/","//");
            data.url=data.url.replace("\/","/");
            this.session.setImagenPerfil(data.url);
            this.loader.dismiss();
            this.mensaje("Foto Cambiada","La foto ha sido cambiada con exito!");
          },
          err => {
              console.log("Base64 set ERROR : "+err.status, err);
              this.loader.dismiss();
              if(err.status==500){
                this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
              }else{
                this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
              }
          }
        );
      });
    });
  }

  private getMedia(): Promise<any> {
    // Get Image from ionic-native's built in camera plugin
    return this.camera.getPicture(this.options)
      .then((fileUri) => {
        // Crop Image, on android this returns something like, '/storage/emulated/0/Android/...'
        // Only giving an android example as ionic-native camera has built in cropping ability
        if (this.platform.is('ios')) {
          return this.crop.crop(fileUri, { quality: 50 });
        } else if (this.platform.is('android')) {
          // Modify fileUri format, may not always be necessary
          fileUri = 'file://' + fileUri;
          /* Using cordova-plugin-crop starts here */
          return this.crop.crop(fileUri, { quality: 50 });
        }
      })
      .then((path) => {
        // path looks like 'file:///storage/emulated/0/Android/data/com.foo.bar/cache/1477008080626-cropped.jpg?1477008106566'
        return path;
      })
  }

  private getBase64(filePath:string): Promise<any>{
    return this.base64.encodeFile(filePath).then((base64File: string) => {
      //console.log(base64File);
      return base64File.replace(/\n/g,'');
    }, (err) => {
      console.log(err);
    });
  }

  private presentConfirmSalirComunidad() {
    let alert = this.alertCtrl.create({
      title: 'Salir de comunidad',
      message: 'Esta seguro que quiere salir de su comunidad actual?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Si',
          handler: () => {
            console.log('yes clicked');
            this.salirComunidad();
          }
        }
      ]
    });
    alert.present();
  }

  private salirComunidad(){
    this.presentLoading();
    this.con.salirComunidad().subscribe(
      data => {
        console.log("salirComunidad succes");
        this.loader.dismiss();
        this.mensaje("Solicitud Completada",data.mensaje);
        this.navCtrl.setRoot("HomePage");
      },
      err => {
        console.log("ERROR getSolicitud!: "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }
      });
  }

  private mensaje(t,s) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();
  }
  
  public presentModalContrasena() {
    let modal = this.modalCtrl.create("CambiarContrasenaPage");
    modal.present();
  }

  public presentModalEditar() {
    let modal = this.modalCtrl.create("PerfilEditarPage");
    modal.present();
  }

  public presentModalContacto(){
    let modal = this.modalCtrl.create("ContactoEmergenciaPage");
    modal.present();
  }

  private presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando"
    });
    this.loader.present();
  }
}
