import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistorialPagosPage } from './historial-pagos';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
@NgModule({
  declarations: [
    HistorialPagosPage,
  ],
  imports: [
    IonicPageModule.forChild(HistorialPagosPage),NgxDatatableModule
  ]
})
export class HistorialPagosPageModule {}
