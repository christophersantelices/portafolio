import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,AlertController } from 'ionic-angular';
import { ConProvider } from '../../providers/conn/conn';
import { SessionProvider } from '../../providers/session/session';
/**
 * Generated class for the HistorialPagosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-historial-pagos',
  templateUrl: 'historial-pagos.html',
})
export class HistorialPagosPage {

  loader:any;

  @ViewChild('myTable') table: any;
  rows: any[] = [];
  expanded: any = {};
  timeout: any;
  loadingIndicator: boolean = true;
  temp = [];
  limit:any;

  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController,private con:ConProvider,private session:SessionProvider) {

    this.getHistorial();
    this.loadingIndicator = false;
    this.limit=6;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Test2Page');
  }

  private onPage(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      //console.log('paged!', event);
    }, 100);
  }


  public getHistorial(): void {
    this.presentLoading();
    this.con.HistorialPagosLista().subscribe(
      data => {
        console.log("getHistorial succes");
        this.session.setSession(data.session);
        //this.rows=this.dataFormat(data.historial);
        this.rows=data.historial;
        //this.temp = [...data.historial];
        this.temp = data.historial;
        //this.rows.push(data.historial);
        this.loader.dismiss();
      },
      err => {
        console.log("ERROR getHistorial: "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }
      });

  }

  private dataFormat(data){
      var temp=[{comunidad:String,plan:String}];
      for(var i =0; i<data.length;i++){
        temp.push({comunidad:data.nombre_sucursal,plan:data.nombre_plan});
      }
      return temp;
  }

  private toggleExpandRow(row) {
    //console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }
  private isGroupShown(row) {
      return row.$$expanded;
  }

  private onDetailToggle(event) {
    //console.log('Detail Toggled', event);
  }
  /*updateFilter(event) {
    var val = event.target.value.trim().toUpperCase();
    // filter our data
    var temp = this.temp.filter(function(d) {
      return d.nombre_sucursal.toUpperCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }*/

  public updateFilter(event) {
    var val = event.target.value.trim().toUpperCase();
    // filter our data
    var temp = this.temp.filter(function(d) {
       if(d.nombre_sucursal.toUpperCase().indexOf(val) !== -1){
          return d;
       } else if(d.created_at.toUpperCase().indexOf(val) !== -1){
          return d;
       }else if(d.nombre_plan.toUpperCase().indexOf(val) !== -1){
          return d;
       }
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  public col1(val){


  }

  private mensaje(t,s) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();
  }

  //formato fecha
  public subString(string) {
  if(string){
    return string.substring(0, 10);
  }else{
    return string;
  }
  }
  //LOADINGBOX
  public presentLoading() {
  this.loader = this.loadingCtrl.create({
    content: "Conectando"
  });
  this.loader.present();
  }

  }
