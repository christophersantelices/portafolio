import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,AlertController,LoadingController } from 'ionic-angular';
import { SessionProvider } from '../../providers/session/session';
import { ConProvider } from '../../providers/conn/conn';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
/**
 * Generated class for the PerfilEditarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-perfil-editar',
  templateUrl: 'perfil-editar.html',
})
export class PerfilEditarPage {
  loader:any;
  dataPerfil:boolean=false;
  paises:Array<{pais_id:number,pais_nombre:string}>;
  regiones:Array<{region_id:number,region_nombre:string,pais_id:number}>;
  provincias:Array<{provincia_id:number,provincia_nombre:string}>;
  comunas:Array<{comuna_id:number,comuna_nombre:string}>;
  FormCreateUser:FormGroup;
  submitAttempt:boolean=false;

  constructor(public formBuilder: FormBuilder,public loadingCtrl: LoadingController,public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,private session:SessionProvider,private con:ConProvider,public viewCtrol: ViewController ) {
    this.getPerfilData();
    this.FormCreateUser = formBuilder.group({
      nombre:     ['', Validators.compose([Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ]{1,}'), Validators.required])],
      apellido_p: ['', Validators.compose([Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ]{1,}'), Validators.required])],
      apellido_m: ['', Validators.compose([Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ]{1,}'), Validators.required])],
      sexo : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      talla_polera : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      correo : ['', Validators.compose([Validators.minLength(2), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'), Validators.required])],
      fecha_nacimiento : ['',Validators.required], 
      fono_contacto: ['', Validators.compose([Validators.minLength(8), Validators.pattern('([0-9]{8})'), Validators.required])],
      pais_id: ['', Validators.compose([Validators.minLength(1), Validators.required])], 
      region_id : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      provincia_id : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      comuna_id : ['', Validators.compose([Validators.minLength(1), Validators.required])],
      perfilprivado : ['', Validators.compose([Validators.required])]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilEditarPage');
  }

  public getPerfilData(){
    this.presentLoading();
    this.con.getUsuario().subscribe(
      data => {
        this.session.setSession(data.session);
        console.log("get data perfil succes",data);
        this.FormCreateUser.controls.nombre.setValue(data.usuario.usuario.nombre);
        this.FormCreateUser.controls.apellido_p.setValue(data.usuario.usuario.apellido_p);
        this.FormCreateUser.controls.apellido_m.setValue(data.usuario.usuario.apellido_m);
        this.FormCreateUser.controls.sexo.setValue(data.usuario.usuario.sexo);
        this.FormCreateUser.controls.talla_polera.setValue(data.usuario.usuario.talla_polera);
        this.FormCreateUser.controls.correo.setValue(data.usuario.usuario.correo);
        this.FormCreateUser.controls.fecha_nacimiento.setValue(this.formatoFecha(data.usuario.usuario.fecha_nacimiento));
        this.FormCreateUser.controls.fono_contacto.setValue(data.usuario.usuario.fono_contacto);
        this.FormCreateUser.controls.pais_id.setValue(data.usuario.usuario.pais_id);
        this.FormCreateUser.controls.region_id.setValue(data.usuario.usuario.region_id);
        this.FormCreateUser.controls.provincia_id.setValue(data.usuario.usuario.provincia_id);
        this.FormCreateUser.controls.comuna_id.setValue(data.usuario.usuario.comuna_id);
        this.FormCreateUser.controls.perfilprivado.setValue(data.usuario.usuario.perfilprivado);

        this.paises=data.usuario.paises;
        this.regiones=data.usuario.regiones;
        this.provincias=data.usuario.provincias;
        this.comunas=data.usuario.comunas;
       

        this.dataPerfil=true;
        this.loader.dismiss();
      },
      err => {
          console.log("divice token set ERROR : "+err.status, err);
          this.loader.dismiss();
          if(err.status==500){
            this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }else{
            this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
          }
      }
    );
  }
  
  //no ocupado ya que no se selecciona pais
public cargarRegion(pais_id){
   this.con.getRegion(pais_id).subscribe(
    data => {
      this.regiones=data;
      this.provincias=[];
      this.comunas=[];
    },
    err => {
        console.log("getRegion set ERROR : "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }
    }
  );
} 

public cargarProvincia(region_id){
 this.con.getProvincia(region_id).subscribe(
   data => {
     this.provincias=data;
     this.comunas=[];
     console.log("getProvincia succes");
   },
   err => {
       console.log("getProvincia set ERROR : "+err.status, err);
       if(err.status==500){
         this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
       }else{
         this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
       }
   }
 );
} 
public cargarComuna(provincia_id){
 this.con.getComuna(provincia_id).subscribe(
   data => {
     this.comunas=data;
     console.log("getComuna succes");
   },
   err => {
       console.log("getComuna set ERROR : "+err.status, err);
       if(err.status==500){
         this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
       }else{
         this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
       }
   }
 );
} 

public setPerfilData(FormUsuario){
  console.log("setPerfilData",FormUsuario);
  this.submitAttempt = true;
  if(this.FormCreateUser.valid){
    this.presentLoading();
    this.con.setUsuario(FormUsuario).subscribe(
     data => {
       console.log("set data perfil succes");
       for(let i=0;i<this.comunas.length;i++){
         if(this.comunas[i].comuna_id.toString()==this.FormCreateUser.controls.comuna_id.value){
           this.session.setComuna(this.comunas[i].comuna_nombre);
         }
       }
       for(let i=0;i<this.provincias.length;i++){
         if(this.provincias[i].provincia_id.toString()==this.FormCreateUser.controls.provincia_id.value){
           this.session.setProvincia(this.provincias[i].provincia_nombre);
         }
       }
       this.loader.dismiss();
       this.mensaje("Perfil Guardado","Tu perfil ha sido modificado con exito!");
       this.viewCtrol.dismiss();
     },
     err => {
         console.log("setPerfilData ERROR : "+err.status, err);
         this.loader.dismiss();
         if(err.status==400){
           //console.log(err._body);
           let obj = JSON.parse(err._body);
           let mensaje="";
 
           if (obj.nombre){
             mensaje=mensaje+obj.nombre[0]+'<br>';
           }
           if(obj.apellido_p){
             mensaje=mensaje+obj.apellido_p[0]+'<br>';
           }
           if(obj.apellido_m){
             mensaje=mensaje+obj.apellido_m[0]+'<br>';
           }
           if(obj.sexo){
             mensaje=mensaje+obj.sexo[0]+'<br>';
           }
           if(obj.talla_polera){
             mensaje=mensaje+obj.talla_polera[0]+'<br>';
           }
           if(obj.fecha_nacimiento){
             mensaje=mensaje+obj.fecha_nacimiento[0]+'<br>';
           }
           if(obj.fono_contacto){
             mensaje=mensaje+obj.fono_contacto[0]+'<br>';
           }
           if(obj.pais_id){
             mensaje=mensaje+obj.pais_id[0]+'<br>';
           }
           if(obj.region_id){
             mensaje=mensaje+obj.region_id[0]+'<br>';
           }
           if(obj.provincia_id){
             mensaje=mensaje+obj.provincia_id[0]+'<br>';
           }
           if(obj.comuna_id){
             mensaje=mensaje+obj.comuna_id[0]+'<br>';
           }
           if(obj.perfilprivado){
             mensaje=mensaje+obj.perfilprivado[0]+'<br>';
           }
           this.mensaje("Error en el formulario",mensaje);
         } else if(err.status==500){
           this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
         }else{
           this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
         }
     }
   );
  }
  
}

  public formatoFecha(fecha){
    var info = fecha.split('-');
    return info[2] + '-' + info[1] + '-' + info[0];
  }

  public close(){
    this.viewCtrol.dismiss();
  }
  private mensaje(t,s) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();
  }
  private presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando"
    });
    this.loader.present();
  }

}
