import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IngresarComunidadPage } from './ingresar-comunidad';

@NgModule({
  declarations: [
    IngresarComunidadPage,
  ],
  imports: [
    IonicPageModule.forChild(IngresarComunidadPage),
  ],
})
export class IngresarComunidadPageModule {}
