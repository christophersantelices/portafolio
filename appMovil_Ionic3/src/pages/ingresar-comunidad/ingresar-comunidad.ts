import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController,AlertController} from 'ionic-angular';
import { SessionProvider } from '../../providers/session/session';
import { ConProvider } from '../../providers/conn/conn';


@IonicPage()
@Component({
  selector: 'page-ingresar-comunidad',
  templateUrl: 'ingresar-comunidad.html',
})
export class IngresarComunidadPage {
  loader:any;
  organismo:boolean=false;
  enviada:boolean=false;
  solicitud:any;
  boxs:Array<{}>;

  constructor(public alertCtrl: AlertController,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams,private session:SessionProvider,private con:ConProvider) {
    this.getSolicitud();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IngresarComunidadPage');
  }

  private getSolicitud(){
    
    this.presentLoading();
    this.con.getSolicitud().subscribe(
      data => {
       this.session.setSession(data.session);
        console.log("getSolicitud succes");
        if(data.enviada){
          this.organismo=data.organismo;
          this.enviada=data.enviada;
          this.solicitud=data.solicitud;
          this.loader.dismiss();
        }else{
          this.organismo=data.organismo;
          this.enviada=data.enviada;
          this.solicitud=data.solicitud;
          this.boxs=data.listadoOrg;
          this.loader.dismiss();
        }
      },
      err => {
        console.log("ERROR getSolicitud!: "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }
      });
  }

  private CancelarSolicitud(){
    this.presentLoading();
    this.con.CancelarSolicitud(this.solicitud.solicitud).subscribe(
      data => {
        console.log("CancelarSolicitud succes");
        this.loader.dismiss();
        this.mensaje("Solicitud Cancelada","La solicitud ha sido cancelada con exito.");
        this.getSolicitud();
      },
      err => {
        console.log("ERROR getSolicitud!: "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Solicitud No Cancelada","No hemos podido procesar tu solicitud, intentelo mas tarde.");
        }
      });
  }

  private MandarSolicitud(FormSignUp){
    this.presentLoading();
    this.con.MandarSolicitud(FormSignUp.box).subscribe(
      data => {
        console.log("MandarSolicitud succes");
        this.loader.dismiss();
        this.mensaje("Solicitud Enviada","La solicitud ha sido Enviada con exito.");
        this.getSolicitud();
      },
      err => {
        console.log("ERROR MandarSolicitud!: "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Error de Solicitud","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }
      });
  }

  private presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando"
    });
    this.loader.present();
  }

  private mensaje(t,s) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();
  }

  private presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Cancelar Solicitud',
      message: 'Esta seguro que quiere cancelar la solicitud?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Si',
          handler: () => {
            console.log('yes clicked');
            this.CancelarSolicitud();
          }
        }
      ]
    });
    alert.present();
  }

}
