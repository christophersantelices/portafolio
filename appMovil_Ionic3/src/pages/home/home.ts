import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController,LoadingController,AlertController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
//provider
import { ConProvider } from '../../providers/conn/conn';
import { SessionProvider } from '../../providers/session/session';



@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  loader:any;
  dataHome:boolean=false; //si la data de home esta cargada.

  proximaClase: Array<any>;
  planes: Array<any>;
  nombre_sucursal: any;
  wid1=false;
  wid2=false;

  constructor(public alertCtrl: AlertController,public navCtrl: NavController, private con:ConProvider, public menuCtrl: MenuController,private session:SessionProvider,
    public navParams: NavParams, public loadingCtrl: LoadingController, private splashScreen:SplashScreen) {

    this.menuCtrl.enable(true);//en el login se quita para no mostrar el menu
    this.homeContenido();
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad LoginPage');
    this.splashScreen.hide();
    this.menuCtrl.enable(true);
    //console.log("home session", this.session.getSession());
  }

  public homeContenido(){
    this.presentLoading();
    this.con.HomePage().subscribe(
      data => {
        console.log("home succes");
        if(!data.session.perfilCompleto){
          this.navCtrl.setRoot("CompletarPerfilPage");
        }
       this.session.setSession(data.session);
       this.session.setUsuario(data.usuario);
        if(this.session.getEsUsuarioOrg()){
          if(data.planesYActividades){
            this.proximaClase=data.proximaClase;
            this.nombre_sucursal=data.planesYActividades.sucursal.nombre_sucursal;
            //mostrar widget en home
            if(this.proximaClase.length==0){
              this.wid1=true;
            }else if(this.proximaClase.length>0){
              this.wid2=true;
            }
            if(data.planesYActividades.planes==0){
              this.dataHome=true;
            }else{
              this.planes=data.planesYActividades.planes;
            }
          }
        }
        this.loader.dismiss();
      },
      err => {
        console.log("ERROR HOME!: "+err.status, err);
        this.loader.dismiss();
        if(err.status==500){
          this.mensaje("Error del Servidor","No hemos podido procesar la solicitud, intentelo mas tarde.");
        }else{
          this.mensaje("Error de Solicitud "+err.status,"No hemos podido procesar la solicitud, intentelo mas tarde.");
        }
      });
  }

  public loadAsistencia(){
      if(this.session.getCheckTyc().check){
        //this.modalTerminos();
        this.navCtrl.setRoot("TerminosycondicionesPage");
      }else{
        this.navCtrl.setRoot("AsistenciaPage");
      }
  }
  public loadPlanesyAccesos(){
    this.navCtrl.setRoot("PlanesAccesosPage");
  }
  public loadIngresarComunidadPage(){
    this.navCtrl.setRoot("IngresarComunidadPage");
  }
  private mensaje(t,s) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: s,
      buttons: ['OK']
    });
    alert.present();
  }
  private presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando"
    });
    this.loader.present();
  }
}
