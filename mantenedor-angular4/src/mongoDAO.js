(function() {
	'use strict';

	const mongoose = require('mongoose');
	mongoose.connect('mongodb://middleware:middleware@127.0.0.1/mantenedorchat');

	var db = mongoose.connection;
	db.on('error', function() {console.log('connection mongodb error:'); db = mongoose.connection;});
	db.once('open', function() {console.log("connection mongodb success");});

	const Schema = mongoose.Schema;

	// esquema para buscar servicios
	var services  = new Schema({
		id: {type:String},
		name: {type:String},
		agentJoinTimeout: {type:"number"},
		connectionTimeout: {type:"number"},
		inactivityTimeout: {type:"number"},
		apa: {type:Boolean},
		availability: {
			mon: [ {start: String,end: String } ],
			tue: [ {start: String,end: String } ],
			wed: [ {start: String,end: String } ],
			thu: [ {start: String,end: String } ],
			fri: [ {start: String,end: String } ],
			sat: [ {start: String,end: String } ],
			sun: [ {start: String,end: String } ]
		},
		specialDates: [
			{ date: {day: {type:String}, month: {type:String},"year": {type:String}}, availability: [{start: String, end: String}]}
		]
	});
	const modeloServices = mongoose.model('services', services);
	//esquema para buscar usuarios
	var users  = new Schema({
		user: {type:String},
		pass: {type:String}
	});
	const userServices = mongoose.model('user', users);

	//modulos inpotados
	module.exports = {
		getServices: function() {
			try {
				return modeloServices.find({}).exec()
				.then((res) => {
				  return res;
				})
				.catch((err) => {
				  return err;
				});
			} catch (error) {
				console.log('updateSercice error') ;
			}

		},
		removeSercice: function() {
			modeloServices.remove({}, function(err) { 
				console.log('collection removed') 
			 });
		},
		insertSercice: function() {
			modeloServices.insertMany(test)
			.then(function(mongooseDocuments) {
				console.log('collection insert',mongooseDocuments) 
			})
			.catch(function(err) {
				/* Error handling */
			});
		},
		updateSercice: function(service) { 
			try {
				//var serv_id=service.id;
				return modeloServices.update({ id: service.id }, { apa:service.apa,name: service.name,agentJoinTimeout: service.agentJoinTimeout,connectionTimeout: service.connectionTimeout, inactivityTimeout:service.inactivityTimeout,availability:JSON.parse(service.availability),specialDates:JSON.parse(service.specialDates)}, {upsert:true}).exec()
				.then((res) => {
					//console.log("servicio "+serv_id+" actualizado... ");
					return res;
				})
				.catch((err) => {
					//console.log("servicio "+serv_id+" no pudo ser actualizado... ");
					return err;
				});
			} catch (error) {
				console.log('updateSercice error',error) 
			}
		},
		loginSercice: function(user) { 
			try {
				return userServices.findOne({ 'user': user }).exec()
				.then((res) => {
					return res;
				})
				.catch((err) => {
					return err;
				});
			} catch (error) {
				console.log('loginSercice error',error) 
			}
		}
		
	}


})();
