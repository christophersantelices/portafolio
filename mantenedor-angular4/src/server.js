(function() {
	'use strict';

	const PORT = 8008;
	const FOLDER = __dirname;
	
	var http = require('http');
	var express = require('express');
	var bodyParser = require('body-parser')
	var app = express();
	var cors = require('cors');
	var mongo = require('./mongoDAO');

	app.use(cors());					
	app.use(express.static(FOLDER));

	// parse application/x-www-form-urlencoded
	app.use(bodyParser.urlencoded({ extended: false }))
	// parse application/json
	app.use(bodyParser.json())

	app.get('/', function(req, res) {
		res.sendFile(FOLDER + '/index.html'); // load the single view file (angular will handle the page changes on the front-end)
	});

	app.listen(PORT, function() {
		console.log("App listening http on port: " + PORT);
	});

	/*** REST */
	app.get('/getServices/', function(req, res) {
		try {
			mongo.getServices().then((services) => {
				res.send(services);
			}).catch((err)=> {
				console.log(err);
				res.send(JSON.parse(err));
			});
		} catch (error) {
			console.log("getService error",error);
		}
	});
	app.post('/update/', function(req, res) {
		try {
			//console.log(req.body);
			mongo.updateSercice(req.body).then((upd) => {
				res.send(upd);
			}).catch((err)=> {
				console.log(err);
				res.send(JSON.parse(err));
			});
			//res.end(JSON.stringify("ok", null, 2));
		} catch (error) {
			console.log("update error");
		}
	});

	app.post('/login/', function(req, res) {
		try {
			mongo.loginSercice(req.body.user).then((ls) => {
				if(ls==null){
					res.status(403).send('{"respuesta": "Bad credential"}');
				}else{
					if(ls.pass==req.body.pass){
						res.status(200).send('{"respuesta": "OK"}');
					}else{
						res.status(403).send('{"respuesta": "Bad credential"}');
					}
				}
			}).catch((err)=> {
				console.log(err);
				res.send(JSON.parse(err));
			});
			//res.end(JSON.stringify("ok", null, 2));
		} catch (error) {
			console.log("update error");
		}
	});

	app.use(function(err, req, res, next) {
		console.error(err.stack);
		res.status(500).send('Something broke!');
		});
		  
})();
