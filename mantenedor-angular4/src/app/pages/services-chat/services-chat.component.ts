import { Component, OnInit } from '@angular/core';
import { MongodbService } from '../../services/mongodb.service';
import {ViewEncapsulation} from '@angular/core';
import { ServiceOptions } from 'selenium-webdriver/remote';
import { Services } from '@angular/core/src/view';
import { Console } from '@angular/core/src/console';
import * as _ from 'lodash';
import {NgForm} from '@angular/forms';
import { } from 'jquery';
import { } from 'bootstrap';

@Component({
  selector: 'app-services-chat',
  templateUrl: './services-chat.component.html',
  styleUrls: ['./services-chat.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ServicesChatComponent implements OnInit {

  //services:Array<any>;
	 services:Array<{
		id: {type:String},
			name: {type:String},
			agentJoinTimeout: {type:"number"},
			connectionTimeout: {type:"number"},
      inactivityTimeout: {type:"number"},
      apa: {type:boolean},
			availability: {
				mon: [ {start: String,end: String } ],
				tue: [ {start: String,end: String } ],
				wed: [ {start: String,end: String } ],
				thu: [ {start: String,end: String } ],
				fri: [ {start: String,end: String } ],
				sat: [ {start: String,end: String } ],
				sun: [ {start: String,end: String } ]
			},
			specialDates: [
				{ date: {day: {type:String}, month: {type:String},year: {type:String}}, availability: [{start: String, end: String}]}
			]
	}>; 
  public disable:boolean=true;
  public eleID=0;
  nuevaFecha=	{ date: {day: {type:String}, month: {type:String},year: {type:String}}, availability: [{start: String, end: String}]};
  closeResult: string;
  public tipodefecha:any;
  private referenciaSpecialDates:any;


  constructor(public mongo: MongodbService) { 
    this.getServices();
  }

  ngOnInit() {
    
  }

  public getServices(){
    this.mongo.getServices().subscribe(
      data => {
        console.log(data);
        this.services=data;
      },
      err => {
        console.log("ERROR getServices!: "+err.status, err);
      });
  }

  public updateServices(service){
    this.mongo.setUpdate(service).subscribe(
      data => {
        console.log(data);
      },
      err => {
        console.log("ERROR updateServices!: "+err.status, err);
      });
  }

  public sicloActualizacion(){
    console.log("siclo");
    for(var i = 0; i<this.services.length;i++){
      this.updateServices(this.services[i]);
    }    
    this.getServices();
  }

  public habilitarEdicion(){
    this.disable=!this.disable;
  }

  public getUTCFullYear(){
      let year = new Date();
      return year.getUTCFullYear();
  }

  public agregarInputFecha(id){
    var y = document.createElement("div");
    y.setAttribute("class", "col-md-6");

    var l = document.createElement("div");
    l.setAttribute("class", "col-md-4");
    var n = document.createElement("button");
    n.setAttribute("type", "button");
    n.setAttribute("class", "btn btn-box-tool");
    n.setAttribute("id","f"+this.eleID);
    this.eleID++;
    var m= document.createElement("i");
    m.setAttribute("class", "fa fa-trash");
    n.appendChild(m);
    l.appendChild(n);
    var x = document.createElement("input");
    x.setAttribute("class", "fecha");
    x.setAttribute("type", "date");
    x.setAttribute("min", "2018-01-01");
    l.appendChild(x);

    var l2 = document.createElement("div");
    l2.setAttribute("class", "col-md-4");
    var j=document.createTextNode("Comienzo");
    l2.appendChild(j);
    var u = document.createElement("input");
    u.setAttribute("class", "fecha");
    u.setAttribute("type", "time");
    l2.appendChild(u);

    var l3 = document.createElement("div");
    l3.setAttribute("class", "col-md-4");
    var k=document.createTextNode("Termino");
    l3.appendChild(k);
    var p = document.createElement("input");
    p.setAttribute("class", "fecha");
    p.setAttribute("type", "time");
    l3.appendChild(p);
 
    y.appendChild(l);
    y.appendChild(l2);
    y.appendChild(l3);
    document.getElementById(id).appendChild(y);
  }

  public eliminarFecha(specialDates,index){
    _.pullAt(specialDates, index);
  }

  public agregarFecha(value){
    var fecha=value.fecha.split("-");
    var any;
    if(value.tipodefecha){
       any=	{ date: {day: fecha[2], month: fecha[1]}, availability: [{start: value.start, end: value.end}]};
    }else{
       any=	{ date: {day: fecha[2], month: fecha[1],year: fecha[0]}, availability: [{start: value.start, end: value.end}]};
    }
    this.referenciaSpecialDates.push(any); 
  }

  public onSubmit(f: NgForm) {
    console.log(f.value);
    this.agregarFecha(f.value);
  }
  public referencia(value) {
    this.referenciaSpecialDates=value;
  }

  public cargandoModal(estado){
    if(estado){
      $('#modalCarga').modal('show');
    }else{
      $('.modal').modal('hide');
      setTimeout(function(){ $('html,body').css('padding-right', '0px'); }, 500);
    }
  }

  public recargarServicios(){
    this.getServices();
  }

}
