import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesChatComponent } from './services-chat.component';

describe('ServicesChatComponent', () => {
  let component: ServicesChatComponent;
  let fixture: ComponentFixture<ServicesChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
