import { Component, OnInit ,ViewEncapsulation} from '@angular/core';
import { MongodbService } from '../../services/mongodb.service';
import { NavigationExtras,Router } from '@angular/router';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  validCredential:boolean=true;

  constructor(public mongo: MongodbService,private router: Router,private session: SessionService) { }

  ngOnInit() {
  }

  public loginMantenedor(form){
    console.log(form);
    this.mongo.getLogin(form.user,form.pass).subscribe(
      data => {
        console.log(data);
        this.validCredential=true;
        this.session.setLogin(true);
        this.session.setUser(form.user);
        this.router.navigate(['/admin'], { replaceUrl: true });
      },
      err => {
        if(err.status==403){
         this.validCredential=false;
         console.log(err._body);
        }else{
          console.log("ERROR loginMantenedor!: "+err.status, err);
        }
      });
  }

}
