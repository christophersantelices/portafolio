import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { Router  } from  '@angular/router';

@Component({
  selector: 'app-starter-header',
  templateUrl: './starter-header.component.html',
  styleUrls: ['./starter-header.component.css']
})
export class StarterHeaderComponent implements OnInit {

  constructor(private router: Router,private session: SessionService) { }

  ngOnInit() {
  }

  public logout(){
    this.session.setLogin(false);
    this.router.navigate(['/login'], { replaceUrl: true })
  }

}
