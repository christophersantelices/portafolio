import { Component, OnInit } from '@angular/core';
import { Router  } from  '@angular/router';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-starter-left-side',
  templateUrl: './starter-left-side.component.html',
  styleUrls: ['./starter-left-side.component.css']
})
export class StarterLeftSideComponent implements OnInit {

  public router:Router;

  constructor(private _router: Router,private session: SessionService) { 
    this.router = _router;
  }

  ngOnInit() {
    console.log(this.router.url); //  /routename
  }

}
