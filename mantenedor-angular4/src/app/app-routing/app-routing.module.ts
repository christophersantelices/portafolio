import { StarterComponent } from './../starter/starter.component';


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ServicesChatComponent } from './../pages/services-chat/services-chat.component';
import { HomeComponent } from './../pages/home/home.component';
import { LoginComponent } from './../pages/login/login.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent},
      { path: '#', redirectTo: '/login', pathMatch: 'full'},
      { path: '', redirectTo: '/login', pathMatch: 'full'},
      { path: 'admin', redirectTo: '/admin/home', pathMatch: 'full' },
      { path: 'admin', component: StarterComponent,
        children: [{ path: 'services',component: ServicesChatComponent},{path: 'home',component: HomeComponent}] 
      }    
    ], { enableTracing: false } )
  ],
  declarations: [],
  exports: [ RouterModule]
})
export class AppRoutingModule { }
