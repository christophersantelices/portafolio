import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

  private isLogin=false;
  private user:string;

  constructor() { }

  public getLogin(){
    return this.isLogin;
  }
  public setLogin(val){
    this.isLogin=val;
  }
  public getUser(){
    return this.user;
  }
  public setUser(val){
    this.user=val;
  }
}
