import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class MongodbService {

  private ipServidor:string="http://127.0.0.1:8008";

  constructor(public http: Http) { 
  }

  public getServices(){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    let url = this.ipServidor+'/getServices';
    let result= this.http.get(url, {headers: headers}).map(res => res.json());
    return result;
  }

  public setUpdate(s){
    let  headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    let  body='id='+s.id+'&name='+s.name+'&agentJoinTimeout='+s.agentJoinTimeout+'&connectionTimeout='+s.connectionTimeout+'&inactivityTimeout='+s.inactivityTimeout+'&apa='+s.apa+'&availability='+JSON.stringify(s.availability)+'&specialDates='+JSON.stringify(s.specialDates);
    let result=this.http.post(this.ipServidor+'/update', body, {headers: headers}).map(res => res.json());
    return result;
  }
  public getLogin(user,pass){
    let  headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    let  body='user='+user+'&pass='+pass;
    let result=this.http.post(this.ipServidor+'/login', body, {headers: headers}).map(res => res.json());
    return result;
  }
  
}
