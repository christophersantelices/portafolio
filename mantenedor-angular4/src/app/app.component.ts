import { Component } from '@angular/core';
import { NavigationExtras,Router,ActivatedRoute } from '@angular/router';
import { SessionService } from './services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MantenedorChat';

  constructor(private router: Router,private session: SessionService) {   }

  public changeOfRoutes(){
    if(!this.session.getLogin()){
      console.log("sin session");
      this.router.navigate(['/login'], { replaceUrl: true });
    }else{
      console.log("con session");
    }
  }

}
